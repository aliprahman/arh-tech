<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransaksiIdToFunding extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('funding', function(Blueprint $table){
          $table->integer('transaksi_id')->nullable()->after('pinjaman_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('funding', function(Blueprint $table){
        $table->dropColumn('transaksi_id');
      });
    }
}
