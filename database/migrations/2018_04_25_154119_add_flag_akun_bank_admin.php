<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFlagAkunBankAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('akun_bank', function(Blueprint $table){
          $table->boolean('is_akun_admin')->after('no_akun_bank')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('akun_bank', function(Blueprint $table){
        $table->dropColumn('is_akun_admin');
      });
    }
}
