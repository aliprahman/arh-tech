<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFeePinjaman extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pinjaman', function(Blueprint $table)
        {
          $table->integer('admin_fee')->nullable()->after('jumlah_pinjaman');
          $table->integer('conf_fee')->nullable()->after('admin_fee');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('pinjaman', function(Blueprint $table)
      {
        $table->dropColumn('admin_fee');
        $table->dropColumn('conf_fee');
      });
    }
}
