<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class AddValueEnum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        # laravel migration doesn't support modify enum columns
        # create navite sql
        DB::statement("ALTER TABLE file MODIFY COLUMN type ENUM('profile_photo','photo_tempat_tinggal','photo_tempat_kerja','photo_sk','dokumen','cv','rekening_koran')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
