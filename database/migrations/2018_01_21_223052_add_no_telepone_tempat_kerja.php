<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNoTeleponeTempatKerja extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE peminjam ADD COLUMN no_telepon_tempat_kerja VARCHAR(100) NULL AFTER alamat_tempat_kerja_id');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE peminjam DROP COLUMN no_telepon_tempat_kerja');
    }
}
