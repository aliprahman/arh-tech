<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBankTujuanTransfer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaksi', function(Blueprint $table){
          $table->integer('bank_tujuan_transfer_id')->nullable();
          $table->string('nama_pengirim_transfer')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('transaksi', function(Blueprint $table){
        $table->dropColumn('bank_tujuan_transfer_id');
        $table->dropColumn('nama_pengirim_transfer');
      });
    }
}
