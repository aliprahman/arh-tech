<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KeuanganPeminjam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keuangan_peminjam',function (Blueprint $table){
            $table->increments('id');
            $table->integer('peminjam_id');
            $table->text('keterangan')->nullable();
            $table->integer('jumlah')->nullable();
            $table->enum('type',['pinjaman','pendapatan'])->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('keuangan_peminjam',function (Blueprint $table){
            $table->foreign('peminjam_id')
                ->references('id')
                ->on('peminjam')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('keuangan_peminjam');
    }
}
