<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyPeminjam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('peminjam',function(Blueprint $table){
            $table->foreign('jabatan_id')
                ->references('id')
                ->on('m_jabatan')
                ->onDelete('cascade');
            $table->foreign('pekerjaan_suami_istri_id')
                ->references('id')
                ->on('m_pekerjaan')
                ->onDelete('cascade');
            $table->foreign('photo_rekening_koran_id')
                ->references('id')
                ->on('file')
                ->onDelete('cascade');
            $table->foreign('photo_curiculum_vitae_id')
                ->references('id')
                ->on('file')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('peminjam', function (Blueprint $table) {
            $table->dropForeign('peminjam_jabatan_id_foreign');
            $table->dropForeign('peminjam_pekerjaan_suami_istri_id_foreign');
            $table->dropForeign('peminjam_photo_rekening_koran_id_foreign');
            $table->dropForeign('peminjam_photo_curiculum_vitae_id_foreign');
        });
    }
}
