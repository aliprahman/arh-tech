<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTablePeminjam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE peminjam
                        ADD COLUMN jabatan_id INT(11) UNSIGNED NULL AFTER nama_tempat_kerja,
                        ADD COLUMN pekerjaan_suami_istri_id INT(11) UNSIGNED NULL AFTER no_ktp_suami_istri,
                        ADD COLUMN pendapatan_suami_istri INT(11) NULL AFTER pekerjaan_suami_istri_id,
                        ADD COLUMN gaji INT(11) NULL AFTER photo_sk_surat_kontrak_id,
                        ADD COLUMN status_pengajar ENUM('tetap', 'tidak_tetap') NULL AFTER photo_sk_surat_kontrak_id,
                        ADD COLUMN lama_mengajar VARCHAR(30) NULL AFTER status_pengajar,
                        ADD COLUMN jumlah_murid_tempat_kerja INT(11) NULL AFTER photo_tempat_kerja_id,
                        ADD COLUMN lama_berdiri_tempat_kerja INT(11) NULL AFTER alamat_tempat_kerja_id,
                        ADD COLUMN photo_rekening_koran_id INT(11) NULL AFTER akun_bank_id,
                        ADD COLUMN photo_curiculum_vitae_id INT(11) NULL AFTER user_id");   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE peminjam
                        DROP COLUMN jabatan_id,
                        DROP COLUMN pekerjaan_suami_istri_id,
                        DROP COLUMN pendapatan_suami_istri,
                        DROP COLUMN lama_mengajar,
                        DROP COLUMN gaji,
                        DROP COLUMN status_pengajar,
                        DROP COLUMN jumlah_murid_tempat_kerja,
                        DROP COLUMN lama_berdiri_tempat_kerja,
                        DROP COLUMN photo_rekening_koran_id,
                        DROP COLUMN photo_curiculum_vitae_id');
    }
}
