<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldBuktiTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('transaksi', function(Blueprint $table){
        $table->integer('file_id')->nullable()->after('akun_bank_nama_akun');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('transaksi', function(Blueprint $table){
        $table->dropColumn('file_id');
      });
    }
}
