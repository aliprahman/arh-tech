<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhotoSkSuratKontrakId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE peminjam ADD COLUMN photo_sk_surat_kontrak_id INT(11) NULL AFTER tanggal_sk_surat_kontrak');   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE peminjam DROP COLUMN photo_sk_surat_kontrak_id');
    }
}
