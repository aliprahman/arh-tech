<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            AclRoleSeeder::class,
            //AclResourceSeeder::class,
            //AclRuleSeeder::class,
            PermissionSeeder::class,
            MasterBankSeeder::class,
            MasterHubunganKontakSeeder::class,
            MasterJabatanSeeder::class,
            MasterJenisDokumenPinjamanSeeder::class,
            MasterJenisPinjamanSeeder::class,
            MasterKepemilikanRumahSeeder::class,
            MasterPekerjaanSeeder::class,
            MasterPendidikanSeeder::class,
            MasterStatusPernikahanSeeder::class,
            UsersTableSeeder::class,
            PemodalNewSeeder::class,
            PemodalConfirmedSeeder::class,
            PemodalBlockedSeeder::class,
            PeminjamNewSeeder::class,
            PeminjamConfirmedSeeder::class,
            PeminjamBlockedSeeder::class,
        ]);
    }
}
