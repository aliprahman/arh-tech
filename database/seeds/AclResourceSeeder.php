<?php

use Illuminate\Database\Seeder;

class AclResourceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('acl_resource')->delete();
        DB::statement("ALTER TABLE acl_resource AUTO_INCREMENT = 1;");

        $rows = [
            '^\/',
            '^acl',
            '^acl\/resource\/add',
            '^acl\/resource\/delete\/',
            '^acl\/resource\/edit\/',
            '^acl\/resource\/index',
            '^acl\/resource\/post_add',
            '^acl\/resource\/post_edit',
            '^acl\/role\/add',
            '^acl\/role\/delete\/',
            '^acl\/role\/edit\/',
            '^acl\/role\/index',
            '^acl\/role\/post_add',
            '^acl\/role\/post_edit',
            '^acl\/rule\/add',
            '^acl\/rule\/delete\/',
            '^acl\/rule\/edit\/',
            '^acl\/rule\/index',
            '^acl\/rule\/post_add',
            '^acl\/rule\/post_edit',
            '^admin\/akun',
            '^admin\/daftar\/akun',
            '^admin\/daftar\/pendanaan',
            '^admin\/daftar\/pinjaman',
            '^admin\/dashboard',
            '^admin\/pendanaan',
            '^admin\/pinjaman',
            '^ajax\/daftar\/kecamatan\/',
            '^ajax\/daftar\/kelurahan\/',
            '^ajax\/daftar\/kota\/',
            '^api\/user',
            '^auth\/resend',
            '^auth\/verify\/',
            '^biodata\/password',
            '^data\/pinjaman',
            '^login',
            '^logout',
            '^old',
            '^password\/email',
            '^password\/reset',
            '^password\/reset\/',
            '^peminjam\/biodata',
            '^peminjam\/daftar',
            '^peminjam\/daftar\/cicilan\/',
            '^peminjam\/daftar\/history',
            '^peminjam\/daftar\/pinjaman',
            '^peminjam\/dashboard',
            '^peminjam\/data_pendukung\/',
            '^peminjam\/history',
            '^peminjam\/pembayaran\/',
            '^peminjam\/pinjaman',
            '^peminjam\/pinjaman\/',
            '^peminjam\/pinjaman\/\/dokumen',
            '^peminjam\/pinjaman\/\/edit',
            '^peminjam\/pinjaman\/delete_doc\/',
            '^peminjam\/pinjaman\/rincian\/',
            '^peminjam\/pinjaman\/update_doc\/',
            '^pemodal\/biodata',
            '^pemodal\/cari',
            '^pemodal\/cicilan',
            '^pemodal\/daftar',
            '^pemodal\/dashboard',
            '^pemodal\/deposit',
            '^pemodal\/deposit_konfirmasi',
            '^pemodal\/deposit_konfirmasi\/',
            '^pemodal\/deposit\/konfirmasi',
            '^pemodal\/history',
            '^pemodal\/konfirm',
            '^pemodal\/list_deposit',
            '^pemodal\/list_penarikan',
            '^pemodal\/order\/deposit',
            '^pemodal\/order\/penarikan',
            '^pemodal\/penarikan',
            '^pemodal\/portofolio\/',
            '^redirect\/biodata',
            '^register',
            '^register\/peminjam',
            '^register\/pemodal',
            '^testing_email',
            '^validasi\/captcha',
            '^validasi\/email',
            '^validasi\/image',
            '^validasi\/ktp',
            '^validasi\/mobile',
            '^validasi\/npwp',
            '^admin\/history_transaksi',
            '^admin\/daftar\/history_transaksi',
            '^dashboard',
            '^akun',
            '^akun\/',
            '^daftar\/akun',
            '^pendanaan',
            '^daftar\/pendanaan',
            '^pinjaman',
            '^pinjaman\/ajuan',
            '^pinjaman\/ajuan\/simpan',
            '^pinjaman\/analisa\/',
            '^pinjaman\/cicilan\/',
            '^cicilan\/pembayaran\/',
            '^pinjaman\/proses\/analisis\/',
            '^pinjaman\/pembayaran\/',
            '^pinjaman\/daftar\/pembayaran',
            '^admin\/print\/history_transaksi',
            '^daftar\/pinjaman\/admin',
            '^daftar\/pinjaman\/peminjam',
            '^pinjaman\/konfirmasi_setuju\/',
            '^pinjaman\/konfirmasi_tolak\/'
        ];

        foreach($rows as $row)
        {
            $data = new App\Resource;
            $data->name = $row;
            $data->type = 'action';
            $data->save();
        }
    }
}
