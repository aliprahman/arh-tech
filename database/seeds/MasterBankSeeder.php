<?php

use Illuminate\Database\Seeder;

class MasterBankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_bank')->delete();
        DB::statement("ALTER TABLE m_bank AUTO_INCREMENT = 1;");

        $rows = ['Bank BCA', 'Bank Mandiri'];

        foreach($rows as $row)
        {
            $data = new App\Bank;
            $data->nama = $row;
            $data->save();    
        }
    }
}
