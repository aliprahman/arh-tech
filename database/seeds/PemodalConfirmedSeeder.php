<?php

use Illuminate\Database\Seeder;

class PemodalConfirmedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 50)->states('pemodal', 'confirmed')->create()->each(function($u) {
            $u->pemodal()->save(factory(App\Pemodal::class)->make([
                'akun_bank_id' => factory(App\AkunBank::class)->create([
                    'nama_akun_bank' => $u->name
                ])->id,
            ]));
        });
    }
}
