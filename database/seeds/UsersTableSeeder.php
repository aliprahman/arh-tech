<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Storage::deleteDirectory('profile_photo');
        Storage::makeDirectory('profile_photo');
        DB::table('file')->delete();
        DB::statement("ALTER TABLE file AUTO_INCREMENT = 1;");

        DB::table('biodata')->delete();
        DB::statement("ALTER TABLE biodata AUTO_INCREMENT = 1;");

        DB::table('alamat')->delete();
        DB::statement("ALTER TABLE alamat AUTO_INCREMENT = 1;");

        DB::table('akun_bank')->delete();
        DB::statement("ALTER TABLE akun_bank AUTO_INCREMENT = 1;");

        DB::table('pemodal')->delete();
        DB::statement("ALTER TABLE pemodal AUTO_INCREMENT = 1;");

        DB::table('peminjam')->delete();
        DB::statement("ALTER TABLE peminjam AUTO_INCREMENT = 1;");

        DB::table('users')->delete();
        DB::statement("ALTER TABLE users AUTO_INCREMENT = 1;");

        // Root Admin Finnesia
        factory(App\User::class)->create([
            'id_pelanggan' => NULL, 
            'name' => 'Master',
            'email' => 'root@administrator.com',
            'password' => bcrypt('f1nn3514'),
            'type' => 'finnesia',
            'role_id' => 1,
            'created_at' => '2017-01-01 00:00:00'
        ]);

        // Staf Finnesia
        factory(App\User::class)->create([
            'id_pelanggan' => NULL,
            'name' => 'Administrator',
            'email' => 'admin@administrator.com',
            'password' => bcrypt('f1nn3514'),
            'type' => 'finnesia',
            'role_id' => 5,
            'created_at' => '2017-01-01 00:00:00'
        ]);

        // Pemodal Fixed
        factory(App\User::class)->states('pemodal', 'confirmed')->create([
            'name' => 'Pemodal Test',
            'email' => 'pemodal@pemodal.com',
            'password' => bcrypt('pass'),
        ])->each(function($u) {
            $u->pemodal()->save(factory(App\Pemodal::class)->make([
                'akun_bank_id' => factory(App\AkunBank::class)->create([
                    'nama_akun_bank' => $u->name
                ])->id,
            ]));
        });

        // Peminjam Fixed
        factory(App\User::class)->states('peminjam', 'confirmed')->create([
            'name' => 'Peminjam Test',
            'email' => 'peminjam@peminjam.com',
            'password' => bcrypt('pass'),
        ])->each(function($u) {
            $u->peminjam()->save(factory(App\Peminjam::class)->make([
                'akun_bank_id' => factory(App\AkunBank::class)->create([
                    'nama_akun_bank' => $u->name
                ])->id
            ]));
        });
    }
}
