<?php

use Illuminate\Database\Seeder;

class MasterStatusPernikahanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_status_pernikahan')->delete();
        DB::statement("ALTER TABLE m_status_pernikahan AUTO_INCREMENT = 1;");

        $rows = ['Lajang', 'Menikah', 'Duda', 'Janda'];

        foreach($rows as $row)
        {
            $data = new App\StatusPernikahan;
            $data->nama = $row;
            $data->save();    
        }
    }
}
