<?php

use Illuminate\Database\Seeder;

class MasterJabatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_jabatan')->delete();
        DB::statement("ALTER TABLE m_jabatan AUTO_INCREMENT = 1;");

        $rows = ['Guru', 'Kepala Sekolah', 'Guru Honorer', 'Guru Sementara', 'Guru Pengganti', 'Guru Magang', 'Asisten Guru'];

        foreach($rows as $row)
        {
            $data = new App\Jabatan;
            $data->nama = $row;
            $data->save();    
        }
    }
}
