<?php

use Illuminate\Database\Seeder;

class MasterJenisDokumenPinjamanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_jenis_dokumen_pinjaman')->delete();
        DB::statement("ALTER TABLE m_jenis_dokumen_pinjaman AUTO_INCREMENT = 1;");

        $rows = ['Ijazah', 'Struk Gaji', 'Tanda Bukti Kepemilikan', 'Foto Lokasi', 'KTP', 'SIM', 'Rekening Koran'];

        foreach($rows as $row)
        {
            $data = new App\JenisDokumen;
            $data->nama = $row;
            $data->save();    
        }
    }
}
