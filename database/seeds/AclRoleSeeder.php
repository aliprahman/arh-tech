<?php

use Illuminate\Database\Seeder;

class AclRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('acl_role')->delete();
        DB::statement("ALTER TABLE acl_role AUTO_INCREMENT = 1;");

        $rows = ['Administrator', 'Guest', 'Pemodal', 'Peminjam', 'Staff'];

        foreach($rows as $row)
        {
            $data = new App\Role;
            $data->name = $row;
            $data->save();    
        }
    }
}
