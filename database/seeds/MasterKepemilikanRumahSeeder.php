<?php

use Illuminate\Database\Seeder;

class MasterKepemilikanRumahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_kepemilikan_rumah')->delete();
        DB::statement("ALTER TABLE m_kepemilikan_rumah AUTO_INCREMENT = 1;");

        $rows = ['Sendiri', 'Sewa'];

        foreach($rows as $row)
        {
            $data = new App\KepemilikanRumah;
            $data->nama = $row;
            $data->save();    
        }
    }
}
