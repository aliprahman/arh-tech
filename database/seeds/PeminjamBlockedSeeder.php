<?php

use Illuminate\Database\Seeder;

class PeminjamBlockedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 20)->states('peminjam', 'blocked')->create()->each(function($u) {
            $u->peminjam()->save(factory(App\Peminjam::class)->make([
                'akun_bank_id' => factory(App\AkunBank::class)->create([
                    'nama_akun_bank' => $u->name
                ])->id,
                'status' => 'blocked'
            ]));
        });
    }
}
