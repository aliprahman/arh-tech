<?php

use Illuminate\Database\Seeder;

class PeminjamConfirmedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 100)->states('peminjam', 'confirmed')->create()->each(function($u) {
            $u->peminjam()->save(factory(App\Peminjam::class)->make([
                'akun_bank_id' => factory(App\AkunBank::class)->create([
                    'nama_akun_bank' => $u->name
                ])->id
            ]));
        });
    }
}
