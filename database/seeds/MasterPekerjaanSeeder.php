<?php

use Illuminate\Database\Seeder;

class MasterPekerjaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_pekerjaan')->delete();
        DB::statement("ALTER TABLE m_pekerjaan AUTO_INCREMENT = 1;");

        $rows = ['Guru SD', 'Guru SMP', 'Guru SMA', 'Dosen', 'Kepala Sekolah', 'Guru Olahraga', 'Wirausaha', 'Karyawan Swasta', 'Pegawai Negeri Sipil', 'Buruh'];

        foreach($rows as $row)
        {
            $data = new App\Pekerjaan;
            $data->nama = $row;
            $data->save();
        }
    }
}
