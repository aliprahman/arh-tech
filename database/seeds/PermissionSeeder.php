<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        define('ADMIN',1);
        define('GUEST',2);
        define('PEMODAL',3);
        define('PEMINJAM',4);
        define('STAFF',5);
        # delete all resource
        DB::table('acl_resource')->delete();
        DB::table('acl_rule')->delete();
        DB::statement("ALTER TABLE acl_resource AUTO_INCREMENT = 1;");
        # root
        $resource = App\Resource::create(['type'=>'action','name'=>'^\/']);
        App\Rule::insert([
          ['role_id'=>GUEST,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # login
        $resource = App\Resource::create(['type'=>'action','name'=>'^login']);
        App\Rule::insert([
          ['role_id'=>GUEST,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # logout
        $resource = App\Resource::create(['type'=>'action','name'=>'^logout']);
        App\Rule::insert([
          ['role_id'=>GUEST,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # halaman pendaftaran peminjam
        $resource = App\Resource::create(['type'=>'action','name'=>'^register\/peminjam']);
        App\Rule::insert([
          ['role_id'=>GUEST,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # halaman pendaftaran pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^register\/pemodal']);
        App\Rule::insert([
          ['role_id'=>GUEST,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # ajax get kota by provinsi
        $resource = App\Resource::create(['type'=>'action','name'=>'^ajax\/daftar\/kota\/']);
        App\Rule::insert([
          ['role_id'=>GUEST,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # ajax get kecamata by kota
        $resource = App\Resource::create(['type'=>'action','name'=>'^ajax\/daftar\/kecamatan\/']);
        App\Rule::insert([
          ['role_id'=>GUEST,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # ajax get kelurahan by kecamatan
        $resource = App\Resource::create(['type'=>'action','name'=>'^ajax\/daftar\/kelurahan\/']);
        App\Rule::insert([
          ['role_id'=>GUEST,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # dashboard
        $resource = App\Resource::create(['type'=>'action','name'=>'^dashboard']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # pinjaman
        $resource = App\Resource::create(['type'=>'action','name'=>'^pinjaman']);
        App\Rule::insert([
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # pinjaman export pdf
        $resource = App\Resource::create(['type'=>'action','name'=>'^daftar\/pinjaman\/peminjam\/pdf']);
        App\Rule::insert([
            ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # ajax datatable pinjaman by peminjam
        $resource = App\Resource::create(['type'=>'action','name'=>'^daftar\/pinjaman\/peminjam']);
        App\Rule::insert([
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # halaman pengajuan pinjaman
        $resource = App\Resource::create(['type'=>'action','name'=>'^pinjaman\/ajuan']);
        App\Rule::insert([
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # simpan pengajuan pinjaman
        $resource = App\Resource::create(['type'=>'action','name'=>'^pinjaman\/ajuan\/simpan']);
        App\Rule::insert([
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # konfirmasi setuju simulasi cicilan
        $resource = App\Resource::create(['type'=>'action','name'=>'^pinjaman\/konfirmasi_setuju\/']);
        App\Rule::insert([
          ['role_id'=>GUEST,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # konfirmasi penolakan simulasi cicilan
        $resource = App\Resource::create(['type'=>'action','name'=>'^pinjaman\/konfirmasi_tolak\/']);
        App\Rule::insert([
          ['role_id'=>GUEST,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # halaman rincian pinjaman
        $resource = App\Resource::create(['type'=>'action','name'=>'^pinjaman\/rincian\/']);
        App\Rule::insert([
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # pembayaran cicilan
        $resource = App\Resource::create(['type'=>'action','name'=>'^cicilan\/pembayaran\/']);
        App\Rule::insert([
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # detail pembayaran cicilan
        $resource = App\Resource::create(['type'=>'action','name'=>'^pembayaran\/detail\/']);
        App\Rule::insert([
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # biodata peminjam
        $resource = App\Resource::create(['type'=>'action','name'=>'^peminjam\/biodata']);
        App\Rule::insert([
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # biodata pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^pemodal\/biodata']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # biodata password old pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^old']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # biodata password pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^biodata\/password']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # biodata redirect pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^redirect\/biodata']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # pop up data pendukung peminjam
        $resource = App\Resource::create(['type'=>'action','name'=>'^peminjam\/data_pendukung\/']);
        App\Rule::insert([
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # halaman history pembayaran peminjam
        $resource = App\Resource::create(['type'=>'action','name'=>'^pinjaman\/pembayaran']);
        App\Rule::insert([
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # export pdf history pembayaran peminjam
        $resource = App\Resource::create(['type'=>'action','name'=>'^pinjaman\/daftar\/pembayaran\/pdf']);
        App\Rule::insert([
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # datatable history pembayaran peminjam
        $resource = App\Resource::create(['type'=>'action','name'=>'^pinjaman\/daftar\/pembayaran']);
        App\Rule::insert([
            ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # halaman pencarian pinjaman untuk pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^pemodal\/cari']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # ajax data table pencarian pinjaman untuk pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^data\/pinjaman']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # detail pinjaman untuk pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^pemodal\/portofolio\/']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # konfirmasi pendanaan untuk pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^pemodal\/konfirm']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # halaman daftar pendanaan untuk pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^pemodal\/daftar']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # export ke pdf daftar pendanaan untuk pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^pemodal\/daftar\/pdf']);
        App\Rule::insert([
            ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # halaman deposit untuk pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^pemodal\/deposit']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # halaman save deposit untuk pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^pemodal\/order\/deposit']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # ajax data table deposit untuk pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^pemodal\/list_deposit']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # halaman konfirmasi deposit untuk pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^pemodal\/deposit_konfirmasi']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # simpan konfirmasi deposit untuk pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^pemodal\/deposit_konfirmasi\/']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # halaman penarikan untuk pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^pemodal\/penarikan']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # simpan penarikan untuk pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^pemodal\/order\/penarikan']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # ajax data table penarikan untuk pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^pemodal\/list_penarikan']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # halaman history transaksi untuk pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^pemodal\/history']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # resume history transaksi untuk pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^pemodal\/history\/resume\/']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # ajax datatable history pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^pemodal\/history\/data']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # pdf history transaksi pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^pemodal\/history\/pdf']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # ajax data table daftar pendanaan untuk pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^pemodal\/daftar\/pendanaan']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # halaman rincian cicilan pinjaman untuk pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^pemodal\/cicilan\/']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # export pdf rincian cicilan pinjaman untuk pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^pemodal\/cicilan\/export\/']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # ajax validasi image
        $resource = App\Resource::create(['type'=>'action','name'=>'^validasi\/image']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>GUEST,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # ajax validasi email
        $resource = App\Resource::create(['type'=>'action','name'=>'^validasi\/email']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>GUEST,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # ajax validasi ktp
        $resource = App\Resource::create(['type'=>'action','name'=>'^validasi\/ktp']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>GUEST,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # ajax validasi npwp
        $resource = App\Resource::create(['type'=>'action','name'=>'^validasi\/npwp']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>GUEST,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # ajax validasi mobile
        $resource = App\Resource::create(['type'=>'action','name'=>'^validasi\/mobile']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>GUEST,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # ajax validasi captcha
        $resource = App\Resource::create(['type'=>'action','name'=>'^validasi\/captcha']);
        App\Rule::insert([
          ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow'],
          ['role_id'=>GUEST,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # validasi registrasi pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^auth\/verify\/']);
        App\Rule::insert([
            ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
            ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow'],
            ['role_id'=>GUEST,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # resend email aktivasi registrasi pemodal
        $resource = App\Resource::create(['type'=>'action','name'=>'^auth\/resend']);
        App\Rule::insert([
            ['role_id'=>PEMODAL,'resource_id'=>$resource->id,'access'=>'allow'],
            ['role_id'=>PEMINJAM,'resource_id'=>$resource->id,'access'=>'allow'],
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow'],
            ['role_id'=>GUEST,'resource_id'=>$resource->id,'access'=>'allow']
        ]);

        # untuk admin staff
        # halaman history transaksi
        $resource = App\Resource::create(['type'=>'action','name'=>'^admin\/history_transaksi']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # halaman history transaksi preview print pdf
        $resource = App\Resource::create(['type'=>'action','name'=>'^admin\/print\/history_transaksi\/']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # ajax data table daftar pinjaman
        $resource = App\Resource::create(['type'=>'action','name'=>'^admin\/daftar\/pinjaman']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # halaman analisa pinjaman
        $resource = App\Resource::create(['type'=>'action','name'=>'^pinjaman\/analisa\/']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # setting tanggal pendanaan
        $resource = App\Resource::create(['type'=>'action','name'=>'^pinjaman\/funding\/']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # reject pinjaman
        $resource = App\Resource::create(['type'=>'action','name'=>'^pinjaman\/reject\/']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # generate simulasi cicilan pinjaman
        $resource = App\Resource::create(['type'=>'action','name'=>'^pinjaman\/simulasi\/']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # kirim simulasi cicilan pinjaman
        $resource = App\Resource::create(['type'=>'action','name'=>'^pinjaman\/kirim_simulasi\/']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # ajax data table history transaksi
        $resource = App\Resource::create(['type'=>'action','name'=>'^admin\/daftar\/history_transaksi']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # halaman daftar semua pendanaan
        $resource = App\Resource::create(['type'=>'action','name'=>'^pendanaan']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # ajax daftar semua pendanaan
        $resource = App\Resource::create(['type'=>'action','name'=>'^daftar\/pendanaan']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # halaman daftar Akun
        $resource = App\Resource::create(['type'=>'action','name'=>'^akun']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # ajax data table daftar Akun
        $resource = App\Resource::create(['type'=>'action','name'=>'^daftar\/akun']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # detail Akun
        $resource = App\Resource::create(['type'=>'action','name'=>'^akun\/']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # halaman biodata admin
        $resource = App\Resource::create(['type'=>'action','name'=>'^admin\/biodata']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # halaman ACL
        $resource = App\Resource::create(['type'=>'action','name'=>'^acl']);
        # halaman daftar semua deposit
        $resource = App\Resource::create(['type'=>'action','name'=>'^admin\/deposit']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # ajax daftar semua deposit
        $resource = App\Resource::create(['type'=>'action','name'=>'^admin\/daftar\/deposit']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # update dan detail deposit
        $resource = App\Resource::create(['type'=>'action','name'=>'^admin\/deposit\/']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # halaman akun bank admin
        $resource = App\Resource::create(['type'=>'action','name'=>'^admin\/akun_bank']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # ajax akun bank admin
        $resource = App\Resource::create(['type'=>'action','name'=>'^admin\/daftar\/akun_bank']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # update akun bank admin
        $resource = App\Resource::create(['type'=>'action','name'=>'^admin\/akun_bank\/']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # halaman master data pekerjaan
        $resource = App\Resource::create(['type'=>'action','name'=>'^admin\/pekerjaan']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # ajax daftar pekerjaan
        $resource = App\Resource::create(['type'=>'action','name'=>'^admin\/daftar\/pekerjaan']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # update data pekerjaan
        $resource = App\Resource::create(['type'=>'action','name'=>'^admin\/pekerjaan\/']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # halaman daftar semua cicilan pembayaran
        $resource = App\Resource::create(['type'=>'action','name'=>'^admin\/cicilan']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # ajax daftar semua cicilan pembayaran
        $resource = App\Resource::create(['type'=>'action','name'=>'^admin\/daftar\/cicilan']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # update dan detail cicilan
        $resource = App\Resource::create(['type'=>'action','name'=>'^admin\/cicilan\/']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # halaman daftar penarikan admin
        $resource = App\Resource::create(['type'=>'action','name'=>'^admin\/penarikan']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # ajax daftar penarikan
        $resource = App\Resource::create(['type'=>'action','name'=>'^admin\/penarikan\/data']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
        # get detail penarikan
        $resource = App\Resource::create(['type'=>'action','name'=>'^admin\/penarikan\/']);
        App\Rule::insert([
            ['role_id'=>STAFF,'resource_id'=>$resource->id,'access'=>'allow']
        ]);
    }
}
