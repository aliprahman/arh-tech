<?php

use Illuminate\Database\Seeder;

class AkunBankAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $akun_bank = new App\AkunBank();
        $akun_bank->bank_id = 2;
        $akun_bank->nama_akun_bank = 'SAHALA FIRDAUS SYAH DOLOKSARIBU';
        $akun_bank->no_akun_bank = '113 - 00 - 7070444 - 4';
        $akun_bank->is_akun_admin = 1;
        $akun_bank->save();
    }
}
