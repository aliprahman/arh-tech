<?php

use Illuminate\Database\Seeder;

class MasterPendidikanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_pendidikan')->delete();
        DB::statement("ALTER TABLE m_pendidikan AUTO_INCREMENT = 1;");

        $rows = ['SMA', 'D1', 'D3', 'S1', 'S2', 'S3'];

        foreach($rows as $row)
        {
            $data = new App\Pendidikan;
            $data->nama = $row;
            $data->save();
        }
    }
}
