<?php

use Illuminate\Database\Seeder;

class MasterHubunganKontakSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_hubungan_kontak')->delete();
        DB::statement("ALTER TABLE m_hubungan_kontak AUTO_INCREMENT = 1;");

        $rows = ['Orang Tua', 'Saudara Kandung', 'Saudara Sepupu', 'Kakak', 'Adik'];

        foreach($rows as $row)
        {
            $data = new App\HubunganKontak;
            $data->nama = $row;
            $data->save();    
        }
    }
}
