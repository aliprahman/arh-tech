<?php

use Illuminate\Database\Seeder;

class AclRuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('acl_rule')->delete();

        $rows = [
            array(2,1,'allow'),
            array(2,28,'allow'),
            array(2,29,'allow'),
            array(2,30,'allow'),
            array(2,32,'allow'),
            array(2,33,'allow'),
            array(2,36,'allow'),
            array(2,37,'allow'),
            array(2,39,'allow'),
            array(2,40,'allow'),
            array(2,42,'allow'),
            array(2,60,'allow'),
            array(2,75,'allow'),
            array(2,76,'allow'),
            array(2,77,'allow'),
            array(2,78,'allow'),
            array(2,80,'allow'),
            array(2,81,'allow'),
            array(2,82,'allow'),
            array(2,83,'allow'),
            array(2,84,'allow'),
            array(2,85,'allow'),
            array(2,126,'allow'),
            array(2,127,'allow'),

            /*
            * Pemodal Rule
            */
            array(3,1,'allow'),
            array(3,35,'allow'),
            array(3,36,'allow'),
            array(3,37,'allow'),
            array(3,38,'allow'),
            array(3,39,'allow'),
            array(3,40,'allow'),
            array(3,57,'allow'),
            array(3,58,'allow'),
            array(3,59,'allow'),
            array(3,60,'allow'),
            array(3,61,'allow'),
            array(3,62,'allow'),
            array(3,63,'allow'),
            array(3,64,'allow'),
            array(3,65,'allow'),
            array(3,66,'allow'),
            array(3,67,'allow'),
            array(3,68,'allow'),
            array(3,69,'allow'),
            array(3,70,'allow'),
            array(3,71,'allow'),
            array(3,72,'allow'),
            array(3,73,'allow'),
            array(3,74,'allow'),
            array(3,75,'allow'),
            array(3,76,'allow'),
            array(3,77,'allow'),
            array(3,86,'allow'),
            array(3,87,'allow'),
            array(3,88,'allow'),
            /*
            * Peminjam Rule
            */
            array(4,1,'allow'),
            array(4,35,'allow'),
            array(4,36,'allow'),
            array(4,37,'allow'),
            array(4,38,'allow'),
            array(4,39,'allow'),
            array(4,40,'allow'),
            array(4,41,'allow'),
            array(4,42,'allow'),
            array(4,43,'allow'),
            array(4,44,'allow'),
            array(4,45,'allow'),
            array(4,46,'allow'),
            array(4,47,'allow'),
            array(4,48,'allow'),
            array(4,49,'allow'),
            array(4,50,'allow'),
            array(4,51,'allow'),
            array(4,52,'allow'),
            array(4,53,'allow'),
            array(4,54,'allow'),
            array(4,55,'allow'),
            array(4,56,'allow'),
            array(4,74,'allow'),
            array(4,75,'allow'),
            array(4,76,'allow'),
            array(4,77,'allow'),
            //array(4,109,'allow'),
            # dashboard
            array(4,86,'allow'),
            array(4,87,'allow'),
            array(4,88,'allow'), # peminjam
            array(4,94,'allow'), # peminjam
            array(4,95,'allow'),
            array(4,96,'allow'),
            # analisa pinjaman
            array(4,98,'allow'),
            array(4,99,'allow'),
            array(1,100,'allow'),
            array(4,101,'allow'),
            array(4,102,'allow'),
            array(4,105,'allow'),
        ];

        foreach($rows as $row)
        {
            $data = new App\Rule;
            $data->role_id = $row[0];
            $data->resource_id = $row[1];
            $data->access = $row[2];
            $data->save();
        }
    }
}
