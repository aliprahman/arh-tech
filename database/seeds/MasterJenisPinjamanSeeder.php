<?php

use Illuminate\Database\Seeder;

class MasterJenisPinjamanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_jenis_pinjaman')->delete();
        DB::statement("ALTER TABLE m_jenis_pinjaman AUTO_INCREMENT = 1;");

        $rows = ['Pinjaman Guru','Pinjaman Umum'];

        foreach($rows as $row)
        {
            $data = new App\JenisPinjaman;
            $data->nama = $row;
            $data->save();
        }
    }
}
