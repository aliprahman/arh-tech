<?php

use Faker\Generator as Faker;

$factory->define(App\AkunBank::class, function (Faker $faker) {
    $bank_count = DB::table('m_bank')->count();
    $bank = DB::table('m_bank')->skip($faker->numberBetween(0, $bank_count - 1))->first();

    return [
        'bank_id' => $bank->id,
        'no_akun_bank' => $faker->numerify('################')
    ];
});
