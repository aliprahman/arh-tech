<?php

use Faker\Generator as Faker;

$factory->define(App\Alamat::class, function (Faker $faker) {
    $village_count = DB::table('m_village')->count();
    $village_id = DB::table('m_village')->skip($faker->numberBetween(0, $village_count - 1))->take(1)->value('id');
    $district_id = substr($village_id, 0, 7);
    $regency_id = substr($district_id, 0, 4);
    $province_id = substr($regency_id, 0, 2);
    return [
        'alamat' => $faker->streetAddress(),
        'kode_pos' => $faker->postCode(),
        'village_id' => $village_id,
        'district_id' => $district_id,
        'regency_id' => $regency_id,
        'province_id' => $province_id
    ];
});
