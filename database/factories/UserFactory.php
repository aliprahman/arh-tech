<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    $client = new GuzzleHttp\Client();
    do {
        $res = $client->request('GET', 'https://randomuser.me/api/');
        $row = json_decode((string) $res->getBody())->results[0];
        // dd($row);
        $name = ucwords($row->name->first) . ' ' . ucwords($row->name->last);
        $email = $row->email;

        $t_rows = DB::table('users')->where('email', $email)->first();
    } while (!empty($t_rows)); // make sure email is unique

    // Download photo_profile.
    $filename = $faker->md5() . '.jpg';
    $client->request('GET', $row->picture->large, ['sink' => Storage::path('profile_photo/' . $filename)]);
    $file = new App\FileUploads;
    $file->type = 'profile_photo';
    $file->name = $filename;
    $file->size = Storage::size('profile_photo/' . $filename);
    $file->mime = Storage::mimetype('profile_photo/' . $filename);
    $file->save();

    // Create Biodata.
    $biodata = new App\Biodata;
    $biodata->tempat_lahir = $faker->city;
    $biodata->tanggal_lahir = $faker->dateTimeBetween('-40 years', '-22 years')->format('Y-m-d');
    $biodata->jenis_kelamin = ($row->gender == 'male' ? 'laki-laki' : 'perempuan');
    $biodata->no_telepon = $faker->phoneNumber();
    $biodata->no_hp = $faker->phoneNumber();
    $biodata->no_ktp = $faker->numerify('################');
    $biodata->no_npwp = $faker->numerify('##.###.###.#-###.###');
    $biodata->save();

    return [
        'id_pelanggan' => $faker->randomNumber(7), 
        'name' => $name,
        'email' => $email,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'verification_token' => str_random(16), 
        'photo_profile_id' => $file->id, 
        'biodata_id' => $biodata->id, 
        'type' => 'peminjam',        
        'status' => 'confirmed'
    ];
});

$factory->state(App\User::class, 'pemodal', function(Faker $faker) {
    return [
        'type' => 'pemodal',
        'role_id' => 3
    ];
});

$factory->state(App\User::class, 'peminjam', function(Faker $faker) {
    return [
        'type' => 'peminjam',
        'role_id' => 4
    ];
});

$factory->state(App\User::class, 'new', function(Faker $faker) {
    return [
        'status' => 'new',
        'created_at' => $faker->dateTimeBetween('-30 days', '-1 days')
    ];
});

$factory->state(App\User::class, 'confirmed', function(Faker $faker) {
    return [
        'status' => 'confirmed',
        'created_at' => $faker->dateTimeBetween('-1 years', '-30 days')
    ];
});

$factory->state(App\User::class, 'blocked', function(Faker $faker) {
    return [
        'status' => 'blocked',
        'created_at' => $faker->dateTimeBetween('-1 years', '-30 days')
    ];
});
