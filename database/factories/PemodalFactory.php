<?php

use Faker\Generator as Faker;

$factory->define(App\Pemodal::class, function (Faker $faker) {
    return [
        'type' => 'Personal',
        'alamat_id' => function() {
            return factory(App\Alamat::class)->create()->id;
        },
        'saldo' => $faker->numberBetween(5, 500) * 100000, 
        'status' => $faker->boolean(80) ? 'verified' : 'unverified',
    ];
});
