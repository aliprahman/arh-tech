<?php

use Faker\Generator as Faker;

$factory->define(App\Peminjam::class, function (Faker $faker) {
    $kepemilikan_count = DB::table('m_kepemilikan_rumah')->count();
    $kepemilikan_id = DB::table('m_kepemilikan_rumah')->skip($faker->numberBetween(0, $kepemilikan_count -1))->first()->id;
    $jabatan_count = DB::table('m_jabatan')->count();
    $jabatan_id = DB::table('m_jabatan')->skip($faker->numberBetween(0, $jabatan_count - 1))->first()->id;
    $status_pernikahan_count = DB::table('m_status_pernikahan')->count();
    $status_pernikahan_id = DB::table('m_status_pernikahan')->skip($faker->numberBetween(0, $status_pernikahan_count - 1))->first()->id;
    $pendidikan_count = DB::table('m_pendidikan')->count();
    $pendidikan_id = DB::table('m_pendidikan')->skip($faker->numberBetween(0, $pendidikan_count - 1))->first()->id;
    $hubungan_kontak_count = DB::table('m_hubungan_kontak')->count();
    $hubungan_kontak_id = DB::table('m_hubungan_kontak')->skip($faker->numberBetween(0, $hubungan_kontak_count - 1))->first()->id;
    $pekerjaan_count = DB::table('m_pekerjaan')->count();
    $pekerjaan_id = DB::table('m_pekerjaan')->skip($faker->numberBetween(0, $pekerjaan_count - 1))->first()->id;
    return [
        'nama_gadis_ibu_kandung' => $faker->name('female'),
        'alamat_id' => function() {
            return factory(App\Alamat::class)->create()->id;
        },
        'kepemilikan_rumah_id' => $kepemilikan_id,
        'nama_tempat_kerja' => $faker->company,
        'jabatan_id' => $jabatan_id,
        'alamat_tempat_kerja_id' => function() {
            return factory(App\Alamat::class)->create()->id;
        },
        'lama_berdiri_tempat_kerja' => $faker->numberBetween(3, 20),
        'jumlah_murid_tempat_kerja' => $faker->numberBetween(100, 800),
        'no_sk_surat_kontrak' => $faker->numerify('SK#####-####-###'),
        'tanggal_sk_surat_kontrak' => $faker->dateTimeBetween('-1 years', '-3 months')->format('Y-m-d'),
        'lama_mengajar' => $faker->numberBetween(5, 20),
        'status_pengajar' => $faker->boolean(80) ? 'tetap' : 'tidak_tetap',
        'gaji' => $faker->numberBetween(3000000, 7000000),
        'status_pernikahan_id' => $status_pernikahan_id,
        'pendidikan_id' => $pendidikan_id,
        'kontak_nama' => $faker->name(),
        'hubungan_kontak_id' => $hubungan_kontak_id, 
        'alamat_kontak_id' => function() {
            return factory(App\Alamat::class)->create()->id;
        },
        'no_telp_kontak' => $faker->phoneNumber(),
        'no_hp_kontak' => $faker->phoneNumber(),
        'nama_suami_istri' => $faker->name(),
        'tempat_lahir_suami_istri' => $faker->city(),
        'tanggal_lahir_suami_istri' => $faker->dateTimeBetween('-50 years', '-25 years')->format('Y-m-d'),
        'no_ktp_suami_istri' => $faker->numerify('################'),
        'pekerjaan_suami_istri_id' => $pekerjaan_id,
        'pendapatan_suami_istri' => $faker->numberBetween(5, 500) * 100000, 
        'jumlah_tanggungan' => $faker->numberBetween(2, 7),

        'saldo' => $faker->numberBetween(5, 500) * 100000, 
        'status' => $faker->boolean(80) ? 'verified' : 'unverified',
    ];
});
