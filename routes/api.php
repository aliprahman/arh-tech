<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*
* Master
 */
Route::get('master/provinsi', 'Ajax@set_list_provinsi');
Route::get('master/kota/{provinsi_id}', 'Ajax@set_list_kota');
Route::get('master/kecamatan/{kota_id}', 'Ajax@set_list_kecamatan');
Route::get('master/kelurahan/{kecamatan_id}', 'Ajax@set_list_kelurahan');
Route::get('master/bank', 'Ajax@set_list_bank');
Route::get('master/jenis-pinjaman', 'Ajax@set_list_jenis_pinjaman');
Route::get('master/jabatan', 'Ajax@set_list_jabatan');

/*
* Auth
 */
Route::post('login', 'Api\Auth\LoginController@login');
Route::post('logout', 'Api\Auth\LoginController@logout');
Route::get('profile', 'Api\Auth\LoginController@profile');

/*
* Peminjam
 */
Route::prefix('peminjam')->group(function(){
	Route::post('register', 'Api\Peminjam\RegisterController@register');
	Route::get('list-pinjaman', 'Api\Peminjam\ListPinjamanController@index');
	Route::get('rincian-pinjaman/{pinjaman_id}', 'Api\Peminjam\ListPinjamanController@rincian');
	Route::get('list-transaksi', 'Api\Peminjam\ListTransaksiController@index');
	Route::post('ajuan', 'Api\Peminjam\AjuanController@store');
	Route::post('pinjaman/pembayaran/{cicilan_id}', 'Api\Peminjam\CicilanController@doPembayaran');
	Route::get('pinjaman/pembayaran/detail/{transaksi_id}', 'Api\Peminjam\CicilanController@detailPembayaran');
	Route::put('update-profile-akun', 'Api\Peminjam\ProfileController@updateAkun');
	Route::put('update-profile-biodata', 'Api\Peminjam\ProfileController@updateBiodata');
	Route::put('update-profile-bank', 'Api\Peminjam\ProfileController@updateBank');
	Route::put('update-profile-pekerjaan', 'Api\Peminjam\ProfileController@updatePekerjaan');
	Route::put('update-profile-keuangan', 'Api\Peminjam\ProfileController@updateKeuangan');
});

/*
* Pemodal
 */
Route::prefix('pemodal')->group(function(){
	Route::post('register', 'Api\Pemodal\RegisterController@register');
	Route::put('update-profile', 'Api\Pemodal\ProfileController@update');
	Route::get('dashboard', 'Api\Pemodal\DashboardController@index');
	Route::get('list-pinjaman', 'Api\Pemodal\ListPinjamanController@index');
	Route::get('portofolio-pinjaman/{pinjaman_id}', 'Api\Pemodal\ListPinjamanController@portofolio');
	Route::get('detail-pinjaman/{pinjaman_id}', 'Api\Pemodal\ListPinjamanController@detail');
	Route::get('pendanaan/list', 'Api\Pemodal\PendanaanController@index');
	Route::post('pendanaan/create', 'Api\Pemodal\PendanaanController@doPendanaan');
	Route::get('deposit/list', 'Api\Pemodal\DepositController@index');
	Route::post('deposit/order', 'Api\Pemodal\DepositController@order');
	Route::get('deposit/detail/{deposit_id}', 'Api\Pemodal\DepositController@detail');
	Route::post('deposit/konfirmasi', 'Api\Pemodal\DepositController@konfirmasi');
	Route::get('penarikan/list', 'Api\Pemodal\PenarikanController@index');
	Route::post('penarikan/order', 'Api\Pemodal\PenarikanController@order');
	Route::get('list-transaksi', 'Api\Pemodal\ListTransaksiController@index');
});
