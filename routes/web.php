<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
  return redirect('/login');
});
// auth
Auth::routes();
//ajax (alamat lengkap)
Route::prefix('ajax')->group(function (){
    Route::get('daftar/kota/{id_provinsi}','Ajax@set_list_kota');
    Route::get('daftar/kecamatan/{id_kota}','Ajax@set_list_kecamatan');
    Route::get('daftar/kelurahan/{id_kecamatan}','Ajax@set_list_kelurahan');
});
// peminjam exlcude auth
Route::namespace('Peminjam')->group(function(){
  Route::get('/register/peminjam','Registration@load_form_peminjam');
  Route::post('/register/peminjam','Registration@register_peminjam');
  Route::get('pinjaman/konfirmasi_setuju/{id}','Credits@approve_from_email');
  Route::get('pinjaman/konfirmasi_tolak/{id}','Credits@reject_from_email');
});
//pemodal exlude auth
Route::namespace('Pemodal')->group(function(){
  //register
  Route::get('/register/pemodal','RegisterController@RegistrasiForm');
  Route::post('/register/pemodal', 'RegisterController@register');
  //verifikasi
  Route::get('auth/verify/{token}', 'RegisterController@verify');
  Route::get('auth/resend/', 'RegisterController@resend');
  //ajax validation with laravel custom validator
  Route::post('validasi/email', 'RegisterController@validasiemail');
  Route::post('validasi/mobile', 'RegisterController@validasimobile');
  Route::post('validasi/ktp', 'RegisterController@validasiktp');
  Route::post('validasi/npwp', 'RegisterController@validasinpwp');
  Route::post('validasi/image', 'RegisterController@image');
  Route::post('validasi/captcha', 'RegisterController@captcha');
});
// routes with authentication
Route::middleware('auth')->group(function() {
  Route::get('dashboard', 'Dashboard@index');
  Route::get('pinjaman',function(){
    if(auth()->user()->type == 'finnesia'){
      return view('mockup.pinjaman.admin_list');
    }else{
      return view('mockup.pinjaman.peminjam_list');
    }
  });

  // staff finnesia
  Route::namespace('Admin')->group(function() {
    // menu akun
    Route::get('akun','Accounts@index');
    Route::get('akun/{id}','Accounts@detail');
    Route::post('daftar/akun','Accounts@dataTable');
    Route::prefix('admin')->group(function(){
      // biodata
      Route::get('biodata','Biodata@index');
      Route::post('biodata','Biodata@update');
      // history transaksi
      Route::post('daftar/history_transaksi','History_transaksi@datatables');
      Route::get('history_transaksi','History_transaksi@index');
      Route::get('print/history_transaksi/{month}/{year}','History_transaksi@print');
      // daftar pinjaman
      Route::post('daftar/pinjaman','Pinjaman@datatables');
      // daftar deposit
      Route::get('deposit','Deposit@index');
      Route::post('daftar/deposit','Deposit@datatable');
      Route::get('deposit/{id}','Deposit@show');
      Route::put('deposit/{id}','Deposit@update');
      // akun bank transaksi
      Route::get('akun_bank','AkunBank@index');
      Route::post('akun_bank','AkunBank@store');
      Route::post('daftar/akun_bank','AkunBank@datatable');
      Route::get('akun_bank/{id}','AkunBank@show');
      Route::put('akun_bank/{id}','AkunBank@update');
      // pekerjaan
      Route::get('pekerjaan','Pekerjaan@index');
      Route::post('pekerjaan','Pekerjaan@store');
      Route::post('daftar/pekerjaan','Pekerjaan@datatable');
      Route::get('pekerjaan/{id}','Pekerjaan@show');
      Route::put('pekerjaan/{id}','Pekerjaan@update');
      Route::delete('pekerjaan/{id}','Pekerjaan@destroy');
      // daftar pembayaran Cicilan
      Route::get('cicilan','Cicilan@index');
      Route::post('daftar/cicilan','Cicilan@datatables');
      Route::get('cicilan/{id}','Cicilan@show');
      Route::put('cicilan/{id}','Cicilan@update');
      // daftar penarikan
      Route::get('penarikan','Penarikan@index');
      Route::post('penarikan/data','Penarikan@datatables');
      Route::get('penarikan/{id}','Penarikan@show');
      Route::put('penarikan/{id}','Penarikan@update');
    });
    // pinjaman
    Route::get('pinjaman/analisa/{id}','Pinjaman@analyze');
    Route::put('pinjaman/reject/{id}','Pinjaman@reject');
    Route::get('pinjaman/simulasi/{id}','Pinjaman@generate_simulai_cicilan');
    Route::post('pinjaman/kirim_simulasi/{id}','Pinjaman@send_simulasi');
    Route::put('pinjaman/funding/{id}','Pinjaman@funding_periode');
    // pendanaan
    Route::get('pendanaan','Funding@index');
    Route::post('daftar/pendanaan','Funding@dataTable');
  });
  // end staff

  // peminjam
  Route::namespace('Peminjam')->group(function(){
    Route::get('pinjaman/ajuan', 'Credits@create');
    Route::post('pinjaman/ajuan/simpan', 'Credits@store');
    Route::get('pinjaman/rincian/{id}','Credits@installment');
    Route::post('cicilan/pembayaran/{id}','Credits@pay_installment');
    Route::get('pembayaran/detail/{id}','Credits@detail_payment');
    Route::get('pinjaman/pembayaran','Credits@payment_history');
    Route::post('pinjaman/daftar/pembayaran','Credits@list_payment');
    Route::get('pinjaman/daftar/pembayaran/pdf','Credits@export_pembayaran_cicilan');
    Route::post('daftar/pinjaman/admin','Credits@list_credits_admin');
    Route::post('daftar/pinjaman/peminjam','Credits@list_credits_peminjam');
    Route::get('daftar/pinjaman/peminjam/pdf','Credits@export_pinjaman');
    Route::prefix('peminjam')->group(function () {
      Route::get('/biodata','Biodata@index');
      Route::post('/biodata','Biodata@update');
      Route::post('/data_pendukung/{id}','Biodata@update_data_pendukung');
    });
  });

  // pemodal
  Route::namespace('Pemodal')->group(function(){
    // pinjaman
    Route::get('/data/pinjaman','ListPinjaman@pinjaman');
    // biodata check old password
    Route::post('/old', 'Biodata@old');
    // biodata
    Route::get('redirect/biodata','Biodata@redirect');
    Route::post('biodata/password', 'Biodata@password');
    Route::prefix('pemodal')->group(function(){
      // pinjaman
      Route::get('cari','ListPinjaman@cari_pinjaman');
      Route::post('konfirm','ListPinjaman@konfirm_pinjaman');
      Route::get('portofolio/{id}','ListPinjaman@detail_pinjaman');
      // pendanaan
      Route::get('daftar','Pendanaan@index');
      Route::post('daftar/pendanaan','Pendanaan@daftarpendanaan');
      Route::get('daftar/pendanaan/pdf','Pendanaan@export_pendanaan');
      Route::get('cicilan/{id}','Pendanaan@cicilan');
      Route::get('cicilan/export/{id}','Pendanaan@export_cicilan');
      // history
      Route::get('history','History@index');
      Route::get('history/resume/{periode}','History@resume');
      Route::post('history/data','History@list');
      Route::get('history/pdf','History@export_pdf');
      // biodata
      Route::get('biodata','Biodata@index');
      Route::post('biodata','Biodata@update');
      //deposit
      Route::get('deposit', 'DepositController@index');
      Route::post('order/deposit','DepositController@order');
      Route::get('list_deposit', 'DepositController@list');
      Route::get('deposit_konfirmasi/{id}', 'DepositController@konfirmasi');
      Route::post('deposit_konfirmasi', 'DepositController@update');
      //penarikan
      Route::get('penarikan','PenarikanController@index');
      Route::post('order/penarikan','PenarikanController@order');
      Route::get('list_penarikan', 'PenarikanController@list');
    });
  });

  // setting acl
  Route::namespace('Acl')->group(function(){
      Route::get('/acl', 'RuleController@index');

      Route::get('/acl/resource/index', 'ResourceController@index');
      Route::get('/acl/resource/add', 'ResourceController@getCreate');
      Route::get('/acl/resource/edit/{id}', 'ResourceController@getEdit');
      Route::post('/acl/resource/post_add', 'ResourceController@postCreate');
      Route::post('/acl/resource/post_edit', 'ResourceController@postEdit');
      Route::post('/acl/resource/delete/{id}', 'ResourceController@postDelete');

      Route::get('/acl/role/index', 'RoleController@index');
      Route::get('/acl/role/add', 'RoleController@getCreate');
      Route::get('/acl/role/edit/{id}', 'RoleController@getEdit');
      Route::post('/acl/role/post_add', 'RoleController@postCreate');
      Route::post('/acl/role/post_edit', 'RoleController@postEdit');
      Route::post('/acl/role/delete/{id}', 'RoleController@postDelete');

      Route::get('/acl/rule/index', 'RuleController@index');
      Route::get('/acl/rule/add', 'RuleController@getCreate');
      Route::get('/acl/rule/edit/{id}', 'RuleController@getEdit');
      Route::post('/acl/rule/post_add', 'RuleController@postCreate');
      Route::post('/acl/rule/post_edit', 'RuleController@postEdit');
      Route::post('/acl/rule/delete/{id}', 'RuleController@postDelete');
  });
});
