
    $(document).ready(function(){
        $('#data').DataTable();
        $('.select2').select2();
        //semua element dengan class text-danger akan di sembunyikan saat load
        $('.text-danger').hide();
        //untuk mengecek bahwa semua textbox tidak boleh kosong
        $('#pemodal input').each(function(){  
            $(this).blur(function(){ //blur function itu dijalankan saat element kehilangan fokus
                if (! $(this).val()){ //this mengacu pada text box yang sedang fokus
                    return get_error_text(this); //function get_error_text ada di bawah
                } else {
                    $(this).removeClass('no-valid'); 
                    $(this).parent().find('.text-danger').hide();//cari element dengan class has-error dari element induk text yang sedang focus
                    $(this).find('.text-danger').hide();
                    $(this).closest('div').removeClass('has-error');
                    $(this).closest('div').addClass('has-success');
                }
            });
        });
        $('#pemodal select').each(function(){  
            $(this).change(function(){ //blur function itu dijalankan saat element kehilangan fokus
                if (! $(this).val()){ //this mengacu pada text box yang sedang fokus
                    return get_error_text(this); //function get_error_text ada di bawah
                } else {
                    $(this).removeClass('no-valid'); 
                    $(this).parent().find('.text-danger').hide();//cari element dengan class has-error dari element induk text yang sedang focus
                    $(this).find('.text-danger').hide();
                    $(this).closest('div').removeClass('has-error');
                    $(this).closest('div').addClass('has-success');
                }
            });
        });
    
         //reset button
         $('#reset_bio').on('click', function(){
            $('#alert').hide();
            $('#submit').prop('disabled', false);
         });

         $('#delete').on('click', function(){
            $('#image').closest('div').removeClass('has-error');
            $('#alert').hide();
         });

          
       $('#email').blur(function(){
          var email= $(this).val();
          var len= email.length;
          if(len>0){ 
            if(!valid_email(email)){ 
              $(this).parent().find('.text-danger').text("");
              $(this).parent().find('.text-danger').text("Email tidak valid (ex: sample@domain.com)"); 
              return apply_feedback_error(this);
            } else {
              if (len>38){ 
                $(this).parent().find('.text-danger').text("");
                $(this).parent().find('.text-danger').text("Maksimal 38 karakter");
                return apply_feedback_error(this);
              } else {
                var valid = false;
                $.ajax({
                           url: $('meta[name="base_url"]').attr('content')+"/validasi/email",
                           type: "POST",
                           data: {
                                  '_token': $('meta[name="_token"]').attr('content'),
                                  'email': email,
                                 },
                           dataType: "text",
                           success: function(data){
                                   if (data==1){ //pada file check email.php, apabila email sudah ada di database makan akan mengembalikan nilai 0
                                       $('#email').parent().find('.text-danger').text("");
                                       $('#email').parent().find('.text-danger').text("Email yang Anda masukan telah digunakan"); 
                                       return apply_feedback_error('#email');
                                   }
                                }
                  });
                }
            }

          } 
        });
          $('#tanggal_lahir').change(function(){
            var tanggal= $(this).val();
            var len= tanggal.length;
            if(len == 0){
              $('#alert_tanggal').show().text('Kolom tidak boleh kosong');
                $('#tanggal_lahir').closest('div').removeClass('has-success');
            }
            if(len >0){
              $('#alert_tanggal').hide();
                $('#tanggal_lahir').closest('div').removeClass('has-error');
                $('#tanggal_lahir').closest('div').addClass('has-success');
            }
          });
          $('#mobile').blur(function(){
              var mobile= $(this).val();
              var len= mobile.length;
              if(len == 0){
                $('#alert_mobile').show().text('Kolom tidak boleh kosong');
                  $('#mobile').closest('div').removeClass('has-success');
              }
              if(len>0){
                  if(!valid_hp(mobile)){ 
                    $('#alert_mobile').show().text('No.HP tidak valid (ex: 08xxxxxxx)');
                      $('#mobile').closest('div').removeClass('has-success');
                  }else{
                    if (len>14){ 
                    $('#alert_mobile').show().text('maksimal 14 digit');
                    $('#mobile').closest('div').removeClass('has-success');
                    } else {
                      var valid = false;
                      $.ajax({
                                 url: $('meta[name="base_url"]').attr('content')+"/validasi/mobile",
                                 type: "POST",
                                 data: {
                                        '_token': $('meta[name="_token"]').attr('content'),
                                        'no_hp': mobile,
                                       },
                                 dataType: "text",
                                 success: function(data){
                                         if (data == 1){ 
                                          $('#alert_mobile').show().text('No.HP telah digunakan');
                                          $('#mobile').closest('div').removeClass('has-success');
                                         }else {
                                          $('#alert_mobile').hide();
                                          $('#mobile').closest('div').addClass('has-success');
                                         }
                                      }
                        });
                      }
                    } 
            
          } 
        });
        $('#home').blur(function(){
          var home= $(this).val();
          var len= home.length;
          if(len == 0){
            $('#alert_home').show().text('Kolom tidak boleh kosong');
              $('#home').closest('div').removeClass('has-success');
          }else {
            if (len > 0) {
              if(!valid_home(home)){ 
                $('#alert_home').show().text('No.Tlp tidak valid (ex: 02xxxxxxx)');
                  $('#home').closest('div').removeClass('has-success');
              }else {
                if (len>14){ 
                  $('#alert_home').show().text('maksimal 14 digit');
                  $('#home').closest('div').removeClass('has-success');
                }else {
                  $('#alert_home').hide();
                    $('#home').closest('div').addClass('has-success');
                }
              }
            }
            
          }
        });
        $('#ktp').blur(function(){
          var ktp= $(this).val();
          var len= ktp.length;
          if(len>0){ 
              if (len>14){ 
                $(this).parent().find('.text-danger').text("");
                $(this).parent().find('.text-danger').text("maksimal 14 karakter");
                return apply_feedback_error(this);
              } else {
                var valid = false;
                $.ajax({
                        url: $('meta[name="base_url"]').attr('content')+"/validasi/ktp",
                        type: "POST",
                        data: {
                              '_token': $('meta[name="_token"]').attr('content'),
                              'no_ktp': ktp,
                              },
                        dataType: "text",
                        success: function(data){
                                           if (data==1){ //pada file check, apabila ktp sudah ada di database makan akan mengembalikan nilai 0
                                              $('#ktp').parent().find('.text-danger').text("");
                                              $('#ktp').parent().find('.text-danger').text("ID KTP yang Anda masukan telah digunakan"); 
                                              return apply_feedback_error('#ktp');
                                           }
                                         }
                  });
                }
            
          } 
        });
        $('#npwp').blur(function(){
          var npwp= $(this).val();
          var len= npwp.length;
          if(len>0){ 
              if (len>14){ 
                $(this).parent().find('.text-danger').text("");
                $(this).parent().find('.text-danger').text("maksimal 14 karakter");
                return apply_feedback_error(this);
              } else {
                var valid = false;
                $.ajax({
                         url: $('meta[name="base_url"]').attr('content')+"/validasi/npwp",
                         type: "POST",
                         data: {
                                '_token': $('meta[name="_token"]').attr('content'),
                                'no_npwp': npwp,
                               },
                         dataType: "text",
                         success: function(data){
                                 if (data==1){ //pada file check , apabila npwp sudah ada di database makan akan mengembalikan nilai 0
                                     $('#npwp').parent().find('.text-danger').text("");
                                     $('#npwp').parent().find('.text-danger').text("ID NPWP yang Anda masukan telah digunakan"); 
                                     return apply_feedback_error('#npwp');
                                 }
                               }
                  });
                }
            
          } 
        });

        //image
         $('#image').change(function(){
            var image= $(this).val();
            var len= image.length;
            if(len>0){ 
                    var form_data = new FormData($('#pemodal')[0]);
                    var valid = false;
                    $.ajax({
                             url: $('meta[name="base_url"]').attr('content')+"/validasi/image",
                             type: "POST",
                             processData: false,
                             contentType: false,
                             async: false,
                             cache: false,
                             data : form_data,
                             success: function(data){
                                       if (data==1){ 
                                            $('#alert').show().text("File harus gambar (png,jpg,jpeg,bmp,img,dll)");
                                            $('#image').closest('div').removeClass('has-success');
                                            $('#image').closest('div').addClass('has-error');
                                            $('#image').addClass('no-valid');
                                        }else {
                                            $('#image').closest('div').removeClass('has-error');
                                            $('#image').closest('div').addClass('has-success');
                                            $('#image').removeClass('no-valid');
                                            $('#alert').hide();
                                        }
                                  }
                        });
                
            } 

        });
         $('#pemodal').submit(function(e){
            e.preventDefault();
            var valid=true;     
            $(this).find('.textbox').each(function(){
                if (! $(this).val()){
                    if($('#tanggal_lahir').val().length == 0){
                        $('#alert_tanggal').show().text('Kolom tidak boleh kosong');
                        $('#tanggal_lahir').closest('div').removeClass('has-success');
                    }
                    if($('#mobile').val().length == 0){
                        $('#alert_mobile').show().text('Kolom tidak boleh kosong');
                        $('#mobile').closest('div').removeClass('has-success');
                    }
                    if($('#home').val().length == 0){
                        $('#alert_home').show().text('Kolom tidak boleh kosong');
                        $('#home').closest('div').removeClass('has-success');
                    }
                    get_error_text(this);
                    valid = false;

                } 
                if ($(this).hasClass('no-valid')){
                    valid = false;
                    
                }
            });
            if ($('#image').hasClass('no-valid')){
              valid = false;
            }
            if (valid){
                      $('#submit').prop('disabled', true);
                      $('#reset').prop('disabled', true);
                      $('#icon').removeClass('fa fa-save');
                      $('#icon').addClass('fa fa-circle-o-notch fa-spin');
                      var form_data = new FormData($('#pemodal')[0]);
                      $.ajax({ 
                                  url: $('meta[name="base_url"]').attr('content')+"/pemodal/biodata", 
                                  type: "POST", 
                                  processData: false,
                                  contentType: false,
                                  async: false,
                                  cache: false,
                                  data : form_data,
                                  success: function(){                 
                                       // window.location= $('meta[name="base_url"]').attr('content')+"/redirect/biodata";
                                       toastr.options = {
                                              "closeButton": true,
                                              "debug": false,
                                              "newestOnTop": false,
                                              "progressBar": false,
                                              "positionClass": "toast-top-right",
                                              "preventDuplicates": false,
                                              "onclick": null,
                                              "showDuration": "300",
                                              "hideDuration": "1000",
                                              "timeOut": "5000",
                                              "extendedTimeOut": "1000",
                                              "showEasing": "swing",
                                              "hideEasing": "linear",
                                              "showMethod": "fadeIn",
                                              "hideMethod": "fadeOut"
                                              };

                                            toastr.success("Profil pengguna diperbaharui.", "Berhasil");
                                            $('#submit').prop('disabled', false);
                                            $('#reset').prop('disabled', false);
                                            $('#icon').removeClass('fa fa-circle-o-notch fa-spin');
                                            $('#icon').addClass('fa fa-save'); 
                                  }, 
                                  error: function (xhr, ajaxOptions, thrownError) { 
                                        toastr.options = {
                                              "closeButton": true,
                                              "debug": false,
                                              "newestOnTop": false,
                                              "progressBar": false,
                                              "positionClass": "toast-top-right",
                                              "preventDuplicates": false,
                                              "onclick": null,
                                              "showDuration": "300",
                                              "hideDuration": "1000",
                                              "timeOut": "5000",
                                              "extendedTimeOut": "1000",
                                              "showEasing": "swing",
                                              "hideEasing": "linear",
                                              "showMethod": "fadeIn",
                                              "hideMethod": "fadeOut"
                                              };

                                            toastr.error("Mohon cek koneksi", "Oops..!");
                                            $('#submit').prop('disabled', false);
                                            $('#reset').prop('disabled', false);
                                            $('#icon').removeClass('fa fa-circle-o-notch fa-spin');
                                            $('#icon').addClass('fa fa-save');
                                            
                                      } 
                      });  
            }
        });
    });
    
    //fungsi cek email
    function valid_email(email){
        var pola= new RegExp(/^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]+$/);
        return pola.test(email);
    }
    //fungsi cek phone 
    function valid_hp(hp){
        var pola = new RegExp(/^[0-9-+]+$/);
        return pola.test(hp);
    }
    function valid_home(home){
        var pola = new RegExp(/^[0-9-+]+$/);
        return pola.test(home);
    }
    //menerapkan gaya validasi form bootstrap saat terjadi eror
    function apply_feedback_error(textbox){
        $(textbox).addClass('no-valid'); //menambah class no valid
        $(textbox).parent().find('.text-danger').show();
        $(textbox).closest('div').removeClass('has-success');
        $(textbox).closest('div').addClass('has-error');
    }

    //untuk mendapat eror teks saat textbox kosong, digunakan saat submit form dan blur fungsi
    function get_error_text(textbox){
        $(textbox).parent().find('.text-danger').text("Kolom tidak boleh kosong."); 
        return apply_feedback_error(textbox); 
    } 
    //set Alamat From: peminjam ,author: Alip  
    function set_kota(source,target,placeholder) {
    $('#'+target).empty().append('<option value="">'+placeholder+'</option>');
    $.get($('meta[name="base_url"]').attr('content')+'/ajax/daftar/kota/'+$('#'+source).val(),function (data) {
        $.each(data, function (i, value) {
            $('#'+target).append('<option value="'+value.id+'">'+value.nama+'</option>');
        });
    });
    }
    function set_kecamatan(source,target,placeholder) {
        $('#'+target).empty().append('<option value="">'+placeholder+'</option>');
        $.get($('meta[name="base_url"]').attr('content')+'/ajax/daftar/kecamatan/'+$('#'+source).val(),function (data) {
            $.each(data, function (i, value) {
                $('#'+target).append('<option value="'+value.id+'">'+value.nama+'</option>');
            });
        });
    }
    function set_kelurahan(source,target,placeholder) {
        $('#'+target).empty().append('<option value="">'+placeholder+'</option>');
        $.get($('meta[name="base_url"]').attr('content')+'/ajax/daftar/kelurahan/'+$('#'+source).val(),function (data) {
            $.each(data, function (i, value) {
                $('#'+target).append('<option value="'+value.id+'">'+value.nama+'</option>');
            });
        });
    }
