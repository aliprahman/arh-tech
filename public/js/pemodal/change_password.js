
    $(document).ready(function(){
        //semua element dengan class text-danger akan di sembunyikan saat load
        $('.text-danger').hide();
        //untuk mengecek bahwa semua textbox tidak boleh kosong
        $('#change input').each(function(){ 
            $(this).blur(function(){ //blur function itu dijalankan saat element kehilangan fokus
                if (! $(this).val()){ //this mengacu pada text box yang sedang fokus
                    return get_error_text(this); //function get_error_text ada di bawah
                } else {
                    $(this).removeClass('no-valid'); 
                    $(this).parent().find('.text-danger').hide();//cari element dengan class has-error dari element induk text yang sedang focus
                    $(this).closest('div').removeClass('has-error');
                    $(this).closest('div').addClass('has-success');
                }
            });
        });

        //old_password
        $('#old').blur(function(){
            var password= $(this).val();
            var len= password.length;
            if (len == 0){
                $(this).parent().find('.text-danger').text("Kolom tidak boleh kosong.");
                return apply_feedback_error(this);
            }
            if (len>0 && len<3) {
                $(this).parent().find('.text-danger').text("password minimal 3 karakter");
                return apply_feedback_error(this);
                } else {
                    if(len>35) {
                        $(this).parent().find('.text-danger').text("password maksimal 35 karakter");
                        return apply_feedback_error(this);
                    } else {
                        $.ajax({
                           url: $('meta[name="base_url"]').attr('content')+"/old",
                           type: "POST",
                           data: {
                                    '_token': $('meta[name="_token"]').attr('content'),
                                    'password': password,
                                 },
                           dataType: "text",
                           success: function(data){
                                     if (data == 0){
                                         $('#old').parent().find('.text-danger').text("Password lama tidak sesuai");
                                         return apply_feedback_error('#old');
                                        }
                                    }
                            });
                        }
                } 
        });
        //mengecek password
        $('#new').blur(function(){ 
            var password=$(this).val();
            var len=password.length;
            if (len>0 && len<3) {
                $(this).parent().find('.text-danger').text("password minimal 3 karakter.");
                return apply_feedback_error(this);
            } else {
                if(len>35) {
                    $(this).parent().find('.text-danger').text("password maksimal 35 karakter.");
                    return apply_feedback_error(this);
                }
            }
        });
        //mengecek konfirmasi password
        $('#repeat').blur(function(){
            var pass = $("#new").val();
            var conf=$(this).val();
            var len=conf.length;
            if (len>0 && pass!==conf) {
                $(this).parent().find('.text-danger').text("konfirmasi password tidak cocok.");
                return apply_feedback_error(this);
            }
        });
         $('#reset').on('click', function(){
             $('.textbox2').closest('div').removeClass('has-success');
             $('.textbox2').closest('div').removeClass('has-error');
             $('.textbox2').parent().find('.text-danger').hide();
         });
         $('#change').submit(function(e){
            e.preventDefault();
            var valid=true;     
            $(this).find('.textbox2').each(function(){
                if (! $(this).val()){
                    get_error_text(this);
                    valid = false;
                    
                } 
                if ($(this).hasClass('no-valid')){
                    valid = false;
                    
                }
            });
            if (valid){
                        $.ajax({
                            url: $('meta[name="base_url"]').attr('content')+"/biodata/password",
                            type: "POST",
                            data: $('#change').serialize(), //serialize() untuk mengambil semua data di dalam form
                            success: function(){                
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                                };

                            toastr.success("Mengganti Password.","Berhasil");
                            $('#m_modal_1').modal('hide');
                            $('#old').val(null);
                            $('#new').val(null);
                            $('#repeat').val(null);
                            $('.textbox2').closest('div').removeClass('has-success');
                            },  
                            
                            error: function (xhr, ajaxOptions, thrownError) {
                                toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                                };

                            toastr.error("Mohon cek koneksi.", "Oops..!");
                            }
                });
            }
        });
    });
    //menerapkan gaya validasi form bootstrap saat terjadi eror
    function apply_feedback_error(textbox){
        $(textbox).addClass('no-valid'); //menambah class no valid
        $(textbox).parent().find('.text-danger').show();
        $(textbox).closest('div').removeClass('has-success');
        $(textbox).closest('div').addClass('has-error');
    }

    //untuk mendapat eror teks saat textbox kosong, digunakan saat submit form dan blur fungsi
    function get_error_text(textbox){
        $(textbox).parent().find('.text-danger').text("Kolom tidak boleh kosong.");
        return apply_feedback_error(textbox);
    }