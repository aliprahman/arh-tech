
    $(document).ready(function(){
        $('#data').DataTable();
        $('.select2').select2();
        //semua element dengan class text-danger akan di sembunyikan saat load
        $('.text-danger').hide();
        //untuk mengecek bahwa semua textbox tidak boleh kosong
        $('#pemodal input').each(function(){  
            $(this).blur(function(){ //blur function itu dijalankan saat element kehilangan fokus
                if (! $(this).val()){ //this mengacu pada text box yang sedang fokus
                    return get_error_text(this); //function get_error_text ada di bawah
                } else {
                    $(this).removeClass('no-valid'); 
                    $(this).parent().find('.text-danger').hide();//cari element dengan class has-danger dari element induk text yang sedang focus
                    $(this).find('.text-danger').hide();
                    $(this).closest('div').removeClass('has-danger');
                    $(this).closest('div').addClass('has-success');
                }
            });
        });
        $('#pemodal select').each(function(){  
            $(this).change(function(){ //blur function itu dijalankan saat element kehilangan fokus
                if (! $(this).val()){ //this mengacu pada text box yang sedang fokus
                    return get_error_text(this); //function get_error_text ada di bawah
                } else {
                    $(this).removeClass('no-valid'); 
                    $(this).parent().find('.text-danger').hide();//cari element dengan class has-danger dari element induk text yang sedang focus
                    $(this).find('.text-danger').hide();
                    $(this).closest('div').removeClass('has-danger');
                    $(this).closest('div').addClass('has-success');
                }
            });
        });
        // $('#pemodal select').each(function(){ 
        //     $(this).change(function(){ //blur function itu dijalankan saat element kehilangan fokus
        //         if (! $(this).val()){ //this mengacu pada text box yang sedang fokus
        //             return get_error_text(this); //function get_error_text ada di bawah
        //         } else {
        //             $(this).removeClass('no-valid'); 
        //             $(this).parent().find('.text-danger').hide();//cari element dengan class has-danger dari element induk text yang sedang focus
        //             $(this).closest('div').removeClass('has-danger');
        //             $(this).closest('div').addClass('has-success');
        //         }
        //     });
        // });

       
         $('#old').blur(function(){
            var password= $(this).val();
            var len= password.length;
            if (len == 0){
                $(this).parent().find('.text-danger').text("Kolom tidak boleh kosong.");
                return apply_feedback_error(this);
            }
            if (len>0 && len<3) {
                $(this).parent().find('.text-danger').text("password minimal 3 karakter");
                return apply_feedback_error(this);
                } else {
                    if(len>35) {
                        $(this).parent().find('.text-danger').text("password maksimal 35 karakter");
                        return apply_feedback_error(this);
                    } else {
                        $.ajax({
                           url: "{{ url('/old') }}",
                           type: "POST",
                           data: {
                                    '_token': '{{ csrf_token() }}',
                                    'password': password,
                                 },
                           dataType: "text",
                           success: function(data){
                                     if (data == 0){
                                                     $('#old').parent().find('.text-danger').text("Password lama tidak sesuai");
                                                     return apply_feedback_error('#old');
                                                    }
                                                   }
                            });
                        }
                } 
        });
         $('#email').blur(function(){
            var email= $(this).val();
            var len= email.length;
            if(len>0){ 
                if(!valid_email(email)){ 
                    $(this).parent().find('.text-danger').text("");
                    $(this).parent().find('.text-danger').text("Email tidak valid (ex: sample@domain.com)"); 
                    return apply_feedback_error(this);
                } else {
                    if (len>38){ 
                        $(this).parent().find('.text-danger').text("");
                        $(this).parent().find('.text-danger').text("maksimal 38 karakter");
                        return apply_feedback_error(this);
                    } else {
                        var valid = false;
                        $.ajax({
                                           url: "{{ url('/validasi/email') }}",
                                           type: "POST",
                                           data: {
                                                    '_token': '{{ csrf_token() }}',
                                                    'email': email,
                                                 },
                                           dataType: "text",
                                           success: function(data){
                                                     if (data==1){ //pada file check email.php, apabila email sudah ada di database makan akan mengembalikan nilai 0
                                                 $('#email').parent().find('.text-danger').text("");
                                 $('#email').parent().find('.text-danger').text("Email yang Anda masukan telah digunakan"); 
                                 return apply_feedback_error('#email');
                                                     }
                                                       }
                            });
                        }
                }
            } 
        });
        $('#mobile').blur(function(){
            var mobile= $(this).val();
            var len= mobile.length;
            if(len>0){ 
                    if (len>14){ 
                        $(this).parent().find('.text-danger').text("");
                        $(this).parent().find('.text-danger').text("maksimal 14 karakter");
                        return apply_feedback_error(this);
                    } else {
                        var valid = false;
                        $.ajax({
                                           url: "{{ url('/validasi/mobile') }}",
                                           type: "POST",
                                           data: {
                                                    '_token': '{{ csrf_token() }}',
                                                    'no_hp': mobile,
                                                 },
                                           dataType: "text",
                                           success: function(data){
                                                     if (data==1){ //pada file check mobile.php, apabila mobile sudah ada di database makan akan mengembalikan nilai 0
                                                 $('#mobile').parent().find('.text-danger').text("");
                                                 $('#mobile').parent().find('.text-danger').text("Nomor HP yang Anda masukan telah digunakan"); 
                                                 return apply_feedback_error('#mobile');
                                                     }
                                                       }
                            });
                        }
                
            } 
        });
        $('#ktp').blur(function(){
            var ktp= $(this).val();
            var len= ktp.length;
            if(len>0){ 
                    if (len>20){ 
                        $(this).parent().find('.text-danger').text("");
                        $(this).parent().find('.text-danger').text("maksimal 20 karakter");
                        return apply_feedback_error(this);
                    } else {
                        var valid = false;
                        $.ajax({
                                           url: "{{ url('/validasi/ktp') }}",
                                           type: "POST",
                                           data: {
                                                    '_token': '{{ csrf_token() }}',
                                                    'no_ktp': ktp,
                                                 },
                                           dataType: "text",
                                           success: function(data){
                                                     if (data==1){ //pada file check, apabila ktp sudah ada di database makan akan mengembalikan nilai 0
                                                 $('#ktp').parent().find('.text-danger').text("");
                                 $('#ktp').parent().find('.text-danger').text("ID KTP yang Anda masukan telah digunakan"); 
                                 return apply_feedback_error('#ktp');
                                                     }
                                                       }
                            });
                        }
                
            } 
        });
        $('#npwp').blur(function(){
            var npwp= $(this).val();
            var len= npwp.length;
            if(len>0){ 
                    if (len>20){ 
                        $(this).parent().find('.text-danger').text("");
                        $(this).parent().find('.text-danger').text("maksimal 20 karakter");
                        return apply_feedback_error(this);
                    } else {
                        var valid = false;
                        $.ajax({
                                           url: "{{ url('/validasi/npwp') }}",
                                           type: "POST",
                                           data: {
                                                    '_token': '{{ csrf_token() }}',
                                                    'no_npwp': npwp,
                                                 },
                                           dataType: "text",
                                           success: function(data){
                                                     if (data==1){ //pada file check , apabila npwp sudah ada di database makan akan mengembalikan nilai 0
                                                 $('#npwp').parent().find('.text-danger').text("");
                                 $('#npwp').parent().find('.text-danger').text("ID NPWP yang Anda masukan telah digunakan"); 
                                 return apply_feedback_error('#npwp');
                                                     }
                                                       }
                            });
                        }
                
            } 
        });
        //mengecek password
        $('#new').blur(function(){ 
            var password=$(this).val();
            var len=password.length;
            if (len>0 && len<8) {
                $(this).parent().find('.text-danger').text("Password minimal 8 karakter");
                return apply_feedback_error(this);
            } else {
                if(len>35) {
                    $(this).parent().find('.text-danger').text("Password maksimal 35 karakter");
                    return apply_feedback_error(this);
                }
            }
        });
        //mengecek konfirmasi password
        $('#repeat').blur(function(){
            var pass = $("#new").val();
            var conf=$(this).val();
            var len=conf.length;
            if (len>0 && pass!==conf) {
                $(this).parent().find('.text-danger').text("Konfirmasi password tidak cocok");
                return apply_feedback_error(this);
            }
        });
         $('#pemodal').submit(function(e){
            e.preventDefault();
            var valid=true;     
            $(this).find('.textbox').each(function(){
                if (! $(this).val()){
                    setTimeout(function(){ 
                        swal({ 
                          title:"Silahkan dilengkapi terlebih dahulu ", 
                          text: "", 
                          type: "info", 
                          confirmButtonClass: "btn green btn-circle", 
                        }); 
                      }, 2000);
                    get_error_text(this);
                    valid = false;

                } 
                if ($(this).hasClass('no-valid')){
                    valid = false;
                    
                }
            });
            if (valid){
                 swal({ 
                    title: "Apakah Anda yakin?", 
                    text: "Pastikan data yang Anda masukan benar.", 
                              type: "info", 
                              showCancelButton: true, 
                    confirmButtonClass: "btn green btn-circle", 
                    cancelButtonClass: "btn red btn-circle", 
                    confirmButtonText: "Yakin", 
                    cancelButtonText: "Batal", 
                    closeOnConfirm: false, 
                    closeOnCancel: false, 
                    reverseButtons: false,  
                  }, 
                  function(isConfirm) { 
                    if (isConfirm) { 
                      $.ajax({ 
                                  url: "{{ url('/register/pemodal') }}", 
                                  type: "POST", 
                                  data: $('#pemodal').serialize(), //serialize() untuk mengambil semua data di dalam form 
                                  dataType: "html", 
                                  success: function(){                 
                                      setTimeout(function(){ 
                                        swal({ 
                                          title:"Silahkan cek email untuk verifikasi ", 
                                          text: "Terimakasih, telah mendaftar", 
                                          type: "success", 
                                          confirmButtonClass: "btn green btn-circle", 
                                        }, function(){ 
                                          window.location="{{ url('/') }}"; 
                                        }); 
                                      }, 2000); 
                                  }, 
                              error: function (xhr, ajaxOptions, thrownError) { 
                                  setTimeout(function(){ 
                                      swal({ 
                                title: "Silahkan cek koneksi lalu ulangi !", 
                                type: "error", 
                                confirmButtonClass: "btn green btn-circle", 
                              }) 
                                    }, 2000);} 
                      }); 
                    } else { 
                      swal({ 
                        title: "Dibatalkan !", 
                        type: "error", 
                        confirmButtonClass: "btn green btn-outline btn-circle", 
                      }) 
                    } 
                  });
            }
        });
    });
    
    //fungsi cek email
    function valid_email(email){
        var pola= new RegExp(/^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]+$/);
        return pola.test(email);
    }
    //fungsi cek phone 
    function valid_hp(hp){
        var pola = new RegExp(/^[0-9-+]+$/);
        return pola.test(hp);
    }
    function valid_hp(home){
        var pola = new RegExp(/^[0-9-+]+$/);
        return pola.test(home);
    }
    //menerapkan gaya validasi form bootstrap saat terjadi eror
    function apply_feedback_error(textbox){
        $(textbox).addClass('no-valid'); //menambah class no valid
        $(textbox).parent().find('.text-danger').show();
        $(textbox).closest('div').removeClass('has-success');
        $(textbox).closest('div').addClass('has-danger');
    }

    //untuk mendapat eror teks saat textbox kosong, digunakan saat submit form dan blur fungsi
    function get_error_text(textbox){
        $(textbox).parent().find('.text-danger').text("Kolom tidak boleh kosong."); 
        return apply_feedback_error(textbox); 
    } 
    //set Alamat From: peminjam ,author: Alip  
      function set_kota(source,target,placeholder) { 
        $('#'+target).empty().append('<option value="">'+placeholder+'</option>'); 
        $.get('{{ url('ajax/daftar/kota') }}'+'/'+$('#'+source).val(),function (data) { 
            $.each(data, function (i, value) { 
                $('#'+target).append('<option value="'+value.id+'">'+value.nama+'</option>'); 
            }); 
        }); 
    } 
    function set_kecamatan(source,target,placeholder) { 
        $('#'+target).empty().append('<option value="">'+placeholder+'</option>'); 
        $.get('{{ url('ajax/daftar/kecamatan') }}'+'/'+$('#'+source).val(),function (data) { 
            $.each(data, function (i, value) { 
                $('#'+target).append('<option value="'+value.id+'">'+value.nama+'</option>'); 
            }); 
        }); 
    } 
    function set_kelurahan(source,target,placeholder) { 
        $('#'+target).empty().append('<option value="">'+placeholder+'</option>'); 
        $.get('{{ url('ajax/daftar/kelurahan') }}'+'/'+$('#'+source).val(),function (data) { 
            $.each(data, function (i, value) { 
                $('#'+target).append('<option value="'+value.id+'">'+value.nama+'</option>'); 
            }); 
        }); 
    } 