$(document).ready(function(){
	$('#data').DataTable();
  $('.select2').select2();
  $('.text-danger').hide();
  $('#peminjam select, #peminjam input').each(function(){
    $(this).blur(function(){ //blur function itu dijalankan saat element kehilangan fokus
      if (! $(this).val() && !$(this).hasClass('exlude-validation')){ //this mengacu pada text box yang sedang fokus
        return get_error_text(this); //function get_error_text ada di bawah
      } else {
        $(this).removeClass('no-valid');
        $(this).parent().find('.text-danger').hide();//cari element dengan class has-error dari element induk text yang sedang focus
        $(this).closest('div').removeClass('has-error');
        $(this).closest('div').addClass('has-success');
      }
    });
  });
  $('#email').blur(function(){
    $('#submit').prop('disabled', false);
		var email= $(this).val();
		var len= email.length;
		if(len>0){
			if(!valid_email(email)){
				$('#submit').prop('disabled', true);
				$(this).parent().find('.text-danger').text("");
				$(this).parent().find('.text-danger').text("Email tidak valid (ex: sample@domain.com)");
				return apply_feedback_error(this);
			} else {
				if (len>38){
					$('#submit').prop('disabled', true);
					$(this).parent().find('.text-danger').text("");
					$(this).parent().find('.text-danger').text("Maksimal 38 karakter");
					return apply_feedback_error(this);
				} else {
					var valid = false;
					$.ajax({
		        url: $('meta[name="base_url"]').attr('content')+"/validasi/email",
				    type: "POST",
				    data: {
					    '_token': $('meta[name="_token"]').attr('content'),
					    'email': email,
						},
		        dataType: "text",
		        success: function(data){
		          if (data==1){ //pada file check email.php, apabila email sudah ada di database makan akan mengembalikan nilai 0
		            $('#submit').prop('disabled', true);
		            $('#email').parent().find('.text-danger').text("");
							  $('#email').parent().find('.text-danger').text("Email yang Anda masukan telah digunakan");
							  return apply_feedback_error('#email');
		          }
			      }
				  });
			  }
		  }
		}
	});
  $('#mobile').blur(function(){
    $('.next').prop('disabled', false);
		var mobile= $(this).val();
		var len= mobile.length;
		if(len == 0){
			$('#alert_mobile').show().text('Kolom tidak boleh kosong');
			$('#mobile').closest('div').removeClass('has-success');
		}
		if(len>0){
			if(!valid_hp(mobile)){
			  $('.next').prop('disabled', true);
				$('#alert_mobile').show().text('No.HP tidak valid (ex: 08xxxxxxx)');
			  $('#mobile').closest('div').removeClass('has-success');
			}else{
				if (len>14){
				  $('.next').prop('disabled', true);
				  $('#alert_mobile').show().text('maksimal 14 digit');
				  $('#mobile').closest('div').removeClass('has-success');
				} else {
					var valid = false;
					$.ajax({
	          url: $('meta[name="base_url"]').attr('content')+"/validasi/mobile",
		        type: "POST",
		        data: {
							'_token': $('meta[name="_token"]').attr('content'),
							'no_hp': mobile,
						},
	          dataType: "text",
	          success: function(data){
	            if (data == 1){
	              $('.next').prop('disabled', true);
	              $('#alert_mobile').show().text('No.HP telah digunakan');
	              $('#mobile').closest('div').removeClass('has-success');
	            }else {
	              $('.next').prop('disabled', false);
	              $('#alert_mobile').hide();
	              $('#mobile').closest('div').addClass('has-success');
	            }
	          }
					});
				}
			}
		}
	});
	$('#home').blur(function(){
		var home= $(this).val();
		var len= home.length;
		if (len > 0) {
			if(!valid_home(home)){
				$('#alert_home').show().text('No.Tlp tidak valid (ex: 02xxxxxxx)');
				$('#home').closest('div').removeClass('has-success');
			}else {
				if (len>14){
					$('#alert_home').show().text('maksimal 14 digit');
					$('#home').closest('div').removeClass('has-success');
				}else {
					$('#alert_home').hide();
					$('#home').closest('div').addClass('has-success');
				}
			}
		}
	});
	$('#ktp').blur(function(){
		$('.next').prop('disabled', false);
		var ktp= $(this).val();
		var len= ktp.length;
		if(len>0){
			if (len>16){
				$('.next').prop('disabled', true);
				$(this).parent().find('.text-danger').text("");
				$(this).parent().find('.text-danger').text("maksimal 16 karakter");
				return apply_feedback_error(this);
			} else {
				var valid = false;
				$.ajax({
		      url: $('meta[name="base_url"]').attr('content')+"/validasi/ktp",
				  type: "POST",
				  data: {
					  '_token': $('meta[name="_token"]').attr('content'),
					  'no_ktp': ktp,
					},
		      dataType: "text",
		      success: function(data){
		        if (data==1){ //pada file check, apabila ktp sudah ada di database makan akan mengembalikan nilai 0
		          $('.next').prop('disabled', true);
		          $('#ktp').parent().find('.text-danger').text("");
							$('#ktp').parent().find('.text-danger').text("ID KTP yang Anda masukan telah digunakan");
							return apply_feedback_error('#ktp');
		        }
			    }
				});
			}
		}
	});
	$('#npwp').blur(function(){
		$('.next').prop('disabled', false);
		var npwp= $(this).val();
		var len= npwp.length;
		if(len>0){
			if (len>15){
			  $('.next').prop('disabled', true);
				$(this).parent().find('.text-danger').text("");
				$(this).parent().find('.text-danger').text("maksimal 15 karakter");
				return apply_feedback_error(this);
		  } else {
				var valid = false;
				$.ajax({
		      url: $('meta[name="base_url"]').attr('content')+"/validasi/npwp",
				  type: "POST",
				  data: {
					  '_token': $('meta[name="_token"]').attr('content'),
					  'no_npwp': npwp,
				  },
		      dataType: "text",
		      success: function(data){
		        if (data==1){ //pada file check , apabila npwp sudah ada di database makan akan mengembalikan nilai 0
		          $('.next').prop('disabled', true);
		          $('#npwp').parent().find('.text-danger').text("");
							$('#npwp').parent().find('.text-danger').text("ID NPWP yang Anda masukan telah digunakan");
							return apply_feedback_error('#npwp');
		        }
			    }
			  });
			}
		}
	});
	//mengecek password
  $('#new').blur(function(){
    var password=$(this).val();
    var len=password.length;
    if (len>0 && len<8) {
      $(this).parent().find('.text-danger').text("Password minimal 8 karakter");
      return apply_feedback_error(this);
    } else {
      if(len>35) {
        $(this).parent().find('.text-danger').text("Password maksimal 35 karakter");
        return apply_feedback_error(this);
      }
    }
  });
  //mengecek konfirmasi password
  $('#repeat').blur(function(){
    var pass = $("#new").val();
    var conf=$(this).val();
    var len=conf.length;
    if (len>0 && pass!==conf) {
      $(this).parent().find('.text-danger').text("Konfirmasi password tidak cocok");
      return apply_feedback_error(this);
    }
  });
  $('#tanggal_lahir').change(function(){
		var tanggal= $(this).val();
		var len= tanggal.length;
		if(len == 0){
			$('#alert_tanggal').show().text('Kolom tidak boleh kosong');
			$('#tanggal_lahir').closest('div').removeClass('has-success');
		}
		if(len >0){
			$('#alert_tanggal').hide();
			$('#tanggal_lahir').closest('div').removeClass('has-error');
			$('#tanggal_lahir').closest('div').addClass('has-success');
			$('#tanggal_lahir').removeClass('no-valid');
		}
	});
	$('input[name="jenis_kelamin"]').change(function(){
		$('#alert_k').hide();
		$('#alert_k').removeClass('no-valid');
	});
	$('input[name="setuju"]').change(function(){
		$('#alert_s').hide();
		$('#alert_s').removeClass('no-valid');
	});
	$('#submit').click(function(){
  	valid = true;
  	$('#step3').find('input, select').blur();
  	$('#step3').find('input, select').each(function() {
			if($(this).val() === "") {
				valid = false;
			}
		});
    if ($('#captcha').hasClass('no-valid')){
      valid = false;
    }
    if ($('#alert_k').hasClass('no-valid')){
      valid = false;
    }
    if ($('#alert_s').hasClass('no-valid')){
      valid = false;
    }
		if($('#setuju').prop('checked')==false) {
			$('#alert_s').show().text('Silahkan klik menyetujui.');
			$('#alert_s').addClass('no-valid');
			valid = false;
		}else {
		  $('#alert_s').hide();
		  $('#alert_s').removeClass('no-valid');
			valid = true;
		}
    //captcha
	  if(grecaptcha.getResponse() != null){
	    $.ajax({
		    url: $('meta[name="base_url"]').attr('content')+"/validasi/captcha",
		    type: "POST",
		    data : {
		      '_token': $('meta[name="_token"]').attr('content'),
					'captcha': grecaptcha.getResponse(),
		    },
		    dataType: "text",
		    success: function(data){
		      if (data==1){
		        grecaptcha.reset();
		        $('#alert').show().text("Silahkan Lengkapi Captcha");
		        $('#captcha').addClass('no-valid');
		        valid = false;
		      }else {
		        valid = true;
			      $('#alert').hide();
			      $('#captcha').removeClass('no-valid');
			      if(valid) {
			       	$('#submit2').click();
			      }
					}
		    }
		  });
	  }
  });
});

	//fungsi cek email
	function valid_email(email){
		var pola= new RegExp(/^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]+$/);
		return pola.test(email);
	}
	//fungsi cek phone
	function valid_hp(hp){
		var pola = new RegExp(/^[0-9-+]+$/);
		return pola.test(hp);
	}
	function valid_home(home){
		var pola = new RegExp(/^[0-9-+]+$/);
		return pola.test(home);
	}
    //menerapkan gaya validasi form bootstrap saat terjadi eror
    function apply_feedback_error(textbox){
        $(textbox).addClass('no-valid'); //menambah class no valid
        $(textbox).parent().find('.text-danger').show();
        $(textbox).closest('div').removeClass('has-success');
        $(textbox).closest('div').addClass('has-error');
    }

    //untuk mendapat eror teks saat textbox kosong, digunakan saat submit form dan blur fungsi
    function get_error_text(textbox){
        $(textbox).parent().find('.text-danger').text("Kolom tidak boleh kosong.");
        return apply_feedback_error(textbox);
    }
    //set Alamat From: peminjam ,author: Alip
    function set_kota(source,target,placeholder) {
    $('#'+target).empty().append('<option value="">'+placeholder+'</option>');
    $.get($('meta[name="base_url"]').attr('content')+'/ajax/daftar/kota/'+$('#'+source).val(),function (data) {
        $.each(data, function (i, value) {
            $('#'+target).append('<option value="'+value.id+'">'+value.nama+'</option>');
        });
    });
	}
	function set_kecamatan(source,target,placeholder) {
	    $('#'+target).empty().append('<option value="">'+placeholder+'</option>');
	    $.get($('meta[name="base_url"]').attr('content')+'/ajax/daftar/kecamatan/'+$('#'+source).val(),function (data) {
	        $.each(data, function (i, value) {
	            $('#'+target).append('<option value="'+value.id+'">'+value.nama+'</option>');
	        });
	    });
	}
	function set_kelurahan(source,target,placeholder) {
	    $('#'+target).empty().append('<option value="">'+placeholder+'</option>');
	    $.get($('meta[name="base_url"]').attr('content')+'/ajax/daftar/kelurahan/'+$('#'+source).val(),function (data) {
	        $.each(data, function (i, value) {
	            $('#'+target).append('<option value="'+value.id+'">'+value.nama+'</option>');
	        });
	    });
	}
  function success(){
    $('#alert').hide();
    $('#captcha').removeClass('no-valid');
  }
