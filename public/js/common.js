function set_kota(source,target,placeholder) {
    $('#'+target).empty().append('<option value="">'+placeholder+'</option>');
    $.get($('meta[name="base_url"]').attr('content')+'/ajax/daftar/kota/'+$('#'+source).val(),function (data) {
        $.each(data, function (i, value) {
            $('#'+target).append('<option value="'+value.id+'">'+value.nama+'</option>');
        });
    });
}
function set_kecamatan(source,target,placeholder) {
    $('#'+target).empty().append('<option value="">'+placeholder+'</option>');
    $.get($('meta[name="base_url"]').attr('content')+'/ajax/daftar/kecamatan/'+$('#'+source).val(),function (data) {
        $.each(data, function (i, value) {
            $('#'+target).append('<option value="'+value.id+'">'+value.nama+'</option>');
        });
    });
}
function set_kelurahan(source,target,placeholder) {
    $('#'+target).empty().append('<option value="">'+placeholder+'</option>');
    $.get($('meta[name="base_url"]').attr('content')+'/ajax/daftar/kelurahan/'+$('#'+source).val(),function (data) {
        $.each(data, function (i, value) {
            $('#'+target).append('<option value="'+value.id+'">'+value.nama+'</option>');
        });
    });
}
function add_pengalaman_kerja() {
  var aValue = "";
  $('input[name="sampai_tahun[]"]').each(function() { aValue = $(this).val(); });
    var html = '<div class="form-group">'+
                  '<label class="control-label col-md-4">Tahun Mengajar</label>'+
                  '<div class="col-md-4">'+
                      '<input type="number" min="0" class="form-control" name="dari_tahun[]" value="'+aValue+'" required>'+
                  '</div>'+
                  '<div class="col-md-4">'+
                      '<input type="number" min="0" class="form-control" name="sampai_tahun[]" value="'+new Date().getFullYear()+'" required>'+
                  '</div>'+
              '</div>'+
              '<div class="form-group">'+
                  '<label class="control-label col-md-4">Tempat Mengajar</label>'+
                  '<div class="col-md-8">'+
                      '<input type="text" class="form-control" name="tempat_mengajar[]" value="" required>'+
                  '</div>'+
              '</div>';

    $('#pengalaman_kerja').append(html);
}
