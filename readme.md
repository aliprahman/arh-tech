# Finnesia #

Safe Funding For Teachers And Lecturers.

### What is this repository for? ###

* This repo is for control source code Finnesia System.
* Version: Alpha 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

#### Setup ####
1. Clone this repo.
2. Run `composer install`.

#### Configuration ####
Copy file .env.example to file .env. and configure setting-setting which you needs.

To simplify the development of email features, you should use http://mailtrap.io.

#### Minimum Requirement ####
* PHP 7.1.7
* MariaDB 10.1.25

#### Database configuration ####
1. Import file `database/finnesia.sql` to database
2. Run `php artisan migrate:refresh --seed`

### Contribution guidelines ###
* If there are any changes to the database, make sure you create its migration and seeder. Do the test by run `php artisan migrate:refresh --seed` for making sure its migration and seeder could run normal without any errors.
