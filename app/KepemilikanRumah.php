<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KepemilikanRumah extends Model
{
    use SoftDeletes;

    protected $table = "m_kepemilikan_rumah";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function peminjam()
    {
        return $this->hasMany('App\Peminjam','kepemilikan_rumah_id');
    }
}
