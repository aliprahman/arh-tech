<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Alamat extends Model
{
    use SoftDeletes;

    protected $table = "alamat";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'alamat', 'kode_pos', 'province_id', 'regency_id', 'district_id', 'village_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function provinsi()
    {
        return $this->belongsTo('App\Provinsi','province_id');
    }

    public function kota()
    {
        return $this->belongsTo('App\Kota','regency_id');
    }

    public function kecamatan()
    {
        return $this->belongsTo('App\Kecamatan','district_id');
    }

    public function desa()
    {
        return $this->belongsTo('App\Desa','village_id');
    }

    public function peminjam()
    {
        return $this->hasOne('App\Peminjam','alamat_id');
    }

    //pemodal
    public function pemodal() 
    { 
        return $this->hasOne('App\Pemodal','alamat_id'); 
    } 
}
