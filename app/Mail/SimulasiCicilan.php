<?php

namespace App\Mail;
use App\Pinjaman;
use App\Traits\Common;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SimulasiCicilan extends Mailable
{
    use Queueable, SerializesModels, Common;

    public $pinjaman;

    public $id;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Pinjaman $pinjaman)
    {
        $this->pinjaman = $pinjaman;
        $this->id = $this->encrypt_id($pinjaman->id);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      return $this->from('no_reply@finnesia.com')
      ->subject('Finnesia - Simulasi Cicilan')
      ->view('mails.simulasi_cicilan');
    }
}
