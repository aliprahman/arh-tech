<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifPengajuan extends Mailable
{
    use Queueable, SerializesModels;

    public $pinjaman;

    public $peminjam;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($pinjaman, $peminjam)
    {
        $this->pinjaman = $pinjaman;
        $this->peminjam = $peminjam;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.notif_pengajuan_pinjaman');
    }
}
