<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Transaksi;
use App\WithDraw;
use App\User;

class RequestWithDraw extends Mailable
{
    use Queueable, SerializesModels;

    public $withdraw;

    public $pemodal;

    public $transaksi;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(WithDraw $withdraw, User $user, Transaksi $transaksi)
    {
        $this->withdraw = $withdraw;
        $this->pemodal = $user;
        $this->transaksi = $transaksi;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no_reply@finnesia.com')
        ->subject('Finnesia - Request WithDraw')
        ->view('mails.request_withdraw');
    }
}
