<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Collection;
use App\Pinjaman;
use App\User;

class EmailPinjamanBaru extends Mailable
{
    use Queueable, SerializesModels;

    public $pinjaman_baru;

    public $pinjaman;

    public $pemodal;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $pemodal, Collection $pinjaman, Pinjaman $pinjaman_baru = null)
    {
        $this->pinjaman = $pinjaman;
        $this->pemodal = $pemodal;
        $this->pinjaman_baru = $pinjaman_baru;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.pinjaman_aktif');
    }
}
