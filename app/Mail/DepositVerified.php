<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Deposit;
use App\Pemodal;

class DepositVerified extends Mailable
{
    use Queueable, SerializesModels;

    public $pemodal;

    public $deposit;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Deposit $deposit, Pemodal $pemodal)
    {
        $this->deposit = $deposit;
        $this->pemodal = $pemodal;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      return $this->from('no_reply@finnesia.com')
      ->subject('Finnesia - Deposit Update')
      ->view('mails.deposit_verified');
    }
}
