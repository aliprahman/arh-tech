<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PengalamanKerja extends Model
{
    use SoftDeletes;

    protected $table = "pengalaman_kerja";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'peminjam_id', 'dari_tahun', 'sampai_tahun', 'tempat_kerja'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function peminjam()
    {
        return $this->belongsTo('App\Peminjam','peminjam_id');
    }

}
