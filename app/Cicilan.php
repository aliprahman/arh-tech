<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cicilan extends Model
{
    use SoftDeletes;

    protected $table = "cicilan";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pinjaman_id', 'transaksi_id', 'tanggal', 'jumlah', 'status'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function pinjaman()
    {
        return $this->belongsTo('App\Pinjaman','pinjaman_id');
    }

    public function transaksi()
    {
        return $this->belongsTo('App\Transaksi','transaksi_id');
    }
}
