<?php
namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Resource extends Model {
	
	protected $table = 'acl_resource';
	
	protected $guarded = array();

	public static $rules = array(
		'name'		=> 'required|unique',
		'type'		=> 'required|in:action,closure,other',
		'parent'	=> 'exists:acl_resources,id'
	);
	
	public function children()
	{
		return $this->hasMany('App\Resource', 'parent');
	}

	public function parent()
	{
		return $this->belongsTo('App\Resource', 'parent');
	}
	
	public function scopeOrderByName($query)
	{
		return $query->orderBy('name');
	}
	
	public function scopeRoots($query)
	{
		return $query->orderByName()
				->where('parent', null);
	}
	
	public function scopeOtherTypes($query)
	{
		return $query->orderByName()
				->where('type', '!=', 'closure')
				->where('type', '!=', 'action');
	}
	
	public function scopeExcept($query, $id)
	{
		return $query->otherTypes()
				->where('id', '!=', $id);
	}
}
