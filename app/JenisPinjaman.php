<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JenisPinjaman extends Model
{
    use SoftDeletes;

    protected $table = "m_jenis_pinjaman";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function pinjaman()
    {
        return $this->hasMany('App\Pinjaman','jenis_pinjaman_id');
    }
}
