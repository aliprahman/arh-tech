<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Biodata extends Model
{
    use SoftDeletes;

    protected $table = "biodata";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tempat_lahir', 'tanggal_lahir', 'jenis_kelamin', 'no_telepon',
        'no_hp', 'no_ktp', 'no_npwp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->hasOne('App\User','biodata_id');
    }

    public function setTanggalLahirAttribute($value)
    {
        $this->attributes['tanggal_lahir'] = date_format(date_create($value),'Y-m-d');
    }

    public function getTanggalLahirAttribute($value)
    {
        return date_format(date_create($value),'d-m-Y');
    }
}
