<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KeuanganPeminjam extends Model
{
    use SoftDeletes;

    protected $table = "keuangan_peminjam";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'peminjam_id', 'keterangan', 'jumlah', 'type'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function peminjam()
    {
        return $this->belongsTo('App\Peminjam','peminjam_id');
    }
}
