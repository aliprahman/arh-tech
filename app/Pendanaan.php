<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pendanaan extends Model
{
    use SoftDeletes;

    protected $table = "funding";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pinjaman_id', 'pemodal_id', 'jumlah', 'tanggal'
    ];

    protected $dates = ['deleted_at'];

    public function pinjaman()
    {
        return $this->belongsTo('App\Pinjaman','pinjaman_id');
    }

    public function pemodal()
    {
        return $this->belongsTo('App\Pemodal','pemodal_id');
    }
}
