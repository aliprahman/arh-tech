<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pemodal extends Model
{
    //
    use SoftDeletes;

    protected $table = "pemodal";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'type', 'alamat_id', 'akun_bank_id', 'perusahaan_id', 'saldo', 'status'
    ];

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function alamat()
    {
        return $this->belongsTo('App\Alamat','alamat_id');
    }

    public function akun_bank()
    {
        return $this->belongsTo('App\AkunBank','akun_bank_id');
    }

    public function perusahaan()
    {
        return $this->belongsTo('App\Perusahaan','perusahaan_id');
    }

    public function pendanaan()
    {
        return $this->hasMany('App\Pendanaan','pemodal_id');
    }

    public function deposit()
    {
    	return $this->hasMany('App\Deposit','pemodal_id');
    }
    public function with_draw()
    {
    	return $this->hasMany('App\WithDraw','pemodal_id');
    }

}
