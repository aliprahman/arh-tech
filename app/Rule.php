<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rule extends Model {
	
	protected $table = 'acl_rule';
	public $timestamps = false;
	protected $guarded = array();

	public function children()
	{
		return $this->belongsToMany('Role', 'acl_role_parents', 'parent_id', 'role_id')->withPivot('order');
	}

	public function parents()
	{
		return $this->belongsToMany('Role', 'acl_role_parents', 'role_id', 'parent_id')->withPivot('order');
	}
}
