<?php

namespace App\Http\Controllers;

use App\Kota;
use App\Desa;
use App\Provinsi;
use App\Kecamatan;
use App\Bank;
use App\AkunBank;
use App\JenisPinjaman;
use App\Jabatan;
use Illuminate\Http\Request AS request_http;
use Illuminate\Support\Facades\Request;

class Ajax extends Controller
{
    public function set_list_provinsi()
    {
        $provinsi = Provinsi::all();

        if(Request::ajax()){
            return response()->json($provinsi);
        }else{
            return $provinsi;
        }
    }

    public function set_list_kota($id_provinsi)
    {
        $provinsi = Provinsi::find($id_provinsi);
        $list_kota = array();
        foreach ($provinsi->kota as $item) {
            $list_kota[] = array(
                'id' => $item->id,
                'nama' => $item->name
            );
        }

        if(Request::ajax()){
            return response()->json($list_kota);
        }else{
            return $list_kota;
        }
    }

    public function set_list_kecamatan($id_kota)
    {
        $kota = Kota::find($id_kota);
        $list_kecamatan = array();
        foreach ($kota->kecamatan as $item) {
            $list_kecamatan[] = array(
              'id' => $item->id,
              'nama' => $item->name
            );
        }

        if(Request::ajax()){
            return response()->json($list_kecamatan);
        }else{
            return $list_kecamatan;
        }
    }

    public function set_list_kelurahan($id_kecamatan)
    {
        $kecamatan = Kecamatan::find($id_kecamatan);
        $list_kelurahan = array();
        foreach ($kecamatan->desa as $item) {
            $list_kelurahan[] = array(
              'id' => $item->id,
              'nama' => $item->name
            );
        }

        if(Request::ajax()){
            return response()->json($list_kelurahan);
        }else{
            return $list_kelurahan;
        }
    }

    public function set_list_bank(request_http $request)
    {
        if(isset($request->is_admin)) {
            $bank = AkunBank::select('m_bank.nama','akun_bank.id')->where('is_akun_admin',1)
                                ->join('m_bank','akun_bank.bank_id','=','m_bank.id')->get();
        } else {
            $bank = Bank::all();
        }

        if(Request::ajax()){
            return response()->json($request->is_admin);
        }else{
            return $bank;
        }
    }

    public function set_list_jenis_pinjaman()
    {
        $jp = JenisPinjaman::all();

        if(Request::ajax()){
            return response()->json($jp);
        }else{
            return $jp;
        }
    }

    public function set_list_jabatan()
    {
        $jabatan = Jabatan::all();

        if(Request::ajax()){
            return response()->json($jabatan);
        }else{
            return $jabatan;
        }
    }
}
