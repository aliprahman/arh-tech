<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
	 * Save URL Referer to Session.
	 * 
	 */
    public function saveReferer()
	{
		if(Session::has('errors')) return;
		Session::put('url.referer', Request()->header('referer'));
	}
	
	/**
	 * Get URL Referer Redirect if exists.
	 * 
	 * @param \Illuminate\Http\RedirectResponse $redirect
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function redirectReferer($redirect)
	{
		if (Session::has('url.referer')) 
		{
			$redirect = Redirect::to(Session::get('url.referer'));
			Session::forget('url.referer');
		}
		return $redirect;
	}
}
