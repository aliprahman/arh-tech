<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use App\Biodata;
use App\Alamat;
use App\Bank;
use App\AkunBank;
use App\Peminjam;
use App\Pemodal;
use App\Perusahaan;
use App\FileUploads;
use App\KeuanganPeminjam;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    # override
    protected function attemptLogin(Request $request)
    {
        return $this->guard('api')->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    # override
    public function login(Request $request)
    {
        $this->validateLogin($request);

        if ($this->attemptLogin($request)) {
            $user = $this->guard('api')->user();
            $user->generateToken();

            return response()->json([
                'data' => $user,
            ], 200);
        }

        return $this->sendFailedLoginResponse($request);
    }

    # override
    public function logout(Request $request) 
    {
        $user = \Auth::guard('api')->user();

        if(!is_null($user)) {
            $user->api_token = null;
            $user->save();

            return response()->json(['status' => 'success', 'message' => 'User has been logged out', 'data' => $user], 200);
        }

        return response()->json(['status' => 'error', 'message' => 'No User logged in', 'data' => $user], 200);
    }

    public function profile(Request $request)
    {
        $user = \Auth::guard('api')->user();
        if(!is_null($user)) {
            $data_user = User::find($user->id);
            $data_user->biodata = Biodata::find($user->biodata_id);
            $data_user->photo_profile = FileUploads::find($user->photo_profile_id);
            if($data_user->photo_profile)
                $data_user->photo_profile->name = asset('storage/profile_photo/'.$data_user->photo_profile->name);
            
            switch ($data_user->type) {
                case 'peminjam':
                    $data_user->biodata->alamat = Alamat::find($data_user->peminjam->alamat_id);
                    $data_user->akun_bank = AkunBank::find($data_user->peminjam->akun_bank_id);
                    $data_user->akun_bank->bank = Bank::find($data_user->akun_bank->bank_id);
                    $data_user->peminjam->alamat_sekolah = Alamat::find($data_user->peminjam->alamat_tempat_kerja_id);
                    $data_user->pendapatan_lainnya = KeuanganPeminjam::where('peminjam_id', $data_user->peminjam->id)
                        ->where('type','pendapatan')->get();
                    $data_user->pinjaman_lainnya = KeuanganPeminjam::where('peminjam_id', $data_user->peminjam->id)
                        ->where('type','pinjaman')->get();

                    // List Upload
                    $data_user->photo_tempat_tinggal = FileUploads::find($user->peminjam->photo_tempat_tinggal_id);
                    if($data_user->photo_tempat_tinggal)
                        $data_user->photo_tempat_tinggal->name = asset('storage/tempat_tinggal/'.$data_user->photo_tempat_tinggal->name);

                    $data_user->photo_curiculum_vitae = FileUploads::find($user->peminjam->photo_curiculum_vitae_id);
                    if($data_user->photo_curiculum_vitae)
                        $data_user->photo_curiculum_vitae->name = asset('storage/curiculum_vitae/'.$data_user->photo_curiculum_vitae->name);

                    $data_user->photo_tempat_kerja = FileUploads::find($user->peminjam->photo_tempat_kerja_id);
                    if($data_user->photo_tempat_kerja)
                    $data_user->photo_tempat_kerja->name = asset('storage/tempat_kerja/'.$data_user->photo_tempat_kerja->name);
                    break;
                case 'pemodal':
                    $data_user->biodata->alamat = Alamat::find($data_user->pemodal->alamat_id);
                    $data_user->pemodal->perusahaan = Perusahaan::find($data_user->pemodal->perusahaan_id);
                    $data_user->akun_bank = AkunBank::find($data_user->pemodal->akun_bank_id);
                    $data_user->akun_bank->bank = Bank::find($data_user->akun_bank->bank_id);
                    break;
                default:
                    break;
            }
            return response()->json(['status' => 'success', 'message' => 'Success', 'data' => $data_user], 200);
        }                

        return response()->json(['status' => 'error', 'message' => 'No User logged in', 'data' => $user], 200);
    }
}
