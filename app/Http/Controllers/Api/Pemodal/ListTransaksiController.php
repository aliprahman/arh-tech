<?php

namespace App\Http\Controllers\Api\Pemodal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Deposit;
use App\WithDraw;
use App\Pendanaan;
use App\Transaksi;
use App\User;

class ListTransaksiController extends Controller
{
    public function index()
    {
      if(!is_null(\Auth::guard('api')->user())) {
        $user = User::find(\Auth::guard('api')->user()->id);

        // query history
        $sql = "(".$this->_getData($user->pemodal->id, "deposit").")";
        $sql .= " UNION ALL ";
        $sql .= "(".$this->_getData($user->pemodal->id, "penarikan").")";
        $sql .= " UNION ALL ";
        $sql .= "(".$this->_getDataPendanaan($user->pemodal->id).")";
        $history = DB::select($sql);
        return response()->json(['status' => 'success', 'data' => $history], 200);
      }
      return response()->json(['status' => 'error', 'message' => 'No User logged in'], 200);
    }


    private function _getData($pemodal_id, $mode = "deposit") {
      $tableMode = "deposit";
      $statusMode = "Kredit";
      switch ($mode) {
        case 'penarikan':
          $tableMode = "withdraw";
          $statusMode = "Debit";
          break;
      }

      $sql = "SELECT ";
      $sql .= "transaksi.kode_transaksi, ";
      $sql .= "transaksi.id, ";
      $sql .= "transaksi.ref, ";
      $sql .= "transaksi.deskripsi, ";
      $sql .= "transaksi.jumlah, ";
      $sql .= "transaksi.created_at, ";
      $sql .= $tableMode.".status, ";
      $sql .= "'".$statusMode."' AS status_transaksi ";
      $sql .= "FROM  ";
      $sql .= "transaksi ";
      $sql .= "LEFT JOIN {$tableMode} ON {$tableMode}.transaksi_id = transaksi.id ";
      $sql .= "WHERE transaksi.deleted_at IS NULL ";
      $sql .= "AND {$tableMode}.pemodal_id = {$pemodal_id} ";
      $sql .= "ORDER BY transaksi.created_at DESC ";
      return $sql;
    }

    private function _getDataPendanaan($pemodal_id) {
      $sql = "SELECT ";
      $sql .= "'01' AS kode_transaksi, ";
      $sql .= "id, ";
      $sql .= "'' AS ref, ";
      $sql .= "'' AS deskripsi, ";
      $sql .= "jumlah, ";
      $sql .= "created_at, ";
      $sql .= "'' AS status, ";
      $sql .= "'Kredit' AS status_transaksi ";
      $sql .= "FROM  ";
      $sql .= "funding ";
      $sql .= "WHERE deleted_at IS NULL ";
      $sql .= "AND pemodal_id = {$pemodal_id} ";
      $sql .= "ORDER BY created_at DESC ";      
      return $sql;
    }
}
