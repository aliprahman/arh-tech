<?php
	namespace App\Http\Controllers\Api\Pemodal;

	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Validator;
	use App\Traits\Common;
	use App\WithDraw;
	use App\Transaksi;
	use App\User;
	
	class PenarikanController extends Controller
	{
		use Common;
		public function index()
		{
        	if(!is_null(\Auth::guard('api')->user())) {
				$user = User::find(\Auth::guard('api')->user()->id);
				$withdraw = WithDraw::select('id','transaksi_id','tanggal','jumlah', 'status','created_at')
						            ->where('pemodal_id', $user->pemodal->id)
						            ->orderBy('created_at','desc')
						            ->get();

	            if($withdraw) {
	            	foreach ($withdraw as $row) {
	            		$row->no_transaksi = $row->transaksi->kode_transaksi.".".(!is_null($row->transaksi->ref) ? $row->transaksi->ref : $row->transaksi->id);
	            		$row->nama_bank = $row->transaksi->akun_bank_nama_bank;
	            		$row->no_rekening = $row->transaksi->akun_bank->no_akun_bank;
	            		if($row->status == "unverified"){
			                $row->status = 'VERIFIKASI';
			            } else {
			                $row->status = 'TRANSFER';
			            }
			            unset($row->transaksi);
	            	}
	            	return response()->json(['status' => 'success', 'data' => $withdraw], 200);
	            }
	            return response()->json(['status' => 'error', 'message' => 'No data available'], 200);
	        }
	        return response()->json(['status' => 'error', 'message' => 'No User logged in'], 200);
		}

		public function order(Request $request) {
			if(!is_null(\Auth::guard('api')->user())) {
				$user = User::find(\Auth::guard('api')->user()->id);
				$validator = Validator::make($request->all(),
		          [
		            'jumlah' => 'required',
		          ],
		          [
		            'jumlah.required' => 'Nominal dibutuhkan!',
		          ]
		        );

		        if($validator->fails()) {
		            return response()->json(['status' => 'error', 'message' => $validator->messages()->first(), 200]);
		        }

		        $int = $this->rupiah_to_int($request->jumlah);

		        $number        = "0000";
			    $date          = date('ym');
			    $count         = Transaksi::where('id','LIKE','%'.$date.'%')->count();
			    $transaksi_id     = $date.$number + $count + 1;

		    	$transaksi = new Transaksi;
		        $transaksi->id = $transaksi_id;
		        $transaksi->ref = $transaksi_id;
		        $transaksi->kode_transaksi = "03";
		        $transaksi->jumlah = $int;
		        $transaksi->tanggal = date('Y-m-d H:i:s');
		        $transaksi->akun_bank_id= $user->pemodal->akun_bank->bank_id;
		        $transaksi->akun_bank_no_akun= $user->pemodal->akun_bank->no_akun_bank;
		        $transaksi->akun_bank_nama_akun= $user->pemodal->akun_bank->nama_akun_bank;
		        $transaksi->akun_bank_nama_bank= $user->pemodal->akun_bank->bank->nama;
		        $transaksi->deskripsi= $request->keterangan;
		    	$transaksi->save();

		    	$withdraw = new WithDraw;
		        $withdraw->pemodal_id = $user->pemodal->id;
		        $withdraw->transaksi_id = $transaksi->id;
		        $withdraw->tanggal = date('Y-m-d H:i:s');
		        $withdraw->status = 'unverified';
		        $withdraw->jumlah = $int;
		        $withdraw->save();

		        return response()->json(['status' => 'success', 'message' => 'Penarikan berhasil dilakukan, Silahkan tunggu verifikasi'], 200);
		    }
		    return response()->json(['status' => 'error', 'message' => 'No User logged in'], 200);
		}
	}

?>