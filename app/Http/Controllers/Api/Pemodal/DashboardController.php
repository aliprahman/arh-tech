<?php
	namespace App\Http\Controllers\Api\Pemodal;

	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Carbon\Carbon;
	use App\Pinjaman;
	use App\Peminjam;
	use App\Pendanaan;
	use App\Pendapatan;
	use App\Deposit;
	use App\WithDraw;
	use App\Traits\Common;

	class DashboardController extends Controller
	{
		use common; 
		public function index(Request $request)
		{
			$user = \Auth::guard('api')->user();
        	if(!is_null($user)) {
        		$data = new \stdClass();
        		$data->pendanaan = Pendanaan::where('pemodal_id', $user->pemodal->id)->sum('jumlah');
        		$data->pendapatan = Pendapatan::join('funding','pendapatan.funding_id','=','funding.id')->where('pemodal_id', $user->pemodal->id)->sum('pendapatan.jumlah');
				$data->dana_tersedia = $user->pemodal->saldo;
				$data->total_nilai_akun = $data->pendanaan + $data->pendapatan + $data->dana_tersedia;

				$start = $month = strtotime(date('Y').'-01-01');
		        $end = strtotime(date('Y').'-12-01');
		        $aYearlyIncome = array();
		        $i = 0;
		        while($month <= $end)
		        {
		        	$oYI = new \stdClass();
		        	$oYI->bulan = $this->month_number_to_text($i);
		            $oYI->jumlah = (int)Pendapatan::join('funding','pendapatan.funding_id','=','funding.id')
		                  	->where('pemodal_id', $user->pemodal->id)
		                  	->whereMonth('pendapatan.created_at', date('m', $month))
		                  	->whereYear('pendapatan.created_at', date('Y'))
		                  	->sum('pendapatan.jumlah');
		        	$aYearlyIncome[] = $oYI;

		        	$month = strtotime("+1 month", $month);
		        	$i++;
		        }
		        $data->pendapatan_tahunan = $aYearlyIncome;

				$data->deposit_terakhir = Deposit::join('transaksi','deposit.transaksi_id','=','transaksi.id')
		          ->where('deposit.pemodal_id', $user->pemodal->id)
		          ->whereMonth('deposit.tanggal', date('m'))
		          ->whereYear('deposit.tanggal', date('Y'))
		          ->where('deposit.status','verified')
		          ->sum('transaksi.jumlah');

		        $data->pendanaan_terakhir = Pendanaan::where('pemodal_id', $user->pemodal->id)
		          ->whereMonth('tanggal',date('m'))
		          ->whereYear('tanggal',date('Y'))
		          ->sum('jumlah');

		        $data->penarikan_terakhir = WithDraw::where('pemodal_id', $user->pemodal->id)
		          ->whereMonth('tanggal', date('m'))
		          ->whereYear('tanggal', date('Y'))
		          ->where('status','verified')
		          ->sum('jumlah');

				return response()->json(['status' => 'success', 'data' => $data], 200);
	        }
	        return response()->json(['status' => 'error', 'message' => 'No User logged in'], 200);
		}
	}

?>