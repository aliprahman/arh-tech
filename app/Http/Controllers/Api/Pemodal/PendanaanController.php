<?php

namespace App\Http\Controllers\Api\Pemodal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Pendanaan;
use App\Pemodal;
use App\Pinjaman;
use App\JenisPinjaman;
use App\Cicilan;
use App\Peminjam;
use App\User;

class PendanaanController extends Controller
{
  public function index(Request $request)
  {
    if(!is_null(\Auth::guard('api')->user())) {
      $user = User::find(\Auth::guard('api')->user()->id);
      $pinjaman = Pinjaman::select('funding.tanggal', 'peminjam_id','kode','pinjaman_id','jumlah_pinjaman',DB::raw('SUM(funding.jumlah) AS jumlah_pendanaan'),'waktu_pinjaman',DB::raw('IFNULL(bunga, 0) AS bagi_hasil'),'status', 'jenis_pinjaman_id AS jenis_pinjaman')
                  ->join('funding','pinjaman.id','=','funding.pinjaman_id')
                  ->where('pemodal_id', $user->pemodal->id)
                  ->groupBy('pinjaman_id')
                  ->groupBy('funding.tanggal')
                  ->get();

        if($pinjaman) {
          foreach ($pinjaman as $row) {
            $row->jenis_pinjaman = !empty($row->jenis_pinjaman) ? JenisPinjaman::find($row->jenis_pinjaman)->nama : "";
            $row->waktu_pinjaman = $row->waktu_pinjaman." Bulan";
            $row->bagi_hasil = $row->bagi_hasil."%";
            $row->progress_pendanaan = (($row->jumlah_funding / $row->jumlah_pinjaman) * 100)."%";
            $row->status = "fully funded";

            $peminjam = Peminjam::find($row->peminjam_id);
            $row->nama_peminjam = $peminjam->user->name;
          }
          return response()->json(['status' => 'success', 'data' => $pinjaman], 200);
        }
        return response()->json(['status' => 'error', 'message' => 'No data available'], 200);
    }
    return response()->json(['status' => 'error', 'message' => 'No User logged in'], 200);
  }

  public function doPendanaan(Request $request) 
  {
    if(!is_null(\Auth::guard('api')->user())) {
      $user = User::find(\Auth::guard('api')->user()->id);
      for($i=0; $i < $request->jumlah_pendanaan; $i++){
        // simpan pendanaan
        $pendanaan = new Pendanaan();
        $pendanaan->pemodal_id = $user->pemodal->id;
        $pendanaan->pinjaman_id = $request->input('pinjaman_id.'.$i);
        $pendanaan->jumlah = $request->input('nominal_pendanaan.'.$i);
        $pendanaan->tanggal = date('Y-m-d H:i:s');
        $pendanaan->save();

        // calcute total pendanaan per id pinjaman
        $total_pendanaan = Pendanaan::where('pinjaman_id', $request->input('pinjaman_id.'.$i))->sum('jumlah');
        $pinjaman = Pinjaman::find( $request->input('pinjaman_id.'.$i));
        $pinjaman->jumlah_funding = $total_pendanaan;
        $pinjaman->save();

        // jika pendanaan telah 100% update tanggal cicilan
        if($pinjaman->jumlah_funding >= $pinjaman->jumlah_pinjaman){
          $cicilan = Cicilan::where('pinjaman_id',$request->input('pinjaman_id.'.$i))->get();
          $date = date_add(date_create(date('Y-m-d')),date_interval_create_from_date_string("1 months"));
          for ($y=0; $y < count($cicilan); $y++) {
            if($y > 0){
              date_add($date,date_interval_create_from_date_string($y." months"));
            }
            $tanggal =  date_format($date,"Y-m-d");

            Cicilan::where('id',$cicilan[$y]->id)->update(['tanggal'=>$tanggal]);
          }
        }
      }

      // update saldo pemodal
      $pemodal = Pemodal::find($user->pemodal->id);
      $pemodal->saldo = $pemodal->saldo - $request->total_pendanaan;
      $pemodal->save();

      return response()->json(['status' => 'success', 'message' => 'Pendanaan berhasil dilakukan'], 200);
    }
    return response()->json(['status' => 'error', 'message' => 'No User logged in'], 200);
  }
}
