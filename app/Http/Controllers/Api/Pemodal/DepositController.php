<?php
	namespace App\Http\Controllers\Api\Pemodal;

	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Mail;
	use Illuminate\Support\Facades\Validator;
	use App\Mail\InfoTransferDeposit;
	use App\Mail\NotifyDeposit;
	use App\Traits\Common;
	use App\FileUploads;
	use App\Transaksi;
	use App\Deposit;
	use App\Bank;
	use App\User;
	use App\AkunBank;

	class DepositController extends Controller
	{
		use Common;
		public function index()
		{
        	if(!is_null(\Auth::guard('api')->user())) {
				$user = User::find(\Auth::guard('api')->user()->id);
				$deposit = Deposit::select('deposit.id','transaksi_id','deposit.tanggal','deposit.jumlah','kode_unik','deposit.status','deposit.created_at','m_bank.nama AS nama_bank')
						            ->join('transaksi','deposit.transaksi_id','=','transaksi.id')
						            ->join('akun_bank','transaksi.akun_bank_id','=','akun_bank.id')
						            ->join('m_bank','akun_bank.bank_id','=','m_bank.id')
						            ->where('pemodal_id', $user->pemodal->id)
						            ->orderBy('deposit.created_at','desc')
						            ->get();

	            if($deposit) {
	            	foreach ($deposit as $row) {
	            		$row->kode_transaksi = $row->transaksi->kode_transaksi.".".$row->transaksi->ref;
	            		if($row->status == "unverified"){
			                $row->status = 'konfirmasi';
			            } elseif ($depo->status == "proses") {
			                $row->status = 'proses';
			            } else {
			                $row->status = 'berhasil';
			            }
			            $row->nominal = $row->jumlah + $row->kode_unik;
			            unset($row->transaksi);
	            	}
	            	return response()->json(['status' => 'success', 'data' => $deposit], 200);
	            }
	            return response()->json(['status' => 'error', 'message' => 'No data available'], 200);
	        }
	        return response()->json(['status' => 'error', 'message' => 'No User logged in'], 200);
		}

		public function order(Request $request) {
			if(!is_null(\Auth::guard('api')->user())) {
				$user = User::find(\Auth::guard('api')->user()->id);
				$validator = Validator::make($request->all(),
		          [
		            'bank' => 'required',
		            'jumlah' => 'required',
		          ],
		          [
		            'bank.required' => 'Bank dibutuhkan!',
		            'jumlah.required' => 'Jumlah dibutuhkan!',
		          ]
		        );

		        if($validator->fails()) {
		            return response()->json(['status' => 'error', 'message' => $validator->messages()->first(), 200]);
		        }

		        $int = $this->rupiah_to_int($request->jumlah);

		        $number             = "0000";
		        $date               = date('ym');
		        $count              = Transaksi::where('id','LIKE','%'.$date.'%')->count();
		        $referensi          = $date.$number + $count + 1;
		        $kode_unik_transfer = $this->generate_kode_unik();

		    	$transaksi = new Transaksi;
		        $transaksi->ref            = $referensi;
		        $transaksi->kode_transaksi = "02";
		        $transaksi->jumlah         = $int + $kode_unik_transfer;
		        $transaksi->akun_bank_id   = $request->bank;
		    	  $transaksi->save();

		    	$deposit = new Deposit;
		        $deposit->pemodal_id   = $user->pemodal->id;
		        $deposit->transaksi_id = $transaksi->id;
		        $deposit->tanggal      = date('Y-m-d H:i:s');
		        $deposit->status       = 'unverified';
		        $deposit->jumlah       = $int;
		        $deposit->kode_unik    = $kode_unik_transfer;
		        $deposit->save();

		        // send email to pemodal untuk detail transfer
		        Mail::to($user->email)->send(new InfoTransferDeposit($deposit));
		        
		        // info untuk transfer
		        $info_transfer = new \stdClass();
		        $info_transfer->nominal = $transaksi->jumlah;
		        $info_transfer->bank_image = asset('img/bank_mandiri.png');
		        $info_transfer->akun_bank  = AkunBank::select('m_bank.nama AS nama_bank','nama_akun_bank','no_akun_bank')
										          ->join('m_bank','akun_bank.bank_id','=','m_bank.id')
										          ->where('akun_bank.id', $request->bank)->get();

		        return response()->json(['status' => 'success', 'message' => 'Deposit telah disimpan, silahkan lakukan transfer dan konfirmasi', 'info_transfer' => $info_transfer], 200);
		    }
		    return response()->json(['status' => 'error', 'message' => 'No User logged in'], 200);
		}

		public function detail($id)
	    {
	    	if(!is_null(\Auth::guard('api')->user())) {
		        $depo      = Deposit::find($id);
		        $transaksi = Transaksi::select('transaksi.*','m_bank.nama AS nama_bank')
		              ->where('transaksi.id',$depo->transaksi_id)
		              ->join('akun_bank','transaksi.akun_bank_id','=','akun_bank.id')
		              ->join('m_bank','akun_bank.bank_id','m_bank.id')
		              ->first();

		        if($transaksi) {
		        	$transaksi->deposit_id = $depo->id;
		        	$transaksi->deposit = $depo;
		        	return response()->json(['status' => 'success', 'message' => 'Berhasil!', 'data' => $transaksi], 200);
		        }
		        return response()->json(['status' => 'error', 'message' => 'No data available'], 200);
		    }
	        return response()->json(['status' => 'error', 'message' => 'No User logged in'], 200);
	    }

	    public function konfirmasi(Request $request)
	    {
	      	if(!is_null(\Auth::guard('api')->user())) {
	      		$validator = Validator::make($request->all(),
	              [
	                'tanggal'       => 'required',
	                'nama_bank'       => 'required',
	                'pemilik_akun'        => 'required',
	                'no_rekening'      	  => 'required',
	              ],
		          [
		            'tanggal.required' => 'Tanggal Transfer dibutuhkan!',
		            'nama_bank.required' => 'Nama Bank dibutuhkan!',
		            'pemilik_akun.required' => 'Pemilik Akun dibutuhkan!',
		            'no_rekening.required' => 'No. Rekening dibutuhkan!',
		          ]
	            );

	            if($validator->fails()) {
	                return response()->json(['status' => 'error', 'message' => $validator->messages()->first(), 200]);
	            }

		    	$depo = Deposit::find($request->deposit_id);
		      	$depo->status = "proses";
		      	$depo->save();

		      	$transaksi = Transaksi::find($request->id);
		      	$transaksi->tanggal = $request->tanggal;
		      	$transaksi->deskripsi = $request->catatan_tambahan;
		      	$transaksi->akun_bank_nama_bank = $request->nama_bank;
		      	$transaksi->akun_bank_no_akun = $request->no_rekening;
		      	$transaksi->akun_bank_nama_akun = $request->pemilik_akun;
		      	$transaksi->save();

		      	if($request->hasFile('bukti_transfer')){
		        	if ($request->file('bukti_transfer')->isValid()) {
			          	$request->file('bukti_transfer')->storeAs('bukti_transfer_deposit','bukti_transfer_deposit_'.date('jmY_His').'_'.$request->user()->id.'.'.$request->file('bukti_transfer')->extension());

			          	$file = new FileUploads();
			          	$file->type = 'dokumen';
			          	$file->name = 'bukti_transfer_deposit_'.date('jmY_His').'_'.$request->user()->id.'.'.$request->file('bukti_transfer')->extension();
			          	$file->descriptions = "BUKTI TRANSFER DEPOSIT";
			          	$file->mime = $request->file('bukti_transfer')->getMimeType();
			          	$file->size = $request->file('bukti_transfer')->getClientSize();
			          	$file->save();

			          	$transaksi->file_id = $file->id;
			          	$transaksi->save();
		        	}
		    	}

		    	# send email to admin
			    $staff = User::where('type','finnesia')->get();
			    Mail::to($staff)->send(new NotifyDeposit($transaksi));

			    return response()->json(['status' => 'success', 'message' => 'Konfirmasi Pembayaran berhasil'], 200);
		  	}
		  	return response()->json(['status' => 'error', 'message' => 'No User logged in'], 200);
	    }

		private function generate_kode_unik()
	    {
	      $kode = mt_rand(100,999);

	      $is_unik = Deposit::where('kode_unik',$kode)->count();
	      if($is_unik > 0){
	        $kode = $this->generate_kode_unik();
	      }

	      return $kode;
	    }
	}

?>