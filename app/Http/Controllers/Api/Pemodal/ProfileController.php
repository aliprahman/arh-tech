<?php
	
	namespace App\Http\Controllers\Api\Pemodal;

	use App\User;
	use App\Biodata;
	use App\Alamat;
	use App\AkunBank;
	use App\Perusahaan;
	use App\Pemodal;
	use App\FileUploads;
	use App\Http\Controllers\Controller;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Validator;
	use Illuminate\Validation\Rule;

	/**
	* 
	*/
	class ProfileController extends Controller
	{
		public function update(Request $request) 
		{
			if(!is_null(\Auth::guard('api')->user())) {
				$user = User::find(\Auth::guard('api')->user()->id);
				$validator = Validator::make($request->all(),
		          [
		            'no_hp'  => [
		                Rule::unique('biodata')->ignore($user->biodata_id),
		            ],
		            'no_ktp' => [
		                Rule::unique('biodata')->ignore($user->biodata_id),
		            ],
		            'no_npwp'=> [
		                Rule::unique('biodata')->ignore($user->biodata_id),
		            ],
		          ]
		        );

		        if($validator->fails()) {
		            return response()->json(['status' => 'error', 'message' => $validator->messages()->first(), 200]);
		        }

				$user->name = $request->nama;
				$user->email = $request->email;
				if(!empty($request->password)) {
					$user->password   = bcrypt($request->password);
				}
				$user->save();

				$biodata = Biodata::find($user->biodata_id);
				$biodata->tempat_lahir = $request->tempat_lahir;
				$biodata->tanggal_lahir = $request->tanggal_lahir;
				$biodata->jenis_kelamin = $request->jenis_kelamin;
				$biodata->no_telepon = $request->telepon;
				$biodata->no_hp = $request->hp;
				$biodata->no_ktp = $request->ktp;
				$biodata->no_npwp = $request->npwp;
				$biodata->save();

				$alamat_diri = Alamat::find($user->pemodal->alamat_id);
		        $alamat_diri->alamat      = $request->alamat;
		        $alamat_diri->kode_pos    = $request->kode_pos;
		        $alamat_diri->province_id = $request->prov_1;
		        $alamat_diri->regency_id  = $request->kota_1;
		        $alamat_diri->district_id = $request->kec_1;
		        $alamat_diri->village_id  = $request->kel_1;
		        $alamat_diri->save();

		        $akun_bank = AkunBank::find($user->pemodal->akun_bank_id);
		        $akun_bank->bank_id        = $request->bank;
		        $akun_bank->nama_akun_bank = $request->nama_akun;
		        $akun_bank->no_akun_bank   = $request->no_rekening;
		        $akun_bank->save();

				if (isset($user->pemodal->perusahaan)) {
		          	$perusahaan = Perusahaan::find($user->pemodal->perusahaan->id);
		          	$perusahaan->nama = $request->nama_perusahaan;
		          	$perusahaan->save();
		        } else {
		          	$perusahaan = new Perusahaan();
		          	$perusahaan->nama = $request->nama_perusahaan;
		          	$perusahaan->save();
		          	Pemodal::where('user_id', $user->id)->update(['perusahaan_id'=>$perusahaan->id]);
		        }

		        if($request->hasFile('photo_profile') && $request->file('photo_profile')->isValid()){
		            if($user->photo_profile_id != ""){
		                $file = FileUploads::find($user->photo_profile_id);
		                if(file_exists(storage_path('app/public/profile_photo/'.$file->name))){
		                  unlink(storage_path('app/public/profile_photo/'.$file->name));
		                }
		            } else {
		                $file = new FileUploads();
		            }
		            $request->file('photo_profile')->storeAs('profile_photo','pemodal_profile_photo_'.$user->id.'.'.$request->file('photo_profile')->extension());

		            $file->type = 'profile_photo';
		            $file->name = 'pemodal_profile_photo_'.$user->id.'.'.$request->file('photo_profile')->extension();
		            $file->descriptions = "Photo Profile";
		            $file->mime = $request->file('photo_profile')->getMimeType();
		            $file->size = $request->file('photo_profile')->getClientSize();
		            $file->save();

		            $user->photo_profile_id = $file->id;
		        }	
		        $user->save();
				
				return response()->json(['status' => 'success', 'message' => 'Data berhasil diubah !'], 200);
			}
			return response()->json(['status' => 'error', 'message' => 'No User logged in'], 200);
		}
	}

?>