<?php

namespace App\Http\Controllers\Api\Pemodal;

use App\User;
use App\Kota;
use App\Desa;
use App\Bank;
use App\Alamat;
use App\Biodata;
use App\Pemodal;
use App\Provinsi;
use App\AkunBank;
use App\Kecamatan;
use App\Perusahaan;
use App\Mail\ActivationEmail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        $number        = "000";
        $date          = date('ym');
        $count         = User::where('id_pelanggan','LIKE','%'.$date.'%')->count();
        $id_pelanggan  = $date.$number + $count + 1;

        $validator = Validator::make($request->all(),
          [
            'email'                => 'email|unique:users,email',
            'handphone'            => 'unique:biodata,no_hp',
            'ktp'                  => 'unique:biodata,no_ktp|max:16',
            'npwp'                 => 'unique:biodata,no_npwp|max:14',
            'password'             => 'required|min:6',
          ]
        );

        if($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->messages()->first(), 200]);
        }

        $biodata = new Biodata();
        $biodata->tempat_lahir = $request->tempat_lahir;
        $biodata->tanggal_lahir= $request->tanggal_lahir;
        $biodata->jenis_kelamin= $request->jenis_kelamin;
        $biodata->no_telepon   = $request->telephone;
        $biodata->no_hp        = $request->handphone;
        $biodata->no_ktp       = $request->ktp;
        $biodata->no_npwp      = $request->npwp;
        $biodata->save();

        $user = new User();
        $user->name       = $request->nama;
        $user->email      = $request->email;
        $user->password   = bcrypt($request->password);
        $user->type       = 'pemodal';
        $user->biodata_id = $biodata->id;
        $user->id_pelanggan = $id_pelanggan;
        $user->remember_token = str_random(24);
        $user->verification_token = str_random(40);
        $user->role_id    = 3;
        $user->status     = 'new';
        $user->save();

        $alamat = new Alamat();
        $alamat->alamat      = $request->alamat;
        $alamat->kode_pos    = $request->kode_pos_1;
        $alamat->province_id = $request->prov_1;
        $alamat->regency_id  = $request->kota_1;
        $alamat->district_id = $request->kec_1;
        $alamat->village_id  = $request->kel_1;
        $alamat->save();

        $akun_bank = new AkunBank();
        $akun_bank->bank_id        = $request->bank;
        $akun_bank->nama_akun_bank = $request->nama_akun;
        $akun_bank->no_akun_bank   = $request->no_rekening;
        $akun_bank->save();

        $perusahaan = new Perusahaan();
        $perusahaan->nama        = $request->n_perusahaan;
        $perusahaan->save();

        $pemodal = new Pemodal();
        $pemodal->user_id                = $user->id;
        $pemodal->type                   = $request->jenis;
        $pemodal->alamat_id              = $alamat->id;
        $pemodal->akun_bank_id           = $akun_bank->id;
        $pemodal->perusahaan_id          = $perusahaan->id;
        $pemodal->status                 = 'unverified';
        $pemodal->save();

        $token = $user->verification_token;

        Mail::send('mails.verification', compact('user','token'), function ($m) use ($user){
            $m->to($user->email, $user->name)->subject('Verifikasi Akun Pemodal Finnesia');
        });
        
        return response()->json(['status' => 'success', 'message' => 'Pendaftaran Berhasil, Silahkan Cek Email Untuk Aktifasi'], 200);
    }
}
