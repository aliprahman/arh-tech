<?php
	namespace App\Http\Controllers\Api\Pemodal;

	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\DB;
	use Carbon\Carbon;
	use App\JenisPinjaman;
	use App\Pinjaman;
	use App\Peminjam;
	use App\User;
	use App\Traits\Common;

	class ListPinjamanController extends Controller
	{
		use Common;
		public function index(Request $request)
		{
        	if(!is_null(\Auth::guard('api')->user())) {
				$pinjaman = Pinjaman::select('id', 'peminjam_id', 'kode' ,'jumlah_pinjaman', 'jumlah_funding', 'waktu_pinjaman', DB::raw('IFNULL(bunga, 0) AS bagi_hasil'), 'tanggal_mulai_funding', 'tanggal_berakhir_funding', 'status', 'jenis_pinjaman_id AS jenis_pinjaman')
	            			->whereIn('status',['listing','funding'])
	            			->whereRaw('jumlah_pinjaman > IFNULL(jumlah_funding,0)')
	            			->get();

	            if($pinjaman) {
	            	foreach ($pinjaman as $row) {
	            		$row->jenis_pinjaman = !empty($row->jenis_pinjaman) ? JenisPinjaman::find($row->jenis_pinjaman)->nama : "";
            			$start_date = Carbon::parse(date('Y-m-d'));
            			$end_date = Carbon::parse($row->tanggal_berakhir_funding);
            			$row->sisa_hari = $end_date->diffInDays($start_date)." Hari Lagi";
	            		$row->waktu_pinjaman = $row->waktu_pinjaman." Bulan";
	            		$row->bagi_hasil = $row->bagi_hasil."%";
	            		$row->progress_pendanaan = (($row->jumlah_funding / $row->jumlah_pinjaman) * 100)."%";

	            		$peminjam = Peminjam::find($row->peminjam_id);
	            		$row->nama_peminjam = $peminjam->user->name;
	            	}
	            	return response()->json(['status' => 'success', 'data' => $pinjaman], 200);
	            }
	            return response()->json(['status' => 'error', 'message' => 'No data available'], 200);
	        }
	        return response()->json(['status' => 'error', 'message' => 'No User logged in'], 200);
		}

		public function portofolio($pinjaman_id)
	    {
	    	if(!is_null(\Auth::guard('api')->user())) {
	    		$real_id = $pinjaman_id;
	        	$pinjaman = Pinjaman::find($real_id);

	        	if($pinjaman) {
	        		$pinjaman->nama_peminjam = $pinjaman->peminjam->user->name;
	        		unset($pinjaman->peminjam);
	        		return response()->json(['status' => 'success', 'data' => $pinjaman], 200);
	        	}
	        	return response()->json(['status' => 'error', 'message' => 'No data available'], 200);
	    	}
	    	return response()->json(['status' => 'error', 'message' => 'No User logged in'], 200);
	    }

	    public function detail($pinjaman_id)
	    {
	    	if(!is_null(\Auth::guard('api')->user())) {
	    		$real_id = $pinjaman_id;
	        	$pinjaman = Pinjaman::select('id', 'peminjam_id', 'kode' ,'jumlah_pinjaman', 'jumlah_funding', 'waktu_pinjaman', DB::raw('IFNULL(bunga, 0) AS bagi_hasil'), 'tanggal_mulai_funding', 'tanggal_berakhir_funding', 'status', 'jenis_pinjaman_id AS jenis_pinjaman')
	            			->where('id', $real_id)
	            			->first();

	        	if($pinjaman) {
	        		$pinjaman->jenis_pinjaman = !empty($pinjaman->jenis_pinjaman) ? JenisPinjaman::find($pinjaman->jenis_pinjaman)->nama : "";
        			$start_date = Carbon::parse(date('Y-m-d'));
        			$end_date = Carbon::parse($pinjaman->tanggal_berakhir_funding);
        			$pinjaman->sisa_hari = $end_date->diffInDays($start_date)." Hari Lagi";
            		$pinjaman->waktu_pinjaman = $pinjaman->waktu_pinjaman." Bulan";
            		$pinjaman->bagi_hasil = $pinjaman->bagi_hasil."%";
            		$pinjaman->progress_pendanaan = (($pinjaman->jumlah_funding / $pinjaman->jumlah_pinjaman) * 100)."%";

            		$peminjam = Peminjam::find($pinjaman->peminjam_id);
            		$pinjaman->nama_peminjam = $peminjam->user->name;

	        		return response()->json(['status' => 'success', 'data' => $pinjaman], 200);
	        	}
	        	return response()->json(['status' => 'error', 'message' => 'No data available'], 200);
	    	}
	    	return response()->json(['status' => 'error', 'message' => 'No User logged in'], 200);	
	    }
	}

?>