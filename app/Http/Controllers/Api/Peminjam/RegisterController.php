<?php

namespace App\Http\Controllers\Api\Peminjam;

use App\User;
use App\Kota;
use App\Desa;
use App\Bank;
use App\Alamat;
use App\Biodata;
use App\Peminjam;
use App\Provinsi;
use App\AkunBank;
use App\Kecamatan;
use Illuminate\Http\Request;
use App\Mail\ActivationEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(),
          [
            'email'                => 'email|unique:users,email',
            'handphone'            => 'unique:biodata,no_hp',
            'ktp'                  => 'unique:biodata,no_ktp|max:16',
            'npwp'                 => 'unique:biodata,no_npwp|max:14',
            'password'             => 'required|min:6',
          ]
        );

        if($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->messages()->first(), 200]);
        }

        $number        = "000";
        $date          = date('ym');
        $count         = User::where('id_pelanggan','LIKE','%'.$date.'%')->count();
        $id_pelanggan  = $date.$number + $count + 1;

        $biodata = new Biodata();
        $biodata->tempat_lahir = $request->tempat_lahir;
        $biodata->tanggal_lahir= $request->tanggal_lahir;
        $biodata->jenis_kelamin= $request->jenis_kelamin;
        $biodata->no_telepon   = $request->telephone;
        $biodata->no_hp        = $request->handphone;
        $biodata->no_ktp       = $request->ktp;
        $biodata->no_npwp      = $request->npwp;
        $biodata->save();

        $user = new User();
        $user->name       = $request->nama;
        $user->email      = $request->email;
        $user->password   = bcrypt($request->password);
        $user->type       = 'peminjam';
        $user->biodata_id = $biodata->id;
        $user->id_pelanggan = $id_pelanggan;
        $user->verification_token = str_random(40);
        $user->role_id    = 4;
        $user->status     = 'new';
        $user->save();

        $alamat_diri = new Alamat();
        $alamat_diri->alamat      = $request->alamat;
        $alamat_diri->kode_pos    = $request->kode_pos_1;
        $alamat_diri->province_id = $request->prov_1;
        $alamat_diri->regency_id  = $request->kota_1;
        $alamat_diri->district_id = $request->kec_1;
        $alamat_diri->village_id  = $request->kel_1;
        $alamat_diri->save();

        $alamat_sekolah = new Alamat();
        $alamat_sekolah->alamat      = $request->alamat_sekolah;
        $alamat_sekolah->kode_pos    = $request->kode_pos_2;
        $alamat_sekolah->province_id = $request->prov_2;
        $alamat_sekolah->regency_id  = $request->kota_2;
        $alamat_sekolah->district_id = $request->kec_2;
        $alamat_sekolah->village_id  = $request->kel_2;
        $alamat_sekolah->save();

        $akun_bank = new AkunBank();
        $akun_bank->bank_id        = $request->bank;
        $akun_bank->nama_akun_bank = $request->nama_akun;
        $akun_bank->no_akun_bank   = $request->no_rekening;
        $akun_bank->save();

        $peminjam = new Peminjam();
        $peminjam->user_id                = $user->id;
        $peminjam->nama_tempat_kerja      = $request->nama_sekolah;
        $peminjam->alamat_tempat_kerja_id = $alamat_sekolah->id;
        $peminjam->alamat_id              = $alamat_diri->id;
        $peminjam->akun_bank_id           = $akun_bank->id;
        $peminjam->no_telepon_tempat_kerja= $request->telephone_sekolah;
        $peminjam->status                 = 'unverified';
        $peminjam->save();

        $token = $user->verification_token;
        Mail::send('mails.verification', compact('user','token'), function ($m) use ($user){
            $m->to($user->email, $user->name)->subject('Verifikasi Akun Peminjam Finnesia');
        });
        
        return response()->json(['status' => 'success', 'message' => 'Pendaftaran Berhasil, Silahkan Cek Email Untuk Aktifasi'], 200);
    }
}
