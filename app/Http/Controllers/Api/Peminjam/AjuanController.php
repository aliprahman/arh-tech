<?php

namespace App\Http\Controllers\Api\Peminjam;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\NotifPengajuan;
use App\Traits\Common;
use App\Pinjaman;
use App\Peminjam;
use App\KeuanganPeminjam;
use App\User;
use App\Provinsi;
use App\Kota;
use App\Kecamatan;
use App\Desa;
use App\AkunBank;


class AjuanController extends Controller
{
    use Common;
    # proses simpan pengajuan pinjaman
    public function store(Request $request)
    {
        $data_user = \Auth::guard('api')->user();
        if(!is_null($data_user)) {
            $validator = Validator::make($request->all(),
              [
                'jenis_pinjaman_id'    => 'required',
                'jumlah_pinjaman'      => 'required',
                'waktu_pinjaman'       => 'required',
                'checklist_setuju'     => 'required',
              ]
            );

            if($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()->first(), 200]);
            }

            $user = User::find($data_user->id);

            $akun_bank = AkunBank::find($user->peminjam->akun_bank_id);
            $akun_bank->bank_id        = $request->bank;
            $akun_bank->nama_akun_bank = $request->nama_akun;
            $akun_bank->no_akun_bank   = $request->no_rekening;
            $akun_bank->save();

            $pinjaman = new Pinjaman();
            $pinjaman->kode = Pinjaman::generate_code();
            $pinjaman->peminjam_id = $user->peminjam->id;
            $pinjaman->jenis_pinjaman_id = $request->jenis_pinjaman_id;
            $pinjaman->jumlah_pinjaman = $request->jumlah_pinjaman;
            $pinjaman->waktu_pinjaman = $request->waktu_pinjaman;
            $pinjaman->tanggal_submit = date('Y-m-d H:i:s');
            $pinjaman->status = 'submit';
            $pinjaman->detail_peminjam = $this->biodata_peminjam(Peminjam::find($user->peminjam->id));
            $pinjaman->save();

            # send notif to admin
            /*$admin = User::where('type','finnesia')->pluck('email')->toArray();
            Mail::to($admin)->send(new NotifPengajuan($pinjaman, $user->name));*/

            return response()->json(['status' => 'success', 'message' => 'Pengajuan Peminjaman berhasil diproses.'], 200);
        }
        return response()->json(['status' => 'error', 'message' => 'No User logged in', 'data' => $data_user], 200);
    }

    # get biodata peminjam
    private function biodata_peminjam(Peminjam $peminjam)
    {
        $data['biodata'] = array(
            'tempat_lahir'           => $peminjam->user->biodata->tempat_lahir,
            'tanggal_lahir'          => $peminjam->user->biodata->tanggal_lahir,
            'jenis_kelamin'          => $peminjam->user->biodata->jenis_kelamin,
            'alamat'                 => [
                'alamat_lengkap'     => $peminjam->alamat->alamat,
                'provinsi'           => Provinsi::where('id',$peminjam->alamat->province_id)->value('name'),
                'kota'               => Kota::where('id',$peminjam->alamat->regency_id)->value('name'),
                'kecamatan'          => Kecamatan::where('id',$peminjam->alamat->district_id)->value('name'),
                'desa'               => Desa::where('id',$peminjam->alamat->village_id)->value('name'),
            ],
            'kepemilikah_rumah'      => isset($peminjam->kepemilikan_rumah->nama)? $peminjam->kepemilikan_rumah->nama : "",
            'nama_tempat_kerja'      => $peminjam->nama_tempat_kerja,
            'alamat_tempat_kerja'    => [
                'alamat_lengkap'     => isset($peminjam->alamat_kerja->alamat)? $peminjam->alamat_kerja->alamat : "",
                'provinsi'           => isset($peminjam->alamat_kerja->province_id)? Provinsi::where('id',$peminjam->alamat_kerja->province_id)->value('name') : "",
                'kota'               => isset($peminjam->alamat_kerja->regency_id)? Kota::where('id',$peminjam->alamat_kerja->regency_id)->value('name') : "",
                'kecamatan'          => isset($peminjam->alamat_kerja->district_id)? Kecamatan::where('id',$peminjam->alamat_kerja->district_id)->value('name') : "",
                'desa'               => isset($peminjam->alamat_kerja->village_id)? Desa::where('id',$peminjam->alamat_kerja->village_id)->value('name') : ""
            ],
            'lama_berdiri_tempat_kerja' => $peminjam->lama_berdiri_tempat_kerja,
            'jumlah_murid_tempat_kerja' => $peminjam->jumlah_murid_tempat_kerja,
            'gaji'                   => $peminjam->gaji,
            'jabatan'                => isset($peminjam->jabatan->nama)? $peminjam->jabatan->nama : "",
            'status_pengajar'        => $peminjam->status_pengajar,
            'lama_mengajar'          => $peminjam->lama_mengajar,
            'pekerjaan_suami_istri'  => isset($peminjam->pekerjaan->nama)? $peminjam->pekerjaan->nama : "",
            'pendapatan_suami_istri' => $peminjam->pendapatan_suami_istri,
            'jumlah_tanggungan'      => $peminjam->jumlah_tanggungan,
            'curiculum_vitae'        => isset($peminjam->photo_curiculum_vitae->name)? $peminjam->photo_curiculum_vitae->name : ""
        );

        $pinjaman = array();
        $pinjaman_lainnya = KeuanganPeminjam::where('peminjam_id',$peminjam->id)->where('type','pinjaman')->get();
        foreach ($pinjaman_lainnya as $item) {
            $pinjaman[] = array(
                'jumlah' => $item->jumlah,
                'keterangan' => $item->keterangan
            );
        }

        $pendapatan = array();
        $pendapatan_lainnya = KeuanganPeminjam::where('peminjam_id',$peminjam->id)->where('type','pendapatan')->get();
        foreach ($pendapatan_lainnya as $item) {
            $pendapatan[] = array(
                'jumlah' => $item->jumlah,
                'keterangan' => $item->keterangan
            );
        }

        $data['keuangan']['pendapatan'] = $pendapatan;
        $data['keuangan']['pinjaman'] = $pinjaman;

        return $data;
    }
}
