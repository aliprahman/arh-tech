<?php

namespace App\Http\Controllers\Api\Peminjam;

use App\User;
use App\Bank;
use App\Peminjam;
use App\Cicilan;
use App\Transaksi;
use App\Traits\Common;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CicilanController extends Controller
{
    use Common;

    public function doPembayaran(Request $request, $cicilan_id)
    {
        $data_user = \Auth::guard('api')->user();
        if(!is_null($data_user)) {
            $validator = Validator::make($request->all(),
              [
                'bank'             => 'required',
                'no_transaksi'     => 'required',
                'nama_akun'        => 'required',
                'no_rekening'      => 'required',
              ]
            );

            if($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()->first(), 200]);
            }

            $real_id = $cicilan_id;
            $nominal = $this->rupiah_to_int($request->nominal);

            $transaksi = new Transaksi();
            $transaksi->kode_transaksi = '01';
            $transaksi->tanggal = date('Y-m-d H:i:s');
            $transaksi->jumlah = $nominal;
            $transaksi->ref = $request->no_transaksi;
            $transaksi->deskripsi = $request->catatan;
            $transaksi->akun_bank_id = $request->bank;
            $transaksi->akun_bank_nama_bank = Bank::where('id',$request->bank)->value('nama');
            $transaksi->akun_bank_no_akun = $request->no_rekening;
            $transaksi->akun_bank_nama_akun = $request->nama_akun;
            $transaksi->save();

            $cicilan = Cicilan::find($real_id);
            $cicilan->transaksi_id = $transaksi->id;
            if(strtotime(date('Y-m-d')) <= strtotime($cicilan->tanggal)){
              $cicilan->status = 'on time';
            }else{
              $cicilan->status = 'late';
            }
            $cicilan->save();
            
            return response()->json(['status' => 'success', 'message' => 'Pembayaran cicilan berhasil dilakukan'], 200);
        }
        return response()->json(['status' => 'error', 'message' => 'No User logged in', 'data' => $data_user], 200);
    }

    public function detailPembayaran($transaksi_id) {
        $data_user = \Auth::guard('api')->user();
        if(!is_null($data_user)) {
            $data = Transaksi::find($transaksi_id);
            return response()->json(['status' => 'success', 'data' => $data], 200);
        }
        return response()->json(['status' => 'error', 'message' => 'No User logged in', 'data' => $data_user], 200);
    }
}
