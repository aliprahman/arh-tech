<?php
	namespace App\Http\Controllers\Api\Peminjam;

	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\DB;
	use App\Http\Controllers\Controller;
	use Carbon\Carbon;
	use App\Pinjaman;
	use App\Peminjam;
	use App\Cicilan;
	use App\JenisPinjaman;
	use App\Traits\Common;

	class ListPinjamanController extends Controller
	{
		use Common;

		public function index(Request $request)
		{
			$user = \Auth::guard('api')->user();
        	if(!is_null($user)) {
				$status_cicilan = isset($request->status_cicilan) ? $request->status_cicilan : 0;

				$pinjaman = Pinjaman::select('id','tanggal_submit','kode','jumlah_pinjaman','waktu_pinjaman','status'
				                    /*DB::raw('IF(
				                	           (select sum(transaksi.jumlah)
				                	            from cicilan
				                	            join transaksi on cicilan.transaksi_id = transaksi.id
				                	            where pinjaman_id = pinjaman.id
				                	            and cicilan.status != "unverified") > 1,1,0)
				                            AS status_cicilan')*/
				                    /*DB::raw('IF(
				                    		   (select count(id) from cicilan where pinjaman_id = pinjaman.id) > 0,
				                    		 IF(
				                	           (select count(id)
				                	            from cicilan
				                	            where pinjaman_id = pinjaman.id
				                	            and cicilan.status = "unverified") > 0, 0, 1), 1)
				                            AS status_cicilan')*/
				                )
				                ->where('pinjaman.peminjam_id', $user->peminjam->id)
				                ->where(function($pinjaman) use ($status_cicilan)  {
						            if(!$status_cicilan) {
						                $pinjaman->whereRaw('(select count(id)
				                	            from cicilan
				                	            where pinjaman_id = pinjaman.id
				                	            and cicilan.status = "unverified") > 0')
						                		->orWhereRaw('(select count(id)
				                	            from cicilan
				                	            where pinjaman_id = pinjaman.id) <= 0');
						            } else {
						            	$pinjaman->whereNotIn('pinjaman.status', ["submit", "listing"])
						            			->WhereRaw('(select count(id)
				                	            from cicilan
				                	            where pinjaman_id = pinjaman.id
				                	        	and cicilan.status = "unverified") <= 0');
						            }
						         })
				                ->get();

	            if($pinjaman) {
	            	foreach ($pinjaman as $key => $row) {
	            		if($status_cicilan) {
	            			$row->tanggal_submit = Carbon::parse($row->tanggal_submit)->format('d-m-Y');
	            			$row->waktu_pinjaman = $row->waktu_pinjaman." Bulan";
	            			$row->status = ucwords('Cicilan Selesai');
	            		} else {
            				$row->tanggal_submit = Carbon::parse($row->tanggal_submit)->format('d-m-Y');
	            			$row->waktu_pinjaman = $row->waktu_pinjaman." Bulan";
	            			if($row->status == 'submit') $row->status = ucwords('Waiting Confirmation');
			                if($row->status == 'listing') $row->status = ucwords('Approve');			                
	            		}	 
	            	}
	            	return response()->json(['status' => 'success', 'data' => $pinjaman], 200);
	            }
	            return response()->json(['status' => 'error', 'message' => 'Error !'], 200);
	        }
	        return response()->json(['status' => 'success', 'message' => 'No User logged in', 'data' => $user], 200);
		}

		public function rincian($pinjaman_id) {
			$user = \Auth::guard('api')->user();
        	if(!is_null($user)) {
				$real_id = $pinjaman_id;
			    $pinjaman = Pinjaman::find($real_id);
		        if($pinjaman) {
			        $pinjaman->jenis_pinjaman = !empty($pinjaman->jenis_pinjaman_id) ? JenisPinjaman::find($pinjaman->jenis_pinjaman_id)->nama : "";
			        $pinjaman->sisa_pinjaman = Pinjaman::sisa_pinjaman($real_id);
			        $pinjaman->status_pinjaman = "Lunas";
        			if($pinjaman->sisa_pinjaman > 0) {
        				$pinjaman->status_pinjaman = "Belum Lunas";
        			}
			        $pinjaman->cicilan = Cicilan::select('cicilan.id','cicilan.jumlah','cicilan.tanggal AS jatuh_tempo','transaksi.tanggal','ref','cicilan.status','transaksi.id AS transaksi_id')
			            						->where('pinjaman_id',$real_id)
			            						->leftjoin('transaksi','cicilan.transaksi_id','=','transaksi.id')
			            						->get();
		        
	            	$pinjaman->tanggal_submit = Carbon::parse($pinjaman->tanggal_submit)->format('d-m-Y');
        			$pinjaman->waktu_pinjaman = $pinjaman->waktu_pinjaman." Bulan";

        			if($pinjaman->status == 'submit') $pinjaman->status = ucwords('Waiting Confirmation');
	                if($pinjaman->status == 'listing') $pinjaman->status = ucwords('Approve');			                
	            	
	            	return response()->json(['status' => 'success', 'data' => $pinjaman], 200);
	            }
	            return response()->json(['status' => 'error', 'message' => 'No data available'], 200);
		    }
		    return response()->json(['status' => 'error', 'message' => 'No User logged in', 'data' => $user], 200);
		}
	}
?>