<?php
	
	namespace App\Http\Controllers\Api\Peminjam;

	use Hash;
	use App\User;
	use App\Biodata;
	use App\Alamat;
	use App\AkunBank;
	use App\Perusahaan;
	use App\Peminjam;
	use App\FileUploads;
	use App\KeuanganPeminjam;
	use App\PengalamanKerja;
	use App\Traits\Common;
	use App\Http\Controllers\Controller;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Validator;
	use Illuminate\Validation\Rule;

	/**
	* 
	*/
	class ProfileController extends Controller
	{
		use Common;

		public function updateAkun(Request $request) {
			if(!is_null(\Auth::guard('api')->user())) {
				$user = User::find(\Auth::guard('api')->user()->id);

				Validator::extend('old_password', function ($attribute, $value, $parameters) {
					$user = User::find(\Auth::guard('api')->user()->id);
		            return Hash::check($value, $user->password);
		        });

		        Validator::replacer('old_password', function ($message, $attribute, $value, $parameters) {
		            return 'Password Lama tidak valid';
		        });

				$validator = Validator::make($request->all(),
		          [
		            'email'        => 'email|unique:users,email,'.$user->id,
		            'old_password' => 'old_password',
		            'new_password' => 'confirmed',
		          ]
		        );

		        if($validator->fails()) {
		            return response()->json(['status' => 'error', 'message' => $validator->messages()->first(), 200]);
		        }

				$user->email = $request->email;
				$user->password = bcrypt($request->new_password);
				$user->save();

				return response()->json(['status' => 'success', 'message' => 'Data Akun berhasil diubah !'], 200);
			}
			return response()->json(['status' => 'error', 'message' => 'No User logged in'], 200);
		}

		public function updateBiodata(Request $request) {
			if(!is_null(\Auth::guard('api')->user())) {
				$user = User::find(\Auth::guard('api')->user()->id);
				$validator = Validator::make($request->all(),
		          [
		            'no_hp'  => [
		                Rule::unique('biodata')->ignore($user->biodata_id),
		            ],
		            'no_ktp' => [
		                Rule::unique('biodata')->ignore($user->biodata_id),
		            ],
		            'no_npwp'=> [
		                Rule::unique('biodata')->ignore($user->biodata_id),
		            ],
		          ]
		        );

		        if($validator->fails()) {
		            return response()->json(['status' => 'error', 'message' => $validator->messages()->first(), 200]);
		        }

		        $user->name = $request->nama;

		        $biodata = Biodata::find($user->biodata_id);
				$biodata->tempat_lahir = $request->tempat_lahir;
				$biodata->tanggal_lahir = $request->tanggal_lahir;
				$biodata->jenis_kelamin = $request->jenis_kelamin;
				$biodata->no_telepon = $request->telepon;
				$biodata->no_hp = $request->hp;
				$biodata->no_ktp = $request->ktp;
				$biodata->no_npwp = $request->npwp;
				$biodata->save();

				$alamat_diri = Alamat::find($user->peminjam->alamat_id);
		        $alamat_diri->alamat      = $request->alamat;
		        $alamat_diri->kode_pos    = $request->kode_pos;
		        $alamat_diri->province_id = $request->prov;
		        $alamat_diri->regency_id  = $request->kota;
		        $alamat_diri->district_id = $request->kec;
		        $alamat_diri->village_id  = $request->kel;
		        $alamat_diri->save();

		        if($request->hasFile('photo_profile') && $request->file('photo_profile')->isValid()){

		            if($user->photo_profile_id != ""){
		                $file = FileUploads::find($user->photo_profile_id);
		                if(file_exists(storage_path('app/public/profile_photo/'.$file->name))){
		                  unlink(storage_path('app/public/profile_photo/'.$file->name));
		                }
		            } else {
		                $file = new FileUploads();
		            }
		            $request->file('photo_profile')->storeAs('profile_photo','peminjam_profile_photo_'.$user->id.'.'.$request->file('photo_profile')->extension());

		            $file->type = 'profile_photo';
		            $file->name = 'peminjam_profile_photo_'.$user->id.'.'.$request->file('photo_profile')->extension();
		            $file->descriptions = "Photo Profile";
		            $file->mime = $request->file('photo_profile')->getMimeType();
		            $file->size = $request->file('photo_profile')->getClientSize();
		            $file->save();

		            $user->photo_profile_id = $file->id;
		        }
		        $user->save();

		        $peminjam = Peminjam::find($user->peminjam->id);
		        if ($request->hasFile('photo_tempat_tinggal')) {
		            if ($request->file('photo_tempat_tinggal')->isValid()) {
		                $request->file('photo_tempat_tinggal')
		                ->storeAs('tempat_tinggal','photo_tempat_tinggal_'.$user->id.'.'.$request->file('photo_tempat_tinggal')->extension());

		                if($peminjam->photo_tempat_tinggal_id != ""){
		                    $file = FileUploads::find($peminjam->photo_tempat_tinggal_id);
		                    if(file_exists(storage_path('app/public/tempat_tinggal/'.$file->name))){
		                      unlink(storage_path('app/public/tempat_tinggal/'.$file->name));
		                    }
		                }else{
		                    $file = new FileUploads();
		                }

		                $file->type = 'photo_tempat_tinggal';
		                $file->name = 'photo_tempat_tinggal_'.$peminjam->user_id.'.'.$request->file('photo_tempat_tinggal')->extension();
		                $file->descriptions = "Photo Tempat Tinggal";
		                $file->mime = $request->file('photo_tempat_tinggal')->getMimeType();
		                $file->size = $request->file('photo_tempat_tinggal')->getClientSize();
		                $file->save();

		                $peminjam->photo_tempat_tinggal_id = $file->id;
		                $peminjam->save();
		            }
		        }

		        if ($request->hasFile('curiculum_vitae')) {
		            if ($request->file('curiculum_vitae')->isValid()) {
		                $request->file('curiculum_vitae')
		                ->storeAs('curiculum_vitae','curiculum_vitae_'.$user->id.'.'.$request->file('curiculum_vitae')->extension());

		                if($peminjam->photo_curiculum_vitae_id != ""){
		                    $file = FileUploads::find($peminjam->photo_curiculum_vitae_id);
		                    if(file_exists(storage_path('app/public/curiculum_vitae/'.$file->name))){
		                      unlink(storage_path('app/public/curiculum_vitae/'.$file->name));
		                    }
		                }else{
		                    $file = new FileUploads();
		                }

		                $file->type = 'cv';
		                $file->name = 'curiculum_vitae_'.$peminjam->user_id.'.'.$request->file('curiculum_vitae')->extension();
		                $file->descriptions = "Curiculum Vitae";
		                $file->mime = $request->file('curiculum_vitae')->getMimeType();
		                $file->size = $request->file('curiculum_vitae')->getClientSize();
		                $file->save();

		                $peminjam->photo_curiculum_vitae_id = $file->id;
		                $peminjam->save();
		            }
		        }

				return response()->json(['status' => 'success', 'message' => 'Biodata berhasil diubah !'], 200);
			}
			return response()->json(['status' => 'error', 'message' => 'No User logged in'], 200);
		}

		public function updateBank(Request $request) {
			if(!is_null(\Auth::guard('api')->user())) {
				$user = User::find(\Auth::guard('api')->user()->id);

				$akun_bank = AkunBank::find($user->peminjam->akun_bank_id);
		        $akun_bank->bank_id        = $request->bank;
		        $akun_bank->nama_akun_bank = $request->nama_akun;
		        $akun_bank->no_akun_bank   = $request->no_rekening;
		        $akun_bank->save();

				return response()->json(['status' => 'success', 'message' => 'Data Bank berhasil diubah !'], 200);
			}
			return response()->json(['status' => 'error', 'message' => 'No User logged in'], 200);
		}

		public function updatePekerjaan(Request $request) {
			if(!is_null(\Auth::guard('api')->user())) {
				$user = User::find(\Auth::guard('api')->user()->id);

				if(isset($request->alamat)) {
					if($user->peminjam->alamat_tempat_kerja_id != ""){
			          	$alamat_sekolah = Alamat::find($user->peminjam->alamat_tempat_kerja_id);
			        }else{
			          	$alamat_sekolah = new Alamat();
			        }
			        $alamat_sekolah->alamat      = $request->alamat;
			        $alamat_sekolah->kode_pos    = $request->kode_pos;
			        $alamat_sekolah->province_id = $request->prov;
			        $alamat_sekolah->regency_id  = $request->kota;
			        $alamat_sekolah->district_id = $request->kec;
			        $alamat_sekolah->village_id  = $request->kel;
			        $alamat_sekolah->save();
				}

				if(isset($request->nama_tempat_kerja)) {
					$peminjam = Peminjam::find($user->peminjam->id);
			        $peminjam->nama_tempat_kerja      = $request->nama_tempat_kerja;
			        $peminjam->jumlah_murid_tempat_kerja = $request->jumlah_murid;
			        $peminjam->lama_berdiri_tempat_kerja = $request->lama_berdiri;
			        $peminjam->jabatan_id = $request->jabatan_id;
			        $peminjam->status_pengajar = $request->status_pengajar;
			        $peminjam->lama_mengajar = $request->lama_mengajar;
			        $peminjam->gaji = $this->rupiah_to_int($request->gaji);
			        if(isset($request->alamat)) {
			        	$peminjam->alamat_tempat_kerja_id = $alamat_sekolah->id;	
			        } else {
			        	$peminjam->alamat_tempat_kerja_id = $user->peminjam->alamat_tempat_kerja_id;	
			        }

			        if ($request->hasFile('photo_tempat_kerja')) {
			            if ($request->file('photo_tempat_kerja')->isValid()) {
			                $request->file('photo_tempat_kerja')
			                ->storeAs('tempat_kerja','photo_tempat_kerja_'.$user->id.'.'.$request->file('photo_tempat_kerja')->extension());

			                if($peminjam->photo_tempat_kerja_id != ""){
			                    $file = FileUploads::find($peminjam->photo_tempat_kerja_id);
			                    if(file_exists(storage_path('app/public/tempat_kerja/'.$file->name))){
			                      unlink(storage_path('app/public/tempat_kerja/'.$file->name));
			                    }
			                }else{
			                    $file = new FileUploads();
			                }

			                $file->type = 'photo_tempat_kerja';
			                $file->name = 'photo_tempat_kerja_'.$peminjam->user_id.'.'.$request->file('photo_tempat_kerja')->extension();
			                $file->descriptions = "Photo Tempat Kerja";
			                $file->mime = $request->file('photo_tempat_kerja')->getMimeType();
			                $file->size = $request->file('photo_tempat_kerja')->getClientSize();
			                $file->save();

			                $peminjam->photo_tempat_kerja_id = $file->id;
			                $peminjam->save();
			            }
			        }
			        $peminjam->save();
				}

				return response()->json(['status' => 'success', 'message' => 'Data Pekerjaan berhasil diubah !'], 200);
			}
			return response()->json(['status' => 'error', 'message' => 'No User logged in'], 200);
		}

		public function updateKeuangan(Request $request) {
			if(!is_null(\Auth::guard('api')->user())) {
				$user = User::find(\Auth::guard('api')->user()->id);

				KeuanganPeminjam::where('peminjam_id', $user->peminjam->id)->where('type','pendapatan')->forceDelete();
		        for($i=0; $i < count($request->pendapatan_jumlah); $i++){
		            $pendapatan = new KeuanganPeminjam();
		            $pendapatan->peminjam_id = $user->peminjam->id;
		            $pendapatan->keterangan = $request->input('pendapatan_keterangan.'.$i);
		            $pendapatan->jumlah = $this->rupiah_to_int($request->input('pendapatan_jumlah.'.$i));
		            $pendapatan->type = 'pendapatan';
		            $pendapatan->save();
		        }

		        KeuanganPeminjam::where('peminjam_id', $user->peminjam->id)->where('type','pinjaman')->forceDelete();
		        for($i=0; $i < count($request->pinjaman_jumlah); $i++){
		            $pinjaman = new KeuanganPeminjam();
		            $pinjaman->peminjam_id = $user->peminjam->id;
		            $pinjaman->keterangan = $request->input('pinjaman_keterangan.'.$i);
		            $pinjaman->jumlah = $this->rupiah_to_int($request->input('pinjaman_jumlah.'.$i));
		            $pinjaman->type = 'pinjaman';
		            $pinjaman->save();
		        }

				return response()->json(['status' => 'success', 'message' => 'Data Keuangan berhasil diubah !'], 200);
			}
			return response()->json(['status' => 'error', 'message' => 'No User logged in'], 200);
		}

		public function updatePendukung(Request $request) 
		{
			if(!is_null(\Auth::guard('api')->user())) {
				$user = User::find(\Auth::guard('api')->user()->id);

				$peminjam = Peminjam::find($user->peminjam->id);
		        $peminjam->no_sk_surat_kontrak = $request->no_sk_surat_kontrak;
		        $peminjam->tanggal_sk_surat_kontrak = $request->tanggal_sk_surat_kontrak;
		        $peminjam->kepemilikan_rumah_id = $request->kepemilikan_rumah_id;
		        $peminjam->pendidikan_id = $request->pendidikan_id;
		        $peminjam->status_pernikahan_id = $request->status_pernikahan_id;
		        $peminjam->nama_gadis_ibu_kandung = $request->nama_gadis_ibu_kandung;
		        $peminjam->kontak_nama = $request->kontak_nama;
		        $peminjam->hubungan_kontak_id = $request->hubungan_kontak_id;
		        $peminjam->no_hp_kontak = $request->no_hp_kontak;
		        $peminjam->no_telp_kontak = $request->no_telp_kontak;
		        $peminjam->nama_suami_istri = $request->nama_suami_istri;
		        $peminjam->tempat_lahir_suami_istri = $request->tempat_lahir_suami_istri;
		        $peminjam->tanggal_lahir_suami_istri = $request->tanggal_lahir_suami_istri;
		        $peminjam->no_ktp_suami_istri = $request->no_ktp_suami_istri;
		        $peminjam->pekerjaan_suami_istri_id = $request->pekerjaan;
		        $peminjam->pendapatan_suami_istri = $this->rupiah_to_int($request->pendapatan);
		        $peminjam->jumlah_tanggungan = $request->jumlah_tanggungan;
		        $peminjam->save();

		        $alamat_kontak = Alamat::updateOrCreate(
		            ['id'=>$peminjam->alamat_kontak_id],
		            ['alamat'=>$request->alamat_rumah_kontak]
		        );

		        $peminjam->alamat_kontak_id = $alamat_kontak->id;
		        $peminjam->save();

		        $tempat_mengajar = array_filter($request->input('tempat_mengajar'));
		        $dari_tahun = array_filter($request->input('dari_tahun'));
		        $sampai_tahun = array_filter($request->input('sampai_tahun'));
		        if(count($tempat_mengajar) > 0){
		            PengalamanKerja::where('peminjam_id',$peminjam->id)->forceDelete();
		        }
		        for($i=0; $i < count($tempat_mengajar); $i++){
		            $pengalaman = new PengalamanKerja();
		            $pengalaman->dari_tahun = $dari_tahun[$i];
		            $pengalaman->sampai_tahun = $sampai_tahun[$i];
		            $pengalaman->tempat_kerja = $tempat_mengajar[$i];
		            $pengalaman->peminjam_id = $peminjam->id;
		            $pengalaman->save();
		        }

		        if ($request->hasFile('photo_sk_surat_kontak')) {
		            if ($request->file('photo_sk_surat_kontak')->isValid()) {
		                $request->file('photo_sk_surat_kontak')
		                ->storeAs('surat_sk','photo_sk_surat_kontak_'.$peminjam->user_id.'.'.$request->file('photo_sk_surat_kontak')->extension());

		                if($peminjam->photo_sk_surat_kontrak_id != ""){
		                    $file = FileUploads::find($peminjam->photo_sk_surat_kontrak_id);
		                    if(file_exists(storage_path('app/public/surat_sk/'.$file->name))){
		                      unlink(storage_path('app/public/surat_sk/'.$file->name));
		                    }
		                }else{
		                    $file = new FileUploads();
		                }

		                $file->type = 'photo_sk';
		                $file->name = 'photo_sk_surat_kontak_'.$peminjam->user_id.'.'.$request->file('photo_sk_surat_kontak')->extension();
		                $file->descriptions = "Photo Surat Kontrak";
		                $file->mime = $request->file('photo_sk_surat_kontak')->getMimeType();
		                $file->size = $request->file('photo_sk_surat_kontak')->getClientSize();
		                $file->save();

		                $peminjam->photo_sk_surat_kontrak_id = $file->id;
		                $peminjam->save();
		            }
		        }	
				
				return response()->json(['status' => 'success', 'message' => 'Data Pendukung berhasil diubah !'], 200);
			}
			return response()->json(['status' => 'error', 'message' => 'No User logged in'], 200);
		}
	}

?>