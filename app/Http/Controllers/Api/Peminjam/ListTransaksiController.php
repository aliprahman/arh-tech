<?php
	namespace App\Http\Controllers\Api\Peminjam;

	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\DB;
	use App\Http\Controllers\Controller;
	use Carbon\Carbon;
	use App\Transaksi;

	class ListTransaksiController extends Controller
	{
		public function index(Request $request)
		{
			$user = \Auth::guard('api')->user();
        	if(!is_null($user)) {
				$list = Transaksi::select(DB::raw('transaksi.tanggal, transaksi.kode_transaksi, ref AS no_transaksi, transaksi.deskripsi, transaksi.akun_bank_nama_bank, 
                            				transaksi.akun_bank_no_akun, transaksi.akun_bank_nama_akun, transaksi.jumlah, cicilan.tanggal AS tanggal_cicilan, cicilan.status, pinjaman.kode AS kode_pinjaman'))
		                				->join('cicilan','transaksi.id', '=', 'cicilan.transaksi_id')
		                				->join('pinjaman', 'cicilan.pinjaman_id', '=', 'pinjaman.id')
		                				->whereNull('pinjaman.deleted_at')
		                				->whereNull('cicilan.deleted_at')
		                				->where('pinjaman.peminjam_id', $user->peminjam->id)
		                				->get();

	            if($list) {
	            	foreach ($list as $row) {
	            		$row->tanggal = Carbon::parse($row->tanggal)->format('d-m-Y');
	            		$row->tanggal_cicilan = Carbon::parse($row->tanggal_cicilan)->format('d-m-Y');
	            		$row->status = ucwords($row->status);
	            	}
	            	return response()->json(['status' => 'success', 'data' => $list], 200);
	            }
	            return response()->json(['status' => 'error', 'message' => 'Error !'], 200);
	        }
	        return response()->json(['status' => 'error', 'message' => 'No User logged in', 'data' => $user], 200);
		}
	}

?>