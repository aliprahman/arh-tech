<?php

namespace App\Http\Controllers;

use App\Peminjam;
use App\Pendidikan;
use App\StatusPernikahan;
use App\KepemilikanRumah;
use App\HubunganKontak;
use App\Pekerjaan;
use App\Pinjaman;
use App\Pendanaan;
use App\Pendapatan;
use App\Deposit;
use App\WithDraw;
use App\Cicilan;
use App\Traits\Common;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Dashboard extends Controller
{
    use Common;

    public function index()
    {
        $data = array();

        if(Auth::user()->type == 'peminjam'){
            $data = $this->dashboard_peminjam();
        }elseif (Auth::user()->type == 'pemodal'){
            $data = $this->dashboard_pemodal();
        }else{
            $data = $this->dashboard_admin();
        }
        return view('mockup.dashboard',$data);
    }

    public function dashboard_peminjam()
    {
        $data['pop_up'] = false;
        if(str_contains(URL::previous(),'login') && !Peminjam::is_data_complete(Auth::user()->peminjam->id)){
            $data['pop_up'] = false; // always false karena sengaja disable
            $data['pendidikan'] = Pendidikan::pluck('id','nama');
            $data['status_pernikahan'] = StatusPernikahan::pluck('id','nama');
            $data['kepemilikan_rumah'] = KepemilikanRumah::pluck('id','nama');
            $data['hubungan_kontak'] = HubunganKontak::pluck('id','nama');
            $data['pekerjaan'] = Pekerjaan::pluck('id','nama');
        }
        $data['user'] = Auth::user();
        $data['peminjam_id'] = $this->encrypt_id(Auth::user()->peminjam->id);

        # data Dashboard
        $data['pengajuan_terdaftar'] = Pinjaman::where('peminjam_id',Auth::user()->peminjam->id)->count();
        $data['pengajuan_disetujui'] = Pinjaman::where('peminjam_id',Auth::user()->peminjam->id)
            ->whereIn('status',['listing','funding','transfer-funding','installment','paid','not paid'])->count();
        $data['nominal_terdaftar'] = Pinjaman::where('peminjam_id',Auth::user()->peminjam->id)->sum('jumlah_pinjaman');
        $data['nominal_disetujui'] = Pinjaman::where('peminjam_id',Auth::user()->peminjam->id)
            ->whereIn('status',['listing','funding','transfer-funding','installment','paid','not paid'])
            ->sum('jumlah_pinjaman');
        $pinjaman = Pinjaman::where('peminjam_id',Auth::user()->peminjam->id)
            ->where('status','funding')->first();
        if(isset($pinjaman)){
            $pinjaman_id = $pinjaman->id;
            $jumlah_pinjaman = $pinjaman->jumlah_pinjaman;
        }else{
            $pinjaman_id = 0;
            $jumlah_pinjaman = 0;
        }
        $cicilan = Cicilan::where('pinjaman_id',$pinjaman_id)->whereIn('status',['on time','late'])->sum('jumlah');
        $data['jumlah_pinjaman'] = $jumlah_pinjaman;
        $data['sisa_pinjaman'] = $jumlah_pinjaman - $cicilan;
        $data['cicilan'] = $cicilan;

        return $data;
    }

    public function dashboard_pemodal()
    {
        $data['user'] = Auth::user();
        $data['saldo']= Auth::user()->pemodal->saldo;
        $data['pendanaan'] = Pendanaan::where('pemodal_id',Auth::user()->pemodal->id)->sum('jumlah');
        $data['pendapatan'] = Pendapatan::join('funding','pendapatan.funding_id','=','funding.id')->where('pemodal_id',Auth::user()->pemodal->id)->sum('pendapatan.jumlah');

        $start = $month = strtotime(date('Y').'-01-01');
        $end = strtotime(date('Y').'-12-01');
        while($month <= $end)
        {
            $data['chart_label'][] = date('M', $month);
            $data['chart_data'][] = (int)Pendapatan::join('funding','pendapatan.funding_id','=','funding.id')
                  ->where('pemodal_id',Auth::user()->pemodal->id)
                  ->whereMonth('pendapatan.created_at',date('m',$month))
                  ->whereYear('pendapatan.created_at',date('Y'))
                  ->sum('pendapatan.jumlah');
            $month = strtotime("+1 month", $month);
        }

        $data['deposit_terakhir'] = Deposit::select('jumlah')
          ->where('deposit.pemodal_id',Auth::user()->pemodal->id)
          ->where('deposit.status','verified')
          ->latest()
          ->first();

        $data['pendanaan_terakhir'] = Pendanaan::select('jumlah')
          ->where('pemodal_id',Auth::user()->pemodal->id)
          ->latest()
          ->first();

        $data['penarikan_terakhir'] = WithDraw::select('jumlah')
          ->where('pemodal_id',Auth::user()->pemodal->id)
          ->where('status','verified')
          ->latest()
          ->first();

        return $data;
    }

    public function dashboard_admin()
    {
        $data['user'] = Auth::user();
        $data['deposit_unverified'] = Deposit::where('status','unverified')->count();
        $data['deposit_proses'] = Deposit::where('status','proses')->count();
        $data['deposit_verified'] = Deposit::where('status','verified')->count();
        $data['withdraw_unverified'] = WithDraw::where('status','unverified')->count();
        $data['withdraw_verified'] = WithDraw::where('status','verified')->count();
        $data['pinjaman_submit'] = Pinjaman::where('status','submit')->count();
        $data['pinjaman_reject'] = Pinjaman::where('status','reject')->count();
        $data['pinjaman_analysis'] = Pinjaman::where('status','analysis')->count();
        $data['pinjaman_listing'] = Pinjaman::where('status','listing')->count();
        $data['pinjaman_funding'] = Pinjaman::where('status','funding')->count();
        $data['pinjaman_installment'] = Pinjaman::where('status','installment')->count();
        $data['pinjaman_paid'] = Pinjaman::where('status','paid')->count();
        $data['pendanaan_total'] = Pendanaan::sum('jumlah');
        $data['pendanaan_jumlah'] = Pendanaan::count();
        return $data;
    }

}
