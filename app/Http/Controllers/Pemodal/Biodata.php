<?php

namespace App\Http\Controllers\Pemodal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Provinsi;
use App\Bank;
use App\Pemodal;
use App\Alamat;
use App\FileUploads;
use App\User;
use App\AkunBank;
use App\Perusahaan;
use App\Biodata as BiodataModel;
use App\Traits\Common;
use Auth;
use Hash;
use Session;
use File;

class Biodata extends Controller
{
    //
    use Common;

    public function index()
    {
        $data['list_provinsi'] = Provinsi::pluck('id','name');
        $data['list_bank'] = Bank::pluck('id','nama');
        $data['user'] = Auth::user();
        $data['pemodal_id'] =  $this->encrypt_id(Auth::user()->pemodal->id);

        return view('mockup.pemodal.biodata',$data);
    }
    //old
    public function old(Request $request)
    {
        $password=Auth::user()->password;

        if (Hash::check($request->password , $password))
        {
            echo 1;
        }else{
            echo 0;
        }
    }
    
    public function update(Request $request)
    {
        $user = Auth::user();

        $biodata = BiodataModel::find($user->biodata_id);
        $biodata->tempat_lahir = $request->tempat_lahir;
        $biodata->tanggal_lahir= $request->tanggal_lahir;
        $biodata->jenis_kelamin= $request->jenis_kelamin;
        $biodata->no_telepon   = $request->home;
        $biodata->no_hp        = $request->mobile;
        $biodata->no_ktp       = $request->ktp;
        $biodata->no_npwp      = $request->npwp;
        $biodata->save();

        $user = User::find($user->id);
        $user->name       = $request->nama;
        $user->email      = $request->email;

        if($request->hasFile('photo_profile') && $request->file('photo_profile')->isValid()){

            if($user->photo_profile_id != ""){
                $file = FileUploads::find($user->photo_profile_id);
                if(file_exists(storage_path('app/public/profile_photo/'.$file->name))){
                  unlink(storage_path('app/public/profile_photo/'.$file->name));
                }
            } else {
                $file = new FileUploads();
            }
            $request->file('photo_profile')->storeAs('profile_photo','pemodal_profile_photo_'.$user->id.'.'.$request->file('photo_profile')->extension());

            $file->type = 'profile_photo';
            $file->name = 'pemodal_profile_photo_'.$user->id.'.'.$request->file('photo_profile')->extension();
            $file->descriptions = "Photo Profile";
            $file->mime = $request->file('photo_profile')->getMimeType();
            $file->size = $request->file('photo_profile')->getClientSize();
            $file->save();

            $user->photo_profile_id = $file->id;
        }   
        $user->save();

        $alamat_diri = Alamat::find($user->pemodal->alamat_id);
        $alamat_diri->alamat      = $request->alamat;
        $alamat_diri->kode_pos    = $request->kode_pos;
        $alamat_diri->province_id = $request->prov_1;
        $alamat_diri->regency_id  = $request->kota_1;
        $alamat_diri->district_id = $request->kec_1;
        $alamat_diri->village_id  = $request->kel_1;
        $alamat_diri->save();

        $akun_bank = AkunBank::find($user->pemodal->akun_bank_id);
        $akun_bank->bank_id        = $request->bank;
        $akun_bank->nama_akun_bank = $request->nama_akun;
        $akun_bank->no_akun_bank   = $request->no_rekening;
        $akun_bank->save();

        if (isset($user->pemodal->perusahaan)) {
          $perusahaan = Perusahaan::find($user->pemodal->perusahaan->id);
          $perusahaan->nama = $request->n_perusahaan;
          $perusahaan->save();
        }else{
          $perusahaan = new Perusahaan();
          $perusahaan->nama = $request->n_perusahaan;
          $perusahaan->save();
          Pemodal::where('user_id',$user->id)->update(['perusahaan_id'=>$perusahaan->id]);
        }



    }
    public function redirect()
    {
        Session::flash("notif",[
            "level"=>"success",
            "title"=>"Berhasil",
            "message"=>"Profile pengguna di perbaharui"
        ]);

        return redirect('/pemodal/biodata');
    }

    public function password(Request $request)
    {
        $user = Auth::user();

        $user->password = bcrypt($request->new);
        $user->save();
    }
}
