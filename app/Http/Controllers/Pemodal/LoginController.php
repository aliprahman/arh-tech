<?php

namespace App\Http\Controllers\Pemodal;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/pemodal/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('user-should-verified');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function LoginForm(Request $request)
    {
        User::where('remember_token',$request->query('token'))->update(
          [
            'status' => 'confirmed'
          ]
        );

        return view('mockup.pemodal.login');
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        if($user->status == 'deleted'){
            $this->guard()->logout();
            $request->session()->invalidate();
            $request->session()->flash('status','Your Account Has Been Deleted, Contact Administrator');
            return redirect()->back();
        }

        return redirect($this->redirectTo);
    }
}
