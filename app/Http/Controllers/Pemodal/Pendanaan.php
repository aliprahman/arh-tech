<?php

namespace App\Http\Controllers\Pemodal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use App\Pendanaan as Funding;
use App\Pinjaman;
use App\Cicilan;
use App\Traits\Common;
use Mpdf\Mpdf;

class Pendanaan extends Controller
{
    use Common;

    public function index()
    {
      return view('mockup.pemodal.daftarpendanaan');
    }

    public function cicilan($pinjaman_id)
    {
      $real_id = $this->decrypt_id($pinjaman_id);
      $data['pinjaman'] = Pinjaman::findOrFail($real_id);
      $data['sisa_cicilan'] = Pinjaman::sisa_pinjaman($real_id);
      $data['total_cicilan'] = Cicilan::where('pinjaman_id',$real_id)->sum('jumlah');
      $data['total_pendanaan'] = Funding::where('pinjaman_id',$real_id)->where('pemodal_id',auth()->user()->pemodal->id)->sum('jumlah');
      $data['cicilan'] = Cicilan::select('cicilan.id','cicilan.jumlah','cicilan.tanggal AS jatuh_tempo','transaksi.tanggal','ref','cicilan.status','transaksi.id AS transaksi_id','pendapatan.jumlah AS pendapatan')
          ->join('pendapatan','cicilan.id','=','pendapatan.cicilan_id')
          ->leftjoin('transaksi','cicilan.transaksi_id','=','transaksi.id')
          ->where('pinjaman_id',$real_id)
          ->get();
      return view('mockup.pemodal.cicilan',$data);
    }

    public function export_cicilan($id)
    {
      $real_id = $this->decrypt_id($id);
      $cicilan = Cicilan::select('cicilan.id','cicilan.jumlah','cicilan.tanggal AS jatuh_tempo','transaksi.tanggal','ref','cicilan.status','transaksi.id AS transaksi_id','pendapatan.jumlah AS pendapatan')
          ->where('pinjaman_id',$real_id)
          ->leftjoin('transaksi','cicilan.transaksi_id','=','transaksi.id')
          ->leftjoin('pendapatan','cicilan.id','=','pendapatan.cicilan_id')
          ->get();

          $viewPdf = view('export.daftar_cicilan',compact('cicilan'));

          $mpdf = new Mpdf([
              'mode' => 'utf-8',
              'format' => 'A4',
              'orientation' => 'L'
          ]);

          $mpdf->WriteHTML($viewPdf);
          $mpdf->Output("Daftar_Cicilan.pdf","I");

    }

    public function daftarpendanaan(Request $request)
    {
      $startDate = date_format(date_create($request->startDate." 00:00:00"),'Y-m-d H:i:s');
      $endDate = date_format(date_create($request->endDate." 23:59:59"),'Y-m-d H:i:s');

      $pinjaman = Pinjaman::select('funding.tanggal','kode','pinjaman_id','jumlah_pinjaman',DB::raw('SUM(funding.jumlah) AS jumlah_pendanaan'),'waktu_pinjaman','bunga','status')
            ->join('funding','pinjaman.id','=','funding.pinjaman_id')
            ->where('pemodal_id',$request->user()->pemodal->id)
            ->whereBetween('funding.tanggal',[$startDate,$endDate])
            ->groupBy('pinjaman_id')
            ->groupBy('funding.tanggal')
            ->get();

      return Datatables::of($pinjaman)
          ->addIndexColumn()
          ->addColumn('action',null)
          ->editColumn('pinjaman_id', function($pinjaman){
              return $this->encrypt_id($pinjaman->pinjaman_id);
          })
          ->editColumn('bunga', function($pinjaman){
            return str_replace('.',',',($pinjaman->bunga+0)).' %';
          })
          ->make(true);
    }

    public function export_pendanaan(Request $request)
    {
        if ($request->has('startDate')) {
            $startDate = date('Y-m-')."01";
        }else{
            $startDate = $request->query('startDate');
        }

        if ($request->has('endDate')) {
            $endDate = date('Y-m-t');
        }else{
            $endDate = $request->query('endDate');
        }

        $startDateTime = date_format(date_create($startDate." 00:00:00"),'Y-m-d H:i:s');
        $endDateTime = date_format(date_create($endDate." 23:59:59"),'Y-m-d H:i:s');

        $pinjaman = Pinjaman::select('funding.tanggal','kode','pinjaman_id','jumlah_pinjaman',DB::raw('SUM(funding.jumlah) AS jumlah_pendanaan'),'waktu_pinjaman','bunga','status')
            ->join('funding','pinjaman.id','=','funding.pinjaman_id')
            ->where('pemodal_id',$request->user()->pemodal->id)
            ->whereBetween('funding.tanggal',[$startDateTime,$endDateTime])
            ->groupBy('pinjaman_id')
            ->groupBy('funding.tanggal')
            ->get();

        $viewPdf = view('export.daftar_pendanaan',compact('pinjaman'));

        $mpdf = new Mpdf([
            'mode' => 'utf-8',
            'format' => 'A4',
            'orientation' => 'L'
        ]);

        $mpdf->WriteHTML($viewPdf);
        $mpdf->Output("Daftar_Pendanaan.pdf","I");
    }
}
