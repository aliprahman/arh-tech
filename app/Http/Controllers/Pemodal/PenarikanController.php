<?php

namespace App\Http\Controllers\Pemodal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use App\Mail\RequestWithDraw;
use App\Traits\Common;
use App\Transaksi;
use App\WithDraw;
use App\User;

class PenarikanController extends Controller
{
    //
    use Common;

    public function index()
    {
      return view('mockup.pemodal.penarikan');
    }

    public function list()
    {
    	$wdraw = WithDraw::select('id','transaksi_id','tanggal','jumlah','status','created_at');

        return Datatables::of($wdraw)
        ->addIndexColumn()
        ->editColumn('id', function($wdraw){
            return $this->encrypt_id($wdraw->id);
        })
        ->addColumn('no_transaksi', function($wdraw){
            return $wdraw->transaksi->kode_transaksi.".".$wdraw->transaksi->id;
        })
        ->addColumn('nama_bank', function($wdraw){
            return $wdraw->transaksi->akun_bank_nama_bank;
        })
        ->addColumn('rekening', function($wdraw){
            return $wdraw->transaksi->akun_bank->no_akun_bank;
        })
        ->editColumn('status', function($wdraw){
            if($wdraw->status == "unverified"){
                return '<center><font color="red"><b>VERIFIKASI</b></font></center>';
            }else{
                return '<center><b>TRANSAFER</b></center>';
            }
        })
        ->rawColumns(['status'])
        ->make(true);
    }
    public function order(Request $request)
    {

        $rp = explode(',',$request->jumlah);
        $int = str_replace('.','',$rp[0]);

        //query generate transaksi_id
        $number        = "0000";
        $date          = date('ym');
        $count         = Transaksi::where('id','LIKE','%'.$date.'%')->count();
        $id_transaksi  = $date.$number + $count + 1;

    	  $transaksi = new Transaksi;
        $transaksi->id = $id_transaksi;
        $transaksi->kode_transaksi = "03";
        $transaksi->jumlah = $int;
        $transaksi->akun_bank_id= Auth::user()->pemodal->akun_bank->bank_id;
        $transaksi->deskripsi= $request->keterangan;
    	  $transaksi->save();

    	  $deposit = new WithDraw;
        $deposit->pemodal_id = $request->user()->pemodal->id;
        $deposit->transaksi_id = $transaksi->id;
        $deposit->tanggal = date('Y-m-d H:i:s');
        $deposit->status = 'unverified';
        $deposit->jumlah = $int;
        $deposit->save();

        $pemodal = User::find($request->user()->id);

        # send email notification request withdraw to admin
        $admin = User::where('type','finnesia')->where('status','confirmed')->get();
        Mail::to($admin)->send(new RequestWithDraw($deposit,$pemodal,$transaksi));
    }
}
