<?php

namespace App\Http\Controllers\Pemodal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Kota;
use App\Desa;
use App\Bank;
use App\Alamat;
use App\Biodata;
use App\Pemodal;
use App\Provinsi;
use App\AkunBank;
use App\Kecamatan;
use App\Perusahaan;
use Session;
use Illuminate\Support\Facades\Mail;
use Validator;
//testing
use App\Transaksi;
use App\Deposit;

use Auth;

class RegisterController extends Controller
{
    public function RegistrasiForm()
    {
        $data['list_provinsi'] = Provinsi::pluck('id','name');
        $data['list_kota'] = Kota::pluck('name');
        $data['list_bank'] = Bank::pluck('id','nama');

        return view('mockup.pemodal.register_pemodal',$data);
    }
    //validasi
    public function captcha(Request $request)
    {
        $secret_key = env('CAPTCHA_SECRET');
        if ($request->captcha != '') {
           $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secret_key) . '&response=' . $request->captcha;
           $recaptcha = file_get_contents($url);
           $recaptcha = json_decode($recaptcha, true);
           if (!$recaptcha['success']) {
              echo "1";
           } else {
              echo "0";
           }
        } else {
          echo "1";

        }
    }
    public function validasiemail(Request $request)
    {
    	$email = User::where('email',$request->email)->count();
        if(Auth::check() && Auth::user()->email){
            if($request->email === Auth::user()->email) {
                echo "0";
            }else if($email > 0){
                echo "1";
            }
        }else{
            if ($email > 0) {
                echo "1";
            }else{
                echo "0";
            }
        }

    }
    public function validasiktp(Request $request)
    {
        $ktp = Biodata::where('no_ktp',$request->no_ktp)->count();
        if(Auth::check() && Auth::user()->biodata->no_ktp){
            if($request->no_ktp === Auth::user()->biodata->no_ktp ) {
                echo "0";
            }else if($ktp > 0){
                echo "1";
            }
        }else{
            if ($ktp > 0) {
                echo "1";
            }else{
                echo "0";
            }
        }
    }
    public function validasinpwp(Request $request)
    {
        $npwp = Biodata::where('no_npwp',$request->no_npwp)->count();
        if(Auth::check() && Auth::user()->biodata->no_npwp){
            if($request->no_npwp === Auth::user()->biodata->no_npwp ) {
                echo "0";
            }else if($npwp > 0){
                echo "1";
            }
        }else{
            if ($npwp > 0) {
                echo "1";
            }else{
                echo "0";
            }
        }
    }

    public function image(Request $request)
    {
        $validator = Validator::make($request->only('photo_profile'),
            ['photo_profile' => 'mimes:jpeg,jpg,png,bmp,svg']
        );

        if ($validator->fails())
        {
            echo "1";
        }else{
            echo "0";
        }
    }
    public function validasimobile(Request $request)
    {
        $no_hp = Biodata::where('no_hp',$request->no_hp)->count();
        if(Auth::check() && Auth::user()->biodata->no_hp){
            if($request->no_hp === Auth::user()->biodata->no_hp ) {
                echo "0";
            }else if($no_hp > 0){
                echo "1";
            }
        }else{
            if ($no_hp > 0) {
                echo "1";
            }else{
                echo "0";
            }
        }
    }
    //registrasi pemodal
    public function register(Request $request)
    {

        //query generate id_pelanggan
        $number        = "000";
        $date          = date('ym');
        $count         = User::where('id_pelanggan','LIKE','%'.$date.'%')->count();
        $id_pelanggan  = $date.$number + $count + 1;

        $biodata = new Biodata();
        $biodata->tempat_lahir = $request->tempat_lahir;
        $biodata->tanggal_lahir= $request->tanggal_lahir;
        $biodata->jenis_kelamin= $request->jenis_kelamin;
        $biodata->no_telepon   = $request->home;
        $biodata->no_hp        = $request->mobile;
        $biodata->no_ktp       = $request->ktp;
        $biodata->no_npwp      = $request->npwp;
        $biodata->save();

        $user = new User();
        $user->name       = $request->nama;
        $user->email      = $request->email;
        $user->password   = bcrypt($request->password);
        $user->type       = 'pemodal';
        $user->role_id    = '3';
        $user->id_pelanggan = $id_pelanggan;
        $user->biodata_id = $biodata->id;
        $user->status     = 'new';
        $user->remember_token = str_random(24);
        //verifikasi
        $user->verification_token = str_random(40);
        $user->save();

        $alamat = new Alamat();
        $alamat->alamat      = $request->alamat;
        $alamat->kode_pos    = $request->kode_pos;
        $alamat->province_id = $request->prov_1;
        $alamat->regency_id  = $request->kota_1;
        $alamat->district_id = $request->kec_1;
        $alamat->village_id  = $request->kel_1;
        $alamat->save();

        $akun_bank = new AkunBank();
        $akun_bank->bank_id        = $request->bank;
        $akun_bank->nama_akun_bank = $request->nama_akun;
        $akun_bank->no_akun_bank   = $request->no_rekening;
        $akun_bank->save();

        $perusahaan = new Perusahaan();
        $perusahaan->nama        = $request->n_perusahaan;
        $perusahaan->save();

        $pemodal = new Pemodal();
        $pemodal->user_id                = $user->id;
        $pemodal->type                   = $request->jenis;
        $pemodal->alamat_id              = $alamat->id;
        $pemodal->akun_bank_id           = $akun_bank->id;
        $pemodal->perusahaan_id          = $perusahaan->id;
        $pemodal->status                 = 'unverified';
        $pemodal->save();

        $token = $user->verification_token;
        Mail::send('mails.verification', compact('user','token'), function ($m) use ($user){
            $m->to($user->email, $user->name)->subject('Verifikasi Akun Pemodal Finnesia');
        });
    }
    public function verify(Request $request, $token)
    {
        $email = $request->email;
        if($user = User::where('verification_token', $token)->where('email', $email)->count() == 0){
            return redirect("/login");
        }else{
            $user = User::where('verification_token', $token)->where('email', $email)->first();
            $user->status = "confirmed";
            $user->verification_token = null;
            $user->save();

            session()->put('actived','');
            return redirect("/login");
        }

    }
    public function resend(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if ($user && $user->status === "new") {
             //verifikasi
            $token = str_random(40);
            $user->verification_token = $token;
            $user->save();
            Mail::send('mails.verification', compact('user','token'), function ($m) use ($user){
                $m->to($user->email, $user->name)->subject('Verifikasi Akun Pemodal Finnesia');
            });
            session()->put('resend','');
        }
        return redirect("/login");
    }

    public function test()
    {
        if (Transaksi::all()->count() == 0) {
            $kode = "01";
        }else{
            $deposit = Deposit::where('pemodal_id',Auth::user()->pemodal->id)->count();
            if($deposit > 0){
                $k = Transaksi::where('id',Deposit::where('pemodal_id',Auth::user()->pemodal->id)->first()->transaksi_id)->first();
                $kode = $k->kode_transaksi;
            }else{
                $number        = date('d');
                $kode  = "0"+$number;
            }

        }

    }
}
