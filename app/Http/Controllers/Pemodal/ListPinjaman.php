<?php

namespace App\Http\Controllers\Pemodal;

use App\Pendanaan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use App\Pinjaman;
use App\Peminjam;
use App\Cicilan;
use App\Traits\Common;
use App\Pemodal;
use App\Transaksi;

class ListPinjaman extends Controller
{
    use Common;

    public function cari_pinjaman()
    {
      return view('mockup.pemodal.cari');
    }

    public function pinjaman(Request $request)
    {
    	$pinjaman = Pinjaman::select('id','kode','jumlah_pinjaman','waktu_pinjaman','bunga','tanggal_mulai_funding',
                'tanggal_berakhir_funding','status','jumlah_funding AS jumlah_funding')
            ->whereIn('status',['listing','funding'])
            ->whereRaw('jumlah_pinjaman > IFNULL(jumlah_funding,0)')
            ->get();

      return Datatables::of($pinjaman)
        ->addIndexColumn()
        ->addColumn('progres', function($pinjaman){
            return ($pinjaman->jumlah_funding / $pinjaman->jumlah_pinjaman) * 100;
        })
        ->editColumn('id', function($pinjaman){
                    return $this->encrypt_id($pinjaman->id);
                })
        ->editColumn('waktu_pinjaman',function($pinjaman){
            return $pinjaman->waktu_pinjaman." Bulan";
        })
        ->editColumn('bunga',function($pinjaman){
            return round($pinjaman->bunga,1)." %";
        })
        ->addColumn('jumlah_tersisa', function ($pinjaman){
            return $pinjaman->jumlah_pinjaman - $pinjaman->jumlah_funding;
        })
        ->addColumn('order','')
        ->make(true);
    }

    public function konfirm_pinjaman(Request $request)
    {
      $list_pendanaan = array();
      for($i=0; $i < $request->total_pinjaman; $i++){
        // pinjaman
        $pinjaman = Pinjaman::find( $request->input('id_pinjaman.'.$i));

        //query generate transaksi_id
        $number             = "0000";
        $date               = date('ym');
        $count              = Transaksi::where('id','LIKE','%'.$date.'%')->count();
        $referensi          = $date.$number + $count + 1;

        // simpan transaksi
        $transaksi = new Transaksi();
        $transaksi->tanggal = date('Y-m-d H:i:s');
        $transaksi->jumlah =  $request->input('dana_pinjaman.'.$i);
        $transaksi->kode_transaksi = "04";
        $transaksi->deskripsi = "pendanaan";
        $transaksi->ref = $referensi;
        $transaksi->akun_bank_id = $pinjaman->peminjam->akun_bank_id;
        $transaksi->save();

        // simpan pendanaan
        $pendanaan = new Pendanaan();
        $pendanaan->pemodal_id = $request->user()->pemodal->id;
        $pendanaan->pinjaman_id = $request->input('id_pinjaman.'.$i);
        $pendanaan->transaksi_id = $transaksi->id;
        $pendanaan->jumlah = $request->input('dana_pinjaman.'.$i);
        $pendanaan->tanggal = date('Y-m-d H:i:s');
        $pendanaan->save();

        // calcute total pendanaan per id pinjaman
        $total_pendanaan = Pendanaan::where('pinjaman_id', $request->input('id_pinjaman.'.$i))->sum('jumlah');
        $pinjaman->jumlah_funding = $total_pendanaan;
        $pinjaman->save();

        $list_pendanaan[$request->input('id_pinjaman.'.$i)] = $request->input('dana_pinjaman.'.$i);

        // jika pendanaan telah 100% update tanggal cicilan
        if($pinjaman->jumlah_funding >= $pinjaman->jumlah_pinjaman){
          $cicilan = Cicilan::where('pinjaman_id',$request->input('id_pinjaman.'.$i))->get();
          $time = strtotime('+1 month',strtotime(date("Y-m-d")));
          $start_cicilan = date('Y-m-d', $time);
          for ($i=0; $i < count($cicilan); $i++) {
            $date = date('Y-m-d', $time);
            Cicilan::where('id',$cicilan[$i]->id)->update(['tanggal'=>$date]);
            $time = strtotime('+1 month', $time);
          }
          $end_cicilan = date('Y-m-d',strtotime('-1 month', $time));
          // update status pinjaman menjadi cicilan
          $pinjaman->tanggal_mulai_cicilan = $start_cicilan;
          $pinjaman->tanggal_berakhir_cicilan = $end_cicilan;
          $pinjaman->status = "installment";
          $pinjaman->save();
        }
      }

      // update saldo pemodal
      $pemodal = Pemodal::find($request->user()->pemodal->id);
      $pemodal->saldo = $pemodal->saldo - $request->total_nilai_pendanaan;
      $pemodal->save();

      $pinjaman = Pinjaman::select('id','kode','jumlah_pinjaman','waktu_pinjaman','bunga','tanggal_mulai_funding',
            'tanggal_berakhir_funding','status','jumlah_funding AS jumlah_funding')
          ->whereIn('id',$request->input('id_pinjaman'))
          ->get();

      $data['list_pinjaman'] = $pinjaman;
      $data['list_pendanaan'] = $list_pendanaan;
      $data['total_pendanaan'] = $request->total_nilai_pendanaan;
      $data['jumlah_pinjaman'] = $request->total_pinjaman;
      return view('mockup.pemodal.konfirm',$data);
    }

    public function detail_pinjaman($pinjaman_id)
    {
        $real_id = $this->decrypt_id($pinjaman_id);

        $data['pinjaman'] = Pinjaman::find($real_id);

        return view('mockup.pemodal.portofolio',$data);
    }
}
