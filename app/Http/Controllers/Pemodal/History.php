<?php

namespace App\Http\Controllers\Pemodal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use App\Deposit;
use App\WithDraw;
use App\Pendanaan;
use App\Transaksi;
use Mpdf\Mpdf;

class History extends Controller
{
    public function index()
    {
      return view('mockup.pemodal.history');
    }

    public function resume($periode)
    {
      $pemodal_id = Auth::user()->pemodal->id;
      $list_bulan = array(
        'January'   => '01',
        'February'  => '02',
        'March'     => '03',
        'April'     => '04',
        'May'       => '05',
        'June'      => '06',
        'July'      => '07',
        'August'    => '08',
        'September' => '09',
        'October'   => '10',
        'November'  => '11',
        'December'  => '12'
      );
      $data = [];
      if($periode != ""){
        $exlude = explode('-',$periode);
        $bulan = $list_bulan[$exlude[0]];
        $tahun = $exlude[1];

        $data['jml_pendanaan'] = Pendanaan::where('pemodal_id',Auth::user()->pemodal->id)
          ->whereMonth('tanggal',$bulan)
          ->whereYear('tanggal',$tahun)
          ->count();
        $data['jml_deposit']   = Deposit::where('pemodal_id',Auth::user()->pemodal->id)
          ->whereMonth('tanggal',$bulan)
          ->whereYear('tanggal',$tahun)
          ->where('status','verified')
          ->count();
        $data['jml_penarikan'] = WithDraw::where('pemodal_id',Auth::user()->pemodal->id)
          ->whereMonth('tanggal',$bulan)
          ->whereYear('tanggal',$tahun)
          ->where('status','verified')
          ->count();

        $data['total_deposit'] = Deposit::where('pemodal_id',Auth::user()->pemodal->id)
          ->whereMonth('tanggal',$bulan)
          ->whereYear('tanggal',$tahun)
          ->sum('jumlah');

        $data['total_pendanaan'] = Pendanaan::where('pemodal_id',Auth::user()->pemodal->id)
          ->whereMonth('tanggal',$bulan)
          ->whereYear('tanggal',$tahun)
          ->sum('jumlah');

        $data['total_penarikan'] = WithDraw::where('pemodal_id',Auth::user()->pemodal->id)
          ->whereMonth('tanggal',$bulan)
          ->whereYear('tanggal',$tahun)
          ->sum('jumlah');

        $deposit_terahir = Deposit::where('pemodal_id',$pemodal_id)
          ->whereMonth('tanggal','<',$bulan)
          ->whereYear('tanggal',$tahun)
          ->where('status','verified')
          ->sum('jumlah');

        $pendanaan_terakhir = Pendanaan::where('pemodal_id',$pemodal_id)
          ->whereMonth('tanggal','<',$bulan)
          ->whereYear('tanggal',$tahun)
          ->sum('jumlah');

        $penarikan_terakhir = WithDraw::where('pemodal_id',$pemodal_id)
          ->whereMonth('tanggal','<',$bulan)
          ->whereYear('tanggal',$tahun)
          ->where('status','verified')
          ->sum('jumlah');

        $saldo_awal = $deposit_terahir - $pendanaan_terakhir - $penarikan_terakhir;
        $saldo_akhir = $saldo_awal + $data['total_deposit'] - $data['total_pendanaan'] - $data['total_penarikan'];

        $data['saldo_awal'] = $saldo_awal;
        $data['saldo_akhir']= $saldo_akhir;

      }

      return response()->json($data);
    }

    public function list(Request $request)
    {
      $pemodal_id = $request->user()->pemodal->id;
      $list_bulan = array(
        'January'   => '01',
        'February'  => '02',
        'March'     => '03',
        'April'     => '04',
        'May'       => '05',
        'June'      => '06',
        'July'      => '07',
        'August'    => '08',
        'September' => '09',
        'October'   => '10',
        'November'  => '11',
        'December'  => '12'
      );
      $exlude = explode('-',$request->periode);
      $bulan = $list_bulan[$exlude[0]];
      $tahun = $exlude[1];

      $deposit_terahir = Deposit::where('pemodal_id',$pemodal_id)
        ->whereMonth('tanggal','<',$bulan)
        ->whereYear('tanggal',$tahun)
        ->where('status','verified')
        ->sum('jumlah');

      $pendanaan_terakhir = Pendanaan::where('pemodal_id',$pemodal_id)
        ->whereMonth('tanggal','<',$bulan)
        ->whereYear('tanggal',$tahun)
        ->sum('jumlah');

      $penarikan_terakhir = WithDraw::where('pemodal_id',$pemodal_id)
        ->whereMonth('tanggal','<',$bulan)
        ->whereYear('tanggal',$tahun)
        ->where('status','verified')
        ->sum('jumlah');

      $saldo_awal = $deposit_terahir - $pendanaan_terakhir - $penarikan_terakhir;

      $kode_transaksi = array(
        '02' => 'Deposit',
        '03' => 'Penarikan',
        '04' => 'Pendanaan'
      );

      $transaksi = Transaksi::select('transaksi.id','kode_transaksi','transaksi.tanggal','funding.jumlah AS jml_funding','deposit.jumlah AS jml_deposit','withdraw.jumlah AS jml_withdraw')
        ->leftjoin('funding', function($join)use($pemodal_id){
          $join->on('transaksi.id','=','funding.transaksi_id')
            ->where('funding.pemodal_id',$pemodal_id);
        })
        ->leftjoin('deposit', function($join)use($pemodal_id){
          $join->on('transaksi.id','=','deposit.transaksi_id')
            ->where('deposit.pemodal_id',$pemodal_id)
            ->where('deposit.status','=','verified');
        })
        ->leftjoin('withdraw', function($join)use($pemodal_id){
          $join->on('transaksi.id','=','withdraw.transaksi_id')
            ->where('withdraw.pemodal_id',$pemodal_id)
            ->where('withdraw.status','=','verified');
        })
        ->whereMonth('transaksi.tanggal',$bulan)
        ->whereYear('transaksi.tanggal',$tahun)
        ->orderBy('transaksi.tanggal')
        ->get();

      $history = [];
      for ($i=0; $i <count($transaksi) ; $i++) {

        if($i==0){
          $saldo = $saldo_awal;
        }

        switch ($transaksi[$i]->kode_transaksi) {
          case '02':
            $debit = $transaksi[$i]->jml_deposit;
            $kredit = 0;
            break;

          case '03':
            $debit = 0;
            $kredit = $transaksi[$i]->jml_withdraw;
            break;

          case '04':
            $debit = 0;
            $kredit = $transaksi[$i]->jml_funding;
            break;
        }

        $saldo = $saldo + $debit - $kredit;

        $history[] = array(
          'tanggal'    => date_format(date_create($transaksi[$i]->tanggal),'d-m-Y H:i:s'),
          'keterangan' => $transaksi[$i]->kode_transaksi.".".$transaksi[$i]->id." - ".$kode_transaksi[$transaksi[$i]->kode_transaksi],
          'debit'      => $debit,
          'kredit'     => $kredit,
          'saldo'      => $saldo
        );
      }

      return Datatables::of(collect($history))->make(true);
    }

    public function export_pdf(Request $request)
    {
      $pemodal_id = $request->user()->pemodal->id;
      $list_bulan = array(
        'January'   => '01',
        'February'  => '02',
        'March'     => '03',
        'April'     => '04',
        'May'       => '05',
        'June'      => '06',
        'July'      => '07',
        'August'    => '08',
        'September' => '09',
        'October'   => '10',
        'November'  => '11',
        'December'  => '12'
      );
      $exlude = explode('-',$request->periode);
      $bulan = $list_bulan[$exlude[0]];
      $tahun = $exlude[1];

      $deposit_terahir = Deposit::where('pemodal_id',$pemodal_id)
        ->whereMonth('tanggal','<',$bulan)
        ->whereYear('tanggal',$tahun)
        ->where('status','verified')
        ->sum('jumlah');

      $pendanaan_terakhir = Pendanaan::where('pemodal_id',$pemodal_id)
        ->whereMonth('tanggal','<',$bulan)
        ->whereYear('tanggal',$tahun)
        ->sum('jumlah');

      $penarikan_terakhir = WithDraw::where('pemodal_id',$pemodal_id)
        ->whereMonth('tanggal','<',$bulan)
        ->whereYear('tanggal',$tahun)
        ->where('status','verified')
        ->sum('jumlah');

      $saldo_awal = $deposit_terahir - $pendanaan_terakhir - $penarikan_terakhir;

      $kode_transaksi = array(
        '02' => 'Deposit',
        '03' => 'Penarikan',
        '04' => 'Pendanaan'
      );

      $transaksi = Transaksi::select('transaksi.id','kode_transaksi','transaksi.tanggal','funding.jumlah AS jml_funding','deposit.jumlah AS jml_deposit','withdraw.jumlah AS jml_withdraw')
        ->leftjoin('funding', function($join)use($pemodal_id){
          $join->on('transaksi.id','=','funding.transaksi_id')
            ->where('funding.pemodal_id',$pemodal_id);
        })
        ->leftjoin('deposit', function($join)use($pemodal_id){
          $join->on('transaksi.id','=','deposit.transaksi_id')
            ->where('deposit.pemodal_id',$pemodal_id)
            ->where('deposit.status','=','verified');
        })
        ->leftjoin('withdraw', function($join)use($pemodal_id){
          $join->on('transaksi.id','=','withdraw.transaksi_id')
            ->where('withdraw.pemodal_id',$pemodal_id)
            ->where('withdraw.status','=','verified');
        })
        ->whereMonth('transaksi.tanggal',$bulan)
        ->whereYear('transaksi.tanggal',$tahun)
        ->orderBy('transaksi.tanggal')
        ->get();

      $history = [];
      for ($i=0; $i <count($transaksi) ; $i++) {

        if($i==0){
          $saldo = $saldo_awal;
        }

        switch ($transaksi[$i]->kode_transaksi) {
          case '02':
            $debit = $transaksi[$i]->jml_deposit;
            $kredit = 0;
            break;

          case '03':
            $debit = 0;
            $kredit = $transaksi[$i]->jml_withdraw;
            break;

          case '04':
            $debit = 0;
            $kredit = $transaksi[$i]->jml_funding;
            break;
        }

        $saldo = $saldo + $debit - $kredit;

        $history[] = array(
          'tanggal'    => date_format(date_create($transaksi[$i]->tanggal),'d-m-Y H:i:s'),
          'keterangan' => $transaksi[$i]->kode_transaksi.".".$transaksi[$i]->id." - ".$kode_transaksi[$transaksi[$i]->kode_transaksi],
          'debit'      => $debit,
          'kredit'     => $kredit,
          'saldo'      => $saldo
        );
      }
      $viewPdf = view('export.history_transaksi_pemodal',compact('history'));

      $mpdf = new Mpdf([
          'mode' => 'utf-8',
          'format' => 'A4',
          'orientation' => 'L'
      ]);

      $mpdf->WriteHTML($viewPdf);
      $mpdf->Output("Daftar_History_Transaksi.pdf","I");
    }
}
