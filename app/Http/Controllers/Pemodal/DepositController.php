<?php

namespace App\Http\Controllers\Pemodal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Mail\InfoTransferDeposit;
use App\Mail\NotifyDeposit;
use Yajra\Datatables\Datatables;
use App\Traits\Common;
use App\FileUploads;
use App\Transaksi;
use App\Deposit;
use App\Bank;
use App\User;
use App\AkunBank;

class DepositController extends Controller
{
    //
    use Common;

    public function index()
    {
      $data['bank'] = AkunBank::select('m_bank.nama','akun_bank.id')->where('is_akun_admin',1)
      ->join('m_bank','akun_bank.bank_id','=','m_bank.id')->get();

      return view('mockup.pemodal.deposit',$data);
    }

    public function list()
    {
    	$depo = Deposit::select('deposit.id','transaksi_id','deposit.tanggal','deposit.jumlah','kode_unik','deposit.status','deposit.created_at','m_bank.nama AS nama_bank')
            ->join('transaksi','deposit.transaksi_id','=','transaksi.id')
            ->join('akun_bank','transaksi.akun_bank_id','=','akun_bank.id')
            ->join('m_bank','akun_bank.bank_id','=','m_bank.id')
            ->where('pemodal_id',auth()->user()->pemodal->id)
            ->orderBy('deposit.created_at','desc');

        return Datatables::of($depo)
        ->addIndexColumn()
        ->editColumn('id', function($depo){
            return $this->encrypt_id($depo->id);
        })
        ->addColumn('no_transaksi', function($depo){
            return $depo->transaksi->kode_transaksi.".".$depo->transaksi->id;
        })
        ->editColumn('status', function($depo){
            if($depo->status == "unverified"){
                return '<center><font color="red"><b>Belum Transfer</b></font></center>';
            }elseif ($depo->status == "proses") {
                return '<center><font color="red"><b>Proses</b></font></center>';
            }else{
                return '<center><b>Berhasil</b></center>';
            }
        })
        ->editColumn('jumlah', function ($depo){
          return $depo->jumlah + $depo->kode_unik;
        })
        ->addColumn('action', function($depo){
            if($depo->status == "unverified"){
                return '<center><a href="/pemodal/deposit_konfirmasi/'.$this->encrypt_id($depo->id).'" class="btn btn-circle btn-xs green btn-outline"><i class="fa fa-pencil-square"></i> Konfirmasi Transaksi</a></center>';
            }else{
                return '';
            }
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public function order(Request $request)
    {
        $validator = Validator::make($request->all(),
          [
            'bank' => 'required',
            'jumlah' => 'required',
            'g-recaptcha-response' =>'required|recaptcha'
          ],
          [
            'g-recaptcha-response.required' => 'Please ensure that you are a human.!',
          ]
        )->validate();

        $int = $this->rupiah_to_int($request->jumlah);

        //query generate transaksi_id
        $number             = "0000";
        $date               = date('ym');
        $count              = Transaksi::where('id','LIKE','%'.$date.'%')->count();
        $referensi          = $date.$number + $count + 1;
        $kode_unik_transfer = $this->generate_kode_unik();

    	  $transaksi = new Transaksi;
        $transaksi->ref            = $referensi;
        $transaksi->kode_transaksi = "02";
        $transaksi->jumlah         = $int + $kode_unik_transfer;
        $transaksi->akun_bank_id   = $request->bank;
    	  $transaksi->save();

    	  $deposit = new Deposit;
        $deposit->pemodal_id   = $request->user()->pemodal->id;
        $deposit->transaksi_id = $transaksi->id;
        $deposit->tanggal      = date('Y-m-d H:i:s');
        $deposit->status       = 'unverified';
        $deposit->jumlah       = $int;
        $deposit->kode_unik    = $kode_unik_transfer;
        $deposit->save();

        // send email to pemodal untuk detail transfer
        Mail::to($request->user()->email)->send(new InfoTransferDeposit($deposit));

        return response()->json([
          'deposit'   => $deposit,
          'akun_bank' => AkunBank::select('m_bank.nama AS nama_bank','nama_akun_bank','no_akun_bank')
          ->join('m_bank','akun_bank.bank_id','=','m_bank.id')
          ->where('akun_bank.id',$request->bank)
          ->first()
        ]);
    }

    public function konfirmasi($id)
    {
        $depo      = Deposit::find($this->decrypt_id($id));
        $depo_id   = $this->encrypt_id($depo->id);
        $transaksi = Transaksi::select('transaksi.*','m_bank.nama AS nama_bank')
              ->where('transaksi.id',$depo->transaksi_id)
              ->join('akun_bank','transaksi.akun_bank_id','=','akun_bank.id')
              ->join('m_bank','akun_bank.bank_id','m_bank.id')
              ->first();
        return view('mockup.pemodal.konfirmasi2',compact('transaksi','depo_id'));
    }

    public function update(Request $request)
    {
      # validate captch
      $validator = Validator::make($request->all(),
        [
          'g-recaptcha-response' =>'required|recaptcha'
        ],
        [
          'g-recaptcha-response.required' => 'Please ensure that you are a human.!',
        ]
      );

      if ($validator->fails()) {
          return redirect()
                  ->back()
                  ->withErrors($validator)
                  ->withInput();
      }

      $depo = Deposit::find(decrypt($request->depo));
      $depo->status = "proses";
      $depo->save();

      $transaksi = Transaksi::find(decrypt($request->id));
      $transaksi->tanggal = $request->tanggal;
      $transaksi->deskripsi = $request->deskripsi;
      $transaksi->akun_bank_nama_bank = $request->nama_bank;
      $transaksi->akun_bank_no_akun = $request->norek;
      $transaksi->akun_bank_nama_akun = $request->pengirim;
      $transaksi->save();

      if($request->hasFile('bukti')){
        if ($request->file('bukti')->isValid()) {
          $request->file('bukti')->storeAs('bukti_transfer_deposit','bukti_transfer_deposit_'.date('jmY_His').'_'.$request->user()->id.'.'.$request->file('bukti')->extension());

          $file = new FileUploads();
          $file->type = 'dokumen';
          $file->name = 'bukti_transfer_deposit_'.date('jmY_His').'_'.$request->user()->id.'.'.$request->file('bukti')->extension();
          $file->descriptions = "BUKTI TRANSFER DEPOSIT";
          $file->mime = $request->file('bukti')->getMimeType();
          $file->size = $request->file('bukti')->getClientSize();
          $file->save();

          $transaksi->file_id = $file->id;
          $transaksi->save();
        }
      }

      # send email to admin
      $staff = User::where('type','finnesia')->get();
      Mail::to($staff)->send(new NotifyDeposit($transaksi));

      $request->session()->flash('success','Konfirmasi Transfer Telah Disimpan');

      return redirect('pemodal/deposit');
    }

    public function generate_kode_unik()
    {
      $kode = mt_rand(100,999);

      $is_unik = Deposit::where('kode_unik',$kode)->count();
      if($is_unik > 0){
        $kode = $this->generate_kode_unik();
      }

      return $kode;
    }
}
