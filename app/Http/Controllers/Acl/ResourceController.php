<?php
namespace App\Http\Controllers\Acl;

use App\Library\Beam\Acl\Acl;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Input;
use App\Resource;
use Session;

class ResourceController extends Controller {

	/**
	 * Display a list of Resources
	 * 
	 * @return Response
	 */
	public function index()
	{
		Acl::updateResources();
		$resources = \App\Resource::where('parent', null)
				->orderBy('name')
				->get();
				return view('mockup.acl.resource.index',compact('resources'));	
	}
	
	/**
	 * Display Resource create form
	 * 
	 * @return Response
	 */
	public function getCreate()
	{
		$resource = null;
		$this->saveReferer();
		return view('mockup.acl.resource.edit', compact('resource'));
	}
	
	/**
	 * Save a new Resource data
	 * 
	 * @return Response
	 */
	public function postCreate(Request $request)
	{
		# validate form
        $validator = Validator::make($request->all(),
          [
            'name'		=> 'required|unique:acl_resource',
			'type'		=> 'required|in:action,closure,other',
			'parent'	=> 'exists:acl_resources,id'
          ]
        );

        if ($validator->fails()) {
            return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
		}
		
		$resource = new Resource;
		$resource->name = Input::get('name');
		$resource->type = 'other';
		if (Input::get('parent_id', false))
			$resource->parent_id = Input::get('parent_id');

		if ($resource->save())
		{
			$request->session()->flash('success', \Lang::get('acl.alerts.resource_created'));
			return redirect('/acl/resource/index');
		}
		else
		{
			$request->session()->flash('error','Resource Gagal Ditambahkan');
			return redirect('/acl/resource/index');
		}
	}
	
	/**
	 * Display Resource edit form
	 * 
	 * @return Response
	 */
	public function getEdit($id)
	{
		$this->saveReferer();
		$resource = Resource::findOrFail($id);
		return view('mockup.acl.resource.edit', compact('resource'));
	}

	/**
	 * Save Resource data
	 * 
	 * @return Response
	 */
	public function postEdit(Request $request)
	{		

		$resource = Resource::findOrFail(Input::get('id'));		
		
		if ($resource->type != 'closure' && $resource->type != 'action')
			$resource->name = Input::get('name');
		
		if (!Input::get('parent_id', false))
			$resource->parent = null;
		else
			$resource->parent = Input::get('parent_id');

		if ($resource->save())
		{
			$request->session()->flash('success',\Lang::get('acl.alerts.resource_updated'));
			return redirect('/acl/resource/index');
		}
		else
		{
			$request->session()->flash('error','Resource Gagal Ditambahkan');
			return redirect('/acl/resource/index');
		}
	}
	
	/**
	 * Delete Resource data
	 * 
	 * @param integer $id
	 * @return Response
	 */
	public function postDelete(Request $request, $id)
	{		
		$resource = Resource::findOrFail($id);
		$deleted_resource = $resource->name;
		$resource->delete();
		$request->session()->flash('error',\Lang::get('acl.alerts.resource_deleted', array('name' => $deleted_resource)));
			return redirect('/acl/resource/index');
	}
}