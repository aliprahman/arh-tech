<?php
namespace App\Http\Controllers\Acl;

use App\Library\Beam\Acl\Acl;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Input;
use App\Resource;
use App\Role;
use Session;

class RoleController extends Controller {

	/**
	 * Display a list of Roles
	 * 
	 * @return Response
	 */
	public function index()
	{
		$roles = Role::with(array('parents' => function($query) {
			$query->orderBy('order');
		}))->get();
		return view('mockup.acl.role.index', compact('roles')); 
	}
	
	/**
	 * Display Role create form
	 * 
	 * @return Response
	 */
	public function getCreate()
	{
		$this->saveReferer();		
		return view('mockup.acl.role.edit'); 
	}
	
	/**
	 * Save a new Role data
	 * 
	 * @return Response
	 */
	public function postCreate(Request $request)
	{	
		# validate form
        $validator = Validator::make($request->all(),
          [
            'name'		=> 'required|unique:acl_role'
          ]
        );

        if ($validator->fails()) {
            return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
		}

		$role = new Role;
		$role->name = Input::get('name');

		if ($role->save())
		{
			$input_parents = Input::get('parents', false);
			if ($input_parents)
			{
				$parents = array();
				foreach($input_parents as $index => $parent)
				{
					$parents[$parent] = array('order' => $index);
				}
				$role->parents()->sync($parents);
			}
			
			$request->session()->flash('success',\Lang::get('acl.alerts.role_created'));
			return redirect('/acl/role/index');
		}
		else
		{
			$request->session()->flash('success','Role Gagal Ditambahkan');
			return redirect('/acl/role/index');
		}
	}
	
	/**
	 * Display Role edit form
	 * 
	 * @return Response
	 */
	public function getEdit($id)
	{
		$this->saveReferer();
		$role = Role::with(array('parents' => function($query) {
			$query->orderBy('order');
		}))->where('id', $id)->first();
		return view('mockup.acl.role.edit', compact('role')); 
	}

	/**
	 * Save Role data
	 * 
	 * @return Response
	 */
	public function postEdit(Request $request)
	{
		$role = Role::with(array('parents' => function($query) {
			$query->orderBy('order');
		}))->where('id', Input::get('id'))->first();
		$role->name = Input::get('name');

		if ($role->save())
		{
			$input_parents = Input::get('parents', false);
			$parents = array();
			if ($input_parents)
			{
				foreach($input_parents as $index => $parent)
				{
					$parents[$parent] = array('order_num' => $index);
				}
			}
			$role->parents()->sync($parents);
			
			$request->session()->flash('success', \Lang::get('acl.alerts.role_updated'));
			return redirect('/acl/role/index');
		}
		else
		{
			$request->session()->flash('error','Resource Gagal Diperbharui');
			return redirect('/acl/role/index');
		}
	}
	
	/**
	 * Delete a Role
	 * 
	 * @param integer $id
	 * @return Response
	 */
	public function postDelete($id)
	{
		$role = Role::findOrFail($id);
		$deleted_role = $role->name;
		$role->delete();
		return $this->redirectReferer(Redirect::action('RoleController@getIndex'))
				->with('success', \Lang::get('acl.alerts.role_deleted', array('name' => $deleted_role)));
	}
}