<?php
namespace App\Http\Controllers\Acl;

use App\Library\Beam\Acl\Acl;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Input;
use App\Resource;
use App\Role;
use Session;

class RuleController extends Controller {
	
	/**
	 * Display a list of Rules
	 * 
	 * @return Response
	 */
	public function index()
	{
		$roles = Role::orderByName()->get();
		$resources = NULL;
		$curr_role = NULL;
		if (Input::get('role_id', false))
		{
			$curr_role = Role::with(array('resources',
					'parents' => function($query) {
						$query->orderBy('order');
					}))
					->where('id', Input::get('role_id'))
					->first();
			if (!is_null($curr_role))
			{
				Acl::updateResources();
				$resources = Resource::roots()
						->get();
				Acl::build();
			}
		}		
		return view('mockup.acl.rule.index', compact('roles', 'curr_role', 'resources'));

	}

	/**
	 * Save Rule data
	 * 
	 * @return Response
	 */
	public function postEdit(Request $request)
	{
		$role = Role::findOrFail(Input::get('role_id'));
		$resource_rules = Input::get('resource_rule');
		
		$rules = array();
		foreach($resource_rules as $resource_id => $access)
		{
			if ($access != 'inherit')
			{
				$rules[$resource_id] = array('access' => $access);
			}
		}
		
		$role->resources()->sync($rules);		

		$request->session()->flash('success', \Lang::get('acl.alerts.rule_updated', array('name' => $role->name)));
			return redirect('/acl/rule/index');
	}
}
