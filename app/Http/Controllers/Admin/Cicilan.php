<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;
use App\Traits\Common;
use App\Cicilan as mCicilan;
use App\Transaksi;
use App\Bank;
use App\User;
use App\Pemodal;
use App\Pinjaman;
use App\Pendanaan;
use App\Pendapatan;

class Cicilan extends Controller
{
    use Common;

    public function index()
    {
      return view('mockup.admin.cicilan');
    }

    public function datatables(Request $request)
    {
      $startDate = date_format(date_create($request->startDate),'Y-m-d');
      $endDate = date_format(date_create($request->endDate),'Y-m-d');
      $statusCicilan = $request->status;

      $cicilan = mCicilan::select('cicilan.id','users.name AS peminjam','cicilan.jumlah','cicilan.status','cicilan.tanggal AS jatuh_tempo','transaksi.tanggal AS tgl_transfer','cicilan.pinjaman_id','transaksi.ref','m_bank.nama AS nama_bank')
      ->join('pinjaman','cicilan.pinjaman_id','=','pinjaman.id')
      ->join('peminjam','pinjaman.peminjam_id','=','peminjam.id')
      ->join('users','peminjam.user_id','=','users.id')
      ->leftjoin('transaksi','cicilan.transaksi_id','=','transaksi.id')
      ->leftjoin('akun_bank','transaksi.akun_bank_id','=','akun_bank.id')
      ->leftjoin('m_bank','akun_bank.bank_id','=','m_bank.id')
      ->whereBetween('cicilan.tanggal',[$startDate,$endDate])
      ->where(function($query)use($statusCicilan){
        if($statusCicilan != ""){
          $query->where('cicilan.status',$statusCicilan);
        }
      });

      return DataTables::of($cicilan)
      ->addIndexColumn()
      ->editColumn('id',function($cicilan){
        return $this->encrypt_id($cicilan->id);
      })
      ->editColumn('pinjaman_id',function($cicilan){
        return $this->encrypt_id($cicilan->pinjaman_id);
      })
      ->addColumn('action','')
      ->make(true);
    }

    public function show($id)
    {
      $depo_id = $this->decrypt_id($id);
      $deposit = mCicilan::select('cicilan.id','transaksi_id','ref','transaksi.tanggal','m_bank.nama AS nama_bank_tujuan_transfer',
        'transaksi.jumlah','akun_bank_nama_bank','akun_bank_no_akun','akun_bank_nama_akun','deskripsi','file.name AS file_bukti','cicilan.status'
        )
        ->join('transaksi','cicilan.transaksi_id','=','transaksi.id')
        ->leftjoin('file','transaksi.file_id','=','file.id')
        ->join('akun_bank','transaksi.akun_bank_id','=','akun_bank.id')
        ->join('m_bank','akun_bank.bank_id','=','m_bank.id')
        ->where('cicilan.id',$depo_id)
        ->first();

      return response()->json($deposit);
    }

    public function update(Request $request, $id)
    {
      $validator = Validator::make($request->all(),
        [
          'g-recaptcha-response' =>'required|recaptcha'
        ],
        [
          'g-recaptcha-response.required' => 'Please ensure that you are a human.!',
        ]
      )->validate();

      # update status cicilan
      $cicilan = mCicilan::find($id);
      $cicilan->status = 'verified';
      $cicilan->save();

      # pembagian komisi ke pemodal
      $pinjaman = Pinjaman::find($cicilan->pinjaman_id);
      $pendanaan = Pendanaan::where('pinjaman_id',$pinjaman->id)->get();

      foreach ($pendanaan as $key) {

        $persentase = ceil(( $key->jumlah / $pinjaman->jumlah_pinjaman ) * 100);
        $komisi = ceil(($persentase / 100) * $cicilan->jumlah);

        $pendapatan = new Pendapatan();
        $pendapatan->cicilan_id = $cicilan->id;
        $pendapatan->funding_id = $key->id;
        $pendapatan->jumlah = $komisi;
        $pendapatan->save();
      }

      # update status pinjaman jika semua cicilan sudah terbayar
      if(mCicilan::where('pinjaman_id',$pinjaman->id)->count() == mCicilan::where('pinjaman_id',$pinjaman->id)->where('status','verified')->count()){
        $pinjaman->status = "paid";
        $pinjaman->save();
      }

      $request->session()->flash('success','Transfer Cicilan Terverifikasi');

      return redirect('admin/cicilan');
    }
}
