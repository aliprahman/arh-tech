<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Traits\Common;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Pendanaan;

class Funding extends Controller
{
    use Common;

    public function index()
    {
        return view('mockup.pendanaan.list');
    }

    public function dataTable(Request $request)
    {
        $pendanaan = Pendanaan::select('tanggal','jumlah')
            ->whereBetween('tanggal',[
                date_format(date_create($request->startDate),'d-m-Y'),
                date_format(date_create($request->endDate),'d-m-Y')
            ])->get();

        return DataTables::of($pendanaan)
            ->addIndexColumn()
            ->addColumn('action',null)
            ->editColumn('id', function($pendanaan){
                return $this->encrypt_id($pendanaan->id);
            })
            ->addColumn('pemodal',function ($pendanaan){
                return $pendanaan->pemodal->user->nama;
            })
            ->addColumn('pinjaman',function ($pendanaan){
                return $pendanaan->pinjaman->kode;
            })
            ->make(true);
    }
}
