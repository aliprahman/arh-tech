<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\DepositVerified;
use Yajra\DataTables\DataTables;
use App\Traits\Common;
use App\Deposit as mDeposit;
use App\Bank;
use App\User;
use App\Pemodal;

class Deposit extends Controller
{
    use Common;

    public function index()
    {
      return view('mockup.admin.deposit');
    }

    public function datatable(Request $request)
    {
      $startDate = date_format(date_create($request->startDate),'Y-m-d');
      $endDate = date_format(date_create($request->endDate),'Y-m-d');
      $statusDeposit = $request->status;

      $depo = mDeposit::select('deposit.id','transaksi_id','deposit.tanggal','deposit.jumlah','kode_unik','deposit.status','deposit.created_at','user_id','m_bank.nama AS nama_bank')
            ->join('pemodal','deposit.pemodal_id','=','pemodal.id')
            ->join('transaksi','deposit.transaksi_id','=','transaksi.id')
            ->join('akun_bank','transaksi.akun_bank_id','=','akun_bank.id')
            ->join('m_bank','akun_bank.bank_id','=','m_bank.id')
            ->whereBetween('deposit.tanggal',[$startDate,$endDate])
            ->where(function($query)use($statusDeposit){
              if($statusDeposit != ""){
                $query->where('deposit.status',$statusDeposit);
              }
            })
            ->orderBy('deposit.created_at','desc');

        return Datatables::of($depo)
        ->addIndexColumn()
        ->editColumn('id', function($depo){
            return $this->encrypt_id($depo->id);
        })
        ->addColumn('nama', function($depo){
          return User::where('id',$depo->user_id)->value('name');
        })
        ->addColumn('no_transaksi', function($depo){
            return $depo->transaksi->kode_transaksi.".".$depo->transaksi->id;
        })
        ->editColumn('jumlah', function ($depo){
          return $depo->jumlah + $depo->kode_unik;
        })
        ->addColumn('action', '')
        ->make(true);
    }

    public function show($id)
    {
      $depo_id = $this->decrypt_id($id);
      $deposit = mDeposit::select('deposit.id','transaksi_id','ref','transaksi.tanggal','m_bank.nama AS nama_bank_tujuan_transfer',
        'transaksi.jumlah','akun_bank_nama_bank','akun_bank_no_akun','akun_bank_nama_akun','deskripsi','file.name AS file_bukti','deposit.status'
        )
        ->join('transaksi','deposit.transaksi_id','=','transaksi.id')
        ->leftjoin('file','transaksi.file_id','=','file.id')
        ->join('akun_bank','transaksi.akun_bank_id','=','akun_bank.id')
        ->join('m_bank','akun_bank.bank_id','=','m_bank.id')
        ->where('deposit.id',$depo_id)
        ->first();

      return response()->json($deposit);
    }

    public function update(Request $request, $id)
    {
      $validator = Validator::make($request->all(),
        [
          'g-recaptcha-response' =>'required|recaptcha'
        ],
        [
          'g-recaptcha-response.required' => 'Please ensure that you are a human.!',
        ]
      )->validate();

      # update status deposit
      $deposit = mDeposit::find($id);
      $deposit->status = 'verified';
      $deposit->save();

      # update saldo pemodal
      $pemodal = Pemodal::find($deposit->pemodal_id);
      $pemodal->saldo = $pemodal->saldo + $deposit->jumlah;
      $pemodal->save();

      # send email to pemodal
      Mail::to($pemodal->user->email)->send(new DepositVerified($deposit, $pemodal));

      $request->session()->flash('success','Transfer Deposit Terverifikasi');

      return redirect('admin/deposit');
    }
}
