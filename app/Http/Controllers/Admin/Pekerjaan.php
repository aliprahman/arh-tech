<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use App\Traits\Common;
use App\Pekerjaan as mPekerjaan;

class Pekerjaan extends Controller
{
  use common;

  public function index()
  {
    return view('mockup.admin.pekerjaan');
  }

  public function datatable(Request $request)
  {
    $pekerjaan = mPekerjaan::select('id','nama')->orderBy('nama');

    return DataTables::of($pekerjaan)
    ->addIndexColumn()
    ->addColumn('action',null)
    ->editColumn('id',function($pekerjaan){
        return $this->encrypt_id($pekerjaan->id);
    })->make(true);
  }

  public function show($id)
  {
    $real_id = $this->decrypt_id($id);
    $pekerjaan = mPekerjaan::findOrFail($real_id);

    return response()->json($pekerjaan);
  }

  public function store(Request $request)
  {
    $pekerjaan = new mPekerjaan();
    $pekerjaan->nama = $request->nama_pekerjaan;
    $pekerjaan->save();

    $request->session()->flash('success','Data Telah Disimpan');

    return redirect()->back();
  }

  public function update(Request $request, $id)
  {
    $pekerjaan = mPekerjaan::findOrFail($id);
    $pekerjaan->nama = $request->nama_pekerjaan;
    $pekerjaan->save();

    $request->session()->flash('success','Data Telah Disimpan');

    return redirect()->back();
  }

  public function destroy(Request $request, $id)
  {
    mPekerjaan::destroy($id);
    $request->session()->flash('success','Data Telah Dihapus');
    return redirect()->back();
  }

}
