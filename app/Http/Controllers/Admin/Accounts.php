<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\AkunBank;
use App\Alamat;
use App\Biodata;
use App\Peminjam;
use App\Pemodal;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Traits\Common;
use App\User;

class Accounts extends Controller
{
    use Common;

    public function index()
    {
        return view('mockup.account.list');
    }

    public function dataTable(Request $request)
    {
        $data_user = User::get_data_user_detail($request->type);

        return DataTables::of($data_user)
        ->addIndexColumn()
        ->addColumn('action',null)
        ->editColumn('id',function($data_user){
            return $this->encrypt_id($data_user->id);
        })->make(true);
    }

    public function detail($id)
    {
        $real_id = $this->decrypt_id($id);

        $data['user'] = User::find($real_id);
        if($data['user']->type == 'peminjam'){
            $alamat_id = $data['user']->peminjam->alamat_id;
            $akun_bank_id = $data['user']->peminjam->akun_bank_id;
        }else{
            $alamat_id = $data['user']->pemodal->alamat_id;
            $akun_bank_id = $data['user']->pemodal->akun_bank_id;
        }
        $data['biodata'] = Biodata::find($data['user']->biodata_id);
        $data['alamat'] = Alamat::select('alamat','kode_pos','m_province.name AS provinsi','m_regency.name AS kota','m_district.name AS kecamatan','m_village.name AS desa')
            ->join('m_province','alamat.province_id','=','m_province.id')
            ->join('m_regency','alamat.regency_id','=','m_regency.id')
            ->join('m_district','alamat.district_id','=','m_district.id')
            ->join('m_village','alamat.village_id','=','m_village.id')
            ->where('alamat.id',$alamat_id)
            ->first();
        $data['akun_bank'] = AkunBank::select('nama_akun_bank','no_akun_bank','m_bank.nama AS nama_bank')
            ->join('m_bank','akun_bank.bank_id','=','m_bank.id')
            ->where('akun_bank.id',$akun_bank_id)
            ->first();
        $data['pemodal'] = Pemodal::select('type','nama','no_npwp','no_telepon','alamat','kode_pos','m_province.name AS provinsi','m_regency.name AS kota','m_district.name AS kecamatan','m_village.name AS desa')
            ->leftjoin('perusahaan','pemodal.perusahaan_id','=','perusahaan.id')
            ->leftjoin('alamat','perusahaan.alamat_id','=','alamat.id')
            ->leftjoin('m_province','alamat.province_id','=','m_province.id')
            ->leftjoin('m_regency','alamat.regency_id','=','m_regency.id')
            ->leftjoin('m_district','alamat.district_id','=','m_district.id')
            ->leftjoin('m_village','alamat.village_id','=','m_village.id')
            ->where('pemodal.user_id',$real_id)
            ->first();
        $data['peminjam'] = Peminjam::select('m_kepemilikan_rumah.nama AS kepemilikan_rumah','m_jabatan.nama AS jabatan','m_status_pernikahan.nama AS status_pernikahan',
                'm_pendidikan.nama AS pendidikan','m_hubungan_kontak.nama AS hubungan','si_pekerjaan.nama AS pekerjaan_suami_istri','peminjam.*',
                'k_alamat.alamat AS k_alamat','k_alamat.kode_pos AS k_kode_pos','k_province.name AS k_provinsi','k_regency.name AS k_kota','k_district.name AS k_kecamatan','k_village.name AS k_desa',
                't_alamat.alamat AS t_alamat','t_alamat.kode_pos AS t_kode_pos','t_province.name AS t_provinsi','t_regency.name AS t_kota','t_district.name AS t_kecamatan','t_village.name AS t_desa'
            )
            ->leftjoin('m_kepemilikan_rumah','peminjam.kepemilikan_rumah_id','=','m_kepemilikan_rumah.id')
            ->leftjoin('m_jabatan','peminjam.jabatan_id','=','m_jabatan.id')
            ->leftjoin('m_status_pernikahan','peminjam.status_pernikahan_id','=','m_status_pernikahan.id')
            ->leftjoin('m_pendidikan','peminjam.pendidikan_id','=','m_pendidikan.id')
            ->leftjoin('m_hubungan_kontak','peminjam.hubungan_kontak_id','=','m_hubungan_kontak.id')
            ->leftjoin('m_pekerjaan AS si_pekerjaan','peminjam.pekerjaan_suami_istri_id','=','si_pekerjaan.id')
            ->leftjoin('alamat AS k_alamat','peminjam.alamat_kontak_id','=','k_alamat.id')
            ->leftjoin('m_province AS k_province','k_alamat.province_id','=','k_province.id')
            ->leftjoin('m_regency AS k_regency','k_alamat.regency_id','=','k_regency.id')
            ->leftjoin('m_district AS k_district','k_alamat.district_id','=','k_district.id')
            ->leftjoin('m_village AS k_village','k_alamat.village_id','=','k_village.id')
            ->leftjoin('alamat AS t_alamat','peminjam.alamat_tempat_kerja_id','=','t_alamat.id')
            ->leftjoin('m_province AS t_province','t_alamat.province_id','=','t_province.id')
            ->leftjoin('m_regency AS t_regency','t_alamat.regency_id','=','t_regency.id')
            ->leftjoin('m_district AS t_district','t_alamat.district_id','=','t_district.id')
            ->leftjoin('m_village AS t_village','t_alamat.village_id','=','t_village.id')
            ->where('peminjam.user_id',$real_id)
            ->first();

        return view('mockup.account.detail',$data);
    }
}
