<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Yajra\Datatables\Datatables;
use App\Mail\SimulasiCicilan;
use App\Jobs\BlastEmailPinjamanBaru;
use App\Pinjaman as mPinjaman;
use App\Traits\Common;
use App\Peminjam;
use App\User;
use App\Biodata;
use App\Alamat;
use App\AkunBank;
use App\Pendanaan;
use App\Cicilan;

class Pinjaman extends Controller
{
    use Common;

    public function index()
    {
        return view('mockup.admin.pinjamanlist');
    }

    public function datatables(Request $request)
    {
        $startDate = date_format(date_create($request->startDate." 00:00:00"),'Y-m-d H:i:s');
        $endDate = date_format(date_create($request->endDate." 23:59:59"),'Y-m-d H:i:s');

        $pinjaman = mPinjaman::select('pinjaman.id','users.name as nama','tanggal_submit AS tanggal','jumlah_pinjaman AS jumlah','waktu_pinjaman AS waktu','pinjaman.status','jumlah_funding')
        ->join('peminjam','peminjam.id','=','pinjaman.peminjam_id')
        ->join('users','peminjam.user_id','users.id')
        ->whereBetween('tanggal_submit',[$startDate,$endDate])
        ->get();

        return Datatables::of($pinjaman)
        ->addIndexColumn()
        ->editColumn('nama',function($pinjaman){
            return ucfirst($pinjaman->nama);
        })
        ->editColumn('tanggal',function($pinjaman){
            return date_format(date_create($pinjaman->tanggal),'Y-m-d');
        })
        ->editColumn('waktu',function($pinjaman){
            return $pinjaman->waktu." Bulan";
        })
        ->editColumn('jumlah',function($pinjaman){
            return (int)$pinjaman->jumlah;
        })
        ->addColumn('progres', function($pinjaman){
            return ($pinjaman->jumlah_funding / $pinjaman->jumlah) * 100;
        })
        ->addColumn('action',null)
        ->editColumn('id', function($pinjaman){
            return $this->encrypt_id($pinjaman->id);
        })
        ->make(true);
    }

    public function analyze($pinjaman_id)
    {
        $real_id = $this->decrypt_id($pinjaman_id);
        $pinjaman = mPinjaman::find($real_id);
        # update status to analisys if pijaman status = submit
        if($pinjaman->status == 'submit'){
            $pinjaman->status = 'analysis';
            $pinjaman->tanggal_mulai_analisis = date('Y-m-d H:i:s');
            $pinjaman->save();
        }

        $data['pinjaman_id'] = $pinjaman_id;
        $data['pinjaman'] = $pinjaman;
        $data['peminjam'] = Peminjam::find($pinjaman->peminjam_id);

        # data user peminjam
        $data['user'] = User::find($pinjaman->peminjam->user_id);
        if($data['user']->type == 'peminjam'){
            $alamat_id = $data['user']->peminjam->alamat_id;
            $akun_bank_id = $data['user']->peminjam->akun_bank_id;
        }else{
            $alamat_id = $data['user']->pemodal->alamat_id;
            $akun_bank_id = $data['user']->pemodal->akun_bank_id;
        }
        $data['biodata'] = Biodata::find($data['user']->biodata_id);
        $data['alamat'] = Alamat::select('alamat','kode_pos','m_province.name AS provinsi','m_regency.name AS kota','m_district.name AS kecamatan','m_village.name AS desa')
            ->join('m_province','alamat.province_id','=','m_province.id')
            ->join('m_regency','alamat.regency_id','=','m_regency.id')
            ->join('m_district','alamat.district_id','=','m_district.id')
            ->join('m_village','alamat.village_id','=','m_village.id')
            ->where('alamat.id',$alamat_id)
            ->first();
        $data['akun_bank'] = AkunBank::select('nama_akun_bank','no_akun_bank','m_bank.nama AS nama_bank')
            ->join('m_bank','akun_bank.bank_id','=','m_bank.id')
            ->where('akun_bank.id',$akun_bank_id)
            ->first();
        $data['peminjam'] = Peminjam::select('m_kepemilikan_rumah.nama AS kepemilikan_rumah','m_jabatan.nama AS jabatan','m_status_pernikahan.nama AS status_pernikahan',
            'm_pendidikan.nama AS pendidikan','m_hubungan_kontak.nama AS hubungan','si_pekerjaan.nama AS pekerjaan_suami_istri','peminjam.*',
            'k_alamat.alamat AS k_alamat','k_alamat.kode_pos AS k_kode_pos','k_province.name AS k_provinsi','k_regency.name AS k_kota','k_district.name AS k_kecamatan','k_village.name AS k_desa',
            't_alamat.alamat AS t_alamat','t_alamat.kode_pos AS t_kode_pos','t_province.name AS t_provinsi','t_regency.name AS t_kota','t_district.name AS t_kecamatan','t_village.name AS t_desa'
        )
            ->leftjoin('m_kepemilikan_rumah','peminjam.kepemilikan_rumah_id','=','m_kepemilikan_rumah.id')
            ->leftjoin('m_jabatan','peminjam.jabatan_id','=','m_jabatan.id')
            ->leftjoin('m_status_pernikahan','peminjam.status_pernikahan_id','=','m_status_pernikahan.id')
            ->leftjoin('m_pendidikan','peminjam.pendidikan_id','=','m_pendidikan.id')
            ->leftjoin('m_hubungan_kontak','peminjam.hubungan_kontak_id','=','m_hubungan_kontak.id')
            ->leftjoin('m_pekerjaan AS si_pekerjaan','peminjam.pekerjaan_suami_istri_id','=','si_pekerjaan.id')
            ->leftjoin('alamat AS k_alamat','peminjam.alamat_kontak_id','=','k_alamat.id')
            ->leftjoin('m_province AS k_province','k_alamat.province_id','=','k_province.id')
            ->leftjoin('m_regency AS k_regency','k_alamat.regency_id','=','k_regency.id')
            ->leftjoin('m_district AS k_district','k_alamat.district_id','=','k_district.id')
            ->leftjoin('m_village AS k_village','k_alamat.village_id','=','k_village.id')
            ->leftjoin('alamat AS t_alamat','peminjam.alamat_tempat_kerja_id','=','t_alamat.id')
            ->leftjoin('m_province AS t_province','t_alamat.province_id','=','t_province.id')
            ->leftjoin('m_regency AS t_regency','t_alamat.regency_id','=','t_regency.id')
            ->leftjoin('m_district AS t_district','t_alamat.district_id','=','t_district.id')
            ->leftjoin('m_village AS t_village','t_alamat.village_id','=','t_village.id')
            ->where('peminjam.id',$pinjaman->peminjam_id)
            ->first();
        $data['pendanaan'] = Pendanaan::select('jumlah','tanggal as tanggal_pendanaan','user_id','name as nama')
            ->join('pemodal','pemodal.id','=','funding.pemodal_id')
            ->join('users','users.id','=','pemodal.user_id')
            ->where('pinjaman_id',$pinjaman->id)
            ->get();
        return view('mockup.pinjaman.analisis',$data);
    }

    public function reject(Request $request, $pinjaman_id)
    {
        $real_id = $this->decrypt_id($pinjaman_id);
        $pinjaman = mPinjaman::find($real_id);
        $pinjaman->alasan_ditolak = $request->note;
        $pinjaman->status = 'reject';
        $pinjaman->tanggal_berakhir_analisis = date('Y-m-d H:i:s');
        $pinjaman->save();
        $request->session()->flash('success','Data Pinjaman Berhasil Di Update');

        return redirect()->back();
    }

    # buat simulasi cicilan
    public function generate_simulai_cicilan(Request $request, $pinjaman_id)
    {
        $real_id = $this->decrypt_id($pinjaman_id);
        $pinjaman = mPinjaman::find($real_id);

        return view('mockup.pinjaman.simulasi',['pinjaman'=>$pinjaman]);
    }

    public function send_simulasi(Request $request, $pinjaman_id)
    {
        $real_id = $this->decrypt_id($pinjaman_id);
        $pinjaman = mPinjaman::find($real_id);
        $pinjaman->admin_fee = $this->rupiah_to_int($request->biaya_admin);
        $pinjaman->conf_fee = $this->rupiah_to_int($request->biaya_conf);
        $pinjaman->save();

        $cicilan = $request->cicilan;
        $admin_fee = $request->admin_fee;
        $conf_fee = $request->conf_fee;

        for ($i=0; $i < count($cicilan); $i++) {
            $c = new Cicilan();
            $c->pinjaman_id = $real_id;
            $c->jumlah = $cicilan[$i] + $admin_fee[$i] + $conf_fee[$i];
            $c->status = 'unverified';
            $c->save();
        }

        Mail::to($pinjaman->peminjam->user->email)->send(new SimulasiCicilan($pinjaman));

        $request->session()->flash('success','Simulai cicilan telah dikirim');
        return redirect()->back();
    }

    public function funding_periode(Request $request, $pinjaman_id)
    {
        $request->validate([
          'tanggal_mulai' => 'required|date',
          'tanggal_akhir' => 'required|date|after:tanggal_mulai'
        ]);
        $real_id = $this->decrypt_id($pinjaman_id);
        $pinjaman = mPinjaman::find($real_id);
        $pinjaman->status = 'funding';
        $pinjaman->tanggal_mulai_funding = date_format(date_create($request->tanggal_mulai),'Y-m-d');
        $pinjaman->tanggal_berakhir_funding = date_format(date_create($request->tanggal_akhir),'Y-m-d');
        $pinjaman->tanggal_berakhir_analisis = date('Y-m-d H:i:s');
        $pinjaman->save();
        $request->session()->flash('success','Data Pinjaman Berhasil Di Update');

        # blash email via queue
        BlastEmailPinjamanBaru::dispatch($pinjaman);

        return redirect()->back();
    }
}
