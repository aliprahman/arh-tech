<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\User;
use App\FileUploads;
use App\Biodata as BiodataModel;

class Biodata extends Controller
{
  public function index(Request $request)
  {
    $data['biodata'] = BiodataModel::find($request->user()->biodata_id);
    $data['user'] = $request->user();
    return view('mockup.admin.biodata',$data);
  }

  public function update(Request $request)
  {
    $user = Auth::user();
    $validator = Validator::make($request->all(),
      [
        'email' => [
            Rule::unique('users')->ignore($user->id),
        ],
        'no_hp' => [
            Rule::unique('biodata')->ignore($user->biodata_id),
        ],
        'no_ktp' => [
            Rule::unique('biodata')->ignore($user->biodata_id),
        ],
        'no_npwp' => [
            Rule::unique('biodata')->ignore($user->biodata_id),
        ],
        //'old_password' => 'old_password',
        'new_password' => 'confirmed',
      ]
    );

    if ($validator->fails()) {
        return redirect()
            ->back()
            ->withErrors($validator)
            ->withInput();
    }

    $biodata = BiodataModel::find($user->biodata_id);
    $biodata->tempat_lahir = $request->tempat_lahir;
    $biodata->tanggal_lahir= $request->tanggal_lahir;
    $biodata->jenis_kelamin= $request->jenis_kelamin;
    $biodata->no_telepon   = $request->no_telepon;
    $biodata->no_hp        = $request->no_hp;
    $biodata->no_ktp       = $request->no_ktp;
    $biodata->no_npwp      = $request->no_npwp;
    $biodata->save();

    $user = User::find($user->id);
    $user->name       = $request->nama;
    $user->email      = $request->email;
    if($request->new_password) {
        $user->password   = bcrypt($request->new_password);
    }
    $user->save();

    if($request->hasFile('profile_photo')){
      if ($request->file('profile_photo')->isValid()) {
        $request->file('profile_photo')->storeAs('profile_photo','profile_photo_'.$user->id.'.'.$request->file('profile_photo')->extension());

        if($user->photo_profile_id != ""){
            $file = FileUploads::find($user->photo_profile_id);
            if(file_exists(storage_path('app/public/profile_photo/'.$file->name))){
              unlink(storage_path('app/public/profile_photo/'.$file->name));
            }
        } else {
            $file = new FileUploads();
        }

        $file->type = 'profile_photo';
        $file->name = 'profile_photo_'.$user->id.'.'.$request->file('profile_photo')->extension();
        $file->descriptions = "Photo Profile";
        $file->mime = $request->file('profile_photo')->getMimeType();
        $file->size = $request->file('profile_photo')->getClientSize();
        $file->save();

        $user->photo_profile_id = $file->id;
        $user->save();
      }
    }

    $request->session()->flash('success','Data Berhasil Di Simpan');
    return redirect()->back();
  }


}
