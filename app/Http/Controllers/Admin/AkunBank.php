<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use App\Traits\Common;
use App\AkunBank as mAkunBank;
use App\Bank;

class AkunBank extends Controller
{
  use common;

  public function index()
  {
    $data['bank_list'] = Bank::all();
    return view('mockup.admin.akun_bank_transaksi',$data);
  }

  public function datatable(Request $request)
  {
    $akun_bank = mAkunBank::select('akun_bank.id','m_bank.nama AS nama_bank','nama_akun_bank','no_akun_bank')
      ->join('m_bank','akun_bank.bank_id','=','m_bank.id')
      ->where('is_akun_admin',1);

    return DataTables::of($akun_bank)
    ->addIndexColumn()
    ->addColumn('action',null)
    ->editColumn('id',function($akun_bank){
        return $this->encrypt_id($akun_bank->id);
    })->make(true);
  }

  public function show($id)
  {
    $real_id = $this->decrypt_id($id);
    $akun_bank = mAkunBank::select('akun_bank.id','m_bank.nama AS nama_bank','bank_id','nama_akun_bank','no_akun_bank')
      ->join('m_bank','akun_bank.bank_id','=','m_bank.id')
      ->where('akun_bank.id',$real_id)
      ->first();

    return response()->json($akun_bank);
  }

  public function store(Request $request)
  {
    $akun_bank = new mAkunBank();
    $akun_bank->bank_id = $request->bank_id;
    $akun_bank->nama_akun_bank = $request->nama_akun_bank;
    $akun_bank->no_akun_bank = $request->no_akun_bank;
    $akun_bank->is_akun_admin = 1;
    $akun_bank->save();

    $request->session()->flash('success','Data Telah Disimpan');

    return redirect()->back();
  }

  public function update(Request $request, $id)
  {
    $akun_bank = mAkunBank::find($id);
    $akun_bank->bank_id = $request->bank_id;
    $akun_bank->nama_akun_bank = $request->nama_akun_bank;
    $akun_bank->no_akun_bank = $request->no_akun_bank;
    $akun_bank->save();

    $request->session()->flash('success','Data Telah Disimpan');

    return redirect()->back();
  }
}
