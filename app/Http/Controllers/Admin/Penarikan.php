<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\ConfirmationWithDraw;
use Yajra\DataTables\DataTables;
use App\Traits\Common;
use App\Transaksi;
use App\WithDraw;
use App\Pemodal;
use App\User;

class Penarikan extends Controller
{
    use Common;

    public function index()
    {
      return view('mockup.admin.penarikan');
    }

    public function datatables(Request $request)
    {
      $startDate = date_format(date_create($request->startDate),'Y-m-d');
      $endDate = date_format(date_create($request->endDate),'Y-m-d');
      $statusWithDraw = $request->status;

      $wdraw = WithDraw::whereBetween('tanggal',[$startDate,$endDate])
        ->where(function($query)use($statusWithDraw){
          if($statusWithDraw != ""){
            $query->where('status',$statusWithDraw);
          }
        })
        ->orderBy('created_at','desc');


      return DataTables::of($wdraw)
        ->addIndexColumn()
        ->editColumn('id', function($wdraw){
            return $this->encrypt_id($wdraw->id);
        })
        ->addColumn('pemodal', function($wdraw){
            return $wdraw->pemodal->user->name;
        })
        ->editColumn('tanggal', function($wdraw){
            return date_format(date_create($wdraw->created_at),'l d F Y H:i');
        })
        ->editColumn('status', function($wdraw){
            return $wdraw->status;
        })
        ->editColumn('jumlah', function($wdraw){
          return "Rp. ".number_format($wdraw->jumlah,0,',','.');
        })
        ->addColumn('action','')
        ->make(true);
    }

    public function show($id)
    {
      $real_id = $this->decrypt_id($id);
      $result= WithDraw::select('withdraw.jumlah','m_bank.nama AS bank_tujuan','akun_bank.nama_akun_bank AS nama_akun_tujuan','akun_bank.no_akun_bank AS no_akun_tujuan',
      'transaksi.ref','transaksi.tanggal','akun_bank_nama_bank','akun_bank_nama_akun','akun_bank_no_akun','status')
      ->join('transaksi','transaksi.id','=','withdraw.transaksi_id')
      ->join('akun_bank','transaksi.akun_bank_id','=','akun_bank.id')
      ->join('m_bank','akun_bank.id','=','m_bank.id')
      ->where('withdraw.id',$real_id)
      ->first();
      return response()->json($result);
    }

    public function update(Request $request, $id)
    {
      $validator = Validator::make($request->all(),
        [
          'g-recaptcha-response' =>'required|recaptcha'
        ],
        [
          'g-recaptcha-response.required' => 'Please ensure that you are a human.!',
        ]
      )->validate();

      $wdraw = WithDraw::find($id);

      # update transaksi
      $transaksi = Transaksi::find($wdraw->transaksi_id);
      $transaksi->akun_bank_nama_bank = $request->via;
      $transaksi->akun_bank_no_akun = $request->norek;
      $transaksi->akun_bank_nama_akun = $request->nama_pemilik;
      $transaksi->tanggal = date_format(date_create($request->tgl_transfer),'Y-m-d');
      $transaksi->ref = $request->ref;
      $transaksi->save();

      # update status penarikan
      $wdraw->status = 'verified';
      $wdraw->save();

      # kurangi saldo pemodal
      $pemodal = Pemodal::find($wdraw->pemodal_id);
      $pemodal->saldo = $pemodal->saldo - $wdraw->jumlah;
      $pemodal->save();

      # send email to pemodal
      Mail::to($pemodal->user->email)->send(new ConfirmationWithDraw($wdraw, $pemodal, $transaksi));

      $request->session()->flash('success','Transfer WidhDraw Terverifikasi');

      return redirect('admin/penarikan');
    }
}
