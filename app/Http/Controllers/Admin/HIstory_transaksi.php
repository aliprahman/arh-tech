<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Transaksi as mTransaksi;
use App\Traits\Common;

class History_transaksi extends Controller
{
    use Common;

    public function index()
    {
        return view('mockup.admin.history_transaksi_list');
    }

    public function datatables(Request $request)
    {

        $transaksi = mTransaksi::history_transaksi($request->month, $request->year);
        
        return Datatables::of($transaksi)
        ->addIndexColumn()
        ->editColumn('tanggal',function($transaksi){
            return date_format(date_create($transaksi->tanggal),'Y-m-d');
        })
        ->editColumn('jumlah',function($transaksi){
            return (int)$transaksi->jumlah;
        })
        ->editColumn('id', function($transaksi){
            return $this->encrypt_id($transaksi->id);
        })
        ->make(true);
    }

    public function print($month = "13", $year = "2018")
    {
        $transaksi = mTransaksi::history_transaksi($month, $year);

        $viewPdf = view('mockup.admin.print', compact('transaksi'));

        $mpdf = new \Mpdf\Mpdf([
            'mode' => 'utf-8',
            'format' => 'A4',
            'orientation' => 'L'
        ]);
        $mpdf->WriteHTML($viewPdf);
        $mpdf->Output("HistoryTransaksi.pdf","I");
    }
}
