<?php

namespace App\Http\Controllers\Peminjam;

use App\User;
use App\Kota;
use App\Desa;
use App\Bank;
use App\Alamat;
use App\Biodata;
use App\Peminjam;
use App\Provinsi;
use App\AkunBank;
use App\Kecamatan;
use Illuminate\Http\Request;
use App\Mail\ActivationEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class Registration extends Controller
{
    public function load_form_peminjam()
    {
        $data['list_provinsi'] = Provinsi::pluck('id','name');
        $data['list_kota'] = Kota::pluck('name');
        $data['list_bank'] = Bank::pluck('id','nama');

        return view('mockup.peminjam.registerpeminjam',$data);
    }

    public function register_peminjam(Request $request)
    {
        /*# validate form
        $validator = Validator::make($request->all(),
          [
            'g-recaptcha-response' =>'required|recaptcha',
            'email'                => 'email|unique:users,email',
            'handphone'            => 'unique:biodata,no_hp',
            'ktp'                  => 'unique:biodata,no_ktp|max:16',
            'npwp'                 => 'unique:biodata,no_npwp',
            'password'             => 'required|confirmed|min:6',
          ],
          [
            'g-recaptcha-response.required' => 'Please ensure that you are a human.!',
          ]
        );

        if ($validator->fails()) {
            return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
        }*/

        //query generate id_pelanggan
        $number        = "000";
        $date          = date('ym');
        $count         = User::where('id_pelanggan','LIKE','%'.$date.'%')->count();
        $id_pelanggan  = $date.$number + $count + 1;

        $biodata = new Biodata();
        $biodata->tempat_lahir = $request->tempat_lahir;
        $biodata->tanggal_lahir= $request->tanggal_lahir;
        $biodata->jenis_kelamin= $request->jenis_kelamin;
        $biodata->no_telepon   = $request->telephone;
        $biodata->no_hp        = $request->handphone;
        $biodata->no_ktp       = $request->ktp;
        $biodata->no_npwp      = $request->npwp;
        $biodata->save();

        $user = new User();
        $user->name       = $request->nama;
        $user->email      = $request->email;
        $user->password   = bcrypt($request->password);
        $user->type       = 'peminjam';
        $user->biodata_id = $biodata->id;
        $user->status     = 'new';
        $user->verification_token = str_random(40);
        $user->role_id    = 4;
        $user->id_pelanggan = $id_pelanggan;
        $user->save();

        $alamat_diri = new Alamat();
        $alamat_diri->alamat      = $request->alamat;
        $alamat_diri->kode_pos    = $request->kode_pos_1;
        $alamat_diri->province_id = $request->prov_1;
        $alamat_diri->regency_id  = $request->kota_1;
        $alamat_diri->district_id = $request->kec_1;
        $alamat_diri->village_id  = $request->kel_1;
        $alamat_diri->save();

        $akun_bank = new AkunBank();
        $akun_bank->bank_id        = $request->bank;
        $akun_bank->nama_akun_bank = $request->nama_akun;
        $akun_bank->no_akun_bank   = $request->no_rekening;
        $akun_bank->save();

        $peminjam = new Peminjam();
        $peminjam->user_id                = $user->id;
        $peminjam->nama_tempat_kerja      = $request->nama_sekolah;
        $peminjam->alamat_id              = $alamat_diri->id;
        $peminjam->akun_bank_id           = $akun_bank->id;
        $peminjam->no_telepon_tempat_kerja= $request->telephone_sekolah;
        $peminjam->status                 = 'unverified';
        $peminjam->save();

        Mail::to($user->email)->send(new ActivationEmail($user));

        $request->session()->flash('success','Pendaftaran Berhasil, Silahkan Cek Email Untuk Aktifasi');
        return redirect()->back();

    }
}
