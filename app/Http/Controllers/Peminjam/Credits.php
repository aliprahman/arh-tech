<?php

namespace App\Http\Controllers\Peminjam;

use App\Http\Controllers\Controller;
use App\Pendanaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\SimulasiCicilan;
use App\Mail\NotifPengajuan;
use App\Traits\Common;
use App\JenisPinjaman;
use App\JenisDokumen;
use App\FileUploads;
use App\Pinjaman;
use App\Dokumen;
use App\Peminjam;
use App\Transaksi;
use App\KeuanganPeminjam;
use App\Pendidikan;
use App\StatusPernikahan;
use App\KepemilikanRumah;
use App\HubunganKontak;
use App\Pekerjaan;
use App\Jabatan;
use App\Provinsi;
use App\Bank;
use App\User;
use App\Biodata;
use App\Alamat;
use App\AkunBank;
use App\Cicilan;
use App\Kota;
use App\Kecamatan;
use App\Desa;
use Mpdf\Mpdf;

class Credits extends Controller
{
    use Common;

    # ajax daftar pinjaman untuk peminjam
    public function list_credits_peminjam(Request $request)
    {
        $pinjaman = Pinjaman::listing_pinjaman(Auth::user()->peminjam->id);

        return Datatables::of($pinjaman)
            ->addIndexColumn()
            ->editColumn('id', function($pinjaman){
                return $this->encrypt_id($pinjaman->id);
            })
            ->editColumn('waktu_pinjaman',function($pinjaman){
                return $pinjaman->waktu_pinjaman." Bulan";
            })
            ->editColumn('status',function($pinjaman){
                $status = $pinjaman->status;
                if($status == 'submit'){
                  $status = 'Waiting Confirmation';
                }
                if($status == 'listing'){
                  $status = 'Approve';
                }
                if ($status == 'analysis' && count($pinjaman->cicilan) > 0) {
                  $status = 'Check Email';
                }
                return $status;
            })
            ->addColumn('progres', function($pinjaman){
                return ($pinjaman->jumlah_funding / $pinjaman->jumlah_pinjaman) * 100;
            })
            ->editColumn('status_cicilan', function($pinjaman){
                if($pinjaman->status_cicilan){
                    return 'Lunas';
                }else{
                    return 'Belum Lunas';
                }
            })
            ->make(true);
    }

    # halaman pengajuan pinjaman baru
    public function create()
    {
        # for biodata
        $data['pendidikan'] = Pendidikan::pluck('id','nama');
        $data['status_pernikahan'] = StatusPernikahan::pluck('id','nama');
        $data['kepemilikan_rumah'] = KepemilikanRumah::pluck('id','nama');
        $data['hubungan_kontak'] = HubunganKontak::pluck('id','nama');
        $data['pekerjaan'] = Pekerjaan::pluck('id','nama');
        $data['jabatan'] = Jabatan::pluck('id','nama');
        $data['list_provinsi'] = Provinsi::pluck('id','name');
        $data['list_bank'] = Bank::pluck('id','nama');
        $data['user'] = Auth::user();
        $data['pendapatan_lainnya'] = KeuanganPeminjam::where('peminjam_id',Auth::user()->peminjam->id)
            ->where('type','pendapatan')->get();
        $data['pinjaman_lainnya'] = KeuanganPeminjam::where('peminjam_id',Auth::user()->peminjam->id)
            ->where('type','pinjaman')->get();
        $data['peminjam_id'] = $this->encrypt_id(Auth::user()->peminjam->id);
        # for pinjaman
        $data['jenis_pinjaman'] = JenisPinjaman::pluck('id','nama');
        $data['jenis_dokumen'] = JenisDokumen::pluck('id','nama');
        return view('mockup.pinjaman.ajuan',$data);
    }

    # proses simpan pengajuan pinjaman
    public function store(Request $request)
    {
        $pinjaman = new Pinjaman();
        $pinjaman->kode = $this->generate_kode();
        $pinjaman->peminjam_id = $request->user()->peminjam->id;
        $pinjaman->jenis_pinjaman_id = $request->jenis_pinjaman;
        $pinjaman->jumlah_pinjaman = $this->rupiah_to_int($request->jumlah_pinjaman);
        $pinjaman->waktu_pinjaman = $request->lama_pinjaman;
        $pinjaman->tanggal_submit = date('Y-m-d H:i:s');
        $pinjaman->status = 'submit';
        $pinjaman->detail_peminjam = $this->biodata_peminjam(Peminjam::find($request->user()->peminjam->id));;
        $pinjaman->save();

        # send notif to admin
        $admin = User::where('type','finnesia')->pluck('email')->toArray();
        Mail::to($admin)->send(new NotifPengajuan($pinjaman, $request->user()->name));

        $request->session()->flash('success','Pinjaman Berhasil Disimpan');
        return redirect('/pinjaman');
    }

    public function approve_from_email(Request $request, $id)
    {
        $real_id = $this->decrypt_id($id);
        $pinjaman = Pinjaman::find($real_id);
        if($pinjaman->status != 'analysis'){
          abort(404);
        }
        $pinjaman->status = 'listing';
        $pinjaman->save();

        if($request->ajax()){
            return response()->json(['status'=>true,'msg'=>'Pengajuan Pinjaman Anda Akan Segera Diproses']);
        }else{
            return view('mockup.reply_email',['pinjaman'=>$pinjaman]);
        }
    }

    public function reject_from_email(Request $request, $id)
    {
        $real_id = $this->decrypt_id($id);
        $pinjaman = Pinjaman::find($real_id);
        if($pinjaman->status != 'analysis'){
          abort(404);
        }
        $pinjaman->status = 'reject';
        $pinjaman->alasan_ditolak = 'Simulasi cicilan ditolak oleh peminjam';
        $pinjaman->save();

        if($request->ajax()){
            return response()->json(['status'=>true,'msg'=>'Pengajuan Pinjaman Anda Telah Dibatalkan']);
        }else{
            return view('mockup.reply_email',['pinjaman'=>$pinjaman]);
        }
    }

    # generate kode pinjam
    private function generate_kode()
    {
        return Pinjaman::generate_code();
    }

    # mendapatkan biodata peminjam
    private function biodata_peminjam(Peminjam $peminjam)
    {
        $data['biodata'] = array(
            'tempat_lahir'           => $peminjam->user->biodata->tempat_lahir,
            'tanggal_lahir'          => $peminjam->user->biodata->tanggal_lahir,
            'jenis_kelamin'          => $peminjam->user->biodata->jenis_kelamin,
            'alamat'                 => [
                'alamat_lengkap'     => $peminjam->alamat->alamat,
                'provinsi'           => Provinsi::where('id',$peminjam->alamat->province_id)->value('name'),
                'kota'               => Kota::where('id',$peminjam->alamat->regency_id)->value('name'),
                'kecamatan'          => Kecamatan::where('id',$peminjam->alamat->district_id)->value('name'),
                'desa'               => Desa::where('id',$peminjam->alamat->village_id)->value('name'),
            ],
            'kepemilikah_rumah'      => isset($peminjam->kepemilikan_rumah->nama)? $peminjam->kepemilikan_rumah->nama : "",
            'nama_tempat_kerja'      => $peminjam->nama_tempat_kerja,
            'alamat_tempat_kerja'    => [
                'alamat_lengkap'     => isset($peminjam->alamat_kerja->alamat)? $peminjam->alamat_kerja->alamat : "",
                'provinsi'           => isset($peminjam->alamat_kerja->province_id)? Provinsi::where('id',$peminjam->alamat_kerja->province_id)->value('name') : "",
                'kota'               => isset($peminjam->alamat_kerja->regency_id)? Kota::where('id',$peminjam->alamat_kerja->regency_id)->value('name') : "",
                'kecamatan'          => isset($peminjam->alamat_kerja->district_id)? Kecamatan::where('id',$peminjam->alamat_kerja->district_id)->value('name') : "",
                'desa'               => isset($peminjam->alamat_kerja->village_id)? Desa::where('id',$peminjam->alamat_kerja->village_id)->value('name') : ""
            ],
            'lama_berdiri_tempat_kerja' => $peminjam->lama_berdiri_tempat_kerja,
            'jumlah_murid_tempat_kerja' => $peminjam->jumlah_murid_tempat_kerja,
            'gaji'                   => $peminjam->gaji,
            'jabatan'                => isset($peminjam->jabatan->nama)? $peminjam->jabatan->nama : "",
            'status_pengajar'        => $peminjam->status_pengajar,
            'lama_mengajar'          => $peminjam->lama_mengajar,
            'pekerjaan_suami_istri'  => isset($peminjam->pekerjaan->nama)? $peminjam->pekerjaan->nama : "",
            'pendapatan_suami_istri' => $peminjam->pendapatan_suami_istri,
            'jumlah_tanggungan'      => $peminjam->jumlah_tanggungan,
            'curiculum_vitae'        => isset($peminjam->photo_curiculum_vitae->name)? $peminjam->photo_curiculum_vitae->name : ""
        );

        $pinjaman = array();
        $pinjaman_lainnya = KeuanganPeminjam::where('peminjam_id',$peminjam->id)->where('type','pinjaman')->get();
        foreach ($pinjaman_lainnya as $item) {
            $pinjaman[] = array(
                'jumlah' => $item->jumlah,
                'keterangan' => $item->keterangan
            );
        }

        $pendapatan = array();
        $pendapatan_lainnya = KeuanganPeminjam::where('peminjam_id',$peminjam->id)->where('type','pendapatan')->get();
        foreach ($pendapatan_lainnya as $item) {
            $pendapatan[] = array(
                'jumlah' => $item->jumlah,
                'keterangan' => $item->keterangan
            );
        }

        $data['keuangan']['pendapatan'] = $pendapatan;
        $data['keuangan']['pinjaman'] = $pinjaman;

        return $data;
    }

    # halaman rincian pinjaman atau daftar pembayaran cicilan ( user type = peminjam )
    public function installment($pinjaman_id)
    {
        $real_id = $this->decrypt_id($pinjaman_id);
        $data['pinjaman_id'] = $pinjaman_id;
        $data['pinjaman'] = Pinjaman::find($real_id);
        $data['sisa_pinjaman'] = Pinjaman::sisa_pinjaman($real_id);
        $data['list_bank'] = AkunBank::where('is_akun_admin',1)->get();
        $data['cicilan'] = Cicilan::select('cicilan.id','cicilan.jumlah','cicilan.tanggal AS jatuh_tempo','transaksi.tanggal','ref','cicilan.status','transaksi.id AS transaksi_id')
            ->where('pinjaman_id',$real_id)
            ->leftjoin('transaksi','cicilan.transaksi_id','=','transaksi.id')
            ->get();

        return view('mockup.pinjaman.rincian',$data);
    }

    # proses pembayaran cicilan
    public function pay_installment(Request $request, $cicilan_id)
    {
        # validate form
        $validator = Validator::make($request->all(),
            [
                'g-recaptcha-response' =>'required|recaptcha'
            ],
            [
                'g-recaptcha-response.required' => 'Please ensure that you are a human.!',
            ]
        );

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $real_id = $this->decrypt_id($cicilan_id);
        $nominal = $this->rupiah_to_int($request->nominal);

        $transaksi = new Transaksi();
        $transaksi->kode_transaksi = '01';
        $transaksi->tanggal = $request->tanggal;
        $transaksi->jumlah = $nominal;
        $transaksi->ref = $request->no_transaksi;
        $transaksi->deskripsi = $request->catatan;
        $transaksi->akun_bank_id = $request->bank;
        $transaksi->akun_bank_nama_bank = $request->nama_bank;
        $transaksi->akun_bank_no_akun = $request->no_rekening;
        $transaksi->akun_bank_nama_akun = $request->nama_akun;
        $transaksi->save();

        if($request->hasFile('bukti')){
          if ($request->file('bukti')->isValid()) {
            $request->file('bukti')->storeAs('bukti_transfer_cicilan','bukti_transfer_cicilan_'.date('jmY_His').'_'.$request->user()->id.'.'.$request->file('bukti')->extension());

            $file = new FileUploads();
            $file->type = 'dokumen';
            $file->name = 'bukti_transfer_cicilan_'.date('jmY_His').'_'.$request->user()->id.'.'.$request->file('bukti')->extension();
            $file->descriptions = "BUKTI TRANSFER CICILAN";
            $file->mime = $request->file('bukti')->getMimeType();
            $file->size = $request->file('bukti')->getClientSize();
            $file->save();

            $transaksi->file_id = $file->id;
            $transaksi->save();
          }
        }


        $cicilan = Cicilan::find($real_id);
        $cicilan->transaksi_id = $transaksi->id;
        $cicilan->status = 'pending';
        $cicilan->save();

        $request->session()->flash('success','Pembayaran Berhasil Disimpan');
        return redirect()->back();
    }

    public function detail_payment($transaksi_id)
    {
      $data = Transaksi::select('transaksi.*','m_bank.nama AS nama_bank','akun_bank.nama_akun_bank','file.name AS nama_file')
      ->join('akun_bank','transaksi.akun_bank_id','=','akun_bank.id')
      ->join('m_bank','akun_bank.bank_id','=','m_bank.id')
      ->leftjoin('file','transaksi.file_id','=','file.id')
      ->where('transaksi.id',$transaksi_id)
      ->first();
      return response()->json($data);
    }

    # ajax list pembayaran cicilan
    public function data_installment($pinjaman_id)
    {
        $real_id = $this->decrypt_id($pinjaman_id);
        $pembayaran = Transaksi::pembayaran_cicilan($real_id);

        return Datatables::of($pembayaran)
            ->addIndexColumn()
            ->addColumn('jenis_transaksi','Cicilan')
            ->make(true);
    }

    # halaman daftar pembayaran
    public function payment_history()
    {
        return view('mockup.pinjaman.pembayaran');
    }

    # ajax list daftar pembayaran
    public function list_payment(Request $request)
    {
        $transaksi = Transaksi::history($request->user()->peminjam->id);

        return Datatables::of($transaksi)
            ->addIndexColumn()
            ->editColumn('tanggal',function ($transaksi){
                return date_format(date_create($transaksi->tanggal),'j F Y');
            })
            ->editColumn('jatuh_tempo', function ($transaksi){
                return date_format(date_create($transaksi->jatuh_tempo),'j F Y');
            })
            ->editColumn('status', function ($transaksi){
                if($transaksi->status == 'late'){
                    $p = "Terlambat";
                }else{
                    $p = "Tepat Waktu";
                }
                return $p;
            })
            ->make(true);
    }

    # export pinjaman ke pdf
    public function export_pinjaman(Request $request)
    {
        $pinjaman = Pinjaman::listing_pinjaman(Auth::user()->peminjam->id);
        $viewPdf = view('export.daftar_pinjaman',compact('pinjaman'));

        $mpdf = new Mpdf([
            'mode' => 'utf-8',
            'format' => 'A4',
            'orientation' => 'L'
        ]);

        $mpdf->WriteHTML($viewPdf);
        $mpdf->Output("Daftar_Pinjaman.pdf","I");
    }

    # export pembayaran cicilan ke pdf
    public function export_pembayaran_cicilan(Request $request)
    {
        $pembayaran = Transaksi::history($request->user()->peminjam->id);
        $viewPdf = view('export.daftar_pembayaran_cicilan',compact('pembayaran'));

        $mpdf = new Mpdf([
            'mode' => 'utf-8',
            'format' => 'A4',
            'orientation' => 'L'
        ]);

        $mpdf->WriteHTML($viewPdf);
        $mpdf->Output("Daftar_Pembayaran.pdf","I");
    }
}
