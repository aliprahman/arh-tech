<?php

namespace App\Http\Controllers\Peminjam;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\HubunganKontak;
use App\Pendidikan;
use App\PengalamanKerja;
use App\KepemilikanRumah;
use App\StatusPernikahan;
use App\Peminjam;
use App\Alamat;
use App\Bank;
use App\Provinsi;
use App\FileUploads;
use App\User;
use App\AkunBank;
use App\Biodata as BiodataModel;
use App\Pekerjaan;
use App\Jabatan;
use App\KeuanganPeminjam;
use App\Traits\Common;
use Hash;

class Biodata extends Controller
{
    use Common;

    public function index()
    {
        $data['pendidikan'] = Pendidikan::pluck('id','nama');
        $data['status_pernikahan'] = StatusPernikahan::pluck('id','nama');
        $data['kepemilikan_rumah'] = KepemilikanRumah::pluck('id','nama');
        $data['hubungan_kontak'] = HubunganKontak::pluck('id','nama');
        $data['pekerjaan'] = Pekerjaan::pluck('id','nama');
        $data['jabatan'] = Jabatan::pluck('id','nama');
        $data['list_provinsi'] = Provinsi::pluck('id','name');
        $data['list_bank'] = Bank::pluck('id','nama');
        $data['user'] = Auth::user();
        $data['pendapatan_lainnya'] = KeuanganPeminjam::where('peminjam_id',Auth::user()->peminjam->id)
            ->where('type','pendapatan')->get();
        $data['pinjaman_lainnya'] = KeuanganPeminjam::where('peminjam_id',Auth::user()->peminjam->id)
            ->where('type','pinjaman')->get();
        $data['peminjam_id'] = $this->encrypt_id(Auth::user()->peminjam->id);

        return view('mockup.peminjam.biodata',$data);
    }

    public function update(Request $request)
    {
        $user = Auth::user();

        # validate form
        /*Validator::extend('old_password', function ($attribute, $value, $parameters) {
            return Hash::check($value, Auth::user()->password);
        });

        Validator::replacer('old_password', function ($message, $attribute, $value, $parameters) {
            return 'Password Lama tidak valid';
        });*/

        $validator = Validator::make($request->all(),
          [
            'email'                => [
                Rule::unique('users')->ignore($user->id),
            ],
            'no_hp'            => [
                Rule::unique('biodata')->ignore($user->biodata_id),
            ],
            'no_ktp'                  => [
                Rule::unique('biodata')->ignore($user->biodata_id),
            ],
            'no_npwp'                 => [
                Rule::unique('biodata')->ignore($user->biodata_id),
            ],
            //'old_password' => 'old_password',
            'new_password' => 'confirmed',
          ]
        );

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $biodata = BiodataModel::find($user->biodata_id);
        $biodata->tempat_lahir = $request->tempat_lahir;
        $biodata->tanggal_lahir= $request->tanggal_lahir;
        $biodata->jenis_kelamin= $request->jenis_kelamin;
        $biodata->no_telepon   = $request->no_telepon;
        $biodata->no_hp        = $request->no_hp;
        $biodata->no_ktp       = $request->no_ktp;
        $biodata->no_npwp      = $request->no_npwp;
        $biodata->save();

        $user = User::find($user->id);
        $user->name       = $request->nama;
        $user->email      = $request->email;
        if($request->new_password) {
            $user->password   = bcrypt($request->new_password);
        }
        $user->save();

        if($request->hasFile('profile_photo')){
          if ($request->file('profile_photo')->isValid()) {
            if($user->photo_profile_id != ""){
                $file = FileUploads::find($user->photo_profile_id);
                if(file_exists(storage_path('app/public/profile_photo/'.$file->name))){
                  unlink(storage_path('app/public/profile_photo/'.$file->name));
                }
            } else {
                $file = new FileUploads();
            }
            $request->file('profile_photo')->storeAs('profile_photo','peminjam_profile_photo_'.$user->id.'.'.$request->file('profile_photo')->extension());

            $file->type = 'profile_photo';
            $file->name = 'peminjam_profile_photo_'.$user->id.'.'.$request->file('profile_photo')->extension();
            $file->descriptions = "Photo Profile";
            $file->mime = $request->file('profile_photo')->getMimeType();
            $file->size = $request->file('profile_photo')->getClientSize();
            $file->save();

            $user->photo_profile_id = $file->id;
            $user->save();
          }  
        }

        $alamat_diri = Alamat::find($user->peminjam->alamat_id);
        $alamat_diri->alamat      = $request->alamat;
        $alamat_diri->kode_pos    = $request->kode_pos_1;
        $alamat_diri->province_id = $request->prov_1;
        $alamat_diri->regency_id  = $request->kota_1;
        $alamat_diri->district_id = $request->kec_1;
        $alamat_diri->village_id  = $request->kel_1;
        $alamat_diri->save();

        $akun_bank = AkunBank::find($user->peminjam->akun_bank_id);
        $akun_bank->bank_id        = $request->bank;
        $akun_bank->nama_akun_bank = $request->nama_akun;
        $akun_bank->no_akun_bank   = $request->no_rekening;
        $akun_bank->save();

        if($user->peminjam->alamat_tempat_kerja_id != ""){
          $alamat_sekolah = Alamat::find($user->peminjam->alamat_tempat_kerja_id);
        }else{
          $alamat_sekolah = new Alamat();
        }
        $alamat_sekolah->alamat      = $request->alamat_sekolah;
        $alamat_sekolah->kode_pos    = $request->kode_pos_2;
        $alamat_sekolah->province_id = $request->prov_2;
        $alamat_sekolah->regency_id  = $request->kota_2;
        $alamat_sekolah->district_id = $request->kec_2;
        $alamat_sekolah->village_id  = $request->kel_2;
        $alamat_sekolah->save();

        $peminjam = Peminjam::find($user->peminjam->id);
        $peminjam->nama_tempat_kerja      = $request->nama_sekolah;
        $peminjam->jumlah_murid_tempat_kerja = $request->jumlah_murid;
        $peminjam->lama_berdiri_tempat_kerja = $request->lama_berdiri;
        $peminjam->jabatan_id = $request->jabatan;
        $peminjam->status_pengajar = $request->status_pengajar;
        $peminjam->lama_mengajar = $request->lama_mengajar;
        $peminjam->gaji = $this->rupiah_to_int($request->gaji);
        $peminjam->alamat_tempat_kerja_id = $alamat_sekolah->id;
        $peminjam->no_telepon_tempat_kerja = $request->no_telepon_tempat_kerja;
        $peminjam->save();

        KeuanganPeminjam::where('peminjam_id',$peminjam->id)->where('type','pendapatan')->forceDelete();
        for($i=0; $i < count($request->pendapatan_jumlah); $i++){
            $pendapatan = new KeuanganPeminjam();
            $pendapatan->peminjam_id = $peminjam->id;
            $pendapatan->keterangan = $request->input('pendapatan_keterangan.'.$i);
            $pendapatan->jumlah = $this->rupiah_to_int($request->input('pendapatan_jumlah.'.$i));
            $pendapatan->type = 'pendapatan';
            $pendapatan->save();
        }

        KeuanganPeminjam::where('peminjam_id',$peminjam->id)->where('type','pinjaman')->forceDelete();
        for($i=0; $i < count($request->pinjaman_jumlah); $i++){
            $pinjaman = new KeuanganPeminjam();
            $pinjaman->peminjam_id = $peminjam->id;
            $pinjaman->keterangan = $request->input('pinjaman_keterangan.'.$i);
            $pinjaman->jumlah = $this->rupiah_to_int($request->input('pinjaman_jumlah.'.$i));
            $pinjaman->type = 'pinjaman';
            $pinjaman->save();
        }

        if ($request->hasFile('photo_tempat_kerja')) {
            if ($request->file('photo_tempat_kerja')->isValid()) {
                $request->file('photo_tempat_kerja')
                ->storeAs('tempat_kerja','photo_tempat_kerja_'.$user->id.'.'.$request->file('photo_tempat_kerja')->extension());

                if($peminjam->photo_tempat_kerja_id != ""){
                    $file = FileUploads::find($peminjam->photo_tempat_kerja_id);
                    if(file_exists(storage_path('app/public/tempat_kerja/'.$file->name))){
                      unlink(storage_path('app/public/tempat_kerja/'.$file->name));
                    }
                }else{
                    $file = new FileUploads();
                }

                $file->type = 'photo_tempat_kerja';
                $file->name = 'photo_tempat_kerja_'.$peminjam->user_id.'.'.$request->file('photo_tempat_kerja')->extension();
                $file->descriptions = "Photo Tempat Kerja";
                $file->mime = $request->file('photo_tempat_kerja')->getMimeType();
                $file->size = $request->file('photo_tempat_kerja')->getClientSize();
                $file->save();

                $peminjam->photo_tempat_kerja_id = $file->id;
                $peminjam->save();
            }
        }

        if ($request->hasFile('photo_tempat_tinggal')) {
            if ($request->file('photo_tempat_tinggal')->isValid()) {
                $request->file('photo_tempat_tinggal')
                ->storeAs('tempat_tinggal','photo_tempat_tinggal_'.$user->id.'.'.$request->file('photo_tempat_tinggal')->extension());

                if($peminjam->photo_tempat_tinggal_id != ""){
                    $file = FileUploads::find($peminjam->photo_tempat_tinggal_id);
                    if(file_exists(storage_path('app/public/tempat_tinggal/'.$file->name))){
                      unlink(storage_path('app/public/tempat_tinggal/'.$file->name));
                    }
                }else{
                    $file = new FileUploads();
                }

                $file->type = 'photo_tempat_tinggal';
                $file->name = 'photo_tempat_tinggal_'.$peminjam->user_id.'.'.$request->file('photo_tempat_tinggal')->extension();
                $file->descriptions = "Photo Tempat Tinggal";
                $file->mime = $request->file('photo_tempat_tinggal')->getMimeType();
                $file->size = $request->file('photo_tempat_tinggal')->getClientSize();
                $file->save();

                $peminjam->photo_tempat_tinggal_id = $file->id;
                $peminjam->save();
            }
        }

        if ($request->hasFile('rekening_koran')) {
            if ($request->file('rekening_koran')->isValid()) {
                $request->file('rekening_koran')
                ->storeAs('rekening_koran','rekening_koran_'.$user->id.'.'.$request->file('rekening_koran')->extension());

                if($peminjam->photo_rekening_koran_id != ""){
                    $file = FileUploads::find($peminjam->photo_rekening_koran_id);
                    if(file_exists(storage_path('app/public/rekening_koran/'.$file->name))){
                      unlink(storage_path('app/public/rekening_koran/'.$file->name));
                    }
                }else{
                    $file = new FileUploads();
                }

                $file->type = 'rekening_koran';
                $file->name = 'rekening_koran_'.$peminjam->user_id.'.'.$request->file('rekening_koran')->extension();
                $file->descriptions = "Rekening Koran";
                $file->mime = $request->file('rekening_koran')->getMimeType();
                $file->size = $request->file('rekening_koran')->getClientSize();
                $file->save();

                $peminjam->photo_rekening_koran_id = $file->id;
                $peminjam->save();
            }
        }

        if ($request->hasFile('curiculum_vitae')) {
            if ($request->file('curiculum_vitae')->isValid()) {
                $request->file('curiculum_vitae')
                ->storeAs('curiculum_vitae','curiculum_vitae_'.$user->id.'.'.$request->file('curiculum_vitae')->extension());

                if($peminjam->photo_curiculum_vitae_id != ""){
                    $file = FileUploads::find($peminjam->photo_curiculum_vitae_id);
                    if(file_exists(storage_path('app/public/curiculum_vitae/'.$file->name))){
                      unlink(storage_path('app/public/curiculum_vitae/'.$file->name));
                    }
                }else{
                    $file = new FileUploads();
                }

                $file->type = 'cv';
                $file->name = 'curiculum_vitae_'.$peminjam->user_id.'.'.$request->file('curiculum_vitae')->extension();
                $file->descriptions = "Curiculum Vitae";
                $file->mime = $request->file('curiculum_vitae')->getMimeType();
                $file->size = $request->file('curiculum_vitae')->getClientSize();
                $file->save();

                $peminjam->photo_curiculum_vitae_id = $file->id;
                $peminjam->save();
            }
        }

        $request->session()->flash('success','Biodata Berhasil Diperbaharui');
        return redirect()->back();

    }
    public function update_data_pendukung(Request $request, $id)
    {
        $peminjam = Peminjam::find($this->decrypt_id($id));
        $peminjam->no_sk_surat_kontrak = $request->no_sk_surat_kontrak;
        $peminjam->tanggal_sk_surat_kontrak = $request->tanggal_sk_surat_kontrak;
        $peminjam->kepemilikan_rumah_id = $request->kepemilikan_rumah_id;
        $peminjam->pendidikan_id = $request->pendidikan_id;
        $peminjam->status_pernikahan_id = $request->status_pernikahan_id;
        $peminjam->nama_gadis_ibu_kandung = $request->nama_gadis_ibu_kandung;
        $peminjam->kontak_nama = $request->kontak_nama;
        $peminjam->hubungan_kontak_id = $request->hubungan_kontak_id;
        $peminjam->no_hp_kontak = $request->no_hp_kontak;
        $peminjam->no_telp_kontak = $request->no_telp_kontak;
        $peminjam->nama_suami_istri = $request->nama_suami_istri;
        $peminjam->tempat_lahir_suami_istri = $request->tempat_lahir_suami_istri;
        $peminjam->tanggal_lahir_suami_istri = $request->tanggal_lahir_suami_istri;
        $peminjam->no_ktp_suami_istri = $request->no_ktp_suami_istri;
        $peminjam->pekerjaan_suami_istri_id = $request->pekerjaan;
        $peminjam->pendapatan_suami_istri = $this->rupiah_to_int($request->pendapatan);
        $peminjam->jumlah_tanggungan = $request->jumlah_tanggungan;
        $peminjam->save();

        $alamat_kontak = Alamat::updateOrCreate(
            ['id'=>$peminjam->alamat_kontak_id],
            ['alamat'=>$request->alamat_rumah_kontak]
        );

        $peminjam->alamat_kontak_id = $alamat_kontak->id;
        $peminjam->save();

        $tempat_mengajar = array_filter($request->input('tempat_mengajar'));
        $dari_tahun = array_filter($request->input('dari_tahun'));
        $sampai_tahun = array_filter($request->input('sampai_tahun'));
        if(count($tempat_mengajar) > 0){
            PengalamanKerja::where('peminjam_id',$peminjam->id)->forceDelete();
        }
        for($i=0; $i < count($tempat_mengajar); $i++){
            $pengalaman = new PengalamanKerja();
            $pengalaman->dari_tahun = $dari_tahun[$i];
            $pengalaman->sampai_tahun = $sampai_tahun[$i];
            $pengalaman->tempat_kerja = $tempat_mengajar[$i];
            $pengalaman->peminjam_id = $peminjam->id;
            $pengalaman->save();
        }

        if ($request->hasFile('photo_sk_surat_kontak')) {
            if ($request->file('photo_sk_surat_kontak')->isValid()) {
                $request->file('photo_sk_surat_kontak')
                ->storeAs('surat_sk','photo_sk_surat_kontak_'.$peminjam->user_id.'.'.$request->file('photo_sk_surat_kontak')->extension());

                if($peminjam->photo_sk_surat_kontrak_id != ""){
                    $file = FileUploads::find($peminjam->photo_sk_surat_kontrak_id);
                    if(file_exists(storage_path('app/public/surat_sk/'.$file->name))){
                      unlink(storage_path('app/public/surat_sk/'.$file->name));
                    }
                }else{
                    $file = new FileUploads();
                }

                $file->type = 'photo_sk';
                $file->name = 'photo_sk_surat_kontak_'.$peminjam->user_id.'.'.$request->file('photo_sk_surat_kontak')->extension();
                $file->descriptions = "Photo Surat Kontrak";
                $file->mime = $request->file('photo_sk_surat_kontak')->getMimeType();
                $file->size = $request->file('photo_sk_surat_kontak')->getClientSize();
                $file->save();

                $peminjam->photo_sk_surat_kontrak_id = $file->id;
                $peminjam->save();
            }
        }

        return redirect()->back();
    }
}
