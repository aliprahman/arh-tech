<?php

namespace App\Http\Middleware;

use Closure;

use Auth;
use Session;

class UserVerification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if (Auth::check() && Auth::user()->status == "new"){
            $link = url('auth/resend')."?email=".urlencode(Auth::user()->email);
            Auth::guard()->logout();
            Session::flash("notif", [
                "level" => "warning",
                "message" => "Silahkan klik link aktivasi yang telah kami kirim. <a class='alert-link' href='$link'>Kirim Lagi</a>"
            ]);
            return redirect('/pemodal/login');
        }
        if (Auth::check() && Auth::user()->status == "deleted"){
            Auth::guard()->logout();
            Session::flash("notif", [
                "level" => "danger",
                "message" => "Akun Anda telah dihapus. Silahkan hubungi bagian Administrator."
            ]);
            return redirect('/pemodal/login');
        }
        if (Auth::check() && Auth::user()->status == "blocked"){
            Auth::guard()->logout();
            Session::flash("notif", [
                "level" => "danger",
                "message" => "Akun Anda telah terblokir. Silahkan hubungi bagian Administrator."
            ]);
            return redirect('/pemodal/login');
        }
        return $response;
    }
}
