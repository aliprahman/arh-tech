<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->status == 'new') {
            Auth::guard()->logout();
            return abort(403);
        }
        if(Auth::user()->status == 'blocked'){
            Auth::guard()->logout();
            return abort(400);
        }
        if(Auth::user()->status == 'deleted'){
            Auth::guard()->logout();
            return abort(401);
        }
        return $next($request);
    }
}
