<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Library\Beam\Acl\Acl;


class CheckAcl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        $role = 'Guest';
        Acl::Build();
        $regexTrue = "/";
        $dbRsc = \DB::table('acl_resource')->get();
        if(!empty($dbRsc)){
            foreach($dbRsc as $regex){               
                $getPathRegex = preg_match("/".$regex->name."/", $request->path(), $matches);                
                if($getPathRegex > 0){
                   $regexTrue = $regex->name;
                }
            }
        }            
        if(!empty($user->role)){
            $role = $user->role->name;
        }
               
        if(Acl::isAllowed($role, $regexTrue)){
            return $next($request);
        }
        return abort(409);
    }
}
