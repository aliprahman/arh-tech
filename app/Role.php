<?php
namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model {
	
	protected $table = 'acl_role';
	
	protected $guarded = array();

	public static $rules = array(
		'name'		=> 'required|unique',
	);
	
	public function children()
	{
		return $this->belongsToMany('\App\Role', 'acl_role_parent', 'parent', 'role_id')->withPivot('order');
	}

	public function parents()
	{
		return $this->belongsToMany('\App\Role', 'acl_role_parent', 'role_id', 'parent')->withPivot('order');
	}
	
	public function resources()
	{
		return $this->belongsToMany('\App\Resource', 'acl_rule', 'role_id', 'resource_id')->withPivot(array('access'));
	}
	
	public function scopeOrderByName($query)
	{
		return $query->orderBy('name');
	}
	
	public function scopeRoots($query)
	{
		return $query->orderByName()
				->where('parent', null);
	}
	
	public function users()
	{
		return $this->hasMany('User');
	}
}
