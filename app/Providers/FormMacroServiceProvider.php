<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class FormMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        /*
        *   Author : Rizki Fauzi
        */
        \Form::macro('selectMonthCustom', function () {
            $month = [                
                '1'    => 'January',
                '2'    => 'February',
                '3'    => 'March',
                '4'    => 'April',
                '5'    => 'May',
                '6'    => 'June',
                '7'    => 'July',
                '8'    => 'August',
                '9'    => 'September',
                '10'   => 'October',
                '11'   => 'November',
                '12'   => 'December',
                '13'    => 'Last 3 Month',
            ];
            return \Form::select('month', $month, 13, ['class' => 'form-control', 'id' => 'month']);
        });

        \Form::macro('selectYearCustom', function () {
            $years = [];
            $currentYear = date('Y');
            for ($year=2018; $year <= $currentYear; $year++) $years[$year] = $year;
            return \Form::select('year', $years, $currentYear, ['class' => 'form-control', 'id' => 'year']);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
