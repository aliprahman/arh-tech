<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Peminjam extends Model
{
    use SoftDeletes;

    protected $table = "peminjam";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'nama_gadis_ibu_kandung', 'alamat_id', 'kepemilikan_rumah_id',
        'photo_tempat_tinggal_id', 'nama_tempat_kerja', 'alamat_tempat_kerja_id',
        'photo_tempat_kerja_id', 'no_sk_surat_kontrak', 'tanggal_sk_surat_kontrak',
        'status_pernikahan_id', 'pendidikan_id', 'kontak_nama', 'hubungan_kontak_id',
        'alamat_kontak_id', 'no_telepon_kontak', 'no_hp_kontak', 'nama_suami_istri',
        'tempat_lahir_suami_istri', 'tanggal_lahir_suami_istri', 'no_ktp_suami_istri',
        'jumlah_tanggungan', 'akun_bank_id', 'saldo', 'status', 'jabatan_id', 'pekerjaan_suami_istri_id',
        'pendapatan_suami_istri', 'lama_mengajar', 'gaji', 'status_pengajar'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function alamat()
    {
        return $this->belongsTo('App\Alamat','alamat_id');
    }

    public function alamat_kerja()
    {
        return $this->belongsTo('App\Alamat','alamat_tempat_kerja_id');
    }

    public function akun_bank()
    {
        return $this->belongsTo('App\AkunBank','akun_bank_id');
    }

    public function photo_tempat_tinggal()
    {
        return $this->belongsTo('App\FileUploads','photo_tempat_tinggal_id');
    }

    public function photo_tempat_kerja()
    {
        return $this->belongsTo('App\FileUploads','photo_tempat_kerja_id');
    }

    public function photo_surat_kontrak()
    {
        return $this->belongsTo('App\FileUploads','photo_sk_surat_kontrak_id');
    }

    public function photo_rekening_koran()
    {
        return $this->belongsTo('App\FileUploads','photo_rekening_koran_id');
    }

    public function photo_curiculum_vitae()
    {
        return $this->belongsTo('App\FileUploads','photo_curiculum_vitae_id');
    }

    public function pendidikan()
    {
        return $this->belongsTo('App\Pendidikan','pendidikan_id');
    }

    public function hubungan_kontak()
    {
        return $this->belongsTo('App\HubunganKontak','hubungan_kontak_id');
    }

    public function status_pernikahan()
    {
        return $this->belongsTo('App\StatusPernikahan','status_pernikahan_id');
    }

    public function kepemilikan_rumah()
    {
        return $this->belongsTo('App\KepemilikanRumah','kepemilikan_rumah_id');
    }

    public function pengalaman_kerja()
    {
        return $this->hasMany('App\PengalamanKerja','peminjam_id');
    }

    public function getTanggalSkSuratKontrakAttribute($value)
    {
        return date_format(date_create($value),'d-m-Y');
    }

    public function setTanggalSkSuratKontrakAttribute($value)
    {
        $this->attributes['tanggal_sk_surat_kontrak'] = date_format(date_create($value),'Y-m-d');
    }

    public function getTanggalLahirSuamiIstriAttribute($value)
    {
        return date_format(date_create($value),'d-m-Y');
    }

    public function setTanggalLahirSuamiIstriAttribute($value)
    {
        $this->attributes['tanggal_lahir_suami_istri'] = date_format(date_create($value),'Y-m-d');
    }

    public function pinjaman()
    {
        return $this->hasMany('App\Pinjaman','peminjam_id');
    }

    public function jabatan()
    {
        return $this->belongsTo('App\Jabatan','jabatan_id');
    }

    public function pekerjaan()
    {
        return $this->belongsTo('App\Pekerjaan','pekerjaan_suami_istri_id');
    }

    public function keuangan_peminjam()
    {
        return $this->hasMany('App\Peminjam','peminjam_id');
    }

    public static function is_data_complete($id)
    {
        $peminjam = self::find($id);
        # surat kontrak mengajar
        if($peminjam->no_sk_surat_kontrak == "" || $peminjam->tanggal_sk_surat_kontrak == "" || $peminjam->photo_sk_surat_kontrak_id == ""){
          return FALSE;
        }
        # pengalaman kerja
        if(count($peminjam->pengalaman_kerja) < 1){
          return FALSE;
        }
        # data pribadi
        if($peminjam->kepemilikan_rumah_id == "" || $peminjam->pendidikan_id == "" || $peminjam->status_pernikahan_id == "" || $peminjam->nama_gadis_ibu_kandung == ""){
          return FALSE;
        }
        # kontak
        if($peminjam->kontak_nama == "" || $peminjam->hubungan_kontak_id == "" || $peminjam->alamat_kontak_id == "" || $peminjam->no_telp_kontak == "" || $peminjam->no_hp_kontak == ""){
          return FALSE;
        }
        # data keluarga
        if($peminjam->nama_suami_istri == "" || $peminjam->tempat_lahir_suami_istri == "" || $peminjam->tanggal_lahir_suami_istri == "" || $peminjam->no_ktp_suami_istri == "" ||
        $peminjam->pekerjaan_suami_istri_id == "" || $peminjam->pendapatan_suami_istri == "" || $peminjam->jumlah_tanggungan == ""){
          return FALSE;
        }
        return TRUE;
    }
}
