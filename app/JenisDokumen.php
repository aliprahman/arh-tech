<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JenisDokumen extends Model
{
    use SoftDeletes;

    protected $table = "m_jenis_dokumen_pinjaman";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function dokumen()
    {
        return $this->hasMany('App\Dokumen','jenis_dokumen_pinjaman_id');
    }
}
