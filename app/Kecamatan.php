<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kecamatan extends Model
{
    use SoftDeletes;

    protected $table = "m_district";

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'regency_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function alamat()
    {
        return $this->hasMany('App\Alamat','district_id');
    }

    public function kota()
    {
        return $this->belongsTo('App\Kota','regency_id');
    }

    public function desa()
    {
        return $this->hasMany('App\Desa','district_id');
    }
}
