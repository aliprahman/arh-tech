<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kota extends Model
{
    use SoftDeletes;

    protected $table = "m_regency";

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'province_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function alamat()
    {
        return $this->hasMany('App\Alamat','regency_id');
    }

    public function provinsi()
    {
        return $this->belongsTo('App\Provinsi','province_id');
    }

    public function kecamatan()
    {
        return $this->hasMany('App\Kecamatan','regency_id');
    }
}
