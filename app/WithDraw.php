<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WithDraw extends Model
{
    use SoftDeletes;

    protected $table = "withdraw";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pemodal_id', 'transaksi_id', 'tanggal', 'jumlah', 'status'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function transaksi()
    {
        return $this->belongsTo('App\Transaksi','transaksi_id');
    }

    //pemodal
    public function pemodal()
    {
        return $this->belongsTo('App\Pemodal','pemodal_id');
    }

}
