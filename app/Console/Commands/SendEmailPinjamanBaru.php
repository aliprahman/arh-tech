<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailPinjamanBaru;
use App\Pinjaman;
use App\User;

class SendEmailPinjamanBaru extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send-notif-pinjaman-baru';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Kirim Email Ke Pemodal Sebagai Notifikasi Ada Pinjaman Baru Atau Pinjaman Aktif Yang Belum Terdanai 100%';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pinjaman = Pinjaman::select('id','kode','jumlah_pinjaman','waktu_pinjaman','bunga','tanggal_mulai_funding',
                  'tanggal_berakhir_funding','status','jumlah_funding AS jumlah_funding')
              ->whereIn('status',['listing','funding'])
              ->whereRaw('jumlah_pinjaman > IFNULL(jumlah_funding,0)')
              ->get();

        $pemodal = User::where('type','pemodal')->where('status','confirmed')->get();

        if(count($pinjaman) > 0){
          $bar = $this->output->createProgressBar(count($pemodal));
          foreach ($pemodal as $user) {
            Mail::to($user->email)->send(new EmailPinjamanBaru($user,$pinjaman));
            $bar->advance();
          }
          $bar->finish();
        }else{
          $this->info('tidak ada pinjaman baru ataupun pinjaman aktif');
        }
    }
}
