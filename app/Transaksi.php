<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Transaksi extends Model
{
    use SoftDeletes;

    protected $table = "transaksi";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tanggal', 'jumlah', 'kode_transaksi', 'deskripsi', 'reft',
        'akun_bank_id', 'akun_bank_nama_bank', 'akun_bank_no_akun',
        'akun_bank_nama_akun','file_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function cicilan()
    {
        return $this->hasMany('App\Cicilan','transaksi_id');
    }

    public function akun_bank()
    {
        return $this->belongsTo('App\AkunBank','akun_bank_id');
    }

    public function withdraw()
    {
        return $this->hasMany('App\WithDraw','transaksi_id');
    }

    public function deposit()
    {
        return $this->hasMany('App\Deposit','transaksi_id');
    }

    public function pinjaman()
    {
        return $this->hasMany('App\Pinjaman','transaksi_id');
    }

    public function file()
    {
        return $this->belongsTo('App\FileUploads','file_id');
    }

    public static function pembayaran_cicilan($pinjaman_id)
    {
        return self::select('transaksi.tanggal','ref','transaksi.jumlah','cicilan.jumlah AS sisa_cicilan')
                ->join('cicilan','transaksi.id','=','cicilan.transaksi_id')
                ->where('cicilan.pinjaman_id',$pinjaman_id)
                ->where('cicilan.status','!=','unverified')
                ->whereNull('cicilan.deleted_at')
                ->get();
    }

    public static function history($peminjam_id)
    {

        return self::select(DB::raw('transaksi.tanggal,ref,transaksi.kode_transaksi,transaksi.deskripsi,transaksi.akun_bank_nama_bank,
                            transaksi.akun_bank_no_akun,transaksi.akun_bank_nama_akun,transaksi.jumlah,cicilan.status,pinjaman.kode,cicilan.pinjaman_id,cicilan.tanggal AS jatuh_tempo'))
                ->join('cicilan','transaksi.id','=','cicilan.transaksi_id')
                ->join('pinjaman','cicilan.pinjaman_id','=','pinjaman.id')
                ->whereNull('pinjaman.deleted_at')
                ->whereNull('cicilan.deleted_at')
                ->where('pinjaman.peminjam_id',$peminjam_id)
                ->orderBy('transaksi.tanggal','desc')
                ->get();
    }

    public static function history_transaksi($month = null, $years = '2018')
    {
        $user = \Auth::user();
        $history = self::select(DB::raw('transaksi.id, transaksi.tanggal,ref,transaksi.kode_transaksi,transaksi.deskripsi,transaksi.akun_bank_nama_bank,
                            transaksi.akun_bank_no_akun,transaksi.akun_bank_nama_akun,transaksi.jumlah,
                            transaksi.kode_transaksi as kode,
                            IF(kode_transaksi = "03" OR kode_transaksi = "01", transaksi.jumlah, 0) as debit,
                            IF(kode_transaksi = "02", transaksi.jumlah, 0) as credit'))
                            ->leftJoin('cicilan','transaksi.id','=','cicilan.transaksi_id')
                            ->leftJoin('pinjaman','cicilan.pinjaman_id','=','pinjaman.id')
                            ->leftjoin('withdraw', 'transaksi.id','=','withdraw.transaksi_id')
                            ->leftjoin('pemodal', 'withdraw.pemodal_id','=','pemodal.id')
                            ->leftjoin('deposit', 'deposit.pemodal_id','=','pemodal.id')
                            ->leftjoin('funding', function($join){
                                 $join->on('funding.pinjaman_id','=','pinjaman.id')->on( 'funding.pemodal_id','=','pemodal.id');
                            })->whereNull('pinjaman.deleted_at')
                            ->whereNull('cicilan.deleted_at')
                            ->whereNull('pemodal.deleted_at')
                            ->whereNull('withdraw.deleted_at')
                            ->whereNull('funding.deleted_at')
                            ->whereYear('transaksi.tanggal','=', $years);

        if($user->role->name == "Peminjam"){
          $history->where('pinjaman.peminjam_id', $user->id);

        }else if($user->role->name == 'Pemodal'){
          $history->where('pemodal.user_id', $user->id);
        }

        if($month < 13 && $month != 0){
            $history->whereMonth('transaksi.tanggal','=',$month);
        }else{
            $history->where('transaksi.tanggal' ,'>=', 'now()-interval 3 month');
        }
        return $history->get();

    }
}
