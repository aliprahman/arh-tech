<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailPinjamanBaru;
use App\Pinjaman;
use App\User;

class BlastEmailPinjamanBaru implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 180;

    public $pinjaman_baru;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Pinjaman $pinjaman)
    {
        $this->pinjaman_baru = $pinjaman;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $pinjaman = Pinjaman::select('id','kode','jumlah_pinjaman','waktu_pinjaman','bunga','tanggal_mulai_funding',
                'tanggal_berakhir_funding','status','jumlah_funding AS jumlah_funding')
            ->whereIn('status',['listing','funding'])
            ->whereRaw('jumlah_pinjaman > IFNULL(jumlah_funding,0)')
            ->get();

      $pemodal = User::where('type','pemodal')->where('status','confirmed')->get();

      foreach ($pemodal as $user) {
        Mail::to($user->email)->send(new EmailPinjamanBaru($user,$pinjaman,$this->pinjaman_baru));
      }
    }
}
