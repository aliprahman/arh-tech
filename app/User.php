<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;


    protected $table = "users";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','id_pelanggan', 'email', 'password', 'photo_profile_id', 'type', 'role_id','biodata_id', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'//, 'api_token'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $casts = [
        'status' => 'string',
    ];

    public function role()
    {
        return $this->belongsTo('App\Role','role_id');
    }

    public function peminjam()
    {
        return $this->hasOne('App\Peminjam','user_id');
    }

    public function biodata()
    {
        return $this->belongsTo('App\Biodata','biodata_id');
    }

    public function file_uploads()
    {
        return $this->belongsTo('App\FileUploads','photo_profile_id');
    }

    //pemodal
    public function pemodal()
    {
        return $this->hasOne('App\Pemodal','user_id');
    }

    public static function get_data_user_detail($type = '')
    {
        return self::select('users.id', 'users.type', 'users.name', 'users.email', 'users.status', 'users.created_at', 'biodata.no_hp', 'biodata.tempat_lahir')
        ->join('biodata', 'users.biodata_id', '=', 'biodata.id')
        ->whereNull('biodata.deleted_at')
        ->where(function($query)use($type){
            if($type != ''){
                return $query->where('users.type','=',$type);
            }else{
                return $query->where('users.type','!=','finnesia');
            }
        })
        ->orderBy('created_at','desc')
        ->get();
    }

    public function generateToken() {
        $this->api_token = str_random(100);
        $this->save();

        return $this->api_token;
    }
}
