<?php

namespace App\Traits;

/**
 *  Author : Alip Rahman Hakim
 */
trait Common
{
    public function encrypt_id($id)
    {
        if(env('APP_DEBUG')){
            $encrypted = $id;
        }else{
            $encrypted = encrypt($id);
        }

        return $encrypted;
    }

    public function decrypt_id($id)
    {
        if(env('APP_DEBUG')){
            $decrypted = $id;
        }else{
            $decrypted = decrypt($id);
        }

        return $decrypted;
    }

    public function rupiah_to_int($number)
    {
        $int = $number;
        if($number != ""){
          $data = explode(',',$number);
          $int  = str_replace('.','',$data[0]);
        }

        return $int;
    }

    public function month_number_to_text($number)
    {
        $months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        return $months[$number];
    }
}
