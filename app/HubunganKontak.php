<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HubunganKontak extends Model
{
    use SoftDeletes;

    protected $table = "m_hubungan_kontak";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function peminjam()
    {
        return $this->hasMany('App\Peminjam','hubungan_kontak_id');
    }

}
