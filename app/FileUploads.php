<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FileUploads extends Model
{
    use SoftDeletes;

    protected $table = "file";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'name', 'descriptions', 'mime', 'size'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->hasOne('App\User','photo_profile_id');
    }

    public function peminjam()
    {
        return $this->hasOne('App\Peminjam','photo_tempat_kerja_id');
    }
}
