<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pendapatan extends Model
{
  use SoftDeletes;

  protected $table = "pendapatan";

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'cicilan_id', 'funding_id', 'jumlah'
  ];

  protected $dates = ['deleted_at'];
}
