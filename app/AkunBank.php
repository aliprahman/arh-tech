<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AkunBank extends Model
{
    use SoftDeletes;

    protected $table = "akun_bank";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bank_id', 'nama_akun_bank', 'no_akun_bank'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function bank()
    {
        return $this->belongsTo('App\Bank','bank_id');
    }

    public function peminjam()
    {
        return $this->hasOne('App\Peminjam','akun_bank_id');
    }
    public function transaksi()
    {
        return $this->hasMany('App\Transaksi','akun_bank_id');
    }

    //pemodal 
    public function pemodal() 
    { 
        return $this->hasOne('App\Pemodal','akun_bank_id'); 
    } 
}
