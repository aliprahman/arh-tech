<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dokumen extends Model
{
    use SoftDeletes;

    protected $table = "dokumen_pinjaman";

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pinjaman_id', 'file_dokumen_id', 'jenis_dokumen_pinjaman_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function pinjaman()
    {
        return $this->belongsTo('App\Pinjaman','pinjaman_id');
    }

    public function jenis_dokumen()
    {
        return $this->belongsTo('App\JenisDokumen','jenis_dokumen_pinjaman_id');
    }

    public static function get_dokumen_pinjaman($pinjaman_id)
    {
        return self::select('j.id AS id_jenis','j.nama AS jenis_dokumen','dokumen_pinjaman.pinjaman_id','dokumen_pinjaman.file_dokumen_id','file.name AS file','file.descriptions','file.created_at')
                ->join('file','dokumen_pinjaman.file_dokumen_id','=','file.id')
                ->join('m_jenis_dokumen_pinjaman AS j','dokumen_pinjaman.jenis_dokumen_pinjaman_id','=','j.id')
                ->where('pinjaman_id',$pinjaman_id)
                ->whereNull('file.deleted_at')
                ->whereNull('j.deleted_at')
                ->get();
    }
}
