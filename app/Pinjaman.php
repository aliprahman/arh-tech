<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pinjaman extends Model
{
    use SoftDeletes;

    protected $table = "pinjaman";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'peminjam_id', 'kode', 'nama', 'deskripsi', 'jenis_pinjaman_id',
        'jumlah_pinjaman', 'waktu_pinjaman', 'bunga', 'tanggal_submit',
        'tanggal_mulai_analisis', 'tanggal_berakhir_analisis', 'hasil_analisis_id',
        'jaminan', 'alasan_ditolak', 'tanggal_mulai_funding', 'jumlah_funding',
        'tanggal_berakhir_funding', 'tanggal_transfer_funding', 'transaksi_transfer_funding_id',
        'tanggal_mulai_cicilan', 'tanggal_berakhir_cicilan', 'tanggal_jatuh_tempo', 'status',
        'detail_peminjam'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'detail_peminjam' => 'array',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function dokumen()
    {
        return $this->hasMany('App\Dokumen','pinjaman_id');
    }

    public function jenis_pinjaman()
    {
        return $this->belongsTo('App\JenisPinjaman','jenis_pinjaman_id');
    }

    public function transaksi()
    {
        return $this->belongsTo('App\Transaksi','transaksi_transfer_funding_id');
    }

    public function cicilan()
    {
        return $this->hasMany('App\Cicilan','pinjaman_id');
    }

    public function peminjam()
    {
        return $this->belongsTo('App\Peminjam','peminjam_id');
    }

    public function pendanaan()
    {
        return $this->hasMany('App\Pendanaan','pinjaman_id');
    }

    public static function listing_pinjaman($peminjam_id)
    {
        return self::select('id','tanggal_submit','kode','jumlah_pinjaman','waktu_pinjaman','status','jumlah_funding',
                    DB::raw('IF(
                	           (select sum(transaksi.jumlah)
                	            from cicilan
                	            join transaksi on cicilan.transaksi_id = transaksi.id
                	            where pinjaman_id = pinjaman.id
                	            and cicilan.status != "unverified") > 1,1,0)
                            AS status_cicilan')
                )
                ->where('pinjaman.peminjam_id',$peminjam_id)
                ->orderBy('tanggal_submit','desc')
                ->get();
    }

    public static function sisa_pinjaman($pinjaman_id)
    {
        return self::join('cicilan','pinjaman.id','=','cicilan.pinjaman_id')
        ->where('pinjaman_id',$pinjaman_id)
        ->whereIn('cicilan.status',['unverified','pending'])
        ->groupBy('pinjaman.id')
        ->sum('cicilan.jumlah');
    }

    public static function generate_code() {
        $prefix = "01.".date('ymd');
        $new_code = "001";

        $last_code = self::select(DB::raw("MAX(SUBSTR(kode, -3)) code"))
                            ->withTrashed()
                            ->whereDate('tanggal_submit', date('Y-m-d'))
                            ->first();
        if(!empty($last_code->code))
            $new_code = (int) $last_code->code + 1;

        return $prefix.str_pad($new_code, 3, "0", STR_PAD_LEFT);
    }

}
