<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Perusahaan extends Model
{
    //
    use SoftDeletes;

    protected $table = "perusahaan";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama', 'alamat_id', 'no_npwp', 'no_telepon'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function pemodal()
    {
        return $this->hasMany('App\Pemodal','perusahaan_id');
    }
}
