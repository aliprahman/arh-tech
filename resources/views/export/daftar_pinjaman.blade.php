<html>
<head>
    <title>Daftar Pinjaman</title>
</head>
<body>
<style>
    body {
        line-height: 1.2em;
        font-size: 10px;
        font-family: Arial, sans-serif;
    }
    table {
        border-collapse: collapse;
        font-size: 10px;
    }
    table td,
    table th {
        padding: 4px;
        vertical-align: top;
        text-align: left;
        font-weight: normal;
    }
    table th {
        padding: 0 4px;
    }
    .fmt-value {
        font-size: 1.2em;
    }
    .fmt-field {
        width: 10px;
        white-space: nowrap;
    }
    hr.inline {
        margin: 0;
    }
    .table-details {
        width: 100%;
        border: 1px solid #1ab7ea;
        margin-top: 5px;
    }
    .table-details th {
        background-color: #248eb1;
        color: #fff;
        padding: 8px 15px;
        text-align: center;
        border: 1px solid #1ab7ea;
    }
    .table-details td {
        padding: 8px 15px;
        border-right: 1px solid #1ab7ea;
    }
    .dt_cell_number {
        text-align: right;
    }
</style>
<htmlpageheader name="header">
    <table align="left" cellpadding="0" style="width: 100%" border="0">
        <tr>
            <td>
                <div style="font-size: 1.8em; padding-top: 4px; color: #0069aa; max-width: 222px; margin-bottom:10px;">
                    PT. FINNESIA - Daftar Pinjaman
                </div>
            </td>
        </tr>
    </table>
</htmlpageheader>
<sethtmlpageheader name="header" page="all" value="on" show-this-page="1" />

<htmlpagefooter name="footer">
    <hr />
    <table align="center" cellpadding="0" style="width: 100%;">
        <tr>
            <td colspan="2" style="text-align: right; font-size: 1em;">
                {PAGENO}
            </td>
        </tr>
    </table>
</htmlpagefooter>
<sethtmlpagefooter name="footer" page="all" value="on" />
<table class="table-details" cellpadding="3" border="0">
    <thead>
    <tr>
        <th>No</th>
        <th>Tanggal Pengajuan</th>
        <th>Kode Pinjam</th>
        <th>Nilai Pinjaman</th>
        <th>Waktu</th>
        <th>Status Pengajuan</th>
        <th>Status Cicilan</th>
    </tr>
    </thead>
    <tbody>
    @php $no=1; @endphp
    @foreach($pinjaman as $item)
        <tr>
            <td>{{ $no++ }}</td>
            <td>{{ date_format(date_create($item->tanggal_submit),'d-m-Y') }}</td>
            <td>{{ $item->kode }}</td>
            <td>Rp. {{ number_format($item->jumlah_pinjaman,2,',','.') }}</td>
            <td>{{ $item->waktu_pinjaman }} Bulan</td>
            <td>{{ strtoupper($item->status) }}</td>
            <td>
                @if($item->status_cicilan)
                    Lunas
                @else
                    Belum Lunas
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>
