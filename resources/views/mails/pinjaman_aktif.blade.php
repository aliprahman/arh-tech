<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('favicon.ico') }}">

    <title>PINJAMAN BARU</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	  <style type="text/css">
    	/* Space out content a bit */
    	body {
    	  padding-top: 1.5rem;
    	  padding-bottom: 1.5rem;
    	}

    	/* Everything but the jumbotron gets side spacing for mobile first views */
    	.header,
    	.marketing,
    	.footer {
    	  padding-right: 1rem;
    	  padding-left: 1rem;
    	}

    	/* Custom page header */
    	.header {
    	  padding-bottom: 1rem;
    	  border-bottom: .05rem solid #e5e5e5;
    	}

    	/* Make the masthead heading the same height as the navigation */
    	.header {
    	  margin-top: 0;
    	  margin-bottom: 0;
    	  line-height: 3rem;
    	  text-align: center;
    	}

    	/* Custom page footer */
    	.footer {
    	  padding-top: 1.5rem;
    	  color: #777;
    	  border-top: .05rem solid #e5e5e5;
    	}

    	/* Customize container */
    	@media (min-width: 48em) {
    	  .container {
    	    width: 1000px
    	  }
    	}
    	.container-narrow > hr {
    	  margin: 2rem 0;
    	}

    	/* Main marketing message and sign up button */
    	.jumbotron {
    	  text-align: center;
    	  border-bottom: .05rem solid #e5e5e5;
    	}
    	.jumbotron .btn {
    	  padding: .75rem 1.5rem;
    	  font-size: 1.5rem;
    	}

    	/* Supporting marketing content */
    	.marketing {
    	  margin: 3rem 0;
    	}
    	.marketing p + h4 {
    	  margin-top: 1.5rem;
    	}

    	/* Responsive: Portrait tablets and up */
    	@media screen and (min-width: 48em) {
    	  /* Remove the padding we set earlier */
    	  .header,
    	  .marketing,
    	  .footer {
    	    padding-right: 0;
    	    padding-left: 0;
    	  }

    	  /* Space out the masthead */
    	  .header {
    	    margin-bottom: 2rem;
    	  }

    	  /* Remove the bottom border on the jumbotron for visual effect */
    	  .jumbotron {
    	    border-bottom: 0;
    	  }
    	}
	  </style>
  </head>

  <body>

    <div class="container">
      <header class="header clearfix">
        <h1>Finnesia</h1>
      </header>

      <main role="main">
        <p>Yth. Bapak/Ibu {{ $pemodal->name }}</p>
        @if ($pinjaman_baru != null)
          <p>Kami ingin menginformasikan bahwa ada pinjaman baru untuk meningkatkan portofolio anda: </p>
          <table class="table">
            <tr style="background-color: #498fff; color: white">
              <td>Kode Pinjaman</td>
              <td>Nilai</td>
              <td>Jenis</td>
            </tr>
            <tr>
              <td>{{ $pinjaman_baru->kode }}</td>
              <td>Rp. {{ number_format($pinjaman_baru->jumlah_pinjaman,0,',','.') }}</td>
              <td>{{ $pinjaman_baru->jenis_pinjaman->nama }}</td>
            </tr>
          </table>
        @endif

        @if (count($pinjaman) > 0)
          <p>Kami @if ($pinjaman_baru != null) juga @endif ingin menginformasikan bahwa ada beberapa pinjaman yang progress-nya hampir 100%. Ini Kesempatan untuk anda mendanai pinjaman-pinjaman di bawah ini.</p>
          <table class="table table-striped">
            <tr style="background-color: #498fff; color: white">
              <td>No</td>
              <td>Kode Pinjaman</td>
              <td>Nilai</td>
              <td>Sisa Hari</td>
              <td>Sisa Pendanaan</td>
            </tr>
            @php
              $no=1;
            @endphp
            @foreach ($pinjaman as $p)
              @php
                $today = new DateTime(date('Y-m-d H:i:s'));
                $end_funding_periode = new DateTime($p->tanggal_berakhir_funding);
              @endphp
              @if ($today < $end_funding_periode)
                  <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $p->kode }}</td>
                    <td>Rp. {{ number_format($p->jumlah_pinjaman,0,',','.') }}</td>
                    <td>
                      @php
                        $interval = $end_funding_periode->diff($today);
                        echo $interval->d." Hari"
                      @endphp
                    </td>
                    <td>Rp. {{ number_format(($p->jumlah_pinjaman - $p->jumlah_funding),0,',','.') }}</td>
                  </tr>
              @endif
            @endforeach
        @endif
        </table>
      </main>

      <footer class="footer">
        <p>&copy; Finnesia 2018</p>
      </footer>

    </div> <!-- /container -->
  </body>
</html>
