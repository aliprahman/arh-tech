
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('favicon.ico') }}">

    <title>DETAIL TRANSFER DEPOSIT</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<style type="text/css">
	/* Space out content a bit */
	body {
	  padding-top: 1.5rem;
	  padding-bottom: 1.5rem;
	}

	/* Everything but the jumbotron gets side spacing for mobile first views */
	.header,
	.marketing,
	.footer {
	  padding-right: 1rem;
	  padding-left: 1rem;
	}

	/* Custom page header */
	.header {
	  padding-bottom: 1rem;
	  border-bottom: .05rem solid #e5e5e5;
	}

	/* Make the masthead heading the same height as the navigation */
	.header {
	  margin-top: 0;
	  margin-bottom: 0;
	  line-height: 3rem;
	  text-align: center;
	}

	/* Custom page footer */
	.footer {
	  padding-top: 1.5rem;
	  color: #777;
	  border-top: .05rem solid #e5e5e5;
	}

	/* Customize container */
	@media (min-width: 48em) {
	  .container {
	    width: 1000px
	  }
	}
	.container-narrow > hr {
	  margin: 2rem 0;
	}

	/* Main marketing message and sign up button */
	.jumbotron {
	  text-align: center;
	  border-bottom: .05rem solid #e5e5e5;
	}
	.jumbotron .btn {
	  padding: .75rem 1.5rem;
	  font-size: 1.5rem;
	}

	/* Supporting marketing content */
	.marketing {
	  margin: 3rem 0;
	}
	.marketing p + h4 {
	  margin-top: 1.5rem;
	}

	/* Responsive: Portrait tablets and up */
	@media screen and (min-width: 48em) {
	  /* Remove the padding we set earlier */
	  .header,
	  .marketing,
	  .footer {
	    padding-right: 0;
	    padding-left: 0;
	  }

	  /* Space out the masthead */
	  .header {
	    margin-bottom: 2rem;
	  }

	  /* Remove the bottom border on the jumbotron for visual effect */
	  .jumbotron {
	    border-bottom: 0;
	  }
	}
	</style>
  </head>

  <body>

    <div class="container">
      <header class="header clearfix">
        <h1>Finnesia Deposit</h1>
      </header>

      <main role="main">
        <center>
          <h3 class="text-muted">Silahkan lakukan transfer ke rekening dibawah ini.</h3>
          <h4 class="text-muted"> mohon lakukan transfer dalam waktu 3 jam dari sekarang.</h4>
        <center>
        <div class="jumbotron">
          <div style="text-align: center">
            <img src="{{ asset('img/bank_mandiri.png') }}" style="width: 200px" />
            <p style="padding-top: 5px">
              <b>No Rekenging : 113 - 00 - 7070444 - 4</b><br>
              <span style="text-size: 16px">SAHALA FIRDAUS SYAH DOLOKSARIBU</span><br>
              KCP PMG Bdr Sultan Badaruddin 11310</br>
            </p>
            <p style="text-size: 16px">Jumlah deposit yang harus ditransfer</p>
              <h4 id="jumlah_deposit" style="text-weight: bolder">Rp. {{ number_format(($deposit->jumlah + $deposit->kode_unik),0,',','.') }}</h4>
            <p>
              Mohon transfer sesuai jumlah yang tertera<br>
              (termasuk 3 digit terakhir)
            </p>
          </div>
          Setelah melakukan transfer harap segera lakukan konfirmasi transaksi agar proses menjadi lebih cepat.
        </div>

      </main>

      <footer class="footer">
        <p>&copy; Finnesia 2017</p>
      </footer>

    </div> <!-- /container -->
  </body>
</html>
