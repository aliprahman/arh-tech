<!DOCTYPE html>
<html lang="en-US">
<!-- Mirrored from finnesia.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 18 Dec 2017 08:58:40 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<!-- /Added by HTTrack -->
<head>

        <meta charset="utf-8" />
        <title>{{ config('app.name', 'Laravel') }} - @yield('title')</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="base_url" content="{{ url('/') }}" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta name="_token" content="{{ csrf_token() }}">
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        @stack('page-plugin-styles')
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{ asset('/assets/global/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ asset('/assets/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/assets/layouts/layout/css/custom.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{ asset('/assets/layouts/layout3/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/assets/layouts/layout3/css/themes/blue-steel.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/assets/layouts/layout3/css/custom.css') }}" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Signika" rel="stylesheet">
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
        <script src='https://www.google.com/recaptcha/api.js?hl=id'></script>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117189017-2"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-117189017-2');
        </script>
        
<meta charset="UTF-8"/>
<link rel="profile" href="https://gmpg.org/xfn/11"/>
<link rel="pingback" href="xmlrpc.php"/>
<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
<title>Finnesia</title>
<link rel='stylesheet' id='qode_startit_default_style-css' href='{{ asset('/wp/wp-content/themes/startit/style5859.css?ver=4.9.1') }}' type='text/css' media='all'/>
<link rel='stylesheet' id='qode_startit_modules_plugins-css' href='{{ asset('/wp/wp-content/themes/startit/assets/css/plugins.min5859.css?ver=4.9.1') }}' type='text/css' media='all'/>
<link rel='stylesheet' id='qode_startit_modules-css' href='{{ asset('/wp/wp-content/themes/startit/assets/css/modules.min5859.css?ver=4.9.1') }}' type='text/css' media='all'/>
<link rel='stylesheet' id='qode_startit_modules_responsive-css' href='{{ asset('/wp/wp-content/themes/startit/assets/css/modules-responsive.min5859.css?ver=4.9.1') }}' type='text/css' media='all'/>
<link rel='stylesheet' id='qode_startit_blog_responsive-css' href='{{ asset('/wp/wp-content/themes/startit/assets/css/blog-responsive.min5859.css?ver=4.9.1') }}' type='text/css' media='all'/>
<link rel='stylesheet' id='qode_startit_style_dynamic-css' href='{{ asset('/wp/wp-content/themes/startit/assets/css/style_dynamic8a0c.css?ver=1512886069') }}' type='text/css' media='all'/>
<link rel='stylesheet' id='qode_startit_google_fonts-css' href='https://fonts.googleapis.com/css?family=Raleway%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRaleway%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&amp;subset=latin%2Clatin-ext&amp;ver=1.0.0' type='text/css' media='all'/>
<!--[if IE 9]><link rel="stylesheet" type="text/css" href="https://finnesia.com/wp-content/themes/startit/assets/css/ie9_stylesheet.min.css" media="screen"><![endif]-->
<style type="text/css">
	.btn-login .item_outer, .btn-login .item_inner {
		background-color: #FFFFFF !important;
		-webkit-transition: text-decoration .2s ease-out, border-color .2s ease-out, background-color .2s ease-out;
		transition: text-decoration .2s ease-out, border-color .2s ease-out, background-color .2s ease-out;	
	}

	.btn-login:hover .item_outer, .btn-login .item_outer  {
		background-color: #FFFFFF !important;
		border: 2px solid;
		border-color: #E3E3E3 !important;
		border-radius: 2px !important;
		color: #707070;
		-webkit-transition: text-decoration .2s ease-out, border-color .2s ease-out, background-color .2s ease-out;
		transition: text-decoration .2s ease-out, border-color .2s ease-out, background-color .2s ease-out;
	}

	.btn-login:hover .item_outer, .btn-login .item_outter:hover, .btn-login .item_inner:hover, .btn-login:hover .item_inner {
		background-color: #E2E2E2 !important;
		-webkit-transition: text-decoration .2s ease-out, border-color .2s ease-out, background-color .2s ease-out;
		transition: text-decoration .2s ease-out, border-color .2s ease-out, background-color .2s ease-out;
	}

	.qodef-footer-inner a {
		text-decoration: none;
		/*color: #a2a2a2;*/
	}
</style>
</head>
<body class="home page-template page-template-full-width page-template-full-width-php page page-id-440 qode-core-1.3.1 startit-ver-2.4 qodef-smooth-scroll qodef-smooth-page-transitions qodef-top-bar-mobile-hide qodef-header-standard qodef-sticky-header-on-scroll-up qodef-default-mobile-header qodef-sticky-up-mobile-header qodef-dropdown-animate-height qodef-search-covers-header qodef-side-menu-slide-with-content qodef-width-470 wpb-js-composer js-comp-ver-5.4.4 vc_responsive">
<section class="qodef-side-menu right">
<div class="qodef-close-side-menu-holder">
	<div class="qodef-close-side-menu-holder-inner">
		<a href="#" target="_self" class="qodef-close-side-menu"><span aria-hidden="true" class="icon_close"></span></a>
	</div>
</div>
<div id="text-15" class="widget qodef-sidearea widget_text">
	<div class="textwidget">
		<a href="index.html"><img src="{{ asset('/wp/wp-content/uploads/2017/12/finnesia-logo.png') }}" alt="logo"></a>
		<div class="vc_empty_space" style="height: 38px">
			<span class="vc_empty_space_inner"></span>
		</div>
		<div class="vc_empty_space" style="height: 28px">
			<span class="vc_empty_space_inner"></span>
		</div>
		<div class="custom-color-row-changer">
			<span class="qodef-icon-shortcode square" style="margin: 0px -4px 0px 0px;width: 36px;height: 36px;line-height: 36px;background-color: rgba(255,255,255,0.01);border-style: solid;border-color: #b4b4b4;border-width: 1px" data-hover-border-color="#e33970" data-hover-background-color="#e33970" data-hover-color="#ffffff" data-color="#7b7b7b">
			<a class="" href="https://www.facebook.com/" target="_blank"><i class="qodef-icon-font-awesome fa fa-facebook qodef-icon-element" style="color: #7b7b7b;font-size:18px"></i></a>
			</span>
			<span class="qodef-icon-shortcode square" style="margin: 0px -4px 0px 0px;width: 36px;height: 36px;line-height: 36px;background-color: rgba(255,255,255,0.01);border-style: solid;border-color: #b4b4b4;border-width: 1px" data-hover-border-color="#e33970" data-hover-background-color="#e33970" data-hover-color="#ffffff" data-color="#7b7b7b">
			<a class="" href="https://twitter.com/" target="_blank"><i class="qodef-icon-font-awesome fa fa-twitter qodef-icon-element" style="color: #7b7b7b;font-size:18px"></i></a>
			</span>
			<span class="qodef-icon-shortcode square" style="margin: 0px -4px 0px 0px;width: 36px;height: 36px;line-height: 36px;background-color: rgba(255,255,255,0.01);border-style: solid;border-color: #b4b4b4;border-width: 1px" data-hover-border-color="#e33970" data-hover-background-color="#e33970" data-hover-color="#ffffff" data-color="#7b7b7b">
			<a class="" href="https://www.linkedin.com/" target="_blank"><i class="qodef-icon-font-awesome fa fa-linkedin qodef-icon-element" style="color: #7b7b7b;font-size:18px"></i></a>
			</span>
			<span class="qodef-icon-shortcode square" style="margin: 0px -4px 0px 0px;width: 36px;height: 36px;line-height: 36px;background-color: rgba(255,255,255,0.01);border-style: solid;border-color: #b4b4b4;border-width: 1px" data-hover-border-color="#e33970" data-hover-background-color="#e33970" data-hover-color="#ffffff" data-color="#7b7b7b">
			<a class="" href="https://instagram.com/" target="_blank"><i class="qodef-icon-font-awesome fa fa-instagram qodef-icon-element" style="color: #7b7b7b;font-size:18px"></i></a>
			</span>
		</div>
	</div>
</div>
</section>
<div class="qodef-wrapper">
	<div class="qodef-wrapper-inner">
		<header class="qodef-page-header">
		<div class="qodef-menu-area" style="background-color:rgba(255, 255, 255, 0)">
			<form role="search" action="https://finnesia.com/" class="qodef-search-cover" method="get">
				<div class="qodef-form-holder-outer">
				</div>
			</form>
			<div class="qodef-vertical-align-containers">
				<div class="qodef-position-left">
					<div class="qodef-position-left-inner">
						<div class="qodef-logo-wrapper">
							<a href="https://finnesia.com/" style="height: 53px;">
							<img class="qodef-normal-logo" src="{{ asset('/wp/wp-content/uploads/2017/12/finnesia-logo.png') }}" alt="logo"/>
							<img class="qodef-dark-logo" src="{{ asset('/wp/wp-content/uploads/2017/12/finnesia-logo.png') }}" alt="dark logo"/><img class="qodef-light-logo" src="{{ asset('/wp/wp-content/uploads/2017/12/finnesia-logo-white.png') }}" alt="light logo"/></a>
						</div>
					</div>
				</div>
				<div class="qodef-position-right">
					<div class="qodef-position-right-inner">
						<nav class="qodef-main-menu qodef-drop-down qodef-default-nav">
						<ul id="menu-main-menu" class="clearfix">
							<li id="nav-menu-item-5394" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-440 current_page_item qodef-active-item narrow">
								<a href="https://finnesia.com/" class=" current "><span class="item_outer"><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon blank fa"></i></span><span class="item_text">Home</span></span><span class="plus"></span></span></a>
							</li>
							<li id="nav-menu-item-5841" class="menu-item menu-item-type-post_type menu-item-object-page narrow">
								<a href="https://finnesia.com/pendanaan/" class=""><span class="item_outer"><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon blank fa"></i></span><span class="item_text">Pendanaan</span></span><span class="plus"></span></span></a>
							</li>
							<li id="nav-menu-item-5392" class="menu-item menu-item-type-post_type menu-item-object-page narrow">
								<a href="https://finnesia.com/pembiayaan-guru/" class=""><span class="item_outer"><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon blank fa"></i></span><span class="item_text">Pembiayaan Guru</span></span><span class="plus"></span></span></a>
							</li>
							<li id="nav-menu-item-5620" class="menu-item menu-item-type-post_type menu-item-object-page narrow">
								<a href="https://finnesia.com/tentang-kami/" class=""><span class="item_outer"><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon blank fa"></i></span><span class="item_text">Tentang Kami</span></span><span class="plus"></span></span></a>
							</li>
							<li id="nav-menu-item-5620" class="menu-item menu-item-type-post_type menu-item-object-page narrow">
								<a href="{{ url('/login') }}" class="btn-login"><span class="item_outer"><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon blank fa"></i></span><span class="item_text">Login</span></span><span class="plus"></span></span></a>
							</li>
						</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
		</header>
		<div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                {{-- Diubah agar universal di buat yield --}}
                @yield('crumbs')
                {{-- @php
                $menu = Menu::handler('main')
                    ->breadcrumbs()
                    ->setElement('ul')
                    ->addClass('page-breadcrumb breadcrumb');
                echo $menu;
                @endphp --}}
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    @yield('content')
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
		<footer>
		<div class="qodef-footer-inner clearfix">
			<div class="qodef-footer-top-holder">
				<div class="qodef-footer-top qodef-footer-top-full">
					<div class="qodef-four-columns clearfix">
						<div class="qodef-four-columns-inner">
							<div class="qodef-column">
								<div class="qodef-column-inner">
									<div id="text-19" class="widget qodef-footer-column-1 widget_text">
										<h4 class="qodef-footer-widget-title">FINNESIA</h4>
										<div class="textwidget">
											<p>
												<strong>Finnesia</strong> adalah perusahaan <em>Financial Technologi (Fintech)</em> berbasis <em>Peer to Peer (P2P) Lending</em>.
											</p>
											<p>
												&nbsp;
											</p>
											<p>
												<a href="https://finnesia.com/tentang-kami">Tentang Finnesia</a>
											</p>
											<p>
												<a href="https://finnesia.com/aturan-penggunaan">Aturan Penggunaan</a>
											</p>
											<p>
												<a href="https://finnesia.com/kebijakan-privasi">Kebijakan Privasi</a>
											</p>
											<p>
												<a href="https://finnesia.com/syarat-ketentuan">Syarat &amp; Ketentuan</a>
											</p>
											<p>
												<a href="https://finnesia.com/faq-pemodal">FAQ</a>
											</p>
											<p>
												<a href="https://finnesia.com/blog">Blog Finnesia</a>
											</p>
											<p>
												Hubungi Kami
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="qodef-column">
								<div class="qodef-column-inner">
									<div id="text-18" class="widget qodef-footer-column-2 widget_text">
										<h4 class="qodef-footer-widget-title">Pendanaan</h4>
										<div class="textwidget">
											<p>
												<a href="https://finnesia.com/cara-kerja-pendanaan">Cara Kerja</a>
											</p>
											<p>
												<a href="https://finnesia.com/risiko-pendanaan">Risiko</a>
											</p>
											<p>
												<a href="https://finnesia.com/bagi-hasil-pendanaan">Bagi hasil Pendanaan (<em>Return</em>)</a>
											</p>
											<p>
												<a href="{{ url('register/pemodal') }}">Daftar sebagai Pendana</a>
											</p>
											<p>
												<a href="{{ url('login') }}">Login</a>
											</p>
										</div>
									</div>
									<div id="text-17" class="widget qodef-footer-column-2 widget_text">
										
									</div>
								</div>
							</div>
							<div class="qodef-column">
								<div class="qodef-column-inner">
									<div id="recent-posts-4" class="widget qodef-footer-column-3 widget_recent_entries">
										<h4 class="qodef-footer-widget-title">Pembiayaan</h4>
										<div class="textwidget">
											<p>
												<a href="https://finnesia.com/cara-kerja-pembiayaan">Cara Kerja</a>
											</p>
											<p>
												<em>Grade</em> &amp; <em>Rate</em> Pembiayaan
											</p>
											<p>
												<a href="{{ url('register/peminjam') }}" onmouseover="this.style.color='#e33970'" onmouseout="this.style.color='#a2a2a2'">Ajukan Pembiayaan</a>
											</p>
											<p>
												<a href="{{ url('login') }}">Login</a>
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="qodef-column">
								<div class="qodef-column-inner">
									<div id="text-3" class="widget qodef-footer-column-4 widget_text">
										<h4 class="qodef-footer-widget-title">Kantor Pusat</h4>
										<div class="textwidget">
											<p>
												Tower Grand Royal KR 27
											</p>
											<p>
												Kebagusan City
											</p>
											<p>
												Jakarta
											</p>
											<p>
												&nbsp;
											</p>
											<p>
												Email: admin@finnesia.com
											</p>
											<p>
												Phone: +62 (21)
											</p>
											<div class="vc_empty_space" style="height: 28px">
												<span class="vc_empty_space_inner"></span>
											</div>
											<div class="custom-color-row-changer">
												
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="qodef-footer-bottom-holder">
				<div class="qodef-footer-bottom-holder-inner">
					<div class="qodef-column-inner">
						<div id="text-5" class="widget qodef-footer-text widget_text">
							<div class="textwidget">
								<p>
									Copyright © 2017 &#8211; PT. Finnesia Kreasi Nusantara
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</footer>
	</div>
        <script src="{{ asset('assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/js.cookie.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
		<script>
			$(document).ready(function()
			{
				$('#clickmewow').click(function()
				{
					$('#radio1003').attr('checked', 'checked');
				});
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
			})
		</script>
		<!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        @stack('page-plugin-scripts')
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{ asset('assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        @stack('page-scripts')
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="{{ asset('assets/layouts/layout3/scripts/layout.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/layouts/layout3/scripts/demo.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/layouts/global/scripts/quick-sidebar.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/layouts/global/scripts/quick-nav.min.js') }}" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->

</body>
<!-- Mirrored from finnesia.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 18 Dec 2017 08:59:44 GMT -->
</html>