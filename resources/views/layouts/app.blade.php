<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="{{ app()->getLocale() }}" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="{{ app()->getLocale() }}" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="{{ app()->getLocale() }}">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Fintech Peer to Peer (P2P) Lending - Solusi Keuangan Indonesia | PT. Finnesia Kreasi Nusantara</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="base_url" content="{{ url('/') }}" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="Finnesia adalah perusahaan Financial Technologi (Fintech) berbasis Peer to Peer (P2P) Lending. Finnesia hadir sebagai salah satu solusi terbaik untuk Pendanaan dan Pembiayaan." />
        <meta name="author" content="FINNESIA"  />
        <meta name="_token" content="{{ csrf_token() }}">
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        @stack('page-plugin-styles')
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{ asset('/assets/global/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ asset('/assets/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/assets/layouts/layout/css/custom.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/assets/pages/css/profile.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{ asset('/assets/layouts/layout3/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/assets/layouts/layout3/css/themes/blue-steel.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/assets/layouts/layout3/css/custom.css') }}" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Signika" rel="stylesheet">
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
        <script src='https://www.google.com/recaptcha/api.js'></script>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117189017-2"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-117189017-2');
        </script>
    </head>
    <!-- END HEAD -->
        <style type="text/css">
            #title{
                font-family: 'Signika', sans-serif;
            }
            .preview{
              max-width: 200px;
              max-height: 250px;
            }
        </style>
    <body class="page-container-bg-solid">
        <!-- BEGIN CONTAINER -->
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <!-- BEGIN HEADER -->
                    <div class="page-header">
                        <!-- BEGIN HEADER TOP -->
                        <div class="page-header-top">
                            <div class="container">
                                <!-- BEGIN LOGO -->
                                <div class="page-logo">
                                  <img src="{{ asset('/wp/wp-content/uploads/2017/12/finnesia-logo.png') }}" alt="logo" height="50" style="padding-top: 10px"/>
                                </div>
                                <!-- END LOGO -->
                                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                                <a href="javascript:;" class="menu-toggler"></a>
                                <!-- END RESPONSIVE MENU TOGGLER -->
                                <!-- BEGIN TOP NAVIGATION MENU -->
                                <div class="top-menu">
                                    <ul class="nav navbar-nav pull-right">
                                        @if (Auth::guest())
                                         <li class="">
                                            <a href="{{ url('/home') }}"> LOGIN PEMINJAM</a>
                                        </li>
                                        <li class="">
                                            <a href="{{ url('/pemodal/dashboard') }}"> LOGIN PEMODAL</a>
                                        </li>
                                        @else
                                        <!-- BEGIN USER LOGIN DROPDOWN -->
                                        <li class="dropdown dropdown-user dropdown-dark">
                                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                              @if(auth()->user()->file_uploads != "" && auth()->user()->file_uploads->name != "")
                                                <img alt="" class="img-circle" src="{{ asset('/storage/profile_photo/'.auth()->user()->file_uploads->name) }}">
                                              @else
                                                <img alt="" class="img-circle" src="{{ asset('/img/dummy-profile.jpg') }}">
                                              @endif
                                                <span class="username username-hide-mobile">{{ Auth::user()->name }}</span>
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-default">
                                                <li>
                                                    <a href="
                                                    @if(auth()->user()->type == 'peminjam')
                                                        {{ url('/peminjam/biodata') }}
                                                    @elseif(auth()->user()->type == 'pemodal')
                                                        {{ url('/pemodal/biodata') }}
                                                    @else
                                                        {{ url('/admin/biodata') }}
                                                    @endif
                                                    ">
                                                        <i class="icon-user"></i> My Profile
                                                    </a>
                                                </li>
                                                @if(auth()->user()->type == 'finnesia')
                                                  <li>
                                                    <a href="{{ url('admin/akun_bank') }}">
                                                      <i class="icon-docs"></i>
                                                      Akun Bank Transaksi
                                                    </a>
                                                  </li>
                                                  <li>
                                                    <a href="{{ url('admin/pekerjaan') }}">
                                                      <i class="icon-docs"></i>
                                                      Data Jenis Pekerjaan
                                                    </a>
                                                  </li>
                                                @endif
                                                <li>
                                                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                        <i class="icon-key"></i> Log Out </a>

                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        {{ csrf_field() }}
                                                    </form>
                                                </li>
                                            </ul>
                                        </li>
                                        <!-- END USER LOGIN DROPDOWN -->
                                        @endif
                                    </ul>
                                </div>
                                <!-- END TOP NAVIGATION MENU -->
                            </div>
                        </div>
                        <!-- END HEADER TOP -->
                        <!-- BEGIN HEADER MENU -->
                        <div class="page-header-menu">
                            <div class="container">
                                <!-- BEGIN HEADER SEARCH BOX -->
                                                                <!-- END HEADER SEARCH BOX -->
                                <!-- BEGIN MEGA MENU -->
                                <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                                <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                                <div class="hor-menu  ">
                                    @php

                                        $menu = Menu::handler('main');

                                        // items
                                        $menu
                                            ->addClass('nav navbar-nav')
                                                ->add('/dashboard', '<i class="icon-screen-desktop"></i> Dashboard')
                                                ->add('/akun', '<i class="icon-docs"></i> Daftar Akun')
                                                ->add('/pinjaman', '<i class="icon-docs"></i> Daftar Pinjaman')
                                                ->add('/admin/cicilan', '<i class="icon-docs"></i> Daftar Cicilan')
                                                ->add('/admin/deposit', '<i class="fa fa-upload"></i> Daftar Deposit')
                                                ->add('/admin/penarikan', '<i class="fa fa-download"></i> Daftar Penarikan')
                                                ->add('/pinjaman/pembayaran', '<i class="icon-hourglass"></i> History Pembayaran')
                                                ->add('/admin/history_transaksi', '<i class="icon-hourglass"></i> History Transaksi')
                                                ->add('/pemodal/cari', '<i class="icon-magnifier"></i> Cari Pinjaman')
                                                ->add('/pemodal/daftar', '<i class="icon-diamond"></i> Daftar Pendanaan')
                                                ->add('/pemodal/deposit', '<i class="fa fa-upload"></i> Deposit')
                                                ->add('/pemodal/penarikan', '<i class="fa fa-download"></i> Penarikan')
                                                ->add('/pemodal/history', '<i class="icon-hourglass"></i> History')
                                                ->add('/acl', 'Acl <i class="icon-arrow-down"></i>', Menu::items()->addClass('dropdown-menu pull-left')
                                                    ->prefixParents()
                                                    ->add('resource/index', 'Resource') // with prefix: /folders/urgent
                                                    ->add('role/index', 'Role')
                                                    ->add('rule/index', 'Rule')
                                                );

                                        // styling
                                        $menu
                                            ->getItemsAtDepth(0)
                                            ->map(function($item) {
                                                $user = \Auth::user();
                                                if ( $item->isActive() || $item->hasActiveChild() )  {
                                                    $item->addClass('active');

                                                }

                                                if($item->hasChildren()){
                                                    $item->addClass('menu-dropdown classic-menu-dropdown parent-dropdown-menu');
                                                }

                                                $regexTrue = "/";
                                                $dbRsc = \DB::table('acl_resource')->get();
                                                if(!empty($dbRsc)){
                                                    foreach($dbRsc as $regex){
                                                        $getPathRegex = preg_match($regex->name.'^', $item->getContent()->getUrl(), $matches);
                                                        if($getPathRegex > 0){
                                                            $regexTrue = $regex->name;
                                                        }
                                                    }
                                                }

                                                if(!\App\Library\Beam\Acl\Acl::isAllowed($user->role->name, $regexTrue)){
                                                    $item->style('display:none;');
                                                }

                                            });


                                        echo $menu;

                                    @endphp
                                </div>
                                <!-- END MEGA MENU -->
                            </div>
                        </div>
                        <!-- END HEADER MENU -->
                    </div>
                    <!-- END HEADER -->
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <!-- BEGIN CONTAINER -->
                    <div class="page-container">
                        <!-- BEGIN CONTENT -->
                        <div class="page-content-wrapper">
                            <!-- BEGIN CONTENT BODY -->
                            <!-- BEGIN PAGE HEAD-->
                            <div class="page-head">
                                <div class="container">
                                    <!-- BEGIN PAGE TITLE -->
                                    <div class="page-title">
                                        <h1>@yield('title')</h1>
                                    </div>
                                    <!-- END PAGE TITLE -->
                                </div>
                            </div>
                            <!-- END PAGE HEAD-->
                            <!-- BEGIN PAGE CONTENT BODY -->
                            <div class="page-content">
                                <div class="container">
                                    <!-- BEGIN PAGE BREADCRUMBS -->
                                    {{-- Diubah agar universal di buat yield --}}
                                    @yield('crumbs')
                                    {{-- @php
                                    $menu = Menu::handler('main')
                                        ->breadcrumbs()
                                        ->setElement('ul')
                                        ->addClass('page-breadcrumb breadcrumb');
                                    echo $menu;
                                    @endphp --}}
                                    <!-- END PAGE BREADCRUMBS -->
                                    <!-- BEGIN PAGE CONTENT INNER -->
                                    <div class="page-content-inner">
                                        @yield('content')
                                    </div>
                                    <!-- END PAGE CONTENT INNER -->
                                </div>
                            </div>
                            <!-- END PAGE CONTENT BODY -->
                            <!-- END CONTENT BODY -->
                        </div>
                        <!-- END CONTENT -->
                    </div>
                    <!-- END CONTAINER -->
                </div>
            </div>
            <div class="page-wrapper-row">
                <div class="page-wrapper-bottom">
                    <!-- BEGIN FOOTER -->
                    <!-- BEGIN PRE-FOOTER -->

                    <!-- END PRE-FOOTER -->
                    <!-- BEGIN INNER FOOTER -->
                    <div class="page-footer">
                        <div class="container" id="title"><center>Copyright 2017 - PT. FINNESIA KREASI NUSANTARA.</center>
                        </div>
                    </div>
                    <div class="scroll-to-top">
                        <i class="icon-arrow-up"></i>
                    </div>
                    <!-- END INNER FOOTER -->
                    <!-- END FOOTER -->
                </div>
            </div>
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN QUICK NAV -->
        <!-- END QUICK NAV -->
        <!--[if lt IE 9]>
<script src="{{ asset('assets/global/plugins/respond.min.js') }}"></script>
<script src="{{ asset('assets/global/plugins/excanvas.min.js') }}"></script>
<script src="{{ asset('assets/global/plugins/ie8.fix.min.js') }}"></script>
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="{{ asset('assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/js.cookie.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        @stack('page-plugin-scripts')
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{ asset('assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
        <script>
            $(document).ready(function()
            {
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
                //var a = $('.parent-dropdown-menu').find('a');
                //console.log(a);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            });
            function preview_image(event,target){
              var reader = new FileReader();
              reader.onload = function(){
                var output = document.getElementById(target);
                output.src = reader.result;
               }
              reader.readAsDataURL(event.target.files[0]);
            }
        </script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        @stack('page-scripts')
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="{{ asset('assets/layouts/layout3/scripts/layout.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/layouts/layout3/scripts/demo.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/layouts/global/scripts/quick-sidebar.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/layouts/global/scripts/quick-nav.min.js') }}" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->

    </body>

</html>
