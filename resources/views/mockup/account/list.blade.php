@extends('layouts.app')

@push('page-plugin-styles')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('title')
Daftar Akun
@endsection

@section('crumbs')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/dashboard') }}">Admin</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Daftar Akun</span>
        </li>
    </ul>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="portlet light ">
        <div class="portlet-title">
            <div class="caption">
                <span class="caption-subject bold uppercase">Daftar Akun</span>
            </div>
            <div class="actions">
                <a class="btn btn-circle btn-default" href="javascript:;">
                    <i class="fa fa-print"></i> Print to PDF
                </a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="col-md-12 margin-bottom-20">
                <form method="POST" id="filter-list" class="form-inline" role="form">
                    <div class="form-group">
                        <select name="type" id="type" class="form-control select2" style="width: 150px">
                            <option value="">Semua</option>
                            <option value="pemodal">Pemodal</option>
                            <option value="peminjam">Peminjam</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">
                        <i class="icon-magnifier"></i>
                        Search
                    </button>
                </form>
            </div>
            <div class="col-md-12">
                <table class="table table-striped table-bordered" id="users-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>No. HP</th>
                            <th>Tipe</th>
                            <th>Status</th>
                            <th>Tgl. Daftar</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="clearfix margin-bottom-20"></div>
         </div>
      </div>
    </div>
 </div>
</div>

@endsection
@push('page-plugin-scripts')
    <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <!-- Select2 -->
    <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
@endpush

@push('page-scripts')
<script type="text/javascript">
$(function() {
    $('.select2').select2();
    var oTable = $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{url("/daftar/akun") }}',
            method: 'POST',
            data: function (d) {
                d.type = $('select[name=type]').val();
            }
        },
        columns: [
            { data: "DT_Row_Index", orderable: false, searchable: false},
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'no_hp', name: 'no_hp' },
            { data: 'type', name: 'type' },
            { data: 'status', name: 'status' },
            { data: 'created_at', name: 'tgl_daftar'},
            { data: "action", orderable: false, searchable: false}
        ],
        rowCallback: function(row,data,index){
            // nomer
            $('td:eq(0)', row).html( '<center>'+data.DT_Row_Index+'</center>' );
            // tanggal_pinjaman
            $('td:eq(6)',row).html(moment(data.created_at).format("DD-MM-YYYY"));
            // action
            $('td:eq(7)',row).html('<a href="{{ url('akun') }}/'+data.id+'">Detail</a>');
        }
    });
    $('#filter-list').on('submit', function(e) {
        oTable.draw();
        e.preventDefault();
    });
});
</script>
@endpush
