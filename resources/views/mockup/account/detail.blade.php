@extends('layouts.app')

@push('page-plugin-styles')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('title')
    Detail Akun
@endsection

@section('crumbs')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/dashboard') }}">Admin</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ url('/akun') }}">Daftar Akun</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Detail Akun</span>
        </li>
    </ul>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-body">
                    <div class="col-md-12 margin-bottom-20">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="portlet-title">
                                    <strong>Data User</strong>
                                </div>
                                <table class="table table-hover">
                                    <tr>
                                        <td>Nama</td>
                                        <td>: {{ ucfirst($user->name) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>: {{ $user->email }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tipe</td>
                                        <td>: {{ ucfirst($user->type) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td>: {{ ucfirst($user->status) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Daftar</td>
                                        <td>: {{ date_format(date_create($user->created_at),'j F Y H:i:s') }}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <div class="portlet-title">
                                    <strong>Data Akun Bank</strong>
                                </div>
                                <table class="table table-hover">
                                    <tr>
                                        <td>Nama Bank</td>
                                        <td>: {{ $akun_bank->nama_bank }}</td>
                                    </tr>
                                    <tr>
                                        <td>Nama Akun</td>
                                        <td>: {{ $akun_bank->nama_akun_bank }}</td>
                                    </tr>
                                    <tr>
                                        <td>No Akun</td>
                                        <td>: {{ $akun_bank->no_akun_bank }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="portlet-title">
                                    <strong>Biodata</strong>
                                </div>
                                <table class="table table-hover">
                                    <tr>
                                        <td>Jenis Kelamin</td>
                                        <td>: {{ $biodata->jenis_kelamin }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Lahir</td>
                                        <td>: {{ date_format(date_create($biodata->tanggal_lahir),'j F Y') }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tempat Lahir</td>
                                        <td>: {{ $biodata->tempat_lahir }}</td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td>: {{ $alamat->alamat.', '.$alamat->desa }} <br>
                                            {{ $alamat->kecamatan.', '.$alamat->kota }} <br>
                                            {{ $alamat->provinsi }}
                                            @if($alamat->kode_pos != "")
                                                {{ ', Kode Pos : '.$alamat->kode_pos }}
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6" style="padding-top: 20px">
                                <table class="table table-hover">
                                    <tr>
                                        <td>No Telepon</td>
                                        <td>: {{ $biodata->no_telepon }}</td>
                                    </tr>
                                    <tr>
                                        <td>No Hp</td>
                                        <td>: {{ $biodata->no_hp }}</td>
                                    </tr>
                                    <tr>
                                        <td>No KTP</td>
                                        <td>: {{ $biodata->no_ktp }}</td>
                                    </tr>
                                    <tr>
                                        <td>No NPWP</td>
                                        <td>: {{ $biodata->no_npwp }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        @if($user->type == 'peminjam')
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="portlet-title">
                                        <strong>Data Kontak Keluarga</strong>
                                    </div>
                                    <table class="table table-hover">
                                        <tr>
                                            <td>Nama Ibu Kandung</td>
                                            <td>: {{ $peminjam->nama_gadis_ibu_kandung }}</td>
                                        </tr>
                                        <tr>
                                            <td>Nama Kontak</td>
                                            <td>: {{ $peminjam->kontak_nama }}</td>
                                        </tr>
                                        <tr>
                                            <td>Hubungan Kontak</td>
                                            <td>: {{ $peminjam->hubungan }}</td>
                                        </tr>
                                        <tr>
                                            <td>No Telepon</td>
                                            <td>: {{ $peminjam->no_telp_kontak }}</td>
                                        </tr>
                                        <tr>
                                            <td>No HP</td>
                                            <td>: {{ $peminjam->no_hp_kontak }}</td>
                                        </tr>
                                        <tr>
                                            <td>Alamat</td>
                                            <td>:
                                                {{ $peminjam->k_alamat.', '.$peminjam->k_desa }} <br>
                                                {{ $peminjam->k_kecamatan.', '.$peminjam->k_kota }} <br>
                                                {{ $peminjam->k_provinsi }}
                                                @if($peminjam->k_kode_pos != "")
                                                    {{ ', Kode Pos : '.$peminjam->k_kode_pos }}
                                                @endif
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <div class="portlet-title">
                                        <strong>Data Pernikahan</strong>
                                    </div>
                                    <table class="table table-hover">
                                        <tr>
                                            <td>Status Pernikahan</td>
                                            <td>: {{ $peminjam->status_pernikahan }}</td>
                                        </tr>
                                        <tr>
                                            <td>Nama Suami/Istri</td>
                                            <td>: {{ $peminjam->nama_suami_istri }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Lahir Suami/Istri</td>
                                            <td>: {{ date_format(date_create($peminjam->tanggal_lahir_suami_istri),' j F Y') }}</td>
                                        </tr>
                                        <tr>
                                            <td>No KTP Suami/Istri</td>
                                            <td>: {{ $peminjam->no_ktp_suami_istri }}</td>
                                        </tr>
                                        <tr>
                                            <td>Pekerjaan Suami/Istri</td>
                                            <td>: {{ $peminjam->pekerjaan_suami_istri }}</td>
                                        </tr>
                                        <tr>
                                            <td>Pendapatan Suami/Istri</td>
                                            <td>: Rp. {{ number_format($peminjam->pendapatan_suami_istri,2,',','.') }}</td>
                                        </tr>
                                        <tr>
                                            <td>Jumlah Tanggungan</td>
                                            <td>: {{ $peminjam->jumlah_tanggungan }}</td>
                                        </tr>
                                        <tr>
                                            <td>Status Kepemilikan Rumah</td>
                                            <td>: {{ $peminjam->kepemilikan_rumah }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="portlet-title">
                                        <strong>Data Pekerjaan</strong>
                                    </div>
                                    <table class="table table-hover">
                                        <tr>
                                            <td>Pendidikan Terakhir</td>
                                            <td>: {{ $peminjam->pendidikan }}</td>
                                        </tr>
                                        <tr>
                                            <td>Lama Mengajar</td>
                                            <td>: {{ $peminjam->lama_mengajar }}</td>
                                        </tr>
                                        <tr>
                                            <td>Status Pengajar</td>
                                            <td>: {{ $peminjam->status_pengajar }}</td>
                                        </tr>
                                        <tr>
                                            <td>No SK</td>
                                            <td>: {{ $peminjam->no_sk_surat_kontrak }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal SK</td>
                                            <td>: {{ date_format(date_create($peminjam->tanggal_sk_surat_kontrak),'j F Y') }}</td>
                                        </tr>
                                        <tr>
                                            <td>Gaji</td>
                                            <td>: Rp. {{ number_format($peminjam->gaji,2,',','.') }}</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-6" style="padding-top: 20px">
                                    <table class="table table-hover">
                                        <tr>
                                            <td>Nama Tempat Kerja</td>
                                            <td>: {{ $peminjam->nama_tempat_kerja }}</td>
                                        </tr>
                                        <tr>
                                            <td>Lama Berdiri</td>
                                            <td>: {{ $peminjam->lama_berdiri_tempat_kerja }} Tahun</td>
                                        </tr>
                                        <tr>
                                            <td>Jumlah Murid</td>
                                            <td>: {{ $peminjam->jumlah_murid_tempat_kerja }} Siswa</td>
                                        </tr>
                                        <tr>
                                            <td>Alamat</td>
                                            <td>:
                                                {{ $peminjam->t_alamat.', '.$peminjam->t_desa }} <br>
                                                {{ $peminjam->t_kecamatan.', '.$peminjam->t_kota }} <br>
                                                {{ $peminjam->t_provinsi }}
                                                @if($peminjam->t_kode_pos != "")
                                                    {{ ', Kode Pos : '.$peminjam->t_kode_pos }}
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                          <td>No Telp</td>
                                          <td>: {{ $peminjam->no_telepon_tempat_kerja }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="portlet-title">
                                  <strong>Pengalaman Kerja</strong>
                                </div>
                                <table class="table table-hover">
                                  @foreach ($peminjam->pengalaman_kerja as $item)
                                    <tr>
                                      <td>{{ $item->tempat_kerja }}</td>
                                      <td>: {{ $item->dari_tahun." - ".$item->sampai_tahun }}</td>
                                    </tr>
                                  @endforeach
                                </table>
                              </div>
                            </div>
                        @else
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="portlet-title">
                                        <strong>Data Pemodal</strong>
                                    </div>
                                    <table class="table table-hover">
                                        <tr>
                                            <td>Tipe</td>
                                            <td>: {{ $pemodal->type }}</td>
                                        </tr>
                                        <tr>
                                            <td>Perusahaan</td>
                                            <td>: {{ $pemodal->nama }}</td>
                                        </tr>
                                        <tr>
                                            <td>No Telepon Perusahaan</td>
                                            <td>: {{ $pemodal->no_telepon }}</td>
                                        </tr>
                                        <tr>
                                            <td>No NPWP Perusahaan</td>
                                            <td>: {{ $pemodal->no_npwp }}</td>
                                        </tr>
                                        <tr>
                                            <td>Alamat Perusahaan</td>
                                            <td>:
                                                {{ $pemodal->alamat.', '.$pemodal->desa }} <br>
                                                {{ $pemodal->kecamatan.', '.$pemodal->kota }} <br>
                                                {{ $pemodal->provinsi }}
                                                @if($pemodal->kode_pos != "")
                                                    {{ ', Kode Pos : '.$pemodal->kode_pos }}
                                                @endif
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        @endif

                    </div>
                    <div class="clearfix margin-bottom-20"></div>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection
@push('page-plugin-scripts')
    <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <!-- Select2 -->
    <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
@endpush

@push('page-scripts')
    <script type="text/javascript">

    </script>
@endpush
