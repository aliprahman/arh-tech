@extends('layouts.app')
@push('page-plugin-styles')
<link href="{{ asset('/assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css">
<style type="text/css">
  .select2 {
   width:100%!important;
   }
</style>
@endpush
@section('styles')
<link rel="stylesheet" href="/assets/bootstrap-select/bootstrap-select.css" media="screen">
@stop

@section('content')
<div class="container">
	<div class="col-md-12">
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		@if(session('success') != "")
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				{{ session('success') }}
			</div>
		@endif
		</div>
	</div>
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
				<div class="portlet light portlet-fit">
				<div class="portlet-title">
                    <div class="caption">	
						<span class="caption-subject bold font-dark uppercase">{{ Lang::get('acl.role.title') }}</span>
					</div>
				</div>	
				<div class="portlet-body">
					@if (isset($role))
					{!! Former::populate($role) !!}
					{!! Former::horizontal_open()
								->method('POST')
								->action('/acl/role/post_edit')
								->accept_charset('UTF-8')
								->role('form') !!}
					@else
					{!! Former::horizontal_open()
								->method('POST')
								->action('/acl/role/post_add')
								->accept_charset('UTF-8')
								->role('form') !!}
					@endif
								
					@include('mockup.acl.role.form')
					@if (isset($role))
					{!! Former::actions( 
								Former::primary_submit('acl.save'),
								Former::link('acl.cancel', Session::get('url.referer', url('/acl/role/index'))),
								Former::danger_button('acl.delete')->addClass('pull-right')->data_toggle('modal')->data_target('#delete-modal')) 
					!!}
					@else
					{!! Former::actions( 
								Former::primary_submit('acl.save'),
								Former::link('acl.cancel', Session::get('url.referer', url('/acl/role/index')))) 
					!!}
					@endif
					
					{!! Former::close() !!}
				</div>	
			</div>	
		</div>
	</div>
</div>

@if (isset($role))
{{ Form::open(array('url' => '/acl/role/delete/'. $role->id)) }}
<div class="modal fade in" id="delete-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">{{ Lang::get('acl.resource.delete_confirmation.title') }}</h4>
            </div>
            <div class="modal-body">
				{{Lang::get('acl.resource.delete_confirmation.content')}}
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn red btn-outline">{{Lang::get('acl.delete')}}</button>
            	<button type="button" class="btn dark btn-outline" data-dismiss="modal">{{ Lang::get('acl.cancel') }}</button>
        	</div>
		</div>
	</div>
</div>	
{{ Form::close() }}
@endif

@stop

@section('scripts')
<script src="/assets/bootstrap-select/bootstrap-select.min.js"></script>
<script src="/assets/jquery-ui/ui/jquery.ui.core.js"></script>
<script src="/assets/jquery-ui/ui/jquery.ui.widget.js"></script>
<script src="/assets/jquery-ui/ui/jquery.ui.mouse.js"></script>
<script src="/assets/jquery-ui/ui/jquery.ui.sortable.js"></script>
<script>
	(function($) {
		$(function() {
			$('#parent-select').selectpicker()
				.on('change', function() {
					var $this = $(this);
					var $list = $('#parents-list');
					var parent_id = $this.val();
					var $parent_option = $this.find('[value="' + parent_id + '"]');
					var parent_name = $parent_option.text();
					
					$list.find('.no-parents').remove();
					$list.append('<li class="list-group-item"><input type="hidden" name="parents[]" value="' + parent_id + '" /><input type="hidden" name="parent_names[]" value="' + parent_name + '" />' + parent_name + '<button type="button" class="close" aria-hidden="true">&times;</button></li>');
					$parent_option.remove();
					$this.selectpicker('refresh');
				});
			$('#parents-list').sortable()
				.on('click', '.close', function() {
					var $this = $(this);
					var $li = $this.parent('li');
					var parent_id = $li.find('[name="parents[]"]').val();
					var parent_name = $li.find('[name="parent_names[]"]').val();
					$li.remove();
					if ($('#parents-list li').length === 0) {
						$('#parents-list').append('<li class="list-group-item no-parents">No Parent Selected</li>');
					}

					$('#parent-select option').each(function() {
						var $this = $(this);
						if ($this.text() > parent_name) {
							$('<option value="' + parent_id + '">' + parent_name + '</option>').insertBefore($this);
							$('#parent-select').selectpicker('refresh');
							return false;
						}
					});
					if ($('#parent-select [value=' + parent_id + ']').length === 0) {
						$('#parent-select').append('<option value="' + parent_id + '">' + parent_name + '</option>')
								.selectpicker('refresh');
					}
				});
		});
	})(jQuery);
</script>
@stop