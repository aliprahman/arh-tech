@extends('layouts.app')
@push('page-plugin-styles')
<link href="{{ asset('/assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css">
<style type="text/css">
  .select2 {
   width:100%!important;
   }
</style>
@endpush
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		@if(session('success') != "")
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				{{ session('success') }}
			</div>
		@endif
		</div>
	</div>
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="portlet light portlet-fit">
				<div class="portlet-title">
                    <div class="caption">	
						<span class="caption-subject bold font-dark uppercase">{{ Lang::get('acl.resource.title') }}</span>
					</div>
				</div>	
				<div class="portlet-body">			
					@if (isset($resource))
					{!! Former::populate($resource) !!}
					{!! Former::horizontal_open()
								->method('POST')
								->action('/acl/resource/post_edit')
								->accept_charset('UTF-8')
								->role('form') !!}
					@else
					{!! Former::horizontal_open()
								->method('POST')
								->action('/acl/resource/post_add')
								->accept_charset('UTF-8')
								->role('form') !!}
					@endif
					@include('mockup.acl.resource.form')
					@if (isset($resource))
					{!! Former::actions(
								Former::primary_submit('acl.save'),
								Former::link('acl.cancel', Session::get('url.referer', url('/acl/resource/index'))),
								Former::danger_button('acl.delete')->addClass('pull-right')->data_toggle('modal')->data_target('#delete-modal')
								) !!}
					@else
					{!! Former::actions( 
								Former::primary_submit('acl.save'),
								Former::link('acl.cancel', Session::get('url.referer', url('/acl/resource/index'))))
					!!}
					@endif
					{!! Former::close() !!}
				</div>
			</div>		
		</div>
	</div>
</div>

@if (isset($resource))
{{ Form::open(array('url' => '/acl/resource/delete/'. $resource->id)) }}
<div class="modal fade in" id="delete-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">{{ Lang::get('acl.resource.delete_confirmation.title') }}</h4>
            </div>
            <div class="modal-body">
				{{Lang::get('acl.resource.delete_confirmation.content')}}
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn red btn-outline">{{Lang::get('acl.delete')}}</button>
            	<button type="button" class="btn dark btn-outline" data-dismiss="modal">{{ Lang::get('acl.cancel') }}</button>
        	</div>
		</div>
	</div>
</div>			
{{ Form::close() }}
@endif

@stop
