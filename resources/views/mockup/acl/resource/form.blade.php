{!! Former::hidden('id') !!}
@if (!empty($resource))
 	@if(($resource->type == 'closure' || $resource->type == 'action'))
		{!! Former::staticControl('name', 'acl.name')->forceValue($resource->name) !!}
	@endif
@else
{!! Former::text('name', 'acl.name')->placeholder('acl.name') !!}
@endif
@if (!empty($resource))
{!! Former::staticControl('type')->forceValue(ucwords($resource->type)) !!}
@else
{!! Former::staticControl('type') !!}
@endif
@if (!empty($resource))
{!! Former::select('parent_id', 'acl.parent')->options(array('' => Lang::get('acl.resource.noparent')))->fromQuery(\App\Resource::except($resource->id)->get(), 'name')->addClass('selectpicker') !!}
@else
{!! Former::select('parent_id', 'acl.parent')->options(array('' => Lang::get('acl.resource.noparent')))->addClass('selectpicker') !!}
@endif

