@extends('layouts.app')
@push('page-plugin-styles')
<link href="{{ asset('/assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css">
<style type="text/css">
  .select2 {
   width:100%!important;
   }
</style>
@endpush
@section('styles')
<style>
	.table-hover td {
		cursor: pointer;
	}
</style>
@stop

@section('content')
	<div class="row">
        <div class="col-md-12">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session('success') != "")
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('success') }}
                </div>
            @endif
        </div>
    </div>

	<div class="row">
		<div class="col-md-12">
			<div class="portlet light portlet-fit">
				<div class="portlet-title">
                    <div class="caption">										
						<span class="caption-subject bold font-dark uppercase">{{ Lang::get('acl.resource.title') }}</span>
						<a href="{{ url('/acl/resource/add') }}" class="btn btn-xs btn-circle-right btn-primary">{{ Lang::get('acl.new') }} <i class="icon-plus"></i></a>						
					</div>	
				</div>
				<div class="portlet-body">
					<table class="table table-bordered table-hover table-responsive table-striped">
						<thead>
							<tr>
								<th>{{ Lang::get('acl.name') }}</th>
								<th>{{ Lang::get('acl.parent') }}</th>
							</tr>
						</thead>
						<tbody>
							@if (count($resources) > 0)
							{{ displayResources($resources) }}
							@else
							<tr>
								<td colspan="5">
									{{ Lang::get('acl.resource.emptylist') }}
								</td>
							</tr>
							@endif
						</tbody>
					</table>
				</div>	
			</div>	
		</div>
	</div>
<?php
function displayResources($rows, $left = 0)
{
	foreach ($rows as $row)
	{
		?>
		<tr data-target="{{ url('/acl/resource/edit/' . $row->id) }}">
			<td>
				<?php
				$icon = 'fa-bookmark';
				switch($row->type)
				{
					case 'closure': 
						$icon = 'fa-bolt';
						break;
					case 'action':
						$icon = 'fa-gear';
						break;
				}
				?>
				<div style="padding-left: {{ $left }}em;">
					<span class="fa {{ $icon }}" title="{{ ucwords($row->type) }}"></span>
					{{ $row->name }}
				</div>
			</td>
			<td>{{ ($row->parent) ? $row->parent->name : '-' }}</td>
		</tr>
		<?php
		if (!$row->children->isEmpty())
		{
			$children = $row->children->sortBy(function($row) 
				{
					return $row->name;
				});
			displayResources($children, $left + 1);
		}
	}
}
?>
@stop


@push('page-plugin-scripts')
<script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/form-wizard.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/common.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.number.min.js') }}" type="text/javascript"></script>
@endpush

@push('page-scripts')
<script src="{{ asset('/assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {

		$('.table-hover').on('click', 'tr[data-target]', function(e) {
			e.preventDefault();
			document.location.href = $(this).data('target');
		});
    });
</script>
@endpush