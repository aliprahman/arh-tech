@extends('layouts.app')

@push('page-plugin-styles')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('title','Detail Pinjaman')

@section('crumbs')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <span>Admin</span>
            <i class="fa fa-circle"></i>
        </li>
        <li class="active">
            <a href="{{ url('pinjaman') }}">Daftar Pinjaman</a>
            <i class="fa fa-circle"></i>
        </li>
        <li class="active">
            <a href="{{ url('pinjaman/analisa/'.$pinjaman_id) }}">Detail Pinjaman</a>
        </li>
    </ul>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    @if(session('success') != "")
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{ session('success') }}
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="portlet light ">
        <div class="portlet-title">
            <div class="caption">
                <span class="caption-subject bold uppercase">Detail Pinjaman</span>
            </div>
        </div>
        <div class="portlet-body">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#data_pinjaman" aria-controls="data_pinjaman" role="tab" data-toggle="tab">Data Pinjaman</a></li>
                <li role="presentation"><a href="#data_pendanaan" aria-controls="data_pendanaan" role="tab" data-toggle="tab">Data Pendanaan</a></li>
                <li role="presentation"><a href="#data_profile" aria-controls="data_profile" role="tab" data-toggle="tab">Profile Peminjam</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="data_pinjaman">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="portlet-title">
                                <strong>Data Pinjaman</strong>
                            </div>
                            <table class="table table-bordered">
                                <tr>
                                    <th>Kode Pinjam</th>
                                    <td>: {{ $pinjaman->kode }}</td>
                                </tr>
                                <tr>
                                    <th>Tanggal Pengajuan</th>
                                    <td>: {{ date_format(date_create($pinjaman->tanggal_submit),'d-m-Y') }}</td>
                                </tr>
                                <tr>
                                    <th>Jumlah Pinjaman</th>
                                    <td>: Rp. {{ number_format($pinjaman->jumlah_pinjaman,2,',','.') }}</td>
                                </tr>
                                <tr>
                                    <th>Lama Pinjaman </th>
                                    <td>: {{ $pinjaman->waktu_pinjaman }} Bulan</td>
                                </tr>
                                <tr>
                                    <th>Biaya Admin </th>
                                    <td>: Rp. {{ number_format($pinjaman->admin_fee,0,',','.') }} </td>
                                </tr>
                                <tr>
                                    <th>Biaya Conf </th>
                                    <td>: Rp. {{ number_format($pinjaman->conf_fee,0,',','.') }} </td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td>: {{ ucfirst($pinjaman->status) }}</td>
                                </tr>
                                <tr>
                                    <th>Alasan Ditolak</th>
                                    <td>: {{ $pinjaman->alasan_ditolak }}</td>
                                </tr>
                                <tr>
                                    <th>Tangal Mulai Pendanaan</th>
                                    <td>: @if($pinjaman->tanggal_mulai_funding != "") {{ date_format(date_create($pinjaman->tanggal_mulai_funding),'d F Y') }} @endif</td>
                                </tr>
                                <tr>
                                    <th>Tangal Berakhir Pendanaan</th>
                                    <td>: @if($pinjaman->tanggal_berakhir_funding != "") {{ date_format(date_create($pinjaman->tanggal_berakhir_funding),'d F Y') }} @endif</td>
                                </tr>
                            </table>
                            <div class="row">
                                @if($pinjaman->status != "reject")
                                    <div class="col-md-4">
                                        @if($pinjaman->tanggal_mulai_funding == "" || $pinjaman->tanggal_berakhir_funding == "")
                                            <button type="button" class="btn btn-danger btn-block" onclick="reject('{{ $pinjaman_id }}')">Reject</button>
                                        @endif
                                    </div>
                                    <div class="col-md-4">
                                        @if (count($pinjaman->cicilan) < 1)
                                            <button type="button" class="btn btn-primary btn-block" onclick="simulasi('{{ $pinjaman_id }}')">Confirmation</button>
                                        @endif
                                    </div>
                                    @if ($pinjaman->status == 'listing')
                                      <div class="col-md-4">
                                          @if($pinjaman->tanggal_mulai_funding == "" || $pinjaman->tanggal_berakhir_funding == "")
                                              <button type="button" class="btn btn-success btn-block" onclick="set_funding('{{ $pinjaman_id }}')">Set Funding</button>
                                          @endif
                                      </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            @if (count($pinjaman->cicilan) > 0)
                                <div class="portlet-title">
                                    <strong>Data Cicilan</strong>
                                </div>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Cicilan Ke</th>
                                        <th>Jumlah</th>
                                        <th>Tanggal</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @for ($i=0; $i < count($pinjaman->cicilan); $i++)
                                        <tr>
                                            <td>{{ $i+1 }}</td>
                                            <td>Rp. {{ number_format($pinjaman->cicilan[$i]->jumlah,2,',','.') }}</td>
                                            <td>
                                                @if ($pinjaman->cicilan[$i]->tanggal != "")
                                                    {{ date_format(date_create($pinjaman->cicilan[$i]->tanggal),'d-m-Y') }}
                                                @endif
                                            </td>
                                            <td>{{ title_case($pinjaman->cicilan[$i]->status) }}</td>
                                        </tr>
                                    @endfor
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="data_pendanaan">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Tanggal</th>
                                        <th>Jumlah</th>
                                    </tr>
                                </thead>
                                <tbody>@php $no=1; @endphp
                                @if(count($pendanaan) > 0)
                                    @foreach($pendanaan as $item)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td><a target="_blank" title="detail akun {{ $item->nama }}" href="{{ url('akun/'.$item->user_id) }}">{{ $item->nama }}</a></td>
                                            <td>{{ date_format(date_create($item->tanggal_pendanaaan),'d F Y') }}</td>
                                            <td>Rp. {{ number_format($item->jumlah,2,',','.') }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                <tr>
                                    <th colspan="4" style="text-align: center">Pinjaman belum didanai</th>
                                </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="data_profile">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="portlet-title">
                                        <strong>Data User</strong>
                                    </div>
                                    <table class="table table-hover">
                                        <tr>
                                            <td>Nama</td>
                                            <td>: {{ ucfirst($user->name) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td>: {{ $user->email }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tipe</td>
                                            <td>: {{ ucfirst($user->type) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td>: {{ ucfirst($user->status) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Daftar</td>
                                            <td>: {{ date_format(date_create($user->created_at),'j F Y H:i:s') }}</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <div class="portlet-title">
                                        <strong>Data Akun Bank</strong>
                                    </div>
                                    <table class="table table-hover">
                                        <tr>
                                            <td>Nama Bank</td>
                                            <td>: {{ $akun_bank->nama_bank }}</td>
                                        </tr>
                                        <tr>
                                            <td>Nama Akun</td>
                                            <td>: {{ $akun_bank->nama_akun_bank }}</td>
                                        </tr>
                                        <tr>
                                            <td>No Akun</td>
                                            <td>: {{ $akun_bank->no_akun_bank }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="portlet-title">
                                        <strong>Biodata</strong>
                                    </div>
                                    <table class="table table-hover">
                                        <tr>
                                            <td>Jenis Kelamin</td>
                                            <td>: {{ $biodata->jenis_kelamin }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Lahir</td>
                                            <td>: {{ date_format(date_create($biodata->tanggal_lahir),'j F Y') }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tempat Lahir</td>
                                            <td>: {{ $biodata->tempat_lahir }}</td>
                                        </tr>
                                        <tr>
                                            <td>Alamat</td>
                                            <td>: {{ $alamat->alamat.', '.$alamat->desa }} <br>
                                                {{ $alamat->kecamatan.', '.$alamat->kota }} <br>
                                                {{ $alamat->provinsi }}
                                                @if($alamat->kode_pos != "")
                                                    {{ ', Kode Pos : '.$alamat->kode_pos }}
                                                @endif
                                            </td>
                                        </tr>
                                        @if ($peminjam->photo_tempat_tinggal_id != "")
                                          <tr>
                                            <td>Foto Tempat Tinggal</td>
                                            <td>
                                              <a href="{{ asset('storage/tempat_tinggal/'.$peminjam->photo_tempat_tinggal->name) }}" title="Klik Untuk Melihat" target="_blank">
                                                <img src="{{ asset('storage/tempat_tinggal/'.$peminjam->photo_tempat_tinggal->name) }}" style="max-height: 128px; max-width: 128px" />
                                              </a>
                                            </td>
                                          </tr>
                                        @endif
                                    </table>
                                </div>
                                <div class="col-md-6" style="padding-top: 20px">
                                    <table class="table table-hover">
                                        <tr>
                                            <td>No Telepon</td>
                                            <td>: {{ $biodata->no_telepon }}</td>
                                        </tr>
                                        <tr>
                                            <td>No Hp</td>
                                            <td>: {{ $biodata->no_hp }}</td>
                                        </tr>
                                        <tr>
                                            <td>No KTP</td>
                                            <td>: {{ $biodata->no_ktp }}</td>
                                        </tr>
                                        <tr>
                                            <td>No NPWP</td>
                                            <td>: {{ $biodata->no_npwp }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="portlet-title">
                                        <strong>Data Kontak Keluarga</strong>
                                    </div>
                                    <table class="table table-hover">
                                        <tr>
                                            <td>Nama Ibu Kandung</td>
                                            <td>: {{ $peminjam->nama_gadis_ibu_kandung }}</td>
                                        </tr>
                                        <tr>
                                            <td>Nama Kontak</td>
                                            <td>: {{ $peminjam->kontak_nama }}</td>
                                        </tr>
                                        <tr>
                                            <td>Hubungan Kontak</td>
                                            <td>: {{ $peminjam->hubungan }}</td>
                                        </tr>
                                        <tr>
                                            <td>No Telepon</td>
                                            <td>: {{ $peminjam->no_telp_kontak }}</td>
                                        </tr>
                                        <tr>
                                            <td>No HP</td>
                                            <td>: {{ $peminjam->no_hp_kontak }}</td>
                                        </tr>
                                        <tr>
                                            <td>Alamat</td>
                                            <td>:
                                                {{ $peminjam->k_alamat.', '.$peminjam->k_desa }} <br>
                                                {{ $peminjam->k_kecamatan.', '.$peminjam->k_kota }} <br>
                                                {{ $peminjam->k_provinsi }}
                                                @if($peminjam->k_kode_pos != "")
                                                    {{ ', Kode Pos : '.$peminjam->k_kode_pos }}
                                                @endif
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <div class="portlet-title">
                                        <strong>Data Pernikahan</strong>
                                    </div>
                                    <table class="table table-hover">
                                        <tr>
                                            <td>Status Pernikahan</td>
                                            <td>: {{ $peminjam->status_pernikahan }}</td>
                                        </tr>
                                        <tr>
                                            <td>Nama Suami/Istri</td>
                                            <td>: {{ $peminjam->nama_suami_istri }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Lahir Suami/Istri</td>
                                            <td>: {{ date_format(date_create($peminjam->tanggal_lahir_suami_istri),' j F Y') }}</td>
                                        </tr>
                                        <tr>
                                            <td>No KTP Suami/Istri</td>
                                            <td>: {{ $peminjam->no_ktp_suami_istri }}</td>
                                        </tr>
                                        <tr>
                                            <td>Pekerjaan Lahir Suami/Istri</td>
                                            <td>: {{ $peminjam->pekerjaan_suami_istri }}</td>
                                        </tr>
                                        <tr>
                                            <td>Pendapatan Lahir Suami/Istri</td>
                                            <td>: Rp. {{ number_format($peminjam->pendapatan_suami_istri,2,',','.') }}</td>
                                        </tr>
                                        <tr>
                                            <td>Jumlah Tanggungan</td>
                                            <td>: {{ $peminjam->jumlah_tanggungan }}</td>
                                        </tr>
                                        <tr>
                                            <td>Status Kepemilikan Rumah</td>
                                            <td>: {{ $peminjam->kepemilikan_rumah }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="portlet-title">
                                        <strong>Data Pekerjaan</strong>
                                    </div>
                                    <table class="table table-hover">
                                        <tr>
                                            <td>Pendidikan Terakhir</td>
                                            <td>: {{ $peminjam->pendidikan }}</td>
                                        </tr>
                                        <tr>
                                            <td>Lama Mengajar</td>
                                            <td>: {{ $peminjam->lama_mengajar }}</td>
                                        </tr>
                                        <tr>
                                            <td>Status Pengajar</td>
                                            <td>: {{ $peminjam->status_pengajar }}</td>
                                        </tr>
                                        <tr>
                                            <td>No SK</td>
                                            <td>: {{ $peminjam->no_sk_surat_kontrak }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal SK</td>
                                            <td>: {{ date_format(date_create($peminjam->tanggal_sk_surat_kontrak),'j F Y') }}</td>
                                        </tr>
                                        <tr>
                                            <td>Gaji</td>
                                            <td>: Rp. {{ number_format($peminjam->gaji,2,',','.') }}</td>
                                        </tr>
                                        @if ($peminjam->photo_curiculum_vitae_id != "")
                                          <tr>
                                            <td>Foto CV</td>
                                            <td>
                                              <a href="{{ asset('storage/curiculum_vitae/'.$peminjam->photo_curiculum_vitae->name) }}" title="Klik Untuk Melihat" target="_blank">
                                                <img src="{{ asset('storage/curiculum_vitae/'.$peminjam->photo_curiculum_vitae->name) }}" style="max-height: 128px; max-width: 128px" />
                                              </a>
                                            </td>
                                          </tr>
                                        @endif
                                    </table>
                                </div>
                                <div class="col-md-6" style="padding-top: 20px">
                                    <table class="table table-hover">
                                        <tr>
                                            <td>Nama Tempat Kerja</td>
                                            <td>: {{ $peminjam->nama_tempat_kerja }}</td>
                                        </tr>
                                        <tr>
                                            <td>Lama Berdiri</td>
                                            <td>: {{ $peminjam->lama_berdiri_tempat_kerja }} Tahun</td>
                                        </tr>
                                        <tr>
                                            <td>Jumlah Murid</td>
                                            <td>: {{ $peminjam->jumlah_murid_tempat_kerja }} Siswa</td>
                                        </tr>
                                        <tr>
                                            <td>Alamat</td>
                                            <td>:
                                                {{ $peminjam->t_alamat.', '.$peminjam->t_desa }} <br>
                                                {{ $peminjam->t_kecamatan.', '.$peminjam->t_kota }} <br>
                                                {{ $peminjam->t_provinsi }}
                                                @if($peminjam->t_kode_pos != "")
                                                    {{ ', Kode Pos : '.$peminjam->t_kode_pos }}
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>No Telp</td>
                                            <td>: {{ $peminjam->no_telepon_tempat_kerja }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="portlet-title">
                                        <strong>Pengalaman Kerja</strong>
                                    </div>
                                    <table class="table table-hover">
                                        @foreach ($peminjam->pengalaman_kerja as $item)
                                            <tr>
                                                <td>{{ $item->tempat_kerja }}</td>
                                                <td>: {{ $item->dari_tahun." - ".$item->sampai_tahun }}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         </div>
      </div>
    </div>
 </div>
 <div class="modal fade" tabindex="-1" role="dialog" id="funding_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="post" id="form_funding">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Atur Periode Pendanaan</h4>
        </div>
        <div class="modal-body">
          {{ csrf_field() }}
          {{ method_field('PUT') }}
          <div class="form-group">
              <label for="tanggal_mulai">Tanggal Mulai Pendanaan</label>
              <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                  <input type="text" name="tanggal_mulai" id="tanggal_mulai" class="form-control" readonly="" required>
                  <span class="input-group-btn">
                      <button class="btn default" type="button">
                           <i class="fa fa-calendar"></i>
                      </button>
                  </span>
              </div>
          </div>
          <div class="form-group">
              <label for="tanggal_akhir">Tanggal Berakhir Pendanaan</label>
              <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                  <input type="text" name="tanggal_akhir" id="tanggal_akhir" class="form-control" readonly="" required>
                  <span class="input-group-btn">
                      <button class="btn default" type="button">
                           <i class="fa fa-calendar"></i>
                      </button>
                  </span>
              </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="reject_modal">
 <div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
     <form method="post" id="form_reject">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h4 class="modal-title">Reject</h4>
       </div>
       <div class="modal-body">
         {{ csrf_field() }}
         {{ method_field('PUT') }}
         <textarea class="form-control" name="note" placeholder="Alasana ditolak....." minlength=10 required></textarea>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         <button type="submit" class="btn btn-primary">Save changes</button>
       </div>
     </form>
   </div><!-- /.modal-content -->
 </div><!-- /.modal-dialog -->
</div>

 <div class="modal fade modal-" tabindex="-1" role="dialog" id="simulasi_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="post" id="form_simulasi">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Simulasi Cicilan</h4>
        </div>
        <div class="modal-body">
          {{ csrf_field() }}
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                  <label for="tanggal_akhir">Admin Fee</label>
                  <div class="input-group">
                      <span class="input-group-addon">Rp. </span>
                      <input type="text" name="biaya_admin" id="admin_fee" class="form-control nominal" autocomplete="off" placeholder="" required>
                  </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                  <label for="tanggal_akhir">Conf Fee</label>
                  <div class="input-group">
                      <span class="input-group-addon">Rp. </span>
                      <input type="text" name="biaya_conf" id="conf_fee" class="form-control nominal" autocomplete="off" placeholder="" required>
                  </div>
              </div>
            </div>
          </div>

          <div id="table_simulasi">

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Kirim Simulasi Cicilan</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

@endsection
@push('page-plugin-scripts')
<!-- DataTables -->
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<!-- Select2 -->
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.number.min.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
@endpush

@push('page-scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $('#status').change(function () {
            if($(this).val() === 'reject'){
                $('#alasan').css('display','block');
                $('#notes').prop('required',true);
            }else{
                $('#alasan').css('display','none');
                $('#notes').removeAttr('required').val('');
            }
        });
        $('.date-picker').datepicker({
            orientation: "right",
            autoclose: true
        });
        $('#admin_fee').number(true,2,',','.');
        $('#conf_fee').number(true,2,',','.');
        $('#admin_fee').blur(function() {
          $('.admin_fee').val($(this).val());
          $('.admin_fee_text').text($.number($(this).val(),2,',','.'));
          calculate();
        });
        $('#conf_fee').blur(function() {
          $('.conf_fee').val($(this).val());
          $('.conf_fee_text').text($.number($(this).val(),2,',','.'));
          calculate();
        });
    });
    function set_funding(pinjaman_id) {
      $('#form_funding').attr('action','{{ url('pinjaman/funding') }}'+'/'+pinjaman_id);
      $('#funding_modal').modal('show');
    }
    function reject(pinjaman_id) {
      $('#form_reject').attr('action','{{ url('pinjaman/reject') }}'+'/'+pinjaman_id);
      $('#reject_modal').modal('show');
    }
    function simulasi(pinjaman_id) {
      $('#form_simulasi').attr('action','{{ url('pinjaman/kirim_simulasi') }}'+'/'+pinjaman_id);
      $('#table_simulasi').load('{{ url('pinjaman/simulasi') }}'+'/'+pinjaman_id, function () {
        $('#simulasi_modal').modal('show');
      },'html');
    }
    function calculate() {
      var admin_fee = ($('#admin_fee').val() == "") ? 0 : parseInt($('#admin_fee').val());
      var conf_fee = ($('#conf_fee').val() == "")? 0 : parseInt($('#conf_fee').val());
      var pinjaman = parseInt($('#cicilan_bulanan').val());

      var total = pinjaman + admin_fee + conf_fee;
      $('.total_text').text($.number(total,2,',','.'));
      $('.total').val(total);
    }
</script>
@endpush
