@extends('layouts.app')

@push('page-plugin-styles')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css">
<style type="text/css">
  .select2 {
   width:100%!important;
   }
</style>
@endpush

@section('title')
Pinjaman Baru
@endsection

@section('crumbs')
  <ul class="page-breadcrumb breadcrumb">
      <li>
          <span>Peminjam</span>
          <i class="fa fa-circle"></i>
      </li>
      <li>
          <a href="{{ url('pinjaman') }}">
              <span>Daftar Pinjaman</span>
          </a>
          <i class="fa fa-circle"></i>
      </li>
      <li class="active">
          <a href="{{ url('pinjaman/ajuan') }}">
              <span>Pengajuan Pinjaman Baru</span>
          </a>
      </li>
  </ul>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-pencil"></i>
                    <span class="uppercase caption-subject bold">detail pengajuan pinjaman</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-6">
                        <form class="form-horizontal" method="POST" action="{{ url('pinjaman/ajuan/simpan') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="control-lable col-sm-5"><h5>Jenis Pinjaman</h5></label>
                                <div class="col-sm-6">
                                    <select class="form-control select2" name="jenis_pinjaman" required>
                                        <option value="">-- PILIH --</option>
                                        @foreach ($jenis_pinjaman as $key => $value)
                                            @if(isset($pinjaman) && $pinjaman->jenis_pinjaman_id == $value)
                                                <option value="{{ $value }}" selected>{{ $key }}</option>
                                            @else
                                                <option value="{{ $value }}">{{ $key }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-lable col-sm-5"><h5>Jumlah Pinjaman</h5></label>
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">Rp.</span>
                                        <input type="text" name="jumlah_pinjaman" id="jumlah_pinjaman" class="form-control" required
                                            @isset($pinjaman)
                                                value="{{ $pinjaman->jumlah_pinjaman }}"
                                            @endisset
                                        >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-lable col-sm-5"><h5>Lama Pinjaman</h5></label>
                                <div class="col-sm-6">
                                    <select class="form-control select2" name="lama_pinjaman" required>
                                        <option value="">-- PILIH --</option>
                                        @for ($i=1; $i <= 12; $i++)
                                            @if(isset($pinjaman) && $pinjaman->waktu_pinjaman == $i)
                                                <option value="{{ $i }}" selected>
                                                    {{ $i }} Bulan
                                                </option>
                                            @else
                                                <option value="{{ $i }}">
                                                    {{ $i }} Bulan
                                                </option>
                                            @endif
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6 col-sm-offset-5">
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        @if(session('success') != "")
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ session('success') }}
            </div>
        @endif
        @if(session('error') != "")
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ session('error') }}
            </div>
        @endif
    </div>
</div>
@include('mockup.peminjam.main-profile')
@include('mockup.peminjam.popup')

@endsection

@push('page-plugin-scripts')
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.number.min.js') }}" type="text/javascript"></script>
@endpush

@push('page-scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.select2').select2({
                theme: "bootstrap"
            });
            $('#jumlah_pinjaman').number(true,2,',','.');
        });
    </script>
@endpush
