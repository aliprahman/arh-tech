@extends('layouts.app')

@push('page-plugin-styles')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .table thead tr th{
            white-space: pre-wrap;
            vertical-align: middle;
            text-align: center;
            padding-top: 20px;
            padding-bottom: 20px;
        }
        .table tbody tr td{
            vertical-align: middle;
        }

        #data th, #data td {
            font-size: 13px;
        }
    </style>
@endpush

@section('title','Daftar Peminjaman')

@section('crumbs')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <span>Peminjam</span>
            <i class="fa fa-circle"></i>
        </li>
        <li class="active">
            <a href="{{ url('pinjaman') }}">
                <span>Daftar Pinjaman</span>
            </a>
        </li>
    </ul>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session('success') != "")
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('success') }}
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption">
                        <a href="{{ url('/pinjaman/ajuan') }}" class="btn green btn-outline btn-circle btn-sm btn-sm"><i class="fa fa-edit"></i> Ajukan Pinjaman Baru</a>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-sm btn-default" href="javascript:;" id="print_pinjaman_pdf">
                            <i class="fa fa-print"></i> Print to PDF
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <center><h1>Daftar Pinjaman</h1></center>
                    <table class="table table-striped table-bordered table-hover" id="data">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Tanggal Pengajuan</th>
                            <th>Kode Pinjaman</th>
                            <th>Nilai Pinjaman</th>
                            <th>Waktu</th>
                            <th>Pendanaan</th>
                            <th>Status Pengajuan Pinjaman</th>
                            <th>Status Cicilan</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    <br>
                    <div class="clearfix margin-bottom-20"> </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="printModal" tabindex="-1" role="dialog" aria-labelledby="printModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="contactModalLabel">Print Preview</h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button id="printModal-print-btn" type="button" class="btn btn-primary"><i class="fa fa-print"></i> Print</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('page-plugin-scripts')
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
@endpush

@push('page-scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            var table = $('#data').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ url('daftar/pinjaman/peminjam') }}',
                    type: 'POST'
                },
                columns: [
                    {data: "DT_Row_Index", orderable: false, searchable: false},
                    {data: "tanggal_submit", name: 'tanggal_submit'},
                    {data: "kode", name: 'kode'},
                    {data: "jumlah_pinjaman", name: 'jumlah_pinjaman', className: 'dt-right'},
                    {data: "waktu_pinjaman", name: 'waktu_pinjaman'},
                    {data: "progres", name: "progress"},
                    {data: "status", name: "status"},
                    {data: "status_cicilan", name: "status_cicilan"}
                ],
                rowCallback: function(row,data,index){
                    // nomer
                    $('td:eq(0)', row).html( '<center>'+data.DT_Row_Index+'</center>' );
                    // tanggal_pinjaman
                    $('td:eq(1)',row).html(moment(data.tanggal_submit).format("DD-MM-YYYY"));
                    // jumlah_pinjaman
                    $('td:eq(3)',row).html(numeral(data.jumlah_pinjaman).format('0,0'));
                    // progres
                    $('td:eq(5)',row).html('<div class="easy-pie-chart">\n' +
                        '                                <div class="number visits" data-percent="'+data.progres+'%">\n' +
                        '                                    <span>'+data.progres+'</span>%\n' +
                        '                                </div>\n' +
                        '                            </div>');
                    // status
                    var style = "";
                    var label = data.status.toUpperCase()
                    if(data.status == 'Check Email'){
                      style = "style='color: red'";
                      label = "Analysis <br>(Please Check Your Email)";
                    }
                    $('td:eq(6)',row).html('<center><p '+style+'><b>'+label+'<b></p></center>');
                    // action
                    var action_link = '<a href="'+$('meta[name="base_url"]').attr('content')+'/pinjaman/rincian/'+data.id+'" class="btn btn-circle btn-sm green btn-outline"><i class="fa fa-search"></i>Lihat Rincian</a>'
                    $('td:eq(7)', row).html( action_link );
                }
            });
            table.on('draw',function () {
                $('.easy-pie-chart .number.visits').easyPieChart( {
                    animate: 1e3, size: 75, lineWidth: 3
                });
            });
            $('#print_pinjaman_pdf').click(function () {
                var iframeHeight = $(window).height() - 220;

                $('#printModal .modal-body').html('<iframe id="printModal-iframe" src="{{url("daftar/pinjaman/peminjam/pdf") }}" style="width: 100%; height: ' + iframeHeight + 'px; border: 1px solid #e5e5e5;background-image: url({{asset('assets/global/img/input-spinner.gif')}}); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>');
                $('#printModal-print-btn').attr('disabled', 'disabled');
                $('#printModal-iframe').on('load', function () {
                    $('#printModal-print-btn').removeAttr('disabled');
                });
                $('#printModal').modal('show');
            });
            $('#printModal-print-btn').on('click', function () {
                $('#printModal-iframe').get(0).contentWindow.print();
            });
        } );
    </script>
@endpush
