@extends('layouts.app')

@push('page-plugin-styles')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <style type="text/css">
        .table thead tr th{
            white-space: pre-wrap;
            vertical-align: middle;
            text-align: center;
            padding-top: 20px;
            padding-bottom: 20px;
        }
        .table tbody tr td{
            vertical-align: middle;
        }
        label{
            text-align: left;
        }
        #data th, #data td {
            font-size: 13px;
        }
    </style>
@endpush

@section('crumbs')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/dashboard') }}">Peminjam</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ url('/pinjaman') }}">Daftar Peminjaman</a>
            <i class="fa fa-circle"></i>
        </li>
        <li class="active">
            <a href="{{ url('/pinjaman/rincian/'.$pinjaman_id) }}">
                <span>Rincian Pinjaman</span>
            </a>
        </li>
    </ul>
@endsection

@section('title')
    Rincian Peminjaman
@endsection

@section('content')
    <div class="panel">
        <div class="panel-body">
            <a href="{{ URL::previous() }}" class="btn btn-circle btn-default"><i class="icon-refresh"></i> Kembali</a>
            <center><h1>Rincian Peminjaman</h1></center><br><br>
            <div class="container-fluid">
                <form class="form-horizontal">
                    <div class="row">
                        <!-- kiri -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class=" col-sm-4"><h5>Kode Pinjaman</h5></label>
                                <div class="col-sm-5">
                                    <input type="text" name="kode" disabled class="form-control" placeholder="{{ $pinjaman->kode }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class=" col-sm-4"><h5>Tanggal Pinjaman</h5></label>
                                <div class="col-sm-5">
                                    <input type="text" name="tgl" disabled class="form-control" placeholder="{{ date_format(date_create($pinjaman->tanggal_submit),'d-m-Y') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class=" col-sm-4"><h5>Waktu Pinjaman</h5></label>
                                <div class="col-sm-5">
                                    <input type="text" name="tgl" disabled class="form-control" placeholder="{{ $pinjaman->waktu_pinjaman }} Bulan">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class=" col-sm-4"><h5>Status pinjaman</h5></label>
                                <div class="col-sm-5">
                                    <input type="text" name="stat" disabled class="form-control"
                                           @if(count($cicilan) > 0)
                                               @if($sisa_pinjaman > 0)
                                                    placeholder="Belum Lunas"
                                               @else
                                                    placeholder="Lunas"
                                               @endif
                                           @else
                                                placeholder="Belum Lunas"
                                           @endif
                                    >
                                </div>
                            </div>
                        </div>
                        <!-- kanan -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class=" col-sm-5"><h5>Status Pengajuan Pinjaman</h5></label>
                                <div class="col-sm-5">
                                    <input type="text" name="pin" disabled class="form-control" placeholder="{{ title_case($pinjaman->status) }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class=" col-sm-5"><h5>Nilai Pinjaman</h5></label>
                                <div class="col-sm-5">
                                    <input type="text" name="nil" disabled class="form-control" placeholder="Rp. {{ number_format($pinjaman->jumlah_pinjaman,2,',','.') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class=" col-sm-5"><h5>Admin Fee</h5></label>
                                <div class="col-sm-5">
                                    <input type="text" name="nil" disabled class="form-control" placeholder="Rp. {{ number_format($pinjaman->admin_fee,2,',','.') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class=" col-sm-5"><h5>Conf Fee</h5></label>
                                <div class="col-sm-5">
                                    <input type="text" name="nil" disabled class="form-control" placeholder="Rp. {{ number_format($pinjaman->conf_fee,2,',','.') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class=" col-sm-5"><h5>Sisa Pinjaman</h5></label>
                                <div class="col-sm-5">
                                    <input type="text" name="sis" disabled class="form-control" placeholder="Rp. {{ number_format($sisa_pinjaman,2,',','.') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div><br><br><br>
            <div class="row">
                <div class="col-md-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-hourglass"></i>
                                <span class="caption-subject bold uppercase">DAFTAR PEMBAYARAN CICILAN</span>
                            </div>
                            <div class="actions">
                                @if(count($cicilan) > 0 && $pinjaman->status == 'analysis')
                                    <a onclick="konfirmasi(1,'{{$pinjaman->id}}')" class="btn btn-success" role="button">Setujui Simulasi Cicilan</a>
                                    <a onclick="konfirmasi(0,'{{$pinjaman->id}}')" class="btn btn-danger" role="button">Tolak Pinjaman</a>
                                @endif
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-bordered" id="cicilan">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Waktu Transaksi</th>
                                    <th>Kode transaksi</th>
                                    <th>Nilai Transaksi</th>
                                    <th>Jatuh Tempo</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                  @php $no=1; $bayar = true; @endphp                                        
                                  @foreach ($cicilan as $key)
                                    <tr @if ($key->status != 'unverified') class="odd" @endif>
                                      <td align="center">{{ $no++ }}</td>
                                      <td align="left">
                                        @if ($key->tanggal != "")
                                          {{ date('d F Y',strtotime($key->tanggal)) }}
                                        @endif
                                      </td>
                                      <td>{{ $key->ref }}</td>
                                      <td align="right">Rp. {{ number_format($key->jumlah,2,',','.') }}</td>
                                      <td align="left">
                                        @if ($key->jatuh_tempo != "")
                                          {{ date('d F Y',strtotime($key->jatuh_tempo)) }}
                                        @endif
                                      </td>
                                      <td align="center">{{ title_case($key->status) }}</td>
                                      <td align="center">
                                         @if(!in_array($pinjaman->status,['analysis','reject','submit','draft']) && $key->jatuh_tempo != "")
                                            @if ($key->status == 'unverified')
                                              @if ($bayar)
                                                <a class="btn btn-xs green btn-outline btn-circle" onclick="popup_pembayaran({{ $key->id }},{{ $key->jumlah }})"> Konfirmasi pembayaran </a>
                                                @php
                                                  $bayar = false;
                                                @endphp
                                              @endif
                                            @else
                                              <a class="btn btn-xs green btn-outline btn-circle" onclick="popup_detail({{ $key->transaksi_id }})"> Detail </a>
                                            @endif
                                          @else
                                         @endif
                                      </td>
                                    </tr>
                                  @endforeach
                                </tbody>
                            </table>
                            <div class="clearfix margin-bottom-20"> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="basic" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form id="form_pembayaran" class="form-horizontal" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Pembayaran Cicilan</h4>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                              <label class=" col-sm-5"><h5>Transfer Ke</h5></label>
                              <div class="col-sm-7">
                                  <select class="form-control" name="bank" required>
                                      @foreach ($list_bank as $key)
                                        @if (old('bank') != "" && old('bank') == $key->id)
                                          <option value="{{ $key->id }}" selected>{{ $key->bank->nama }} ({{ $key->nama_akun_bank }})</option>
                                        @else
                                          <option value="{{ $key->id }}">{{ $key->bank->nama }} ({{ $key->nama_akun_bank }})</option>
                                        @endif
                                      @endforeach
                                  </select>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class=" col-sm-5"><h5>Nominal Pembayaran</h5></label>
                              <div class="col-sm-7">
                                  <input type="text" id="nominal" name="nominal" value="{{ old('nominal') }}" class="form-control" required>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class=" col-sm-5"><h5>Tanggal Transfer</h5></label>
                              <div class="col-sm-7">
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' class="form-control textbox" name="tanggal" id="tanggal" placeholder="format: 2017-05-01 13:15" value="{{ old('tanggal') }}" required />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class=" col-sm-5"><h5>Transfer Via</h5></label>
                              <div class="col-sm-7">
                                  <input type="text" name="nama_bank" class="form-control" value="{{ old('nama_bank') }}" placeholder="Nama Bank" required>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class=" col-sm-5"><h5>Nama Pemilik Rekening</h5></label>
                              <div class="col-sm-7">
                                  <input type="text" name="nama_akun" class="form-control" value="{{ old('nama_akun') }}" required>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class=" col-sm-5"><h5>No Rekening</h5></label>
                              <div class="col-sm-7">
                                  <input type="text" name="no_rekening" class="form-control" value="{{ old('no_rekening') }}" required>
                              </div>
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group">
                              <label class=" col-sm-5"><h5>No. Transaksi</h5></label>
                              <div class="col-sm-7">
                                  <input type="text" name="no_transaksi" class="form-control" value="{{ old('no_transaksi') }}">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class=" col-sm-5"><h5>Catatan</h5></label>
                              <div class="col-sm-7">
                                  <textarea class="form-control" name="catatan" placeholder="Catatan Tambahan (Optional)">{{ old('catatan') }}</textarea>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class=" col-sm-5"><h5>Bukti Transfer</h5></label>
                              <div class="col-sm-7">
                                  <input type="file" name="bukti" class="form-control" required />
                              </div>
                          </div>
                          <div class="form-group">
                              <div class="col-sm-offset-3">
                                  <div class="g-recaptcha" data-sitekey="{{ env('CAPTCHA_SITEKEY') }}"></div>
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline" data-dismiss="modal" aria-hidden="true">Close</button>
                        <button type="submit" class="btn green">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="detail_modal" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form id="form_pembayaran" class="form-horizontal" method="post" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Detail Pembayaran Cicilan</h4>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                              <label class=" col-sm-5"><h5>Transfer Ke</h5></label>
                              <div class="col-sm-7">
                                <input type="text" class="form-control" id="detail_nama_bank" readonly />
                              </div>
                          </div>
                          <div class="form-group">
                              <label class=" col-sm-5"><h5>Nominal Pembayaran</h5></label>
                              <div class="col-sm-7">
                                <input type="text" class="form-control" id="detail_nominal" readonly />
                              </div>
                          </div>
                          <div class="form-group">
                              <label class=" col-sm-5"><h5>Tanggal Transfer </h5></label>
                              <div class="col-sm-7">
                                  <input type="text" class="form-control" id="tanggal_transfer" readonly />
                              </div>
                          </div>
                          <div class="form-group">
                              <label class=" col-sm-5"><h5>Transfer Via</h5></label>
                              <div class="col-sm-7">
                                  <input type="text" class="form-control" id="detail_bank" readonly />
                              </div>
                          </div>
                          <div class="form-group">
                              <label class=" col-sm-5"><h5>Nama Pemilik Rekening</h5></label>
                              <div class="col-sm-7">
                                  <input type="text" class="form-control" id="detail_pemilik" readonly />
                              </div>
                          </div>
                          <div class="form-group">
                              <label class=" col-sm-5"><h5>No Rekening</h5></label>
                              <div class="col-sm-7">
                                  <input type="text" class="form-control" id="detail_norek" readonly />
                              </div>
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class=" col-sm-5"><h5>No. Transaksi</h5></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="detail_no_transaksi" readonly />
                            </div>
                          </div>
                          <div class="form-group">
                              <label class=" col-sm-5"><h5>Catatan</h5></label>
                              <div class="col-sm-7">
                                  <textarea class="form-control" id="detail_catatan" readonly></textarea>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class=" col-sm-5"><h5>Bukti Transfer</h5></label>
                              <div class="col-sm-12">
                                <img id="bukti" style="max-height: 160px; max-width: 250px" alt="Bukti Transfer">
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn dark btn-outline" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('page-plugin-scripts')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery.number.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
@endpush

@push('page-scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.select2').select2({
                theme: "bootstrap"
            });
            $('#datetimepicker1').datetimepicker({
                locale: 'id'
            });
        });
        function popup_pembayaran(cicilan_id,jumlah) {
          $('#form_pembayaran').attr('action','{{ url('cicilan/pembayaran') }}/'+cicilan_id);
          $('#nominal').val(jumlah).attr('readonly','true');
          $('#nominal').number(true,2,',','.');
          $('#basic').modal('show');
        }
        function popup_detail(transaksi_id) {
          $.ajax({
            type: 'GET',
            url: '{{ url('pembayaran/detail') }}'+'/'+transaksi_id,
            success: function(result){
              if(result != ""){
                $('#detail_bank').val(result.akun_bank_nama_bank);
                $('#detail_nominal').val(numeral(result.jumlah).format('0,0'));
                $('#detail_no_transaksi').val(result.ref);
                $('#detail_pemilik').val(result.akun_bank_nama_akun);
                $('#detail_norek').val(result.akun_bank_no_akun);
                $('#detail_catatan').val(result.deskripsi);
                $('#tanggal_transfer').val(result.tanggal);
                $('#detail_nama_bank').val(result.nama_bank+' ('+result.nama_akun_bank+')');
                if(result.nama_file == null){
                  $('#bukti').css('display','none');
                }else{
                  $('#bukti').attr('src','{{ asset('storage/bukti_transfer_cicilan') }}'+'/'+result.nama_file);
                }
                $('#detail_modal').modal('show');
              }
            }
          })
        }
        function konfirmasi(status, id) {
            if(status){
                url = '{{ url('pinjaman/konfirmasi_setuju') }}'+'/'+id;
            }else{
                url = '{{ url('pinjaman/konfirmasi_tolak') }}'+'/'+id;
            }
            $.ajax({
                type: 'GET',
                url: url,
                success: function(result){
                    if(result.status){
                        swal({
                            text: result.msg,
                            title: "Terimakasih",
                            type: "success",
                            showCancelButton: true,
                            closeOnConfirm: false,
                            confirmButtonClass: "btn green btn-circle btn-outline"
                        },function (isConfirm) {
                            window.location.reload();
                        });
                    }
                }
            })
        }
    </script>
    @if (session()->has('success'))
      <script type="text/javascript">
        $(function () {
          swal({
              allowOutsideClick: false,
              allowEscapeKey: false,
              text: "Konfirmasi transfer telah disimpan",
              title: "Terimakasih",
              type: "success",
              confirmButtonClass: "btn green btn-circle btn-outline"
          });
        });
      </script>
    @endif
@endpush
