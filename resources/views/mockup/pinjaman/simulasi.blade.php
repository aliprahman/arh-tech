<table class="table table-striped table-bordered">
  <thead>
    <tr>
      <th>Cicilan Ke</th>
      <th>Jumlah</th>
      <th>Admin Fee</th>
      <th>Conf Fee</th>
      <td>Total</th>
    </tr>
  </thead>
  <tbody>
    @for ($i=0; $i < $pinjaman->waktu_pinjaman; $i++)
      <tr>
        <td>{{ $i+1 }}</td>
        <td>
          Rp. <span class="cicilan_text">{{ number_format(ceil($pinjaman->jumlah_pinjaman / $pinjaman->waktu_pinjaman),2,',','.') }}</span>
          <input type="hidden" class="cicilan" name="cicilan[]" value="{{ ceil($pinjaman->jumlah_pinjaman / $pinjaman->waktu_pinjaman) }}" />
        </td>
        <td>
          Rp. <span class="admin_fee_text">0</span>
          <input type="hidden" class="admin_fee" name="admin_fee[]" />
        </td>
        <td>
          Rp. <span class="conf_fee_text">0</span>
          <input type="hidden" class="conf_fee" name="conf_fee[]" />
        </td>
        <td>
          Rp. <span class="total_text">{{ number_format(ceil($pinjaman->jumlah_pinjaman / $pinjaman->waktu_pinjaman),2,',','.') }}</span>
          <input type="hidden" class="total" name="total[]" />
        </td>
      </tr>
    @endfor
  </tbody>
</table>
<input type="hidden" id="jumlah_pinjaman" name="jumlah_pinjaman" value="{{ $pinjaman->jumlah_pinjaman }}" />
<input type="hidden" id="waktu_pinjaman" name="waktu_pinjaman" value="{{ $pinjaman->waktu_pinjaman }}" />
<input type="hidden" id="cicilan_bulanan" name="cicilan_bulanan" value="{{ $pinjaman->jumlah_pinjaman / $pinjaman->waktu_pinjaman }}" />
