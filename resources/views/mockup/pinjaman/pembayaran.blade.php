@extends('layouts.app')

@push('page-plugin-styles')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .table thead tr th{
            white-space: pre-wrap;
            vertical-align: middle;
            text-align: center;
            padding-top: 20px;
            padding-bottom: 20px;
        }
        .table tbody tr td{
            vertical-align: middle;
        }
        #data th, #data td {
            font-size: 13px;
        }
    </style>
@endpush

@section('crumbs')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/dashboard') }}">Peminjam</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>History Transaksi</span>
        </li>
    </ul>
@endsection

@section('title',' History Transakasi')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-hourglass"></i>
                        <span class="caption-subject bold uppercase">HISTORY TRANSAKSI PEMBAYARAN</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-sm btn-default" href="javascript:;" id="print_pembayaran_pdf">
                            <i class="fa fa-print"></i> Print to PDF
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <center><h1>HISTORY TRANSAKSI PEMBAYARAN</h1></center><br>
                    <table class="table table-striped table-bordered table-hover" id="data">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Waktu Transaksi</th>
                            <th>Kode Transaksi</th>
                            <th>Nilai Transaksi</th>
                            <th>Kode Pinjaman</th>
                            <th>Tanggal Jatuh Tempo</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    <br>
                    <div class="clearfix margin-bottom-20"> </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="printModal" tabindex="-1" role="dialog" aria-labelledby="printModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="contactModalLabel">Print Preview</h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button id="printModal-print-btn" type="button" class="btn btn-primary"><i class="fa fa-print"></i> Print</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('page-plugin-scripts')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
@endpush

@push('page-scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#data').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ url('pinjaman/daftar/pembayaran') }}',
                    type: 'POST'
                },
                columns:[
                    {data: "DT_Row_Index", orderable: false, searchable: false, className: 'dt-center'},
                    {data: "tanggal", name: 'tanggal'},
                    {data: "ref", name: 'ref'},
                    {data: "jumlah", name: 'jumlah', className: 'dt-right'},
                    {data: "kode", name: 'kode'},
                    {data: "jatuh_tempo", name: 'jatuh_tempo'},
                    {data: "status", name: 'status'}
                ],
                rowCallback: function(row,data,index){
                    // nomer
                    $('td:eq(0)', row).html( '<center>'+data.DT_Row_Index+'</center>' );
                    // jumlah_pinjaman
                    $('td:eq(3)',row).html('Rp. '+numeral(data.jumlah).format('0,0'));
                    // kode pinjam
                    $('td:eq(4)',row).html('<a target="_blank" href="{{ url('pinjaman/rincian') }}/'+data.pinjaman_id+'">'+data.kode+'</a>');
                }
            });
            $('#print_pembayaran_pdf').click(function () {
                var iframeHeight = $(window).height() - 220;

                $('#printModal .modal-body').html('<iframe id="printModal-iframe" src="{{url("pinjaman/daftar/pembayaran/pdf") }}" style="width: 100%; height: ' + iframeHeight + 'px; border: 1px solid #e5e5e5;background-image: url({{asset('assets/global/img/input-spinner.gif')}}); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>');
                $('#printModal-print-btn').attr('disabled', 'disabled');
                $('#printModal-iframe').on('load', function () {
                    $('#printModal-print-btn').removeAttr('disabled');
                });
                $('#printModal').modal('show');
            });
            $('#printModal-print-btn').on('click', function () {
                $('#printModal-iframe').get(0).contentWindow.print();
            });
        });
    </script>
@endpush
