@extends('layouts.app')

@section('title')
    Dashboard
@endsection

@section('crumbs')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <span>
                @if(auth()->user()->type == 'finnesia')
                    Admin
                @else
                    {{ ucfirst(auth()->user()->type) }}
                @endif
            </span>
            <i class="fa fa-circle"></i>
        </li>
        <li class="active">
            <a href="{{ url('/dashboard') }}">
                <span>Dashboard</span>
            </a>
        </li>
    </ul>
@endsection

@section('content')
    @if(auth()->user()->type == 'peminjam')
        <div class="row">
            <div class="col-md-4">
                <!-- BEGIN WIDGET THUMB -->
                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                    <h4 class="widget-thumb-heading">Jumlah Sisa Pinjaman</h4>
                    <div class="widget-thumb-wrap">
                        <i class="widget-thumb-icon bg-green icon-notebook"></i>
                        <div class="widget-thumb-body">
                            <span class="widget-thumb-subtitle">Rp.</span>
                            <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{ $sisa_pinjaman }}" style="font-size: 20px;">0</span>
                        </div>
                    </div>
                </div>
                <!-- END WIDGET THUMB -->
            </div>
            <div class="col-md-4">
                <!-- BEGIN WIDGET THUMB -->
                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                    <h4 class="widget-thumb-heading">Jumlah Pinjaman</h4>
                    <div class="widget-thumb-wrap">
                        <i class="widget-thumb-icon bg-blue  icon-diamond"></i>
                        <div class="widget-thumb-body">
                            <span class="widget-thumb-subtitle">Rp.</span>
                            <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{ $jumlah_pinjaman }}" style="font-size: 20px;">0</span>
                        </div>
                    </div>
                </div>
                <!-- END WIDGET THUMB -->
            </div>
            <div class="col-md-4">
                <!-- BEGIN WIDGET THUMB -->
                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                    <h4 class="widget-thumb-heading">Cicilan Bulan Ini&nbsp;&nbsp;&nbsp;(Lunas)</h4>
                    <div class="widget-thumb-wrap">
                        <i class="widget-thumb-icon bg-red icon-calendar"></i>
                        <div class="widget-thumb-body">
                            <span class="widget-thumb-subtitle">Rp.</span>
                            <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{ $cicilan }}" style="font-size: 20px;">0</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="portlet light portlet-fit ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-microphone font-dark hide"></i>
                            <span class="caption-subject bold font-dark uppercase">Resume History Transaksi</span>
                            <span class="caption-helper"></span>
                        </div>
                        <div class="actions">
                            <a href="{{ url('/pinjaman') }}" class="btn green btn-outline btn-circle" href="javascript:;">
                                <i class="fa fa-search"></i> Detail
                            </a>
                        </div>
                    </div>

                    <div class="portlet-body widget-thumb widget-bg-color-white">
                        <table class="table">
                            <tr>
                                <td>Total Pengajuan Terdaftar</td>
                                <td>:</td>
                                <td class="widget-thumb-heading">{{ $pengajuan_terdaftar }}</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Total Nominal Pinjaman Terdaftar</td>
                                <td>:</td>
                                <td class="widget-thumb-heading">Rp.{{ number_format($nominal_terdaftar) }}</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Total Pinjaman Disetujui</td>
                                <td>:</td>
                                <td class="widget-thumb-heading">{{ $pengajuan_disetujui }}</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Total Nominal Pinjaman Disetujui</td>
                                <td>:</td>
                                <td class="widget-thumb-heading">Rp. {{ number_format($nominal_disetujui) }}</td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="portlet light portlet-fit ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-microphone font-dark hide"></i>
                            <span class="caption-subject bold font-dark uppercase">Resume Status Pinjaman</span>
                            <span class="caption-helper"></span>
                        </div>
                    </div>

                    <div class="portlet-body widget-thumb widget-bg-color-white">
                        <table class="table">
                            <tr>
                                <td>Total Jumlah Pinjaman</td>
                                <td>:</td>
                                <td class="widget-thumb-heading">Rp. {{ number_format($jumlah_pinjaman) }},-</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Total Jumlah Sisa Pinjaman</td>
                                <td>:</td>
                                <td class="widget-thumb-heading">Rp. {{ number_format($sisa_pinjaman) }},-</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Total Jumlah Cicilan Bulanan</td>
                                <td>:</td>
                                <td class="widget-thumb-heading">Rp. {{ number_format($cicilan) }},-</td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @if($pop_up)
            @include('mockup.popup_data_pelengkap')
        @endif
        {{-- end dashboard peminjam --}}
    @elseif(auth()->user()->type == 'pemodal')
      <div class="row">
          <div class="col-md-3">
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                <div class="widget-thumb-wrap">
                    <div class="widget-thumb-body">
                      <center>
                        <h4>{{ number_format($saldo+$pendanaan+$pendapatan,0,',','.') }}</h4>
                        <b>Total Nilai Akun</b>
                      </center>
                    </div>
                </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20">
              <div class="widget-thumb-wrap">
                <div class="widget-thumb-body">
                  <center>
                    <h4>{{ number_format($saldo,0,',','.') }}</h4>
                    <b>Saldo Deposit</b>
                  </center>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20">
              <div class="widget-thumb-wrap">
                <div class="widget-thumb-body">
                  <center>
                    <h4>{{ number_format($pendanaan,0,',','.') }}</h4>
                    <b>Pendanaan</b>
                  </center>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20">
              <div class="widget-thumb-wrap">
                <div class="widget-thumb-body">
                  <center>
                    <h4>{{ number_format($pendapatan,0,',','.') }}</h4>
                    <b>Pendapatan</b>
                  </center>
                </div>
              </div>
            </div>
          </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <canvas id="chartdiv" width="100%" height="30"></canvas>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <h3>Resume History Transaksi</h3>
          <div class="widget-thumb widget-bg-color-white margin-bottom-20">
            <div class="widget-thumb-body">
              <table style="width: 100%;" class="table">
                <tr>
                  <td style="width: 30%">Total Pendapatan</td>
                  <th>
                    : Rp. {{ number_format($pendapatan,0,',','.') }}
                  </th>
                </tr>
                <tr>
                  <td>Total Pendanaan</td>
                  <th>
                    : Rp. {{ number_format($pendanaan,0,',','.') }}
                  </th>
                </tr>
              </table>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <h3>Transaksi Terakhir</h3>
          <div class="widget-thumb widget-bg-color-white margin-bottom-20">
            <div class="widget-thumb-body">
              <table style="width: 100%;" class="table">
                <tr>
                  <td style="width: 20%">Deposit</td>
                  <th>
                    : Rp.
                    @if (isset($deposit_terakhir->jumlah))
                      {{ number_format($deposit_terakhir->jumlah,0,',','.') }}
                    @else
                      0
                    @endif
                    <a href="{{ url('pemodal/deposit') }}" class="btn btn-xs btn-default btn-circle" style="float: right">Detail</a>
                  </th>
                </tr>
                <tr>
                  <td>Pendanaan</td>
                  <th>
                    : Rp.
                    @if (isset($pendanaan_terakhir->jumlah))
                      {{ number_format($pendanaan_terakhir->jumlah,0,',','.') }}
                    @else
                      0
                    @endif
                    <a href="{{ url('pemodal/cari') }}" class="btn btn-xs btn-default btn-circle" style="float: right">Detail</a>
                  </th>
                </tr>
                <tr>
                  <td>Penarikan</td>
                  <th>
                    : Rp.
                    @if (isset($penarikan_terakhir->jumlah))
                      {{ number_format($penarikan_terakhir->jumlah,0,',','.') }}
                    @else
                      0
                    @endif
                    <a href="{{ url('pemodal/penarikan') }}" class="btn btn-xs btn-default btn-circle" style="float: right">Detail</a>
                  </th>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    @else
      <div class="row">
        <div class="col-lg-3">
          <div class="widget-thumb widget-bg-color-white margin-bottom-20">
            <div class="widget-thumb-body">
              <table style="width: 100%;" class="table">
                <thead>
                  <tr>
                    <th colspan="2">Transaksi Pinjaman</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td style="width: 50%">Submit</td>
                    <th>: {{ $pinjaman_submit }}</th>
                  </tr>
                  <tr>
                    <td>Reject</td>
                    <th>: {{ $pinjaman_reject }}</th>
                  </tr>
                  <tr>
                    <td>Analysis</td>
                    <th>: {{ $pinjaman_analysis }}</th>
                  </tr>
                  <tr>
                    <td>Listing</td>
                    <th>: {{ $pinjaman_listing }}</th>
                  </tr>
                  <tr>
                    <td>Funding</td>
                    <th>: {{ $pinjaman_funding }}</th>
                  </tr>
                  <tr>
                    <td>Installment</td>
                    <th>: {{ $pinjaman_installment }}</th>
                  </tr>
                  <tr>
                    <td>Paid</td>
                    <th>: {{ $pinjaman_paid }}</th>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="widget-thumb widget-bg-color-white margin-bottom-20">
            <div class="widget-thumb-body">
              <table style="width: 100%;" class="table">
                <thead>
                  <tr>
                    <th colspan="2">Transaksi Deposit</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td style="width: 50%">Unverified</td>
                    <th>: {{ $deposit_unverified }}</th>
                  </tr>
                  <tr>
                    <td>Proses</td>
                    <th>: {{ $deposit_proses }}</th>
                  </tr>
                  <tr>
                    <td>Verified</td>
                    <th>: {{ $deposit_verified }}</th>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="widget-thumb widget-bg-color-white margin-bottom-20">
            <div class="widget-thumb-body">
              <table style="width: 100%;" class="table">
                <thead>
                  <tr>
                    <th colspan="2">Transaksi Penarikan</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td style="width: 50%">Unverified</td>
                    <th>: {{ $withdraw_unverified }}</th>
                  </tr>
                  <tr>
                    <td>Verified</td>
                    <th>: {{ $withdraw_verified }}</th>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="widget-thumb widget-bg-color-white margin-bottom-20">
            <div class="widget-thumb-body">
              <table style="width: 100%;" class="table">
                <thead>
                  <tr>
                    <th colspan="2">Transaksi Pendanaan</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td style="width: 50%">Jumlah</td>
                    <th>: {{ $pendanaan_jumlah }}</th>
                  </tr>
                  <tr>
                    <td>Total</td>
                    <th>: {{ number_format($pendanaan_total,0,',','.') }}</th>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    @endif

@endsection

@push('page-plugin-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>
    <script src="{{ asset('/assets/global/plugins/flot/jquery.flot.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/global/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/global/plugins/morris/raphael-min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/common.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery.number.min.js') }}" type="text/javascript"></script>
@endpush

@push('page-scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#basic').modal('show');
            $('.rupiah').number(true,2,',','.');
        });
    </script>
    @if (auth()->user()->type == 'pemodal')
      <script type="text/javascript">
        $(document).ready(function(){
          var ctx = document.getElementById("chartdiv");
          var myChart = new Chart(ctx, {
              type: 'line',
              data: {
                  labels: {!! json_encode($chart_label) !!},
                  datasets: [{
                      label: 'Grafik Pendapatan 1 Tahun Terakhir',
                      data: {!! json_encode($chart_data) !!},
                      fill: false,
                      borderColor: 'rgba(0,255,255)'
                  }]
              },
          });
        })
      </script>
    @endif
@endpush
