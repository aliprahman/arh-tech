@extends('layouts.app')

@push('page-plugin-styles')
<link href="{{ asset('/assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css">
<style type="text/css">
  .select2 {
   width:100%!important;
   }
</style>
@endpush

@section('title')
Biodata
@endsection
@section('crumbs')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/peminjam/dashboard') }}">Peminjam</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Biodata Peminjaman</span>
        </li>
    </ul>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session('success') != "")
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('success') }}
                </div>
            @endif
        </div>
    </div>

@include('mockup.peminjam.main-profile')
@include('mockup.popup_data_pelengkap')

@endsection

@push('page-plugin-scripts')
<script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/form-wizard.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/common.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.number.min.js') }}" type="text/javascript"></script>
@endpush

@push('page-scripts')
<script src="{{ asset('/assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('a[data-toggle="tab"]').click(function() {
            $('#tab-title').html($(this).html());
        });

        $('.select2').select2({
            theme: "bootstrap"
        });
        $('.rupiah').number(true,2,',','.');
    });
    function add_row(table) {
        var total_row = $('#'+table+' tr').length;
        var row = "<tr id='"+table+"_"+total_row+"'>"+
                    "<td>"+
                        "<textarea class='form-control' name='"+table+"_keterangan[]' style='height:34px;'></textarea>"+
                    "</td>"+
                    "<td>"+
                        "<input type='text' class='form-control rupiah' name='"+table+"_jumlah[]'></textarea>"+
                    "</td>"+
                    "<td>"+
                        "<button type='button' class='btn btn-danger' onclick='remove_row(\""+table+"\","+total_row+")'>Hapus</button>"+
                    "</td>"+
                "</tr>";

        $('#'+table+' tr:last').after(row);
        $('.rupiah').number(true,2,',','.');
    }
    function remove_row(table,index) {
        $("#"+table+"_"+index).remove();
    }
</script>
@endpush
