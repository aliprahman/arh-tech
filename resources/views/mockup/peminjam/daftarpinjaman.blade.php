@extends('layouts.app')

@push('page-plugin-styles')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
  .table thead tr th{
    white-space: pre-wrap;
    vertical-align: middle;
    text-align: center;
    padding-top: 20px;
    padding-bottom: 20px;
  }
  .table tbody tr td{
    vertical-align: middle;
  }

  #data th, #data td {
    font-size: 13px;
  }
</style>
@endpush

@section('title')
Daftar Peminjaman
@endsection
@section('crumbs')
  <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/peminjam/dashboard') }}">Peminjam</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Daftar Peminjaman</span>
        </li>
    </ul>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session('success') != "")
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('success') }}
                </div>
            @endif
        </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption">
                    <a href="{{ url('/peminjam/pinjaman') }}" class="btn green btn-outline btn-circle btn-sm btn-sm"><i class="fa fa-edit"></i> Ajukan Pinjaman Baru</a>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-sm btn-default" href="javascript:;">
                        <i class="fa fa-print"></i> Print to PDF
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <center><h1>Daftar Pinjaman</h1></center>
                <table class="table table-striped table-bordered table-hover" id="data">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Tanggal Pinjam</th>
                            <th>Kode Pinjaman</th>
                            <th>Nilai Pinjaman</th>
                            <th>Waktu</th>
                            <th>Status Pengajuan Pinjaman</th>
                            <th>Status Cicilan</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
                <br>
                <div class="clearfix margin-bottom-20"> </div>
            </div>
          </div>
        </div>
     </div>
    </div>
@endsection

@push('page-plugin-scripts')
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
@endpush

@push('page-scripts')
<script type="text/javascript">
$(document).ready(function() {
    $('#data').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{ url('peminjam/daftar/pinjaman') }}',
            type: 'POST'
        },
        columns: [
            {data: "DT_Row_Index", orderable: false, searchable: false},
            {data: "tanggal_submit", name: 'tanggal_submit'},
            {data: "kode", name: 'kode'},
            {data: "jumlah_pinjaman", name: 'jumlah_pinjaman', className: 'dt-right'},
            {data: "waktu_pinjaman", name: 'waktu_pinjaman'},
            {data: "status", name: "status"},
            {data: "status_cicilan", name: "status_cicilan"},
            {data: "action", orderable: false, searchable: false},
        ],
        rowCallback: function(row,data,index){
            // nomer
            $('td:eq(0)', row).html( '<center>'+data.DT_Row_Index+'</center>' );
            // tanggal_pinjaman
            $('td:eq(1)',row).html(moment(data.tanggal_submit).format("DD-MM-YYYY"));
            // jumlah_pinjaman
            $('td:eq(3)',row).html(numeral(data.jumlah_pinjaman).format('0,0'));
            // action
            var action_link = '<a href="'+$('meta[name="base_url"]').attr('content')+'/peminjam/pinjaman/rincian/'+data.id+'" class="btn btn-circle btn-sm green btn-outline"><i class="fa fa-search"></i>Lihat Rincian</a>'
            $('td:eq(7)', row).html( action_link );
        }
    });
} );
</script>
@endpush
