@extends('layouts.finnesia')

@push('page-plugin-styles')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
@endpush

@section('title')
INFORMASI PEMINJAM
@endsection

@section('content')
@if(auth::guest())
<div class="row">
    <div class="col-md-12">
      <div class="portlet light portlet-fit">
	        <div class="portlet-title">
	            <div class="caption">
	            	<i class="icon-user"></i>
	                <span class="caption-subject bold font-dark uppercase ">Daftar Sebagai Peminjam<h6>(Silahkan isi data dibawah ini sesuai dengan profile Anda.)</h6></span>
	            </div>
	        </div>
	        <form class="form-horizontal" action="{{ url('register/peminjam') }}" method="post">
          {{ csrf_field() }}
	        <div class="portlet-body">
            <div class="row">
              <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
              </div>
            </div>
	        	<div class="row">
	        		<div class="col-md-6">
	        			<div class="portlet light portlet-fit">
	        				<div class="portlet-title">
	        					<strong>Informasi Data Diri</strong>
	        				</div>
	        				<div class="portlet-body">
					        		<div class="form-group">
					        			<label class="col-sm-4">Nama</label>
					        			<div class="col-sm-8">
					        				<input type="text" name="nama" class="form-control" value="{{ old('nama') }}" required>
					        			</div>
					        		</div>
					        		<div class="form-group">
					        			<label class="col-sm-4">Tempat/Tanggal Lahir</label>
					        			<div class="col-sm-4">
											<select class="form-control select2" name="tempat_lahir" required>
												<option value=""></option>
												@foreach ($list_kota as $key => $value)
													@if(old('tempat_lahir') != "" && old('tempat_lahir') == $value)
														<option value="{{ $value }}" selected>{{ $value }}</option>
													@else
														<option value="{{ $value }}">{{ $value }}</option>
													@endif
												@endforeach
											</select>
					        			</div>
					        			<div class="col-sm-4">
					        				<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
			                        <input type="text" name="tanggal_lahir" class="form-control col-sm-4" value="{{ old('tanggal_lahir') }}" placeholder="02-03-2012" required>
			                        <span class="input-group-btn">
			                            <button class="btn default" type="button">
			                                 <i class="fa fa-calendar"></i>
			                            </button>
			                        </span>
			                    </div>
					        			</div>
					        		</div>
					        		<div class="form-group">
					        			<label class="col-sm-4">Alamat</label>
					        				<div class="col-sm-8">
					        					<input type="text" name="alamat" class="form-control" value="{{ old('alamat') }}" required>
					        				</div>
					        		</div>
					        		<div class="form-group">
					        			<div class="col-sm-4 col-sm-offset-4">
					        				<select class="form-control select2" id="prov_1" name="prov_1" onchange="set_kota('prov_1','kota_1','Pilih Kab/Kota')" required>
					        					<option value="">Pilih Provinsi</option>
                            @foreach ($list_provinsi as $key => $value)
                              @if(old('prov_1') != "" && old('prov_1') == $value)
                                <option value="{{ $value }}" selected>{{ $key }}</option>
                              @else
                                <option value="{{ $value }}">{{ $key }}</option>
                              @endif
                            @endforeach
					        				</select>
					        			</div>
					        			<div class="col-sm-4">
					        				<select class="form-control select2" id="kota_1" name="kota_1" onchange="set_kecamatan('kota_1','kec_1','Pilih Kecamatan')" required>
					        					<option value="">Pilih Kab/Kota</option>
                            @if(old('kota_1') != "")
                              @php
                                $kota = app()->make('App\Http\Controllers\Ajax')->set_list_kota(old('prov_1'));
                                foreach ($kota as $key) {
                                    if(old('kota_1') == $key['id']){
                                        echo '<option value="'.$key['id'].'" selected>'.$key['nama'].'</option>';
                                    }else{
                                        echo '<option value="'.$key['id'].'">'.$key['nama'].'</option>';
                                    }
                                }
                              @endphp
                            @endif
					        				</select>
					        			</div>
					        		</div>
					        		<div class="form-group">
					        			<div class="col-sm-4 col-sm-offset-4">
					        				<select class="form-control select2" id="kec_1" name="kec_1" onchange="set_kelurahan('kec_1','kel_1','Pilih Kelurahan')" required>
					        					<option value="">Pilih Kecamatan</option>
                            @if(old('kec_1') != "")
                              @php
                                $kecamatan = app()->make('App\Http\Controllers\Ajax')->set_list_kecamatan(old('kota_1'));
                                foreach ($kecamatan as $key) {
                                    if(old('kec_1') == $key['id']){
                                        echo '<option value="'.$key['id'].'" selected>'.$key['nama'].'</option>';
                                    }else{
                                        echo '<option value="'.$key['id'].'">'.$key['nama'].'</option>';
                                    }
                                }
                              @endphp
                            @endif
					        				</select>
					        			</div>
					        			<div class="col-sm-4">
					        				<select class="form-control select2" id="kel_1" name="kel_1">
					        					<option value="">Pilih Kelurahan</option>
                            @if(old('kel_1') != "")
                              @php
                                $kecamatan = app()->make('App\Http\Controllers\Ajax')->set_list_kelurahan(old('kec_1'));
                                foreach ($kecamatan as $key) {
                                    if(old('kel_1') == $key['id']){
                                        echo '<option value="'.$key['id'].'" selected>'.$key['nama'].'</option>';
                                    }else{
                                        echo '<option value="'.$key['id'].'">'.$key['nama'].'</option>';
                                    }
                                }
                              @endphp
                            @endif
					        				</select>
					        			</div>
					        		</div>
					        		<div class="form-group">
					        			<div class="col-sm-4 col-sm-offset-4">
					        				<input type="text" name="kode_pos_1" class="form-control" placeholder="Kode Pos" value="{{ old('kode_pos_1') }}">
					        			</div>
					        		</div>
					        		<div class="form-group">
					        			<label class="col-sm-4">Email</label>
					        			<div class="col-sm-8">
					        				<input type="email" name="email" class="form-control" value="{{ old('email') }}" autocomplete="off" required>
					        			</div>
					        		</div>
					        		<div class="form-group">
			                  <label class="control-lable col-sm-4">Telepon</label>
			                  <div class="col-sm-4">
			                    <div class="input-group">
			                        <input type="text" class="form-control" name="handphone" value="{{ old('handphone') }}" required>
			                        <span class="input-group-addon">
			                            <i class="fa fa-mobile-phone"></i>
			                        </span>
			                    </div>
			                  </div>
			                  <div class="col-sm-4">
			                      <div class="input-group">
			                          <input type="text" class="form-control" name="telephone" value="{{ old('telephone') }}">
			                          <span class="input-group-addon">
			                              <i class="fa fa-phone"></i>
			                          </span>
			                      </div>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label class="col-sm-4">ID KTP</label>
			                     <div class="col-sm-8">
			                        <input type="text" name="ktp" class="form-control" value="{{ old('ktp') }}" required>
			                     </div>
			                </div>
			                <div class="form-group">
			                  <label class="col-sm-4">ID NPWP</label>
			                  <div class="col-sm-8">
			                     <input type="text" name="npwp" class="form-control" value="{{ old('npwp') }}">
			                  </div>
			                </div>
			                <div class="form-group">
					        			<label class="col-sm-4">Jenis Kelamin</label>
					        			<div class="col-sm-8">
						        		  <div class="mt-radio-inline">
						                 <label class="mt-radio">
						                     <input type="radio" name="jenis_kelamin" value="laki-laki" @if(old('jenis_kelamin') != "" && old('jenis_kelamin') == 'laki-laki') checked @endif required> Laki-Laki
						                     <span></span>
						                 </label>
						                 <label class="mt-radio">
						                     <input type="radio" name="jenis_kelamin" value="perempuan" @if(old('jenis_kelamin') != "" && old('jenis_kelamin') == 'perempuan') checked @endif required> Perempuan
						                     <span></span>
						                 </label>
						              </div>
						            </div>
					        		</div>
                      <div class="form-group">
			                  <label class="col-sm-4">Password</label>
			                     <div class="col-sm-8">
			                        <input type="password" name="password" class="form-control" required>
			                     </div>
			                </div>
			                <div class="form-group">
			                  <label class="col-sm-4">Confirm Password</label>
			                  <div class="col-sm-8">
			                     <input type="password" name="password_confirmation" class="form-control" required>
			                  </div>
			                </div>
	        				</div>
	        			</div>
	        		</div>
	        		<div class="col-md-6">
                <div class="portlet light portlet-fit" style="margin-bottom:0">
					        <div class="portlet-title">
	        					<strong>Informasi Bank</strong>
	        				</div>
	        				<div class="portlet-body">
		        					<div class="form-group">
		        						<label class="col-sm-4">Bank</label>
		        						<div class="col-sm-8">
					        				<select class="form-control select2" name="bank" required>
					        					<option value="">Pilih Bank</option>
                            @foreach ($list_bank as $key => $value)
                                @if(old('bank') != "" && old('bank') == $value)
                                  <option value="{{ $value }}" selected>{{ $key }}</option>
                                @else
                                  <option value="{{ $value }}">{{ $key }}</option>
                                @endif
                            @endforeach
					        				</select>
					        			</div>
		        					</div>
		        					<div class="form-group">
		        						<label class="col-sm-4">Nama Akun Bank</label>
		        						<div class="col-sm-8">
		        							<input type="text" name="nama_akun" class="form-control" value="{{ old('nama_akun') }}" required>
		        						</div>
		        					</div>
		        					<div class="form-group">
		        						<label class="col-sm-4">Nomor Akun Bank</label>
		        						<div class="col-sm-8">
		        							<input type="text" name="no_rekening" class="form-control" value="{{ old('no_rekening') }}" required>
		        						</div>
		        					</div>
		        			</div>
		        		</div>
	        			<div class="portlet light portlet-fit">
		        		<div class="portlet-title">
	        						<strong>Informasi Sekolah / Universitas</strong>
	        				</div>
	        				<div class="portlet-body">
	        						<div class="form-group">
		        						<label class="col-sm-4">Nama Sekolah / Universitas</label>
		        						<div class="col-sm-8">
		        							<input type="text" name="nama_sekolah" class="form-control" value="{{ old('nama_sekolah') }}" required>
		        						</div>
		        					</div>
		        					<div class="form-group">
		        						<label class="col-sm-4">Kategori Sekolah / Universitas</label>
		        						<div class="col-sm-8">
					        				<select class="form-control select2" name="kategori_sekolah">
					        					<option value="">Pilih Kategori</option>
                            <option value="negeri" @if(old('kategori_sekolah') != "" && old('kategori_sekolah') == 'negeri') selected @endif>Negeri</option>
                            <option value="swasta" @if(old('kategori_sekolah') != "" && old('kategori_sekolah') == 'swasta') selected @endif>Swasta</option>
					        				</select>
					        			</div>
		        					</div>
                      <div class="form-group">
			                  <label class="control-lable col-sm-4">Telepon Sekolah</label>
			                  <div class="col-sm-8">
			                      <div class="input-group">
			                          <input type="text" class="form-control" name="telephone_sekolah" value="{{ old('telephone_sekolah') }}">
			                          <span class="input-group-addon">
			                              <i class="fa fa-phone"></i>
			                          </span>
			                      </div>
			                  </div>
			                </div>
		        					<div class="form-group">
					        			<label class="col-sm-4">Alamat</label>
					        				<div class="col-sm-8">
					        					<input type="text" name="alamat_sekolah" class="form-control" value="{{ old('alamat_sekolah') }}" required>
					        				</div>
					        		</div>
					        		<div class="form-group">
					        			<div class="col-sm-4 col-sm-offset-4">
					        				<select class="form-control select2" id="prov_2" name="prov_2" onchange="set_kota('prov_2','kota_2','Pilih Kab/Kota')" required>
					        					<option value="">Pilih Provinsi</option>
                            @foreach ($list_provinsi as $key => $value)
                              @if(old('prov_2') != "" && old('prov_2') == $value)
                                <option value="{{ $value }}" selected>{{ $key }}</option>
                              @else
                                <option value="{{ $value }}">{{ $key }}</option>
                              @endif
                            @endforeach
					        				</select>
					        			</div>
					        			<div class="col-sm-4">
					        				<select class="form-control select2" id="kota_2" name="kota_2" onchange="set_kecamatan('kota_2','kec_2','Pilih Kecamatan')" required>
					        					<option value="">Pilih Kab/Kota</option>
                            @if(old('kota_2') != "")
                              @php
                                $kota = app()->make('App\Http\Controllers\Ajax')->set_list_kota(old('prov_2'));
                                foreach ($kota as $key) {
                                    if(old('kota_2') == $key['id']){
                                        echo '<option value="'.$key['id'].'" selected>'.$key['nama'].'</option>';
                                    }else{
                                        echo '<option value="'.$key['id'].'">'.$key['nama'].'</option>';
                                    }
                                }
                              @endphp
                            @endif
					        				</select>
					        			</div>
					        		</div>
					        		<div class="form-group">
					        			<div class="col-sm-4 col-sm-offset-4">
					        				<select class="form-control select2" id="kec_2" name="kec_2" onchange="set_kelurahan('kec_2','kel_2','Pilih Kelurahan')" required>
					        					<option value="">Pilih Kecamatan</option>
                            @if(old('kec_2') != "")
                              @php
                                $kecamatan = app()->make('App\Http\Controllers\Ajax')->set_list_kecamatan(old('kota_2'));
                                foreach ($kecamatan as $key) {
                                    if(old('kec_2') == $key['id']){
                                        echo '<option value="'.$key['id'].'" selected>'.$key['nama'].'</option>';
                                    }else{
                                        echo '<option value="'.$key['id'].'">'.$key['nama'].'</option>';
                                    }
                                }
                              @endphp
                            @endif
					        				</select>
					        			</div>
					        			<div class="col-sm-4">
					        				<select class="form-control select2" id="kel_2" name="kel_2" required>
					        					<option value="">Pilih Kelurahan</option>
                            @if(old('kel_2') != "")
                              @php
                                $kelurahan = app()->make('App\Http\Controllers\Ajax')->set_list_kelurahan(old('kec_2'));
                                foreach ($kelurahan as $key) {
                                    if(old('kel_2') == $key['id']){
                                        echo '<option value="'.$key['id'].'" selected>'.$key['nama'].'</option>';
                                    }else{
                                        echo '<option value="'.$key['id'].'">'.$key['nama'].'</option>';
                                    }
                                }
                              @endphp
                            @endif
					        				</select>
					        			</div>
					        		</div>
					        		<div class="form-group">
					        			<div class="col-sm-4 col-sm-offset-4">
					        				<input type="text" name="kode_pos_2" class="form-control" placeholder="Kode Pos" value="{{ old('kode_pos_2') }}">
					        			</div>
					        		</div>

		        					<hr>
		        					<br>
		        					<center>
  		        					<div class="form-group">
                          <div class="g-recaptcha" data-sitekey="{{ env('CAPTCHA_SITEKEY') }}"></div>
                        </div>
  		                  <br>
  		                  <div class="form-group">
  		                      <label class="mt-checkbox">
  					                  <a href="https://finnesia.com/syarat-ketentuan" target="blank">Baca Syarat dan Ketentuan Umum</a>
  					                </label>
  		                  </div>
                        <div class="form-group">
  		                      <label class="mt-checkbox">
  					                  <input type="checkbox" id="setuju" value="setuju" name="setuju" required=""> Saya telah membaca dan menyetujui ketentuan yang berlaku.
  					                  <span></span>
  					                </label>
  		                  </div>
  		                  <div class="form-group">
  		                      <button type="submit" class="btn green btn-outline btn-circle"><i class="icon-users"></i> Daftar</button>
  		                  </div>
		        					</center>
	        				</div>
	        			</div>
	        		</div>
	        	</div>
	        </div>
	    </form>
    </div>
    </div>
</div>
@else
<script type="text/javascript">
	window.location = "{{ url('/404') }}";
</script>
@endif
@endsection
@push('page-plugin-scripts')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/common.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
@endpush

@push('page-scripts')
<script src="{{ asset('assets/pages/scripts/table-datatables-managed.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#data').DataTable();
    $('.select2').select2();
    window.setTimeout(function() { $(".alert").fadeOut(); }, 10000);
});
</script>
@if(session('success') != "")
	<script>
        $(document).ready(function () {
            swal({
                text:"Silahkan cek email untuk aktivasi akun ",
                title: "Terimakasih, telah mendaftar",
                type: "success",
                confirmButtonClass: "btn green btn-circle btn-outline"
            });
        })
	</script>
@endif
@endpush
