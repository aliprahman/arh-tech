<form class="form-horizontal" method="POST" action="{{ url('peminjam/biodata') }}" enctype="multipart/form-data">
{{ csrf_field() }}

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold font-dark uppercase">Informasi Data Diri Peminjam</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="portlet light portlet-fit">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <span class="caption-subject bold font-dark uppercase">Biodata</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="form-group">
                                        <h5><label class="control-lable col-sm-4">Nama</label></h5>
                                        <div class="col-sm-8">
                                            <input class="form-control" name="nama" type="text" value="{{ $user->name }}" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5><label class="control-lable col-sm-4">Tempat Lahir</label></h5>
                                        <div class="col-sm-8">
                                            <input class="form-control" name="tempat_lahir" type="text" value="{{ $user->biodata->tempat_lahir }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5><label class="control-lable col-sm-4">Tanggal Lahir</label></h5>
                                        <div class="col-sm-8">
                                            <input class="form-control form-control date-picker" name="tanggal_lahir" size="16" type="text" value="{{ $user->biodata->tanggal_lahir }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5><label class="control-lable col-sm-4">Jenis Kelamin</label></h5>
                                        <div class="col-sm-8">
                                             <div class="mt-radio-inline">
                                                <label class="mt-radio">
                                                    <input type="radio" name="jenis_kelamin" value="laki-Laki" @if($user->biodata->jenis_kelamin == 'laki-laki') checked @endif> Laki-Laki
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="jenis_kelamin" value="perempuan" @if($user->biodata->jenis_kelamin == 'perempuan') checked @endif> Perempuan
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5><label class="control-lable col-sm-4">Email</label></h5>
                                        <div class="col-sm-8">
                                            <input class="form-control" name="email" type="email" value="{{ $user->email }}" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5><label class="control-lable col-sm-4">Telepon</label></h5>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="no_hp" value="{{ $user->biodata->no_hp }}">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-mobile-phone"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="no_telepon" value="{{ $user->biodata->no_telepon }}">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-phone"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5><label class="control-lable col-sm-4">KTP</label></h5>
                                        <div class="col-sm-8">
                                            <input class="form-control" name="no_ktp" type="text" value="{{ $user->biodata->no_ktp }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5><label class="control-lable col-sm-4">NPWP</label></h5>
                                        <div class="col-sm-8">
                                            <input class="form-control" name="no_npwp" type="text" value="{{ $user->biodata->no_npwp }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <span class="btn btn-circle btn-default btn-file">
                                            <span class="fileinput-new"><i class="fa fa-cloud-upload"></i>Foto Profile </span>
                                            <span class="fileinput-exists"> </span>
                                            <input type="hidden"><input type="file" name="photo_profile" onchange="preview_image(event,'photo_profile')">
                                            </span>
                                        </div>
                                        <div class="col-sm-8">
                                            @if($user->photo_profile_id != "")
                                                <img src="{{ asset('storage/photo_profile/'.$user->file_uploads->name) }}" class="preview" id="photo_profile" />
                                            @else
                                                <img class="preview" id="photo_profile" />
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <span class="btn btn-circle btn-default btn-file">
                                            <span class="fileinput-new"><i class="fa fa-cloud-upload"></i>Curiculum Vitae</span>
                                            <span class="fileinput-exists"> </span>
                                            <input type="hidden"><input type="file" name="curiculum_vitae" onchange="preview_image(event,'curiculum_vitae')">
                                            </span>
                                        </div>
                                        <div class="col-sm-8">
                                            @if($user->peminjam->photo_curiculum_vitae_id != "")
                                                <img src="{{ asset('storage/curiculum_vitae/'.$user->peminjam->photo_curiculum_vitae->name) }}" class="preview" id="curiculum_vitae" />
                                            @else
                                                <img class="preview" id="curiculum_vitae" />
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portlet light portlet-fit">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <span class="caption-subject bold font-dark uppercase">&nbsp;</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="form-group">
                                        <h5><label class="control-lable col-sm-4">Alamat Rumah</label></h5>
                                        <div class="col-sm-8">
                                            <textarea class="form-control" cols="35" name="alamat" rows="3">{{ $user->peminjam->alamat->alamat }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5><label class="control-lable col-sm-4">Provinsi</label></h5>
                                        <div class="col-sm-8">
                                            <select class="form-control select2" name="prov_1" id="prov_1" onchange="set_kota('prov_1','kota_1','Pilih Kab/Kota')">
                                                <option value="">-- Pilih --</option>
                                                @foreach ($list_provinsi as $key => $value)
                                                    @if($user->peminjam->alamat->province_id == $value)
                                                        <option value="{{ $value }}" selected>{{ $key }}</option>
                                                    @else
                                                        <option value="{{ $value }}">{{ $key }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5><label class="control-lable col-sm-4">Kota / Kabupaten</label></h5>
                                        <div class="col-sm-8">
                                            <select class="form-control select2" name="kota_1" id="kota_1" onchange="set_kecamatan('kota_1','kec_1','Pilih Kecamatan')">
                                                <option value="">-- Pilih --</option>
                                                @php
                                                    $kota = app()->make('App\Http\Controllers\Ajax')->set_list_kota($user->peminjam->alamat->province_id);
                                                    foreach ($kota as $key) {
                                                        if($user->peminjam->alamat->regency_id == $key['id']){
                                                            echo '<option value="'.$key['id'].'" selected>'.$key['nama'].'</option>';
                                                        }else{
                                                            echo '<option value="'.$key['id'].'">'.$key['nama'].'</option>';
                                                        }
                                                    }
                                                @endphp
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5><label class="control-lable col-sm-4">Kecamatan</label></h5>
                                        <div class="col-sm-8">
                                            <select class="form-control select2" name="kec_1" id="kec_1" onchange="set_kelurahan('kec_1','kel_1','Pilih Kelurahan')">
                                                <option value="">-- Pilih --</option>
                                                @php
                                                    $kecamatan = app()->make('App\Http\Controllers\Ajax')->set_list_kecamatan($user->peminjam->alamat->regency_id);
                                                    foreach ($kecamatan as $key) {
                                                        if($user->peminjam->alamat->district_id == $key['id']){
                                                            echo '<option value="'.$key['id'].'" selected>'.$key['nama'].'</option>';
                                                        }else{
                                                            echo '<option value="'.$key['id'].'">'.$key['nama'].'</option>';
                                                        }
                                                    }
                                                @endphp
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5><label class="control-lable col-sm-4">Kelurahan</label></h5>
                                        <div class="col-sm-8">
                                            <select class="form-control select2" name="kel_1" id="kel_1">
                                                <option value="">-- Pilih --</option>
                                                @php
                                                    $kelurahan = app()->make('App\Http\Controllers\Ajax')->set_list_kelurahan($user->peminjam->alamat->district_id);
                                                    foreach ($kelurahan as $key) {
                                                        if($user->peminjam->alamat->village_id == $key['id']){
                                                            echo '<option value="'.$key['id'].'" selected>'.$key['nama'].'</option>';
                                                        }else{
                                                            echo '<option value="'.$key['id'].'">'.$key['nama'].'</option>';
                                                        }
                                                    }
                                                @endphp
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5><label class="control-lable col-sm-4">Kode Pos</label></h5>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="kode_pos_1" value="{{ $user->peminjam->alamat->kode_pos }}" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <span class="btn btn-circle btn-default btn-file">
                                            <span class="fileinput-new"><i class="fa fa-cloud-upload"></i>Foto Tempat Tinggal </span>
                                            <span class="fileinput-exists"> </span>
                                            <input type="hidden"><input type="file" name="photo_tempat_tinggal" onchange="preview_image(event,'photo_tempat_tinggal')">
                                            </span>
                                        </div>
                                        <div class="col-sm-8">
                                            @if($user->peminjam->photo_tempat_tinggal_id != "")
                                                <img src="{{ asset('storage/tempat_tinggal/'.$user->peminjam->photo_tempat_tinggal->name) }}" class="preview" id="photo_tempat_tinggal" />
                                            @else
                                                <img class="preview" id="photo_tempat_tinggal" />
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold font-dark uppercase">Data Keuangan</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light portlet-fit">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <span class="caption-subject bold font-dark uppercase">Sumber Pendapatan Lainnya</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-hover" id="pendapatan">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Keterangan
                                                    </th>
                                                    <th style="width: 20%">
                                                        Jumlah
                                                    </th>
                                                    <th style="width: 10%">
                                                        Hapus
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $index = 1;
                                                @endphp
                                                @foreach ($pendapatan_lainnya as $item)
                                                    <tr id="pendapatan_{{ $index }}">
                                                        <td>
                                                            <textarea class="form-control" name="pendapatan_keterangan[]" style="height: 34px;">{{ $item->keterangan }}</textarea>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="pendapatan_jumlah[]" value="{{ number_format($item->jumlah,2,',','.') }}" />
                                                        </td>
                                                        <td>
                                                            <button type="button" class="btn btn-danger" onclick="remove_row('pendapatan',{{ $index++ }})">Hapus</button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <button type="button" class="btn btn-success" onclick="add_row('pendapatan')">Tambah</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="portlet light portlet-fit">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <span class="caption-subject bold font-dark uppercase">Pinjaman Lainnya</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-hover" id="pinjaman">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Keterangan
                                                    </th>
                                                    <th style="width: 20%">
                                                        Jumlah
                                                    </th>
                                                    <th style="width: 10%">
                                                        Hapus
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $index = 1;
                                                @endphp
                                                @foreach ($pinjaman_lainnya as $item)
                                                    <tr id="pinjaman_{{ $index }}">
                                                        <td>
                                                            <textarea class="form-control" name="pinjaman_keterangan[]" style="height: 34px;">{{ $item->keterangan }}</textarea>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="pinjaman_jumlah[]" value="{{ number_format($item->jumlah,2,',','.') }}" />
                                                        </td>
                                                        <td>
                                                            <button type="button" class="btn btn-danger" onclick="remove_row('pinjaman',{{ $index++ }})">Hapus</button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <button type="button" class="btn btn-success" onclick="add_row('pinjaman')">Tambah</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-fit">
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="portlet light portlet-fit">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <span class="caption-subject bold font-dark uppercase">Tempat Mengajar Saat Ini</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="form-group">
                                        <h5><label class="control-lable col-sm-4">Nama Sekolah</label></h5>
                                        <div class="col-sm-8">
                                            <input class="form-control" name="nama_sekolah" type="text" value="{{ $user->peminjam->nama_tempat_kerja }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5><label class="control-lable col-sm-4">Jumlah Murid</label></h5>
                                        <div class="col-sm-8">
                                            <input class="form-control" name="jumlah_murid" type="number" min="0" value="{{ $user->peminjam->jumlah_murid_tempat_kerja }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5><label class="control-lable col-sm-4">Lama Berdiri</label></h5>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input class="form-control" name="lama_berdiri" type="number" min="0" value="{{ $user->peminjam->lama_berdiri_tempat_kerja }}">
                                                <span class="input-group-addon">Tahun</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5><label class="control-lable col-sm-4">Alamat Sekolah</label></h5>
                                        <div class="col-sm-8">
                                            <textarea class="form-control" cols="35" name="alamat_sekolah" rows="3">{{ $user->peminjam->alamat_kerja->alamat }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5><label class="control-lable col-sm-4">Provinsi</label></h5>
                                        <div class="col-sm-8">
                                            <select class="form-control select2" name="prov_2" id="prov_2" onchange="set_kota('prov_2','kota_2','Pilih Kab/Kota')">
                                                <option value="">-- Pilih --</option>
                                                @foreach ($list_provinsi as $key => $value)
                                                    @if($user->peminjam->alamat_kerja->province_id == $value)
                                                        <option value="{{ $value }}" selected>{{ $key }}</option>
                                                    @else
                                                        <option value="{{ $value }}">{{ $key }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5><label class="control-lable col-sm-4">Kota / Kabupaten</label></h5>
                                        <div class="col-sm-8">
                                            <select class="form-control select2" id="kota_2" name="kota_2" onchange="set_kecamatan('kota_2','kec_2','Pilih Kecamatan')">
                                                <option value="">-- Pilih --</option>
                                                @php
                                                    $kota = app()->make('App\Http\Controllers\Ajax')->set_list_kota($user->peminjam->alamat_kerja->province_id);
                                                    foreach ($kota as $key) {
                                                        if($user->peminjam->alamat_kerja->regency_id == $key['id']){
                                                            echo '<option value="'.$key['id'].'" selected>'.$key['nama'].'</option>';
                                                        }else{
                                                            echo '<option value="'.$key['id'].'">'.$key['nama'].'</option>';
                                                        }
                                                    }
                                                @endphp
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5><label class="control-lable col-sm-4">Kecamatan</label></h5>
                                        <div class="col-sm-8">
                                            <select class="form-control select2" id="kec_2" name="kec_2" onchange="set_kelurahan('kec_2','kel_2','Pilih Kelurahan')">
                                                <option value="">-- Pilih --</option>
                                                @php
                                                    $kecamatan = app()->make('App\Http\Controllers\Ajax')->set_list_kecamatan($user->peminjam->alamat_kerja->regency_id);
                                                    foreach ($kecamatan as $key) {
                                                        if($user->peminjam->alamat_kerja->district_id == $key['id']){
                                                            echo '<option value="'.$key['id'].'" selected>'.$key['nama'].'</option>';
                                                        }else{
                                                            echo '<option value="'.$key['id'].'">'.$key['nama'].'</option>';
                                                        }
                                                    }
                                                @endphp
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5><label class="control-lable col-sm-4">Kelurahan</label></h5>
                                        <div class="col-sm-8">
                                            <select class="form-control select2" name="kel_2" id="kel_2">
                                                <option value="">-- Pilih --</option>
                                                @php
                                                    $kelurahan = app()->make('App\Http\Controllers\Ajax')->set_list_kelurahan($user->peminjam->alamat_kerja->district_id);
                                                    foreach ($kelurahan as $key) {
                                                        if($user->peminjam->alamat_kerja->village_id == $key['id']){
                                                            echo '<option value="'.$key['id'].'" selected>'.$key['nama'].'</option>';
                                                        }else{
                                                            echo '<option value="'.$key['id'].'">'.$key['nama'].'</option>';
                                                        }
                                                    }
                                                @endphp
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5><label class="control-lable col-sm-4">Kode Pos</label></h5>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="kode_pos_2" value="{{ $user->peminjam->alamat_kerja->kode_pos }}" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <span class="btn btn-circle btn-default btn-file">
                                            <span class="fileinput-new"><i class="fa fa-cloud-upload"></i>Foto Tempat Kerja </span>
                                            <span class="fileinput-exists"> </span>
                                            <input type="hidden"><input type="file" name="photo_tempat_kerja" onchange="preview_image(event,'photo_tempat_kerja')">
                                            </span>
                                        </div>
                                        <div class="col-sm-8">
                                            @if($user->peminjam->photo_tempat_kerja_id != "")
                                                <img src="{{ asset('storage/tempat_kerja/'.$user->peminjam->photo_tempat_kerja->name) }}" class="preview" id="photo_tempat_kerja" />
                                            @else
                                                <img class="preview" id="photo_tempat_kerja" />
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="portlet light portlet-fit">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <span class="caption-subject bold font-dark uppercase">Informasi Pekerjaan Saat Ini</span>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-group">
                                                <label class=" col-sm-4">Jabatan</label>
                                                <div class="col-sm-8">
                                                    <select class="form-control select2" name="jabatan">
                                                        <option value="">-- Pilih --</option>
                                                        @foreach ($jabatan as $key => $value)
                                                            @if($user->peminjam->jabatan_id == $value)
                                                                <option value="{{ $value }}" selected>{{ $key }}</option>
                                                            @else
                                                                <option value="{{ $value }}">{{ $key }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class=" col-sm-4">Status</label>
                                                <div class="col-sm-8">
                                                    <div class="mt-radio-inline">
                                                       <label class="mt-radio">
                                                           <input type="radio" name="status_pengajar" value="tetap" @if($user->peminjam->status_pengajar == 'tetap') checked @endif> Tetap
                                                           <span></span>
                                                       </label>
                                                       <label class="mt-radio">
                                                           <input type="radio" name="status_pengajar" value="tidak_tetap" @if($user->peminjam->status_pengajar == 'tidak_tetap') checked @endif> Tidak Tetap
                                                           <span></span>
                                                       </label>
                                                   </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class=" col-sm-4">Lama Mengajar</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="lama_mengajar" class="form-control" value="{{ $user->peminjam->lama_mengajar }}" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class=" col-sm-4">Gaji</label>
                                                <div class="col-sm-8">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">Rp.</span>
                                                        <input type="text" name="gaji" class="form-control rupiah" value="{{ $user->peminjam->gaji }}" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="portlet light portlet-fit">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <span class="caption-subject bold font-dark uppercase">Informasi Bank</span>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-group">
                                                <label class=" col-sm-4">Bank</label>
                                                <div class="col-sm-8">
                                                    <select class="form-control select2" name="bank">
                                                        <option value="">-- Pilih --</option>
                                                        @foreach ($list_bank as $key => $value)
                                                            @if($user->peminjam->akun_bank->bank_id == $value)
                                                                <option value="{{ $value }}" selected>{{ $key }}</option>
                                                            @else
                                                                <option value="{{ $value }}">{{ $key }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <h5><label class="control-lable col-sm-4">Nama Akun Bank</label></h5>
                                                <div class="col-sm-8">
                                                    <input class="form-control" name="nama_akun" type="text" value="{{ $user->peminjam->akun_bank->nama_akun_bank }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <h5><label class="control-lable col-sm-4">Nomor Akun Bank</label></h5>
                                                <div class="col-sm-8">
                                                    <input class="form-control" name="no_rekening" type="text" value="{{ $user->peminjam->akun_bank->no_akun_bank }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                    <span class="btn btn-circle btn-default btn-file">
                                                    <span class="fileinput-new"><i class="fa fa-cloud-upload"></i>Rekening Koran</span>
                                                    <span class="fileinput-exists"> </span>
                                                    <input type="hidden"><input type="file" name="rekening_koran" onchange="preview_image(event,'rekening_koran')">
                                                    </span>
                                                </div>
                                                <div class="col-sm-8">
                                                    @if($user->peminjam->photo_rekening_koran_id != "")
                                                        <img src="{{ asset('storage/rekening_koran/'.$user->peminjam->photo_rekening_koran->name) }}" class="preview" id="rekening_koran" />
                                                    @else
                                                        <img class="preview" id="rekening_koran" />
                                                    @endif
                                                </div>
                                            </div>
                                            <br>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-success pull-left">Simpan</button>
                                                <a class="btn red btn-outline sbold panel-title pull-right" href="#basic" data-toggle="modal">Data Pendukung</a>
                                                <hr width="100%">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
