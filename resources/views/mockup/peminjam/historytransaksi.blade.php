@extends('layouts.app')

@push('page-plugin-styles')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .table thead tr th{
            white-space: pre-wrap;
            vertical-align: middle;
            text-align: center;
            padding-top: 20px;
            padding-bottom: 20px;
        }
        .table tbody tr td{
            vertical-align: middle;
        }
        #data th, #data td {
            font-size: 13px;
        }
    </style>
@endpush

@section('crumbs')
  <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/pinjaman/dashboard') }}">Peminjam</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>History Transaksi</span>
        </li>
    </ul>
@endsection

@section('title')
    History Transakasi
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-hourglass"></i>
                        <span class="caption-subject bold uppercase">HISTORY TRANSAKSI PEMBAYARAN</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-sm btn-default" href="javascript:;">
                            <i class="fa fa-print"></i> Print to PDF
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <center><h1>HISTORY TRANSAKSI PEMBAYARAN</h1></center><br>
                    <table class="table table-striped table-bordered table-hover" id="data">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Waktu Transaksi</th>
                                <th>Kode Transaksi</th>
                                <th>Nilai Transaksi</th>
                                <th>Kode Pinjaman</th>
                                <th>Jenis Transaksi</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    <br>
                    <div class="clearfix margin-bottom-20"> </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('page-plugin-scripts')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
@endpush

@push('page-scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#data').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ url('peminjam/daftar/history') }}',
                    type: 'POST'
                },
                columns:[
                    {data: "DT_Row_Index", orderable: false, searchable: false, className: 'dt-center'},
                    {data: "tanggal", name: 'tanggal'},
                    {data: "ref", name: 'ref'},
                    {data: "jumlah", name: 'jumlah', className: 'dt-right'},
                    {data: "kode", name: 'kode'},
                    {data: "jenis_transaksi", name: 'jenis_transaksi'},
                    {data: "status", name: 'status'},
                ],
                rowCallback: function(row,data,index){
                    // nomer
                    $('td:eq(0)', row).html( '<center>'+data.DT_Row_Index+'</center>' );
                    // tanggal_pinjaman
                    $('td:eq(1)',row).html(moment(data.tanggal).format("DD-MM-YYYY"));
                    // jumlah_pinjaman
                    $('td:eq(3)',row).html('Rp. '+numeral(data.jumlah).format('0,0'));
                }
            });
        });
    </script>
@endpush
