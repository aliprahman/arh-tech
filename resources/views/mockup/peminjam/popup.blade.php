<div class="modal fade in" id="basic" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Data Pendukung</h4>
            </div>
            <div class="modal-body">
            	<div class="portlet light " id="form_wizard_1">
                    <div class="portlet-title">
                		<div class="caption">
                        	<i class=" icon-layers font-red"></i>
                    		<span class="caption-subject font-red bold uppercase"> Data Pendukung -
                            	<span class="step-title"> Step 1 of 4 </span>
                    		</span>
                		</div>
                    </div>
                    <div class="portlet-body form">
                        <form class="form-horizontal" action="{{ url('peminjam/data_pendukung/'.$peminjam_id) }}" id="submit_form" method="POST" novalidate="novalidate" enctype="multipart/form-data">
							{{ csrf_field() }}
                            <div class="form-wizard">
                                <div class="form-body">
                                    <ul class="nav nav-pills nav-justified steps">
                                        <li class="active">
                                          	<a href="#tab1" data-toggle="tab" class="step" aria-expanded="true">
                                            	<span class="number"> 1 </span>
                                            	<span class="desc"><i class="fa fa-check"></i> First Step </span>
                                          	</a>
                                        </li>
                                        <li>
                                            <a href="#tab2" data-toggle="tab" class="step">
                                                <span class="number"> 2 </span>
                                                <span class="desc"><i class="fa fa-check"></i> Second Step </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab3" data-toggle="tab" class="step active">
                                                <span class="number"> 3 </span>
                                                <span class="desc"><i class="fa fa-check"></i> Next Step </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab4" data-toggle="tab" class="step">
                                                <span class="number"> 4 </span>
                                                <span class="desc"><i class="fa fa-check"></i> Confirm </span>
                                            </a>
                                        </li>
                                    </ul>
                                    <div id="bar" class="progress progress-striped" role="progressbar">
                                        <div class="progress-bar progress-bar-success" style="width: 25%;"> </div>
                                    </div>
                                    <div class="tab-content">
                                        <div class="alert alert-danger display-none">
                                            <button class="close" data-dismiss="alert"></button> Harap Isi Dari Semua Kolom Yang Ada.
										</div>
                                            <div class="alert alert-success display-none">
                                                <button class="close" data-dismiss="alert"></button> Your form validation is successful!
											</div>

                                      		<!-- TAB SATU -->
                                            <div class="tab-pane active" id="tab1">
                                              	<h3 class="block">Surat Kontrak Mengajar</h3><hr width="100%">
                                              	<div class="form-group">
                                                  	<label class="control-label col-md-4">No. SK / Surat Kontrak</label>
                                                  	<div class="col-md-8">
                                                      	<input type="text" class="form-control" name="no_sk_surat_kontrak" value="{{ $user->peminjam->no_sk_surat_kontrak }}">
                                                  	</div>
                                              	</div>
                                              	<div class="form-group">
                                                  	<label class="control-label col-md-4">Tanggal Surat</label>
                                                  	<div class="col-md-8">
														<div class="input-group date" data-provide="datepicker" data-date-format="dd-mm-yyyy">
														    <input type="text" class="form-control" name="tanggal_sk_surat_kontrak" value="{{ $user->peminjam->tanggal_sk_surat_kontrak }}">
														    <div class="input-group-addon">
														        <i class="fa fa-calendar"></i>
														    </div>
														</div>
                                                  	</div>
                                              	</div>
                                              	<div class="form-group">
                                                 	<div class="col-md-4" style="text-align:right">
                                                  		<a href="#" class="btn btn-default" type="button" onclick="document.getElementById('fileID').click(); return false;">
															<i class="fa fa-cloud-upload"> Upload </i>
														</a>
														<input id="fileID" name="photo_sk_surat_kontak" type="file" style="visibility: hidden; display: none;" onchange="preview_image(event,'photo_sk_surat_kontak')" />
                                                 	</div>
                                                    <div class="col-md-8">
                                                        @if($user->peminjam->photo_sk_surat_kontrak_id != "")
                                                            <img src="{{ asset('storage/surat_sk/'.App\FileUploads::where('id',$user->peminjam->photo_sk_surat_kontrak_id)->value('name')) }}" class="preview" id="photo_sk_surat_kontak" />
                                                        @else
                                                            <img class="preview" id="photo_sk_surat_kontak" />  
                                                        @endif
                                                    </div>
                                              	</div>
                                            </div>

                                      		<!-- TAB DUA -->
                                            <div class="tab-pane" id="tab2">
                                                <h3 class="block">Pengalaman Mengajar</h3><hr width="100%">
												@if(count($user->peminjam->pengalaman_kerja) < 1)
                                                    <div id="pengalaman_kerja">
    													<div class="form-group">
    														<label class="control-label col-md-4">Tahun Mengajar</label>
    														<div class="col-md-4">
    															<input type="number" min="0" class="form-control" name="dari_tahun[]">
    														</div>
    														<div class="col-md-4">
    															<input type="number" min="0" class="form-control" name="sampai_tahun[]" >
    														</div>
    													</div>
    													<div class="form-group">
    														<label class="control-label col-md-4">Tempat Mengajar</label>
    														<div class="col-md-8">
    															<input type="text" class="form-control" name="tempat_mengajar[]">
    														</div>
    													</div>
                                                    </div>
												@else
                                                    <div id="pengalaman_kerja">
													@foreach ($user->peminjam->pengalaman_kerja as $key)
    													<div class="form-group">
    		                                                <label class="control-label col-md-4">Tahun Mengajar</label>
    		                                                <div class="col-md-4">
    		                                                    <input type="number" min="0" class="form-control" name="dari_tahun[]" value="{{ $key->dari_tahun }}">
    		                                                </div>
    		                                                <div class="col-md-4">
    		                                                    <input type="number" min="0" class="form-control" name="sampai_tahun[]" value="{{ $key->sampai_tahun }}">
    		                                                </div>
    		                                               </div>
    		                                            <div class="form-group">
    		                                                <label class="control-label col-md-4">Tempat Mengajar</label>
    		                                                <div class="col-md-8">
    		                                                    <input type="text" class="form-control" name="tempat_mengajar[]" value="{{ $key->tempat_kerja }}">
    		                                                </div>
    		                                            </div>
													@endforeach
                                                    </div>
												@endif
                                                <div class="form-group">
                                                  	<div class="col-md-4" style="text-align:right">
                                                    	<a onclick="add_pengalaman_kerja()" class="form-control btn btn-default"><i class="fa fa-edit"> Tambah </i></a>
                                                  	</div>
                                                </div>
                                            </div>

                                        	<!-- TAB TIGA -->
                                            <div class="tab-pane" id="tab3"><hr width="100%">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Kepemilikan Rumah</label>
													@foreach ($kepemilikan_rumah as $key => $value)
														<div class="col-md-2">
	                                                        <input type="radio" name="kepemilikan_rumah_id" @if ($user->peminjam->kepemilikan_rumah_id == $value) checked @endif value="{{ $value }}"> {{ $key }}
	                                                    </div>
													@endforeach
                                                </div>
                                                <div class="form-group">
                                                	<label class="control-label col-md-4">Pendidikan Terakhir</label>
                                                 	<div class="col-md-5">
                                                  		<select class="form-control" name="pendidikan_id">
                                                    		<option value="">-- Pilih --</option>
															@foreach ($pendidikan as $key => $value)
																@if($user->peminjam->pendidikan_id == $value)
																	<option value="{{ $value }}" selected>{{ $key }}</option>
																@else
																	<option value="{{ $value }}">{{ $key }}</option>
																@endif
															@endforeach
                                                  		</select>
                                                 	</div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Status Perkawinan</label>
													@foreach ($status_pernikahan as $key => $value)
														<div class="col-md-2">
	                                                        <input type="radio" name="status_pernikahan_id" @if($user->peminjam->status_pernikahan_id == $value) checked @endif value="{{ $value }}"> {{ $key }}
	                                                    </div>
													@endforeach
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nama Gadis Ibu Kandung</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="nama_gadis_ibu_kandung" value="{{ $user->peminjam->nama_gadis_ibu_kandung }}">
                                                    </div>
                                                </div>
												<br>
                                                <h3 class="block">Kontak Keluarga Tidak Serumah</h3>
												<hr width="100%">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nama Lengkap</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="kontak_nama" value="{{ $user->peminjam->kontak_nama }}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                	<label class="control-label col-md-4">Hubungan Keluarga</label>
                                                 	<div class="col-md-5">
	                                                  	<select name="hubungan_kontak_id" class="form-control">
		                                                    <option value="">-- Pilih --</option>
															@foreach ($hubungan_kontak as $key => $value)
																@if ($user->peminjam->hubungan_kontak_id == $value)
																	<option value="{{ $value }}" selected>{{ $key }}</option>
																@else
																	<option value="{{ $value }}">{{ $key }}</option>
																@endif
															@endforeach
	                                                  	</select>
	                                                </div>
                                                </div>
                                                <div class="form-group">
                                                	<label class="control-label col-md-4"><h5>Alamat Rumah</h5></label>
                                                  	<div class="col-md-8">
                                                    	<textarea name="alamat_rumah_kontak" rows="3" cols="35" class="form-control">
															{{ App\Alamat::where('id',$user->peminjam->alamat_kontak_id)->value('alamat') }}
														</textarea>
                                                  	</div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Telepon</label>
                                                    <div class="col-md-4">
														<div class="input-group">
									                        <input type="text" class="form-control" name="no_hp_kontak" value="{{ $user->peminjam->no_hp_kontak }}">
									                        <span class="input-group-addon">
									                            <i class="fa fa-mobile-phone"></i>
									                        </span>
									                    </div>
                                                    </div>
                                                    <div class="col-md-4">
														<div class="input-group">
									                        <input type="text" class="form-control" name="no_telp_kontak" value="{{ $user->peminjam->no_telp_kontak }}">
									                        <span class="input-group-addon">
									                            <i class="fa fa-phone"></i>
									                        </span>
									                    </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- TAB EMPAT -->
                                            <div class="tab-pane" id="tab4">
                                                  	<h3 class="block">Data Keluarga</h3><hr width="100%">
                                                <div class="form-group">
	                                                <label class="control-label col-md-4">Nama Suami / Istri</label>
	                                                <div class="col-md-8">
	                                                    <input type="text" class="form-control" name="nama_suami_istri" value="{{ $user->peminjam->nama_suami_istri }}">
	                                                </div>
                                                </div>
	                                            <div class="form-group">
	                                                <label class="control-label col-md-4">Tempat & Tanggal Lahir</label>
	                                                <div class="col-md-4">
	                                                   <input type="text" class="form-control" name="tempat_lahir_suami_istri" value="{{ $user->peminjam->tempat_lahir_suami_istri }}">
	                                                </div>
	                                                <div class="col-md-4">
														<div class="input-group date" data-provide="datepicker" data-date-format="dd-mm-yyyy">
														    <input type="text" class="form-control" name="tanggal_lahir_suami_istri" value="{{ $user->peminjam->tanggal_lahir_suami_istri }}">
														    <div class="input-group-addon">
														        <i class="fa fa-calendar"></i>
														    </div>
														</div>
	                                                </div>
	                                            </div>
	                                            <div class="form-group">
	                                                <label class="control-label col-md-4">No. Ktp</label>
	                                                <div class="col-md-8">
	                                                    <input type="text" class="form-control" name="no_ktp_suami_istri" value="{{ $user->peminjam->no_ktp_suami_istri }}">
	                                                </div>
	                                            </div>
                                                <div class="form-group">
	                                                <label class="control-label col-md-4">Pekerjaan</label>
	                                                <div class="col-md-8">
                                                        <select class="form-control select2" name="pekerjaan">
                                                            <option value="">-- Pilih --</option>
                                                            @foreach ($pekerjaan as $key => $value)
                                                                @if($user->peminjam->pekerjaan_suami_istri_id == $value)
                                                                    <option value="{{ $value }}" selected>{{ $key }}</option>
                                                                @else
                                                                    <option value="{{ $value }}">{{ $key }}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
	                                                </div>
	                                            </div>
                                                <div class="form-group">
	                                                <label class="control-label col-md-4">Pendapatan</label>
	                                                <div class="col-md-8">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">Rp.</span>
                                                            <input type="text" class="form-control rupiah" name="pendapatan" placeholder="isi 0 jika tidak memiliki" value="{{ $user->peminjam->pendapatan_suami_istri }}">
                                                        </div>
	                                                </div>
	                                            </div>
	                                            <div class="form-group">
	                                                <label class="control-label col-md-4">Jumlah Tanggungan</label>
	                                                <div class="col-md-8">
                                                        <div class="input-group">
                                                            <input type="number" min="0" class="form-control" name="jumlah_tanggungan" placeholder="isi 0 jika tidak memiliki" value="{{ $user->peminjam->jumlah_tanggungan }}">
                                                            <span class="input-group-addon">Anak</span>
                                                        </div>
	                                                </div>
	                                            </div>
                                            </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <a href="javascript:;" class="btn default button-previous disabled panel-title" style="display: none;">
                                                <i class="fa fa-angle-left"> Back </i></a>
                                            <a href="javascript:;" class="fa btn btn-outline green button-next"> Continue
                                                <i class="fa fa-angle-right"></i>
                                            </a>
											<button type="submit" class="btn green button-submit" style="display: none;">
												Submit <i class="fa fa-check"></i>
											</button>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </form>
                        </div>
                    </div>
			</div>
            <div class="modal-footer">
            	<button type="button" class="btn dark btn-outline" data-dismiss="modal">Tutup</button>
        	</div>
    	</div>
    </div>
</div>

@push('page-plugin-scripts')
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/form-wizard.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
@endpush
