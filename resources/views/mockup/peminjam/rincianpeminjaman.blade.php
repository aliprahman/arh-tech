@extends('layouts.app')

@push('page-plugin-styles')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<style type="text/css">
  .table thead tr th{
    white-space: pre-wrap;
    vertical-align: middle;
    text-align: center;
    padding-top: 20px;
    padding-bottom: 20px;
  }
  .table tbody tr td{
    vertical-align: middle;
  }
  label{
    text-align: left;
  }
  #data th, #data td {
    font-size: 13px;
  }
</style>
@endpush

@section('crumbs')
  <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/peminjam/dashboard') }}">Peminjam</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ url('/peminjam/daftar') }}">Daftar Peminjaman</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Rincian Peminjaman</span>
        </li>
    </ul>
@endsection

@section('title')
Rincian Peminjaman
@endsection

@section('content')
	<div class="panel">
        <div class="panel-body">
            <a href="{{ URL::previous() }}" class="btn btn-circle btn-default"><i class="icon-refresh"></i> Kembali</a>
            <center><h1>Rincian Peminjaman</h1></center><br><br>
            <div class="container-fluid">
                <form class="form-horizontal">
                    <div class="row">
                        <!-- kiri -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class=" col-sm-4"><h5>Kode Pinjaman</h5></label>
                                <div class="col-sm-5">
                                    <input type="text" name="kode" disabled class="form-control" placeholder="{{ $pinjaman->kode }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class=" col-sm-4"><h5>Tanggal Pinjaman</h5></label>
                                <div class="col-sm-5">
                                    <input type="text" name="tgl" disabled class="form-control" placeholder="{{ date_format(date_create($pinjaman->tanggal_submit),'d-m-Y') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class=" col-sm-4"><h5>Status pinjaman</h5></label>
                                <div class="col-sm-5">
                                    <input type="text" name="stat" disabled class="form-control"
                                    @if($sisa_pinjaman > 0)
                                        placeholder="Belum Lunas"
                                    @else
                                        placeholder="Lunas"
                                    @endif
                                    >
                                </div>
                            </div>
                        </div>
                        <!-- kanan -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class=" col-sm-5"><h5>Status Pengajuan Pinjaman</h5></label>
                                <div class="col-sm-5">
                                    <input type="text" name="pin" disabled class="form-control" placeholder="{{ title_case($pinjaman->status) }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class=" col-sm-5"><h5>Nilai Pinjaman</h5></label>
                                <div class="col-sm-5">
                                    <input type="text" name="nil" disabled class="form-control" placeholder="Rp. {{ number_format($pinjaman->jumlah_pinjaman,2,',','.') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class=" col-sm-5"><h5>Sisa Pinjaman</h5></label>
                                <div class="col-sm-5">
                                    <input type="text" name="sis" disabled class="form-control" placeholder="Rp. {{ number_format($sisa_pinjaman,2,',','.') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div><br><br><br>
            <div class="row">
                <div class="col-md-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session('success') != "")
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session('success') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-hourglass"></i>
                                <span class="caption-subject bold uppercase">DAFTAR PEMBAYARAN CICILAN</span>
                            </div>
                            <div class="actions">
                                <a class="btn green btn-outline sbold panel-title pull-right btn-circle " data-toggle="modal" href="#basic"><i class="fa fa-pencil"></i> Pembayaran Baru </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table data" id="data">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Waktu Transaksi</th>
                                        <th>Kode transaksi</th>
                                        <th>Nilai Transaksi</th>
                                        <th>Jenis Transaksi</th>
                                        <th>Sisa Cicilan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <div class="clearfix margin-bottom-20"> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="basic" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="form_pembayaran" class="form-horizontal" method="post" action="{{ url('peminjam/pembayaran/'.$pinjaman_id) }}">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Pembayaran Cicilan</h4>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class=" col-sm-5"><h5>Bank</h5></label>
                            <div class="col-sm-7">
                                <select class="form-control select2" name="bank" required>
                                    <option value=""> -- Pilih -- </option>
                                    @foreach ($list_bank as $key => $value)
                                        @if(old('bank') != "" && old('bank') == $value)
                                            <option value="{{ $value }}" selected>{{ $key }}</option>
                                        @else
                                            <option value="{{ $value }}">{{ $key }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class=" col-sm-5"><h5>Nominal Pembayaran</h5></label>
                            <div class="col-sm-7">
                                <input type="text" id="nominal" name="nominal" value="{{ old('nominal') }}" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class=" col-sm-5"><h5>No. Transaksi</h5></label>
                            <div class="col-sm-7">
                                <input type="text" name="no_transaksi" class="form-control" value="{{ old('no_transaksi') }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class=" col-sm-5"><h5>Nama Pemilik Rekening</h5></label>
                            <div class="col-sm-7">
                                <input type="text" name="nama_akun" class="form-control" value="{{ old('nama_akun') }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class=" col-sm-5"><h5>No Rekening</h5></label>
                            <div class="col-sm-7">
                                <input type="text" name="no_rekening" class="form-control" value="{{ old('no_rekening') }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class=" col-sm-5"><h5>Catatan</h5></label>
                            <div class="col-sm-7">
                                <textarea class="form-control" name="catatan" placeholder="Catatan Tambahan (Optional)">{{ old('catatan') }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3">
                                <div class="g-recaptcha" data-sitekey="{{ env('CAPTCHA_SITEKEY') }}"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline" data-dismiss="modal" aria-hidden="true">Close</button>
                        <button type="submit" class="btn green">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('page-plugin-scripts')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.number.min.js') }}" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
@endpush

@push('page-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2({
            theme: "bootstrap"
        });
        $('#data').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ url('peminjam/daftar/cicilan/'.$pinjaman_id) }}',
                type: 'POST'
            },
            columns:[
                {data: "DT_Row_Index", orderable: false, searchable: false, className: 'dt-center'},
                {data: "tanggal", name: 'tanggal'},
                {data: "ref", name: 'ref'},
                {data: "jumlah", name: 'jumlah', className: 'dt-right'},
                {data: "jenis_transaksi", name: 'jenis_transaksi'},
                {data: "sisa_cicilan", name: 'sisa_cicilan', className: 'dt-right'},
            ],
            rowCallback: function(row,data,index){
                // nomer
                $('td:eq(0)', row).html( '<center>'+data.DT_Row_Index+'</center>' );
                // tanggal_pinjaman
                $('td:eq(1)',row).html(moment(data.tanggal).format("DD-MM-YYYY"));
                // jumlah_pinjaman
                $('td:eq(3)',row).html('Rp. '+numeral(data.jumlah).format('0,0'));
                // sisa cicilan
                $('td:eq(5)',row).html('Rp. '+numeral(data.sisa_cicilan).format('0,0'));
            }
        });
        $('#nominal').number(true,2,',','.');
    });
</script>
@endpush
