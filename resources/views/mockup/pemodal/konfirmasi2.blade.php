@extends('layouts.app')

@push('page-plugin-styles')
<link href="{{ asset('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
@endpush

@section('title')
Deposit
@endsection

@section('crumbs')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/dashboard') }}">Pemodal</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ url('/pemodal/deposit') }}">Deposit</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Konfirmasi Pembayaran</span>
        </li>
    </ul>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="portlet light ">
        <div class="portlet-title">
            <div class="row">
                <div class="col-md-12">
                    <center><h1>KONFIRMASI PEMBAYARAN</h1><p>Penting! Mohon konfirmasi ini hanya dilakukan setelah Anda melakukan pembayaran.<br>Isi data dengan benar untuk memudahkan kami memverifikasi konfirmasi anda.</p></center>
                </div>
            </div>
        </div>
        <div class="portlet-body">
          @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif
            <div class="row">
              <form class="form-horizontal" role="form" action="{{ url('pemodal/deposit_konfirmasi') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ encrypt($transaksi->id) }}">
                <input type="hidden" name="depo" value="{{ encrypt($depo_id) }}">
                <div class="col-md-6 line">
                        <div class="form-group">
                            <label for="no" class="col-md-4 control-label"><font class="pull-left">No Transaksi</font></label>
                            <div class="col-md-8">
                                <input type="text" name="no" class="form-control" value="{{ $transaksi->ref }}" disabled="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nominal" class="col-md-4 control-label"><font class="pull-left">Nominal Deposit (Rp)</font></label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon">Rp.</span>
                                    <input type="text" name="nominal" class="form-control" value="{{ $transaksi->jumlah }}" disabled id="jumlah">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bank" class="col-md-4 control-label"><font class="pull-left">Bank Tujuan</font></label>
                            <div class="col-md-8">
                                <input type="bank" class="form-control" disabled="" value="{{ $transaksi->nama_bank }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tanggal" class="col-md-4 control-label"><font class="pull-left">Tanggal Transfer</font></label>
                            <div class="col-md-8">
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' class="form-control textbox" name="tanggal" id="tanggal" placeholder="contoh format: 2017-05-01 13:15" value="{{ old('tanggal') }}" required />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                <span class="text-danger" id="alert_j"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pengirim" class="col-md-4 control-label"><font class="pull-left">Transfer Via</font></label>
                            <div class="col-md-8">
                                <input type="text" name="nama_bank" class="form-control" value="{{ old('nama_bank') }}" placeholder="Nama Bank yang digunakan" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pengirim" class="col-md-4 control-label"><font class="pull-left">Nama Pemilik Akun</font></label>
                            <div class="col-md-8">
                                <input type="text" name="pengirim" class="form-control" value="{{ old('pengirim') }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pengirim" class="col-md-4 control-label"><font class="pull-left">No Rekening</font></label>
                            <div class="col-md-8">
                                <input type="text" name="norek" class="form-control" value="{{ old('norek') }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="catatan" class="col-md-4 control-label"><font class="pull-left">Catatan Tambahan</font></label>
                            <div class="col-md-8">
                                <textarea class="form-control textbox" id="deskripsi" name="deskripsi">{{ old('deskripsi') }}</textarea>
                                <span class="text-danger"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="captch" class="col-md-4 control-label"></label>
                            <div class="col-md-8">
                               <div class="g-recaptcha" data-sitekey="{{ env('CAPTCHA_SITEKEY') }}" data-callback="success" id="captcha"></div>
                               <span class="text-danger" id="alert"></span>
                            </div>
                        </div>

                </div>
                <div class="col-md-6">
                    <h4><strong>Konfirmasi di atas jam 21.00 atau di Hari Libur?</strong></h4>
                    <p>Untuk pembayaran yang dilakukan di atas jam 21.00 WIB atau pada hari libur di atas jam 17.00 WIB, silahkan upload bukti transfer melalui form dibawah ini agar pembayaran Anda dapat diproses segera.</p>
                    <h5><strong>Bukti Pembayaran</strong></h5>
                    <span class="btn btn-circle btn-default btn-file">
                    <span class="fileinput-new"><i class="fa fa-cloud-upload"></i> Upload Bukti Pembayaran </span>
                    <span class="fileinput-exists"> </span>
                    <input type="hidden"><input type="file" name="bukti" onchange="preview_image(event,'bukti_pembayaran')"> </span>
                    <img class="preview" id="bukti_pembayaran" />
                    <div class="form-group" style="padding-top: 50px;">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn green btn-outline btn-circle btn-lg"><i id="icon" class="fa fa-send"></i> Kirim</button>
                        </div>
                    </div>
                </div>
              </form>
            </div>
        </div>
        <div class="clearfix margin-bottom-20"> </div>
    </div>
 </div>
</div>
@endsection
@push('page-plugin-scripts')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.number.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
@endpush

@push('page-scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#datetimepicker1').datetimepicker({
            locale: 'id'
        });
        $('#jumlah').number(true,2,',','.');
        $('.text-danger').hide();
    });
</script>
@endpush
