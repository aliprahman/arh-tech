@extends('layouts.app')

@push('page-plugin-styles')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
    .table th{
        text-align: center;
    }
</style>
@endpush

@section('title')
Daftar Pendanaan
@endsection

@section('crumbs')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/dashboard') }}">Pemodal</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ url('/pemodal/daftar') }}">Daftar Pinjaman</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Cicilan Pinjaman</span>
        </li>
    </ul>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="portlet light ">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-docs"></i>
                <span class="caption-subject bold uppercase">Cicilan Pinjaman</span>
            </div>
            <div class="actions">
                <a class="btn btn-circle btn-default" href="javascript:;" id="print_cicilan_pdf">
                    <i class="fa fa-print"></i> Print to PDF
                </a>
            </div>
        </div>
        <div class="portlet-body">
                    <center><h1>Cicilan Pinjaman</h1></center>
                    <br><br>
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-hover table-striped">
                                <tr>
                                    <td> Waktu Transakasi</td>
                                    <td><strong>:</strong></td>
                                    <td><strong></strong></td>
                                </tr>
                                <tr>
                                    <td> Kode Pinjam</td>
                                    <td><strong>:</strong></td>
                                    <td><strong> <a href="{{ url('/pemodal/portofolio/'.$pinjaman->id) }}">{{ $pinjaman->kode }}</a></strong></td>
                                </tr>
                                <tr>
                                    <td> Nilai Pinjaman </td>
                                    <td><strong>:</strong></td>
                                    <td><strong> Rp.<font class="pull-right">{{ number_format($pinjaman->jumlah_pinjaman,2,',','.') }} </font></strong></td>
                                </tr>
                                <tr>
                                    <td> Pendanaan</td>
                                    <td><strong>:</strong></td>
                                    <td><strong> Rp.<font class="pull-right">{{ number_format($total_pendanaan,2,',','.') }}</font></strong></td>
                                </tr>
                                <tr>
                                    <td> Waktu </td>
                                    <td><strong>:</strong></td>
                                    <td><strong> {{ $pinjaman->waktu_pinjaman }} Bulan</strong></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table" style="margin-top: 70px;">

                                <tr>
                                    <td><h3>TOTAL CICILAN</h3></td>
                                    <td>:</td>
                                    <td><h3>Rp.<font class="pull-right">{{ number_format($total_cicilan,2,',','.') }}</font></h3></td>
                                </tr>
                                 <tr>
                                    <td><h3>SISA CICILAN</h3></td>
                                    <td>:</td>
                                    <td><h3>Rp.<font class="pull-right">{{ number_format($sisa_cicilan,2,',','.') }}</font></h3></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                          <table class="table table-striped table-bordered table-hover table-checkable order-column" id="data">
                              <thead>
                                  <tr>
                                      <th> NO </th>
                                      <th> Waktu Transakasi</th>
                                      <th> Kode Transakasi</th>
                                      <th> Pendapatan </th>
                                      <th> Periode </th>
                                      <th> Status</th>
                                  </tr>
                              </thead>
                              <tbody>
                                @php $no=1; @endphp
                                @foreach ($cicilan as $key)
                                  <tr>
                                    <td align="center">{{ $no++ }}</td>
                                    <td align="left">
                                      @if ($key->tanggal != "")
                                        {{ date('d F Y',strtotime($key->tanggal)) }}
                                      @endif
                                    </td>
                                    <td>{{ $key->ref }}</td>
                                    <td align="right">
                                      @if (!empty($key->pendapatan))
                                        Rp. {{ number_format($key->pendapatan,2,',','.') }}
                                      @endif
                                    </td>
                                    <td align="left">
                                      @if ($key->jatuh_tempo != "")
                                        {{ date('d F Y',strtotime($key->jatuh_tempo)) }}
                                      @endif
                                    </td>
                                    <td align="center">{{ title_case($key->status) }}</td>
                                  </tr>
                                @endforeach
                              </tbody>
                          </table>
                          <br>
                        </div>
                      </div>
                  <div class="row">
                      <div class="col-md-12">
                          <center>
                              <a href="{{ URL::previous() }}" class="btn btn-circle btn-default"><i class="icon-refresh"></i> Kembali</a>
                          </center>
                      </div>
                  </div>
        </div>
        <div class="clearfix margin-bottom-20"> </div>
    </div>
  </div>
</div>
<div class="modal fade" id="printModal" tabindex="-1" role="dialog" aria-labelledby="printModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="contactModalLabel">Print Preview</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button id="printModal-print-btn" type="button" class="btn btn-primary"><i class="fa fa-print"></i> Print</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('page-plugin-scripts')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
@endpush

@push('page-scripts')
<script src="{{ asset('assets/pages/scripts/table-datatables-managed.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#data').DataTable();
    $('#print_cicilan_pdf').click(function(){
      var iframeHeight = $(window).height() - 220;
      var url = '{{ url("pemodal/cicilan/export/".$pinjaman->id) }}';

      $('#printModal .modal-body').html('<iframe id="printModal-iframe" src="'+url+'" style="width: 100%; height: ' + iframeHeight + 'px; border: 1px solid #e5e5e5;background-image: url({{asset('assets/global/img/input-spinner.gif')}}); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>');
      $('#printModal-print-btn').attr('disabled', 'disabled');
      $('#printModal-iframe').on('load', function () {
          $('#printModal-print-btn').removeAttr('disabled');
      });
      $('#printModal').modal('show');
    });
    $('#printModal-print-btn').on('click', function () {
        $('#printModal-iframe').get(0).contentWindow.print();
    });
} );
</script>
@endpush
