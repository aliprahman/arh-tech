{{-- Modal --}}
<div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
						&times;
					</span>
				</button>
                <h5 class="modal-title" id="exampleModalLabel">
					<i class="fa fa-lock"></i>
					Ubah Password
				</h5>
            </div>
			<div class="modal-body">
			<form class="m-form m-form--fit m-form--label-align-right" id="change">
				{{ csrf_field() }}
				<input type="hidden" name="id" {{ Auth::user()->id }}>
				<div class="m-portlet__body">
					<div class="form-group m-form__group">
						<label for="">
							Password Lama
						</label>
						<input type="password" name="password" class="form-control m-input m-input--circle textbox_2" id="old" placeholder="Password Lama">
						<span class="text-danger" ></span>
					</div>
					<div class="form-group m-form__group">
						<label for="">
							Password Baru
						</label>
						<input type="password" name="new" class="form-control m-input m-input--circle textbox_2" id="new"  placeholder="Password Baru">
 						 <span class="text-danger" ></span>
					</div>
					<div class="form-group m-form__group">
						<label for="">
							Ulangi Password
						</label>
						<input type="password" name="repeat" class="form-control m-input m-input--circle textbox_2" id="repeat"  placeholder="Ulangi Password">
						 <span class="text-danger" ></span>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn green btn-circle btn-outline ">
					<i class="fa fa-save"></i>  Simpan
				</button>
				<button type="reset" id="reset" class="btn red btn-circle btn-outline">
					<i class="fa fa-refresh"></i>  Reset
				</button>
			</div>
			</form>
		</div>
	</div>
</div>