@extends('layouts.app')

@push('page-plugin-styles')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
  .table thead tr th{
    white-space: pre-wrap;
    vertical-align: middle;
    text-align: center;
    padding-top: 20px;
    padding-bottom: 20px;
  }
  .table tbody tr td{
    vertical-align: middle;
  }

  #data th, #data td {
    font-size: 13px;
  }
</style>
@endpush

@section('title')
History Transaksi
@endsection

@section('crumbs')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/dashboard') }}">Pemodal</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>History Transakasi</span>
        </li>
    </ul>
@endsection

@section('content')
@php
function rupiah($angka)
{
 $rupiah = number_format($angka,2,',','.');
 return $rupiah;
}
@endphp
<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
        <div class="portlet-title">
            <div class="actions">
                <a class="btn btn-circle btn-default" href="javascript:;" id="print_pendanaan_pdf">
                    <i class="fa fa-print"></i> Print to PDF
                </a>
            </div>
        </div>
         <div class="portlet-body">
            <div class="row">
                <div class="col-md-12">
                    <center><h1>HISTORY TRANSAKSI</h1></center>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-8">
                    <table class="table">
                    <tr>
                        <td>Bulan</td>
                        <td>
                            <div id="datepicker" class="input-group input-medium date date-picker pull-right">
                                <input type="text" class="form-control" readonly="" value="{{ date('F-Y') }}" id="filter">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </td>
                    </tr>
                </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <table class="table">
                      <tbody>
                        <tr>
                          <td>&nbsp;Saldo Awal</td>
                          <td><strong>&nbsp;Rp.<font class="pull-right"><span id="saldo_awal"></span></font></strong></td>
                          <td>&nbsp;Saldo akhir&nbsp;</td>
                          <td><strong>&nbsp;Rp.<font class="pull-right"><span id="saldo_akhir"></span></font></strong></td>
                        </tr>
                        <tr>
                          <td>&nbsp;Jumlah Deposit</td>
                          <td><strong>&nbsp;<span id="jml_deposit"></span></strong></td>
                          <td>&nbsp;Total Deposit</td>
                          <td><strong>&nbsp;Rp.<font class="pull-right"><span id="total_deposit"></span></font></strong></td>
                        </tr>
                          <tr>
                          <td>&nbsp;Jumlah Pendapatan</td>
                          <td><strong>&nbsp;<span id="jml_pendanaan"></span></strong></td>
                          <td>&nbsp;Total Pendanaan</td>
                          <td><strong>&nbsp;Rp.<font class="pull-right"><span id="total_pendanaan"></span></font></strong></td>
                        </tr>
                        <tr>
                          <td>&nbsp;Jumlah Penarikan</td>
                          <td><strong>&nbsp;<span id="jml_penarikan"></span></strong></td>
                          <td>&nbsp;Total Penarikan</td>
                          <td><strong>&nbsp;Rp.<font class="pull-right"><span id="total_penarikan"></span></font></strong></td>
                        </tr>
                      </tbody>
                    </table>
                </div>
            </div>
         </div>
        </div>
    </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-file-text-o"></i>
                <span class="caption-subject bold uppercase">History Pendanaan</span>
            </div>
        </div>
        <div class="portlet-body">

                    <table class="table table-striped table-bordered table-hover order-column" id="data">
                        <thead>
                            <tr>
                                <th> Tanggal </th>
                                <th> Keterangan</th>
                                <th> Debit(Rp) </th>
                                <th> Kredit(Rp) </th>
                                <th> Saldo(Rp) </th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                <br>

            <div class="clearfix margin-bottom-20"> </div>
        </div>
      </div>
    </div>
 </div>
</div>

<div class="modal fade" id="printModal" tabindex="-1" role="dialog" aria-labelledby="printModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="contactModalLabel">Print Preview</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button id="printModal-print-btn" type="button" class="btn btn-primary"><i class="fa fa-print"></i> Print</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('page-plugin-scripts')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
@endpush

@push('page-scripts')
  <script type="text/javascript">
    $(document).ready(function() {
        getResume('{{ date('F-Y') }}');

        var table = $('#data').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ url('/pemodal/history/data') }}',
                type: 'post',
                data: function(d){
                  d.periode = $('#filter').val()
                }
            },
            columns: [
                {data: "tanggal", name: 'tanggal'},
                {data: "keterangan", name: 'keterangan'},
                {data: "debit", name: 'debit'},
                {data: "kredit", name: 'kredit'},
                {data: "saldo", name: 'saldo'},
            ],
            rowCallback: function(row,data,index){
                $('td:eq(2)',row).html('<span class="pull-left">Rp.</span><span class="pull-right">'+numeral(data.debit).format('0,0')+'</span>');
                $('td:eq(3)',row).html('<span class="pull-left">Rp.</span><span class="pull-right">'+numeral(data.kredit).format('0,0')+'</span>');
                $('td:eq(4)',row).html('<span class="pull-left">Rp.</span><span class="pull-right">'+numeral(data.saldo).format('0,0')+'</span>');
            }
        });

        $('#datepicker').datepicker( {
          autoclose: true,
          format: "MM-yyyy",
          viewMode: "months",
          minViewMode: "months"
        }).on('changeDate', function() {
          getResume($('#filter').val());
          table.ajax.reload();
        });

        $('#print_pendanaan_pdf').click(function () {
            var iframeHeight = $(window).height() - 220;
            var periode = $('#filter').val();
            var url = '{{ url("pemodal/history/pdf") }}?periode='+periode;

            $('#printModal .modal-body').html('<iframe id="printModal-iframe" src="'+url+'" style="width: 100%; height: ' + iframeHeight + 'px; border: 1px solid #e5e5e5;background-image: url({{asset('assets/global/img/input-spinner.gif')}}); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>');
            $('#printModal-print-btn').attr('disabled', 'disabled');
            $('#printModal-iframe').on('load', function () {
                $('#printModal-print-btn').removeAttr('disabled');
            });
            $('#printModal').modal('show');
        });
        $('#printModal-print-btn').on('click', function () {
            $('#printModal-iframe').get(0).contentWindow.print();
        });
    });
    function getResume(periode) {
      $.ajax({
        type: 'GET',
        url: '{{ url('pemodal/history/resume') }}'+'/'+periode,
        success: function (data) {
          if(data != ""){
            console.log(data);
            $('#jml_deposit').text(data.jml_deposit);
            $('#jml_penarikan').text(data.jml_penarikan);
            $('#jml_pendanaan').text(data.jml_pendanaan);
            $('#saldo_awal').text(numeral(data.saldo_awal).format('0,0'));
            $('#saldo_akhir').text(numeral(data.saldo_akhir).format('0,0'));
            $('#total_deposit').text(numeral(data.total_deposit).format('0,0'));
            $('#total_penarikan').text(numeral(data.total_penarikan).format('0,0'));
            $('#total_pendanaan').text(numeral(data.total_pendanaan).format('0,0'));
          }
        }
      });
    }
  </script>
  <script src="{{ asset('/assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
@endpush
