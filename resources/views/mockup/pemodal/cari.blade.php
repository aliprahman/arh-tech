@extends('layouts.app')

@push('page-plugin-styles')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('/assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">

  <style type="text/css">
    .table thead tr th{
      white-space: pre-wrap;
      vertical-align: middle;
      text-align: center;
      padding-top: 20px;
      padding-bottom: 20px;
    }
    .table tbody tr td{
      vertical-align: middle;
    }

    #data th, #data td {
      font-size: 13px;
    }
  </style>
@endpush

@section('title')
  Cari Pinjaman
@endsection

@section('crumbs')
  <ul class="page-breadcrumb breadcrumb">
    <li>
      <a href="{{ url('/dashboard') }}">Pemodal</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Cari Pinjaman</span>
    </li>
  </ul>
@endsection

@section('content')
  @php
  function rupiah($angka)
  {
   $rupiah = number_format($angka,2,',','.');
   return $rupiah;
  }
  @endphp
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption">
            <i class="icon-magnifier"></i>
            <span class="caption-subject bold uppercase">Cari Pinjaman</span>
          </div>
          <span class="bold uppercase" style="float: right; font-size: 16px" id="saldo_label">Saldo : Rp. {{ number_format(auth()->user()->pemodal->saldo,0,',','.') }}</span>
        </div>
        <div class="portlet-body" id="body_konfirmasi"></div>
        <div class="portlet-body" id="body_pendanaan">
          <div class="row">
            <div class="col-md-12">
              @if ($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
              @endif
              @if(session('success') != "")
                <div class="alert alert-success" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  {{ session('success') }}
                </div>
              @endif
              @if(session('error') != "")
                <div class="alert alert-danger" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  {{ session('error') }}
                </div>
              @endif
            </div>
          </div>
          <form method="post" action="{{ url('/pemodal/konfirm') }}" id="form_pendanaan">
            {{ csrf_field() }}
            <center><h1>Daftar Pinjaman</h1></center>
            <table class="table table-striped table-bordered table-hover" id="data">
              <thead>
                <tr>
                  <th> No </th>
                  <th> Kode Pinjaman</th>
                  <th> Nilai Pinjaman</th>
                  <th> Waktu </th>
                  <th> Progres Pendanaan </th>
                  <th> Jumlah Tersisa<br>Waktu Tersisa</th>
                  <th> Order </th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
            </br>
            <div class="col-md-5 col-md-offset-7" id="hidden_input">
              <table class="table">
                <tr>
                  <td>Total Nilai Pendanaan</td>
                  <td>:</td>
                  <td>Rp.<font class="pull-right"><span id="total_nilai_pendanaan_label">0</span></font></td>
                </tr>
                <tr>
                  <td>Jumlah Pendanaan</td>
                  <td>:</td>
                  <td><font class="pull-right"><span id="total_pinjaman_label">0</span> Pinjaman</font></td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td>
                    <br>
                      <p class="pull-right">
                        <button type="reset" class="btn btn-circle default" onclick="reset_pendanaan()" id="reset_dana"><i class="fa fa-refresh"></i> Reset</button>
                        <a id="confirm_submit" class="btn yellow-gold btn-outline btn-circle active"><i class="fa fa-pencil-square"></i> Konfirmasi</a>
                      </p>
                  </td>
                </tr>
              </table>
              <input type="hidden" name="total_nilai_pendanaan" id="total_nilai_pendanaan" value="0">
              <input type="hidden" name="total_pinjaman" id="total_pinjaman" value="0">
              <input type="hidden" name="total_saldo" id="total_saldo" value="{{ auth()->user()->pemodal->saldo }}" />
            </div>
          </form>
          <div class="clearfix margin-bottom-20"> </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@push('page-plugin-scripts')
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
  <script src="{{ asset('js/jquery.number.min.js') }}" type="text/javascript"></script>
  <script src="https://unpkg.com/sweetalert2@7.19.1/dist/sweetalert2.all.js" type="text/javascript"></script>
@endpush

@push('page-scripts')
  <script type="text/javascript">
    var table = "";
    $(document).ready(function() {
      table = $('#data').DataTable({
        processing: true,
        severSide: true,
        ajax: "{{ url('/data/pinjaman') }}",
        columns:[
          {data: "DT_Row_Index", orderable: false, searchable: false},
          {data: 'kode',name: 'kode',className: 'dt-center'},
          {data: 'jumlah_pinjaman',name: 'jumlah_pinjaman'},
          {data: 'waktu_pinjaman',name: 'waktu_pinjaman',className: 'dt-center'},
          {data: 'progres',name: 'progres', orderable: false},
          {data: 'jumlah_tersisa', name: 'jumlah_tersisa',  orderable: false},
          {data: 'order',name: 'order', className: 'dt-center', orderable: false, searchable: false},
        ],
        rowCallback: function(row,data,index){
          // nomer
          $('td:eq(0)', row).html( '<center>'+data.DT_Row_Index+'</center>' );
          // kode pinjaman
          $('td:eq(1)', row).html('<a href="{!! url('pemodal/portofolio') !!}/'+data.id+'" target="_blank">'+data.kode+'</a>');
          // jumlah_pinjaman
          $('td:eq(2)',row).html('Rp.<span class="pull-right">'+numeral(data.jumlah_pinjaman).format('0,0')+'</span>');
          // progres
          $('td:eq(4)',row).html('<div class="easy-pie-chart">\n' +
              '                                <div class="number visits" data-percent="'+data.progres+'%">\n' +
              '                                    <span>'+data.progres+'</span>%\n' +
              '                                </div>\n' +
              '                            </div>');
          // sisa waktu
          var disabled = '';
          var label = '';
          if(moment().isAfter(data.tanggal_mulai_funding) && moment().isBefore(data.tanggal_berakhir_funding)){
            interval = moment(data.tanggal_berakhir_funding).diff(moment(),'days');
            label = '<h5>\n' +
              '        <span class="label bg-red"><strong>  '+interval+' Hari </strong></span>\n' +
              '     </h5>';
            }else{
              if(data.tanggal_mulai_funding == null || moment().isBefore(data.tanggal_mulai_funding)){
                ket = 'Belum dimulai';
              }else{
                ket = 'Telah Selesai'
              }
              label = '<h5>\n' +
              '      <span class="label bg-red"><strong>  '+ket+' </strong></span>\n' +
              '    </h5>';
              disabled = 'disabled';
            }
            if(data.jumlah_tersisa <= 0){
              disabled = 'disabled';
            }
            // jumlah tersisa
            $('td:eq(5)',row).html('Rp.<span class="pull-right">'+numeral(data.jumlah_tersisa).format('0,0')+'</span><input type="hidden" id="jumlah_tersisa_'+data.id+'" value="'+data.jumlah_tersisa+'" /><br>'+label);

            // form pendanaan
            var form = '<div class="input-group" style="text-align:left; width: 200px">\n' +
            '    <input type="text" class="form-control order rupiah" min="0" autocomplete="off" name="order[]" id="pendanaan_'+data.id+'" '+disabled+'>\n' +
            '    <span class="input-group-btn">\n' +
            '        <button type="button" class="btn green-jungle" id="submit_'+data.id+'" onclick="submit_data('+data.id+')" '+disabled+'>\n' +
            '            <i class="fa fa-plus"></i>' +
            '        </button>\n' +
            '    </span>\n' +
            '</div><span class="notifi" id="notif_'+data.id+'" style="color: red"></span>';
            $('td:eq(6)',row).html(form);
          }
        });
      table.on('draw',function () {

          $('.easy-pie-chart .number.visits').easyPieChart( {
              animate: 1e3, size: 75, lineWidth: 3
          });
          $('.rupiah').number(true,2,',','.');
      });
      $('#confirm_submit').click(function(){
        swal({
          title: 'Konfirmasi',
          text: "Apakah anda yakin akan melakukan pendanaan ini ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, Lanjutkan',
          cancelButtonText: 'Batal'
        }).then((result) => {
          if (result.value) {
            $("#form_pendanaan").trigger("submit");
          }
        })
      });
      $("#form_pendanaan").submit(function(event){
        event.preventDefault();
        var saldo = parseInt($('#total_saldo').val());
        var dana = parseInt($('#total_nilai_pendanaan').val());
        var sisa_saldo = parseInt(saldo) - parseInt(dana);
        if(dana > saldo){
          swal(
            'Gagal!',
            'Maaf saldo anda tidak mencukupi',
            'error'
          );
        }else if(dana < 1){
          swal(
            'Gagal!',
            'Maaf silahkan lakukan pendanaan terlebih dahulu',
            'error'
          );
        }else{
          $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),
            success: function(result){
              $('#body_konfirmasi').html(result);
            },
            complete: function(){
              $('.chart').easyPieChart();
              reset_pendanaan();
              $('#total_saldo').val(sisa_saldo);
              $('#saldo_label').text('Saldo : Rp. '+numeral(sisa_saldo).format('0,0'));
              setTimeout(function(){ $('#body_konfirmasi').empty() }, 3000);
              table.ajax.reload();
            }
          });
        }
      });
    });

    function submit_data(id) {
      var val = $('#pendanaan_'+id).val();
      var jumlah_tersisa = $('#jumlah_tersisa_'+id).val();
      if(parseInt(val) > parseInt(jumlah_tersisa)){
        $('#notif_'+id).text('Pendanaan Terlalu Besar');
      }else if(parseInt(val) > 0){
        var input_nilai = '<input type="hidden" class="nilai_pendanaan" name="dana_pinjaman[]" id="dana_pinjaman_'+id+'" value="'+val+'">';
        var input_id = '<input type="hidden" class="list_id_pinjaman" name="id_pinjaman[]" id="id_pinjaman_'+id+'" value="'+id+'">';
        $('#hidden_input').append(input_nilai);
        $('#hidden_input').append(input_id);
        $('#notif_'+id).text('');
        $('#submit_'+id).attr('disabled',true);
        $('#pendanaan_'+id).attr('disabled',true);
        calculate();
      }
    }

    function calculate() {
      var total = 0;
      $('.nilai_pendanaan').each(function () {
          total = parseInt($(this).val()) + parseInt(total);
      });
      var jml = $('.nilai_pendanaan').length;

      $('#total_nilai_pendanaan').val(total);
      $('#total_pinjaman').val(jml);

      $('#total_nilai_pendanaan_label').text(numeral(total).format('0,0'));
      $('#total_pinjaman_label').text(jml);
    }

    function reset_pendanaan() {
      $('.nilai_pendanaan').remove();
      $('.list_id_pinjaman').remove();
      calculate();
      $('.notifi').text('');
      table.ajax.reload();
    }
  </script>
@endpush
