@extends('layouts.app')

@push('page-plugin-styles')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/assets/global/plugins/bootstrap-toastr/toastr.min.css') }}" rel="stylesheet" type="text/css">
<style type="text/css">
  .table thead tr th{
    white-space: pre-wrap;
    vertical-align: middle;
    text-align: center;
    padding-top: 20px;
    padding-bottom: 20px;
  }
  .table tbody tr td{
    vertical-align: middle;
    text-align: center;
  }

  #data th, #data td {
    font-size: 13px;
  }
</style>
@endpush

@section('title')
Penarikan
@endsection

@section('crumbs')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/dashboard') }}">Pemodal</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Penarikan</span>
        </li>
    </ul>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
         <div class="portlet-body">
            <div class="row">
                <div class="col-md-12">
                    <center><h1>PENARIKAN</h1></center>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="portlet light">
                        <div class="portlet-body">
                            {{-- Begin form --}}
                            <form action="" method="" id="penarikan">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="nama" class="col-md-5 control-label"><font class="pull-left">Nama Bank</font></label>
                                    <div class="col-md-7">
                                        <input type="nama" class="form-control" disabled="" value="{{ Auth::user()->pemodal->akun_bank->bank->nama }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="cabang" class="col-md-5 control-label"><font class="pull-left">Cabang</font></label>
                                    <div class="col-md-7">
                                        <input type="text" name="cabang" class="form-control textbox">
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nomor" class="col-md-5 control-label"><font class="pull-left">Nomor Rekening</font></label>
                                    <div class="col-md-7">
                                        <input type="nama" class="form-control" disabled="" value="{{ Auth::user()->pemodal->akun_bank->no_akun_bank }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="portlet light">
                        <div class="portlet-body">
                             <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="pemilik" class="col-md-5 control-label"><font class="pull-left">Nama Pemilik Rekening</font></label>
                                    <div class="col-md-7">
                                        <input type="pemilik" class="form-control" value="{{ Auth::user()->name }}" disabled="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nominal" class="col-md-5 control-label"><font class="pull-left">Nominal (IDR)</font></label>
                                    <div class="col-md-7">
                                        <div class="input-group">
                                            <span class="input-group-addon">Rp.</span>
                                            <input type="text" name="jumlah" class="form-control textbox" id="nominal" />
                                        </div>
                                        <span class="text-danger" id="alert_d"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nomor" class="col-md-5 control-label"><font class="pull-left">Keterangan</font></label>
                                    <div class="col-md-7">
                                       <textarea class="form-control textbox" name="keterangan" id="ket"></textarea>
                                       <span class="text-danger" id="alert_k"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <center><p>Mohon menunggu 1-2 hari kerja untuk proses verifikasi.<br>Biaya transfer ke akun selain Bank Sinarmas akan dikenakan biaya transfer sebesar 5.000 - 7.500 rupiah oleh sistem secara otomatis.<br>Kami akan mengirimkan email konfirmasi kepada Anda setelah proses verifikasi selesai.</p>
                    <p>
                        <label class="mt-checkbox">
                            <input type="checkbox" id="setuju" value="setuju" name="setuju"> Saya telah membaca dan setuju dengan Syarat dan Ketentuan di atas.
                            <span></span>
                        </label>
                        <br>
                        <span class="text-danger" id="alert_s"></span>
                    </p>
                    <div>
                        <button type="submit" class="btn green btn-outline btn-circle" id="submit"><i id="icon" class="fa fa-send"></i> Kirim</button>
                    </div>
                    </center>
                {{-- END FORM --}}
                </form>
                </div>
            </div>
         </div>
        </div>
    </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-file-text-o"></i>
                <span class="caption-subject bold uppercase">History Penarikan</span>
            </div>
        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="data">
                <thead>
                    <tr>
                        <th> NO </th>
                        <th> Tanggal Order</th>
                        <th> No Transaksi</th>
                        <th> Bank </th>
                        <th> Nomor Rekening</th>
                        <th> Jumlah Transfer </th>
                        <th> Status</th>
                    </tr>
                </thead>
                <tbody>
                   {{-- --}}
                </tbody>
            </table>
                <br>

            <div class="clearfix margin-bottom-20"> </div>
        </div>
      </div>
    </div>
 </div>
</div>

@endsection
@push('page-plugin-scripts')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/ui-toastr.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-toastr/toastr.min.js') }}" type="text/javascript"></script>
@endpush

@push('page-scripts')
<script src="{{ asset('assets/pages/scripts/table-datatables-managed.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.number.min.js') }}" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script type="text/javascript">

var table =    $('#data').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ url('/pemodal/list_penarikan') }}',
                    type: 'get'
                },
                columns: [
                    {data: "DT_Row_Index", orderable: false, searchable: false},
                    {data: "created_at", name: 'created_at'},
                    {data: "no_transaksi", name: 'no_transaksi'},
                    {data: "nama_bank", name: 'nama_bank'},
                    {data: "rekening", name: 'rekening'},
                    {data: "jumlah", name: 'jumlah'},
                    {data: "status", name: 'status'},
                ],
                rowCallback: function(row,data,index){
                    // nomer
                    $('td:eq(0)', row).html( '<center>'+data.DT_Row_Index+'</center>' );
                    // jumlah_pinjaman
                    $('td:eq(5)',row).html('<span class="pull-left">Rp.</span><span class="pull-right">'+numeral(data.jumlah).format('0,0')+'</span>');
                }
            });
    $(document).ready(function(){
        $('#nominal').number(true,2,',','.');
        $('.text-danger').hide();
        $('.textbox').each(function(){
            $(this).blur(function(){ //blur function itu dijalankan saat element kehilangan fokus
                if (! $(this).val()){ //this mengacu pada text box yang sedang fokus
                    return get_error_text(this); //function get_error_text ada di bawah
                } else {
                    $(this).removeClass('no-valid');
                    $(this).parent().find('.text-danger').hide();//cari element dengan class has-error dari element induk text yang sedang focus
                    $(this).closest('div').removeClass('has-error');
                    $(this).closest('div').addClass('has-success');
                }
            });
        });
        $('#nominal').blur(function(){
            var nomi= $(this).val();
            var len= nomi.length;
            if(len == 0){
                $('#alert_d').show().text('Kolom tidak boleh kosong');
                $('#nominal').closest('div').removeClass('has-success');
            }
            if(len>0){
                $('#nominal').closest('div').addClass('has-success');
                $('#alert_d').hide();
            }
        });

        $('input[name="setuju"]').change(function(){
                $('#alert_s').hide();
                $('#alert_s').removeClass('no-valid');
        });
        $('#penarikan').submit(function(e){
            e.preventDefault();
            var valid=true;
            $(this).find('.textbox').each(function(){
                if (! $(this).val()){
                    $('#nominal').closest('div').addClass('has-error');
                    get_error_text(this);
                    valid = false;
                }
                if ($(this).hasClass('no-valid')){
                    valid = false;

                }
            });
            if(this.setuju.checked==false) {
                $('#alert_s').show().text('Silahkan klik menyetujui.');
                $('#alert_s').addClass('no-valid');
                valid = false;
            }else {
                $('#alert_s').hide();
                $('#alert_s').removeClass('no-valid');
                valid = true;
            }
            if (!$('#nominal').val()) {
                $('#alert_d').show().text('Kolom tidak boleh kosong');
                $('#nominal').closest('div').removeClass('has-success');
                $('#nominal').closest('div').addClass('has-error');
                valid = false;

            }
            if (!$('#ket').val()) {
                $('#alert_k').show().text('Kolom tidak boleh kosong');
                $('#ket').closest('div').removeClass('has-success');
                $('#ket').closest('div').addClass('has-error');
                valid = false;

            }

             if (valid){
                    $('#submit').prop('disabled', true);
                    $('#icon').removeClass('fa fa-send');
                    $('#icon').addClass('fa fa-circle-o-notch fa-spin');
                    $.ajax({
                        url: $('meta[name="base_url"]').attr('content')+"/pemodal/order/penarikan",
                        type: "POST",
                        data: $('#penarikan').serialize(), //serialize() untuk mengambil semua data di dalam form
                        success: function(){
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                            };
                        toastr.success("Penarikan dilakukan, menunggu verifikasi.","Berhasil");
                        $('#penarikan')[0].reset();
                        $('#submit').prop('disabled', false);
                        $('#icon').removeClass('fa fa-circle-o-notch fa-spin ');
                        $('#icon').addClass('fa fa-send');
                        $('.text-danger').hide();
                        $('.textbox').closest('div').removeClass('has-success');
                        $('.textbox').closest('div').removeClass('has-error');
                        table.ajax.reload();
                        },

                        error: function (xhr, ajaxOptions, thrownError) {
                            toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                            };
                        toastr.error("Mohon cek koneksi.", "Oops..!");
                        $('#penarikan')[0].reset();
                        $('#submit').prop('disabled', false);
                        $('#icon').removeClass('fa fa-circle-o-notch fa-spin ');
                        $('#icon').addClass('fa fa-send');
                        $('.text-danger').hide();
                        $('.textbox').closest('div').removeClass('has-success');
                        $('.textbox').closest('div').removeClass('has-error');
                        table.ajax.reload();
                        }
                    });
                }

        });

    });
    //menerapkan gaya validasi form bootstrap saat terjadi eror
    function apply_feedback_error(textbox){
        $(textbox).addClass('no-valid'); //menambah class no valid
        $(textbox).parent().find('.text-danger').show();
        $(textbox).closest('div').removeClass('has-success');
        $(textbox).closest('div').addClass('has-error');
    }

    //untuk mendapat eror teks saat textbox kosong, digunakan saat submit form dan blur fungsi
    function get_error_text(textbox){
        $(textbox).parent().find('.text-danger').text("Kolom tidak boleh kosong.");
        return apply_feedback_error(textbox);
    }


</script>
@endpush
