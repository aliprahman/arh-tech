@extends('layouts.app')

@push('page-plugin-styles')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
  .table thead tr th{
    white-space: pre-wrap;
    vertical-align: middle;
    text-align: center;
    padding-top: 20px;
    padding-bottom: 20px;
  }
  .table tbody tr td{
    vertical-align: middle;
  }

  #data th, #data td {
    font-size: 13px;
  }
</style>
@endpush

@section('title')
Portofolio
@endsection

@section('crumbs')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/dashboard') }}">Pemodal</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Portofolio Peminjam</span>
        </li>
    </ul>
@endsection

@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <a class="" href="{{ URL::previous() }}"><i class="fa fa-arrow-left"></i></a>
                <span class="caption-subject font-dark bold uppercase">Portofolio Peminjam</span>
            </div>
        </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12">
                        <center><h1>Portofolio Peminjaman</h1><h6>KODE PINJAMAN : {{ $pinjaman->kode }}</h6></center>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                      <b>Data Pinjaman</b>
                      <table class="table table-hover">
                        <tr>
                          <td>Jumlah Pinjaman</td>
                          <td>: Rp. {{ number_format($pinjaman->jumlah_pinjaman,2,',','.') }}</td>
                        </tr>
                        <tr>
                          <td>Lama Pinjaman</td>
                          <td>: {{ $pinjaman->waktu_pinjaman }} Bulan</td>
                        </tr>
                      </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                      <b>Data Peminjam</b>
                      <table class="table table-hover">
                        <tr>
                          <td style="width: 30%">Jenis Kelamin</td>
                          <td style="width: 70%">:
                            @isset($pinjaman->detail_peminjam['biodata']['jenis_kelamin'])
                              {{ $pinjaman->detail_peminjam['biodata']['jenis_kelamin'] }}
                            @endisset
                            </td>
                        </tr>
                        <tr>
                          <td>Usia</td>
                          <td>:
                            @isset($pinjaman->detail_peminjam['biodata']['tanggal_lahir'])
                              {{ \Carbon\Carbon::parse($pinjaman->detail_peminjam['biodata']['tanggal_lahir'])->diff(\Carbon\Carbon::now())->format('%y')." tahun" }}
                            @endisset
                          </td>
                        </tr>
                        <tr>
                          <td>Alamat</td>
                          <td>:
                            @isset($pinjaman->detail_peminjam['biodata']['alamat']['alamat_lengkap'])
                              {{ $pinjaman->detail_peminjam['biodata']['alamat']['alamat_lengkap'] }}
                            @endisset
                            @isset($pinjaman->detail_peminjam['biodata']['alamat']['desa'])
                              {{ $pinjaman->detail_peminjam['biodata']['alamat']['desa'] }}
                            @endisset
                            @isset($pinjaman->detail_peminjam['biodata']['alamat']['kecamatan'])
                              {{ $pinjaman->detail_peminjam['biodata']['alamat']['kecamatan'] }}
                            @endisset
                            @isset($pinjaman->detail_peminjam['biodata']['alamat']['kota'])
                              {{ $pinjaman->detail_peminjam['biodata']['alamat']['kota'] }}
                            @endisset
                            @isset($pinjaman->detail_peminjam['biodata']['alamat']['provinsi'])
                              {{ $pinjaman->detail_peminjam['biodata']['alamat']['provinsi'] }}
                            @endisset
                          </td>
                        </tr>
                        <tr>
                          <td>Status Tempat Tinggal</td>
                          <td>:
                            @isset($pinjaman->detail_peminjam['biodata']['kepemilikah_rumah'])
                              {{ $pinjaman->detail_peminjam['biodata']['kepemilikah_rumah'] }}
                            @endisset
                        </tr>
                        <tr>
                          <td>Pekerjaan Suami/Istri</td>
                          <td>:
                            @isset($pinjaman->detail_peminjam['biodata']['pekerjaan_suami_istri'])
                              {{ $pinjaman->detail_peminjam['biodata']['pekerjaan_suami_istri'] }}
                            @endisset
                          </td>
                        </tr>
                        <tr>
                          <td>Pendapatan Suami/Istri</td>
                          <td>: Rp.
                            @isset($pinjaman->detail_peminjam['biodata']['pendapatan_suami_istri'])
                              {{ number_format($pinjaman->detail_peminjam['biodata']['pendapatan_suami_istri'],2,',','.') }}
                            @endisset
                          </td>
                        </tr>
                        <tr>
                          <td>Jumlah Anak</td>
                          <td>:
                            @isset($pinjaman->detail_peminjam['biodata']['jumlah_tanggungan'])
                              {{ $pinjaman->detail_peminjam['biodata']['jumlah_tanggungan'] }} orang
                            @endisset
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div class="col-md-6">
                      <b>Data Pekerjaan Peminjam</b>
                      <table class="table table-hover">
                        <tr>
                          <td style="width: 30%">Jabatan</td>
                          <td style="width: 70%">:
                            @isset($pinjaman->detail_peminjam['biodata']['jabatan'])
                              {{ $pinjaman->detail_peminjam['biodata']['jabatan'] }}
                            @endisset
                          </td>
                        </tr>
                        <tr>
                          <td>Lama Mengajar</td>
                          <td>:
                            @isset($pinjaman->detail_peminjam['biodata']['lama_mengajar'])
                              {{ $pinjaman->detail_peminjam['biodata']['lama_mengajar'] }}
                            @endisset
                          </td>
                        </tr>
                        <tr>
                          <td>Status Pengajar</td>
                          <td>:
                            @isset($pinjaman->detail_peminjam['biodata']['status_pengajar'])
                              {{ $pinjaman->detail_peminjam['biodata']['status_pengajar'] }}
                            @endisset
                          </td>
                        </tr>
                        <tr>
                          <td>Gaji</td>
                          <td>: Rp.
                            @isset($pinjaman->detail_peminjam['biodata']['gaji'])
                              {{ number_format($pinjaman->detail_peminjam['biodata']['gaji'],2,',','.') }}
                            @endisset
                          </td>
                        </tr>
                      </table>
                      <b>Data Tempat Kerja</b>
                      <table class="table table-hover">
                        <tr>
                          <td style="width: 30%">Alamat</td>
                          <td style="width: 70%">:
                            @isset($pinjaman->detail_peminjam['biodata']['alamat_tempat_kerja']['alamat_lengkap'])
                              {{ $pinjaman->detail_peminjam['biodata']['alamat_tempat_kerja']['alamat_lengkap'] }}
                            @endisset
                            @isset($pinjaman->detail_peminjam['biodata']['alamat_tempat_kerja']['desa'])
                              {{ $pinjaman->detail_peminjam['biodata']['alamat_tempat_kerja']['desa'] }}
                            @endisset
                            @isset($pinjaman->detail_peminjam['biodata']['alamat_tempat_kerja']['kecamatan'])
                              {{ $pinjaman->detail_peminjam['biodata']['alamat_tempat_kerja']['kecamatan'] }}
                            @endisset
                            @isset($pinjaman->detail_peminjam['biodata']['alamat_tempat_kerja']['kota'])
                              {{ $pinjaman->detail_peminjam['biodata']['alamat_tempat_kerja']['kota'] }}
                            @endisset
                            @isset($pinjaman->detail_peminjam['biodata']['alamat_tempat_kerja']['provinsi'])
                              {{ $pinjaman->detail_peminjam['biodata']['alamat_tempat_kerja']['provinsi'] }}
                            @endisset
                          </td>
                        </tr>
                        <tr>
                          <td>Jumlah Murid</td>
                          <td>:
                            @isset($pinjaman->detail_peminjam['biodata']['jumlah_murid_tempat_kerja'])
                              {{ $pinjaman->detail_peminjam['biodata']['jumlah_murid_tempat_kerja'] }} orang
                            @endisset
                          </td>
                        </tr>
                        <tr>
                          <td>Lama Berdiri</td>
                          <td>:
                            @isset($pinjaman->detail_peminjam['biodata']['lama_berdiri_tempat_kerja'])
                              {{ $pinjaman->detail_peminjam['biodata']['lama_berdiri_tempat_kerja'] }} tahun
                            @endisset
                          </td>
                        </tr>
                      </table>
                    </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <b>Pendapatan Lain-lain</b>
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th style="width: 50%">Keterangan</th>
                          <th style="width: 50%">Jumlah</th>
                        </tr>
                      </thead>
                      <tbody>
                        @isset($pinjaman->detail_peminjam['keuangan'])
                          @foreach ($pinjaman->detail_peminjam['keuangan']['pendapatan'] as $p)
                            <tr>
                              <td>{{ $p['keterangan'] }}</td>
                              <td>: Rp. {{ number_format($p['jumlah'],2,',','.') }}</td>
                            </tr>
                          @endforeach
                        @endisset
                      </tbody>
                    </table>
                  </div>
                  <div class="col-md-6">
                    <b>Pinjaman Lain-lain</b>
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th style="width: 50%">Keterangan</th>
                          <th style="width: 50%">Jumlah</th>
                        </tr>
                      </thead>
                      <tbody>
                        @isset($pinjaman->detail_peminjam['keuangan'])
                          @foreach ($pinjaman->detail_peminjam['keuangan']['pinjaman'] as $p)
                            <tr>
                              <td>{{ $p['keterangan'] }}</td>
                              <td>: Rp. {{ number_format($p['jumlah'],2,',','.') }}</td>
                            </tr>
                          @endforeach
                        @endisset
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>


@endsection
@push('page-plugin-scripts')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
@endpush

@push('page-scripts')
<script src="{{ asset('assets/pages/scripts/table-datatables-managed.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#data').DataTable();
} );
</script>
@endpush
