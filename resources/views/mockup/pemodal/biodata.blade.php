@extends('layouts.app')

@push('page-plugin-styles')
<link href="{{ asset('/assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/bootstrap-toastr/toastr.min.css') }}" rel="stylesheet" type="text/css">
<style type="text/css">
  .select2 {
   width:100%!important;
   }
</style>
@endpush

@section('title')
Biodata
@endsection

@section('crumbs')
  <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/dashboard') }}">Pemodal</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Biodata Pemodal</span>
        </li>
    </ul>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
       <div class="portlet light portlet-fit">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-user"></i>
                    <span class="caption-subject bold font-dark uppercase ">Biodata<h6></h6></span>
                </div>
                <div class="actions">
                    <a href="#m_modal_1" data-toggle="modal" class="btn btn-circle btn-default">
                        <i class="fa fa-lock"></i> Ubah Password</a>
                </div>
                @include('mockup.pemodal.change_password')
            </div>
        <form class="form-horizontal" role="form" enctype="multipart/form-data" id="pemodal">
            {{ csrf_field() }}
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-6">

                        <div class="portlet light portlet-fit">
                            <div class="portlet-title">
                                <strong>Informasi Data Diri</strong>
                            </div>
                            <div class="portlet-body">
                                    <div class="form-group">
                                        <label class="col-sm-4">Nama</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="nama" value="{{ $user->name }}" class="form-control textbox">
                                            <span class="text-danger" ></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4">Nama Perusahaan</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="n_perusahaan"
                                            @isset($user->pemodal->perusahaan)
                                              value="{{ $user->pemodal->perusahaan->nama }}"
                                            @endisset class="form-control textbox"> 
                                            <span class="text-danger" ></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4">Tempat/Tanggal Lahir</label>
                                        <div class="col-sm-4">
                                            <input type="text" name="tempat_lahir" class="form-control textbox" value="{{ $user->biodata->tempat_lahir }}">
                                            <span class="text-danger"></span>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                                                <input type="text" name="tanggal_lahir" class="form-control col-sm-4 textbox" readonly="" value="{{ $user->biodata->tanggal_lahir }}" id="tanggal_lahir">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            </div>
                                            <span class="text-danger"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4">Alamat</label>
                                            <div class="col-sm-8">
                                                <input type="" name="alamat" value="{{ $user->pemodal->alamat->alamat }}" class="form-control textbox">
                                                <span class="text-danger" ></span>
                                            </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-4">
                                            <select class="form-control select2 textbox" id="prov_1" name="prov_1" onchange="set_kota('prov_1','kota_1','Pilih Kab/Kota')" >
                                                <option value="">Pilih Provinsi</option>
                                                    @foreach ($list_provinsi as $key => $value)
                                                        @if($user->pemodal->alamat->province_id == $value)
                                                            <option value="{{ $value }}" selected>{{ $key }}</option>
                                                        @else
                                                            <option value="{{ $value }}">{{ $key }}</option>
                                                        @endif
                                                    @endforeach
                                              </select>
                                             <span class="text-danger" ></span>
                                        </div>
                                        <div class="col-sm-4">
                                             <select class="form-control select2 textbox" id="kota_1" name="kota_1" onchange="set_kecamatan('kota_1','kec_1','Pilih Kecamatan')" >
                                                <option value="">Pilih Kab/Kota</option>
                                                    @php
                                                        $kota = app()->make('App\Http\Controllers\Ajax')->set_list_kota($user->pemodal->alamat->province_id);
                                                        foreach ($kota as $key) {
                                                            if($user->pemodal->alamat->regency_id == $key['id']){
                                                                echo '<option value="'.$key['id'].'" selected>'.$key['nama'].'</option>';
                                                            }else{
                                                                echo '<option value="'.$key['id'].'">'.$key['nama'].'</option>';
                                                            }
                                                        }
                                                    @endphp
                                              </select>
                                              <span class="text-danger" ></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-4">
                                            <select class="form-control select2 textbox" id="kec_1" name="kec_1" onchange="set_kelurahan('kec_1','kel_1','Pilih Kelurahan')" >
                                                <option value="">Pilih Kecamatan</option>
                                                    @php
                                                        $kecamatan = app()->make('App\Http\Controllers\Ajax')->set_list_kecamatan($user->pemodal->alamat->regency_id);
                                                        foreach ($kecamatan as $key) {
                                                            if($user->pemodal->alamat->district_id == $key['id']){
                                                                echo '<option value="'.$key['id'].'" selected>'.$key['nama'].'</option>';
                                                            }else{
                                                                echo '<option value="'.$key['id'].'">'.$key['nama'].'</option>';
                                                            }
                                                        }
                                                    @endphp
                                              </select>
                                             <span class="text-danger" ></span>
                                        </div>
                                        <div class="col-sm-4">
                                            <select class="form-control select2 textbox" id="kel_1" name="kel_1">
                                                <option value="">Pilih Kelurahan</option>
                                                    @php
                                                        $kelurahan = app()->make('App\Http\Controllers\Ajax')->set_list_kelurahan($user->pemodal->alamat->district_id);
                                                        foreach ($kelurahan as $key) {
                                                            if($user->pemodal->alamat->village_id == $key['id']){
                                                                echo '<option value="'.$key['id'].'" selected>'.$key['nama'].'</option>';
                                                            }else{
                                                                echo '<option value="'.$key['id'].'">'.$key['nama'].'</option>';
                                                            }
                                                        }
                                                    @endphp
                                              </select>
                                              <span class="text-danger" ></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-4">
                                            <input type="text" name="kode_pos" class="form-control textbox" value="{{ $user->pemodal->alamat->kode_pos }}" placeholder="Kode Pos">
                                            <span class="text-danger" ></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4">Email</label>
                                        <div class="col-sm-8">
                                             <input type="email" name="email" id="email" value="{{ $user->email }}" class="form-control textbox">
                                            <span class="text-danger" ></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-lable col-sm-4">Telepon</label>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <input type="text" class="form-control textbox" id="mobile" name="mobile" value="{{ $user->biodata->no_hp }}"placeholder="">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-mobile-phone"></i>
                                                </span>
                                            </div>
                                            <span class="text-danger" id="alert_mobile" hidden=""></span>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <input type="text" class="form-control textbox" id="home" name="home" value="{{ $user->biodata->no_telepon }}" placeholder="">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-phone"></i>
                                                </span>
                                            </div>
                                            <span class="text-danger" id="alert_home" hidden=""></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4">ID KTP</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="ktp" value="{{ $user->biodata->no_ktp }}" id="ktp" class="form-control textbox">
                                            <span class="text-danger" ></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4">ID NPWP</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="npwp" value="{{ $user->biodata->no_npwp }}" id="npwp" class="form-control textbox">
                                            <span class="text-danger" ></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4">Jenis Kelamin</label>
                                          <div class="col-sm-8">
                                            <div class="mt-radio-inline">
                                                <label class="mt-radio">
                                                    <input type="radio" name="jenis_kelamin" value="laki-Laki" @if($user->biodata->jenis_kelamin == 'laki-laki') checked @endif> Laki-Laki
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="jenis_kelamin" value="perempuan" @if($user->biodata->jenis_kelamin == 'perempuan') checked @endif> Perempuan
                                                    <span></span>
                                                </label>
                                                  <span class="text-danger" ></span>
                                            </div>
                                          </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="portlet light portlet-fit">
                            <div class="portlet-title">
                                    <strong>Informasi Bank</strong>
                            </div>
                            <div class="portlet-body">
                                    <div class="form-group">
                                        <label class="col-sm-4">Bank</label>
                                        <div class="col-sm-8">
                                            <select class="form-control select2" name="bank">
                                                <option value="">Pilih Bank</option>
                                                @foreach ($list_bank as $key => $value)
                                                    @if($user->pemodal->akun_bank->bank_id == $value)
                                                        <option value="{{ $value }}" selected>{{ $key }}</option>
                                                    @else
                                                        <option value="{{ $value }}">{{ $key }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                              <span class="text-danger" ></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4">Nama Akun Bank</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="nama_akun" value="{{ $user->pemodal->akun_bank->nama_akun_bank }}" class="form-control textbox">
                                            <span class="text-danger" ></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4">Nomor Akun Bank</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="no_rekening" value="{{ $user->pemodal->akun_bank->no_akun_bank }}" class="form-control textbox textbox">
                                             <span class="text-danger" ></span>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <div class="portlet light portlet-fit">
                                <div class="portlet-title"><strong>Foto Profil</strong></div>
                                    <div class="portlet-body">
                                        <div class="form-group ">
                                        <label class="col-sm-3">&nbsp;</label>
                                        <div class="col-sm-7">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                                @if($user->file_uploads != "" && $user->file_uploads->name != "")
                                                  <img src="{{ asset('storage/profile_photo/'.$user->file_uploads->name) }}" class="preview img-responsive" id="profile_photo" style="width: 100px; height: 100px;" />
                                                @else
                                                  <img src="{{ asset('/img/dummy-profile.jpg') }}" class="preview img-responsive" id="profile_photo" style="width: 100px; height: 100px;" />
                                                @endif
                                            </div>
                                                <div>
                                                    <span class="btn btn-default btn-outline btn-file btn-circle">
                                                        <span class="fileinput-new"><i class="fa fa-cloud-upload"></i> Foto profil</span>
                                                        <span class="fileinput-exists"><i class="fa fa-pencil"></i> Ubah</span>
                                                        <input type="file" name="photo_profile" id="image"> </span>
                                                    <a href="javascript:;" class="btn btn-circle btn-outline red fileinput-exists " data-dismiss="fileinput" id="delete"><i class="fa fa-trash"></i> Hapus</a>
                                                </div>
                                                <span id="alert" class="text-danger" hidden="">File harus gambar (png,jpg,jpeg,bmp,img,dll)</span>
                                        </div>
                                    </div>
                                    </div>
                                <hr>
                                <center>
                                    <div class="form-group">
                                        <button type="submit" class="btn green btn-outline btn-circle" id="submit"><i id="icon" class="fa fa-save"></i> Simpan</button>
                                        <button type="reset" class="btn red btn-outline btn-circle" id="reset_bio"><i class="fa fa-refresh"></i> Reset</button>
                                    </div>
                                </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@push('page-plugin-scripts')
<script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/form-wizard.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
</script>
<script src="{{ asset('/assets/pages/scripts/ui-toastr.min.js') }}" type="text/javascript"></script>
@endpush
@push('page-plugin-scripts')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-toastr/toastr.min.js') }}" type="text/javascript"></script>
@endpush

@push('page-scripts')
<script src="{{ asset('assets/pages/scripts/table-datatables-managed.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('/js/pemodal/biodata.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/pemodal/change_password.js') }}"></script>

{{-- session --}}
<script type="text/javascript">
    @if(session()->has('notif'))
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
        };

    toastr.{{ session()->get('notif.level') }}("{{ session()->get('notif.message') }}","{{ session()->get('notif.title') }}");
    @endif
</script>
@endpush
