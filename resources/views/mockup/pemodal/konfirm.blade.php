@php
  function rupiah($angka)
  {
    $rupiah = number_format($angka,2,',','.');
    return $rupiah;
  }
@endphp
<center><h1>Pendanaan Berhasil Dilakukan</h1><h4>Terima kasih telah bertransaksi di www.finnesia.co.id</h4></center>
<table class="table table-striped table-bordered table-hover table-checkable order-column" id="data">
  <thead>
    <tr>
      <th> No </th>
      <th> Kode Pinjaman</th>
      <th> Nilai Pinjaman</th>
      <th> Waktu </th>
      <th> Proses Pendanaan </th>
      <th> Jumlah Tersisa</th>
      <th> Order </th>
    </tr>
  </thead>
  <tbody>
    @php
      $no = 1;
    @endphp
    @foreach($list_pinjaman as $pinjaman)
      <tr>
        <td>{{ $no }}</td>
        <td>{{ $pinjaman->kode }}</td>
        <td>Rp. {{ rupiah($pinjaman->jumlah_pinjaman) }}</td>
        <td>{{ $pinjaman->waktu_pinjaman }} Bulan</td>
        <td>
          @php
            $progres = ($pinjaman->jumlah_funding / $pinjaman->jumlah_pinjaman) * 100;
          @endphp
          <div class="easy-pie-chart">
            <div class="number visits" data-percent="{{ $progres }}%">
              <span>{{ $progres }}</span>%
            </div>
          </div>
        </td>
        <td>Rp. {{ rupiah($pinjaman->jumlah_pinjaman - $pinjaman->jumlah_funding) }}</td>
        <td>Rp. {{ rupiah($list_pendanaan[$pinjaman->id]) }}</td>
      </tr>
    @endforeach
  </tbody>
</table>
<br>
<div class="col-md-5 col-md-offset-7">
  <table class="table">
    <tr>
      <td>Total Nilai Pendanaan</td>
      <td>:</td>
      <td>Rp.<font class="pull-right">{{ rupiah($total_pendanaan) }}</font></td>
    </tr>
    <tr>
      <td>Jumlah Pendanaan</td>
      <td>:</td>
      <td><font class="pull-right">{{ $jumlah_pinjaman }} Pinjaman</font></td>
    </tr>
  </table>
</div>
