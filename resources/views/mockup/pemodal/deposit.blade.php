@extends('layouts.app')

@push('page-plugin-styles')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/bootstrap-toastr/toastr.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
<style type="text/css">
  .table thead tr th{
    white-space: pre-wrap;
    vertical-align: middle;
    text-align: center;
    padding-top: 20px;
    padding-bottom: 20px;
  }
  .table tbody tr td{
    vertical-align: middle;
    text-align: center;
  }

  #data th, #data td {
    font-size: 13px;
  }
</style>
@endpush

@section('title')
Deposit
@endsection

@section('crumbs')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/dashboard') }}">Pemodal</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Deposit</span>
        </li>
    </ul>
@endsection

@section('content')
@php
function rupiah($angka)
{
 $rupiah = number_format($angka,2,',','.');
 return $rupiah;
}
@endphp
<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
         <div class="portlet-body">
            <div class="row">
                <div class="col-md-8">
                    <!-- SYARAT DAN KETENTUAN -->
                    <div class="portlet light" id="term">
                        <div class="portlet-title"><strong>Syarat dan Ketentuan</strong></div>
                        <div class="portlet-body">
                            <p>Untuk melakukan deposit melalui PFX Indonesia, anda diharuskan memenuhi persyaratan dibawah ini:
                                <ol>
                                  <li>Akun Anda sudah berhasil melewati validasi di PT. Finnesia Teknologi Indonesia.</li>
                                  <li>Minimal deposit adalah minimal transfer i-banking bank atau setara $1.5.</li>
                                  <li>Proses deposit akan berjalan normal sekitar 5 s/d 15 menit dalam kondisi:
                                    <ul>
                                      <li>Server www.finnesia.co.id dalam kondisi normal, tidak sedang maintenance atau tidak sedang mengalami gangguan.</li>
                                      <li>Member sudah melakukan transfer sesuai isian form deposit berikut angka uniknya.</li>
                                    </ul>
                                  </li>
                                  <li>Jam proses deposit adalah sesuai jam mutasi online masing - masing bank, berikut jadwalnya :
                                    <ul>
                                      <li>Bank BCA, BNI, BRI 01:00 - 21:00 WIB</li>
                                      <li>Bank MANDIRI 03:00 - 23:00 WIB</li>
                                    </ul>
                                  </li>
                                  <li>&nbsp;Jika terjadi kondisi diluar point diatas maka kemungkinan layanan tidak berjalan normal.</li>
                                </ol>
                            </p>
                        </div>
                    </div>
                    <!-- INFROMASI TRANSFER -->
                    <div class="portlet light" id="info" style="display: none">
                      <div class="portlet-title"><strong>Bank Transfer</strong></div>
                      <div class="portlet-body">
                        <p>Silahkan lakukan transfer ke rekening dibawah ini, mohon lakukan transfer dalam waktu 3 jam dari sekarang</p>
                        <div style="text-align: center">
                          <img id="info_icon_bank" src="{{ asset('img/bank_mandiri.png') }}" style="width: 200px" />
                          <p style="padding-top: 5px">
                            <span id="info_no_akun"><b>No Rekenging : 113 - 00 - 7070444 - 4</b></span><br>
                            <span id="info_nama_akun" style="text-size: 16px; text-transform: uppercase;">SAHALA FIRDAUS SYAH DOLOKSARIBU</span><br>
                            <span id="info_cabang_akun">KCP PMG Bdr Sultan Badaruddin 11310</br>
                          </p>
                          <p style="text-size: 16px">Jumlah deposit yang harus ditransfer</p>
                            <h4 id="jumlah_deposit" style="text-weight: bolder">Rp. 2.000.234</h4>
                          <p>
                            Mohon transfer sesuai jumlah yang tertera<br>
                            (termasuk 3 digit terakhir)
                          </p>
                        </div>
                        Setelah melakukan transfer harap segera lakukan konfirmasi transaksi agar proses menjadi lebih cepat.
                      </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-credit-card"></i> FORM DEPOSIT
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form id="deposit" method="post" action="{{ url('pemodal/order/deposit') }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label>Pilih Bank Tujuan Transfer :</label>
                                    <br>
                                      <select class="form-control textbox" name="bank" id="bank" required>
                                        @foreach ($bank as $data)
                                          <option value="{{ $data->id }}">{{ $data->nama }}</option>
                                        @endforeach
                                      </select>
                                      <span class="text-danger" ></span>
                                </div>
                                <div class="form-group">
                                    <label>Nominal Deposit(Rp) :</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">Rp.</span>
                                        <input type="text" name="jumlah" class="form-control textbox" id="i_deposit" required />
                                    </div>
                                    <span class="text-danger" id="alert_d"></span>
                                </div>
                                <div class="form-group">
                                       <div class="g-recaptcha" data-sitekey="{{ env('CAPTCHA_SITEKEY') }}" data-callback="success" id="captcha"></div>
                                       <span class="text-danger" id="alert"></span>
                                    </div>
                                 <div>
                                    <button type="button" id="icon_loading" class="btn green btn-outline btn-circle" style="display: none;">
                                      <i class="fa fa-spinner fa-spin" style="font-size: 20px"></i>
                                    </button>

                                     <button type="submit" class="btn green btn-outline btn-circle" id="submit">
                                       <i id="icon_send" class="fa fa-send"></i>
                                       Kirim
                                     </button>
                                 </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
         </div>
        </div>
    </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-file-text-o"></i>
                <span class="caption-subject bold uppercase">History Deposit</span>
            </div>
        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="data">
                <thead>
                    <tr>
                        <th> NO </th>
                        <th> Tanggal Order</th>
                        <th> No.Transaksi</th>
                        <th> Bank </th>
                        <th> Jumlah Transaksi </th>
                        <th> Status</th>
                        <th>  </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        <br>

            <div class="clearfix margin-bottom-20"> </div>
        </div>
      </div>
    </div>
 </div>
</div>

@endsection
@push('page-plugin-scripts')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/ui-toastr.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-toastr/toastr.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
@endpush

@push('page-scripts')
<script src="{{ asset('js/jquery.number.min.js') }}" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#i_deposit').number(true,2,',','.');
    var table = $('#data').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{ url('/pemodal/list_deposit') }}',
            type: 'get'
        },
        columns: [
            {data: "DT_Row_Index", orderable: false, searchable: false},
            {data: "created_at", name: 'created_at'},
            {data: "no_transaksi", name: 'no_transaksi'},
            {data: "nama_bank", name: 'nama_bank'},
            {data: "jumlah", name: 'jumlah'},
            {data: "status", name: 'status'},
            {data: "action", orderable: false, searchable: false},
        ],
        rowCallback: function(row,data,index){
            // nomer
            $('td:eq(0)', row).html( '<center>'+data.DT_Row_Index+'</center>' );
            // jumlah_pinjaman
            $('td:eq(4)',row).html('<span class="pull-left">Rp.</span><span class="pull-right">'+numeral(data.jumlah).format('0,0')+'</span>');
        }
    });
    $('#deposit').submit(function(event){
      event.preventDefault();
      $.ajax({
        type: 'POST',
        url: $(this).attr('action'),
        data: $(this).serialize(),
        dataType: 'json',
        beforeSend: function(){
          $('#icon_loading').css('display','block');
          $('#submit').css('display','none');
        },
        success: function(result,status,xhr){
          console.log(result);
          swal({
              allowOutsideClick: false,
              allowEscapeKey: false,
              text: "Deposit telah disimpan, silahkan lakukan transfer dan konfirmasi",
              title: "Terimakasih",
              type: "success",
              confirmButtonClass: "btn green btn-circle btn-outline"
          },function(){
            document.getElementById('deposit').reset();
            grecaptcha.reset();
          });
          $('#jumlah_deposit').text("Rp. "+numeral((parseInt(result.deposit.jumlah) + parseInt(result.deposit.kode_unik))).format('0,0'));
          $('#info_no_akun').text("No Rekenging : "+result.akun_bank.no_akun_bank);
          $('#info_nama_akun').text(result.akun_bank.nama_akun_bank);
          if(result.akun_bank.nama_bank == 'Bank Mandiri'){
            $('#info_icon_bank').attr('src','{{ asset('img/bank_mandiri.png') }}');
            $('#info_cabang_akun').text('KCP PMG Bdr Sultan Badaruddin 11310');
          }
          $('#info').fadeIn();
          $('#term').fadeOut();
        },
        error: function(xhr,status,error){
          swal({
              text: "Please ensure that you are a human.!",
              title: "Ooop..!",
              type: "error",
              confirmButtonClass: "btn btn-danger btn-circle btn-outline"
          });
        },
        complete: function(){
          $('#icon_loading').css('display','none');
          $('#submit').css('display','block');
          table.ajax.reload();
        }
      })
    })
  });
</script>

@if (session()->has('success'))
  <script type="text/javascript">
    $(function () {
      swal({
          allowOutsideClick: false,
          allowEscapeKey: false,
          text: "Konfirmasi transfer telah disimpan",
          title: "Terimakasih",
          type: "success",
          confirmButtonClass: "btn green btn-circle btn-outline"
      });
    });
  </script>
@endif
@endpush
