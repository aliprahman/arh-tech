@extends('layouts.finnesia')

@push('page-plugin-styles')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/pages/css/register-custom.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('title')
Daftar Pendana
@endsection

@section('content')
@if(auth::guest())
<div class="row">
    <div class="col-md-12">
       <div class="portlet light portlet-fit">
	        <div class="portlet-title portlet-title-custom">
	            <div class="caption" style="width: 100%;">
	                <span class="caption-subject font-dark uppercase title">Registrasi sebagai Pendana</span>
	                <span class="pull-right">
		                <span class="form-step trans-label-step"><p>STEP</p></span>
	            		<span class="form-step current-step"><p>1</p></span>
	            		<span class="form-step trans-label-step"><p>DARI</p></span>
	            		<span class="form-step"><p>3</p></span>
	            	</span>
	            </div>
	        </div>
	    <form class="form-horizontal" role="form" id="pemodal">
            {{ csrf_field() }}
            <div class="portlet-body">
            	<div class="row">
              		<div class="col-md-12 center-content" id="step1">
          				<div class="portlet light portlet-fit portlet-custom">
	        				<div class="portlet-title">
	        					<strong>Informasi Data Diri</strong>
	        				</div>
	        				<div class="portlet-body">
				        		<div class="form-group" style="display: none;">
				        			<label class="col-sm-12">Kategori</label>
				        			  <div class="col-sm-12">
					        			<div class="mt-radio-inline">
					                        <label class="mt-radio">
					                            <input type="radio" name="jenis" value="Personal" id="personal" checked="checked"> Personal
					                            <span></span>
					                        </label>
					                        <label class="mt-radio">
					                            <input type="radio" name="jenis" value="Corporate" id="corporate"> Corporate
					                            <span></span>
					                        </label>
					                        <br>
					                        <span class="text-danger" id="alert_j"></span>
					                    </div>
					                  </div>
				        		</div>
				        		<div class="form-group">
				        			<label class="col-sm-12">Nama</label>
				        			<div class="col-sm-12">
				        				<input type="text" name="nama" value="{{ old('nama') }}" class="form-control textbox input-lg">
				        				<span class="text-danger" ></span>
				        			</div>
				        		</div>

				        		<div class="form-group">
				        			<label class="col-sm-12">Nama Perusahaan</label>
				        			<div class="col-sm-12">
				        				<input type="text" name="n_perusahaan" value="{{ old('n_perusahaan') }}" class="form-control textbox input-lg">
				        				<span class="text-danger" ></span>
				        			</div>
				        		</div>
				        		<div class="form-group">
				        			<label class="col-sm-12">Tempat/Tanggal Lahir</label>
				        			<div class="col-sm-6">
                        <select class="form-control input-lg select2" name="tempat_lahir">
  												<option value=""></option>
  												@foreach ($list_kota as $key => $value)
  													@if(old('tempat_lahir') != "" && old('tempat_lahir') == $value)
  														<option value="{{ $value }}" selected>{{ $value }}</option>
  													@else
  														<option value="{{ $value }}">{{ $value }}</option>
  													@endif
  												@endforeach
  											</select>
				        				<span class="text-danger"></span>
				        			</div>
				        			<div class="col-sm-6">
				        				<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
		                                    <input type="text" name="tanggal_lahir" class="form-control col-sm-4 textbox input-lg" value="{{ old('tanggal_lahir') }}" id="tanggal_lahir" placeholder="DD-MM-YYYY">
		                                    <span class="input-group-addon">
		                                        <i class="fa fa-calendar"></i>
		                                    </span>
		                                </div>
		                                <span class="text-danger" id="alert_tanggal" hidden=""></span>
		                            </div>
				        		</div>
				        		<div class="form-group">
				        			<label class="col-sm-12">Jenis Kelamin</label>
				        			<div class="col-sm-12">
					        			<div class="mt-radio-inline">
					                         <label class="mt-radio">
				                                 <input type="radio" name="jenis_kelamin" value="laki-laki" @if(old('jenis_kelamin') != "" && old('jenis_kelamin') == 'laki-laki') checked @endif> Laki-Laki
				                                 <span></span>
				                             </label>
				                             <label class="mt-radio">
				                                 <input type="radio" name="jenis_kelamin" value="perempuan" @if(old('jenis_kelamin') != "" && old('jenis_kelamin') == 'perempuan') checked @endif> Perempuan
				                                 <span></span>
				                             </label>
				                              <br>
					                          <span class="text-danger" id="alert_k"></span>
					                    </div>
					                </div>
				        		</div>
				        		<div class="form-group">
			                            <label class="control-lable col-sm-12">Telepon</label>
			                            <div class="col-sm-6">
			                                <div class="input-group">
			                                    <input type="text" class="form-control textbox input-lg" id="mobile" name="mobile" value="{{ old('mobile') }}"placeholder="Handphone">
			                                    <span class="input-group-addon">
			                                        <i class="fa fa-mobile-phone"></i>
			                                    </span>
			                                </div>
			                                <span class="text-danger" id="alert_mobile" hidden=""></span>
			                            </div>
			                            <div class="col-sm-6">
			                                <div class="input-group">
			                                    <input type="text" class="form-control textbox input-lg exlude-validation" id="home" name="home" value="{{ old('home') }}"placeholder="Rumah">
			                                    <span class="input-group-addon">
			                                        <i class="fa fa-phone"></i>
			                                    </span>
			                                </div>
			                                <span class="text-danger" id="alert_home" hidden=""></span>
			                            </div>
			                        </div>

				        		<div class="form-group">
				        			<label class="col-sm-12">Alamat</label>
			        				<div class="col-sm-12">
			        					<input type="text" name="alamat" value="{{ old('alamat') }}" class="form-control textbox input-lg">
			        					<span class="text-danger" ></span>
			        				</div>
				        		</div>
				        		<div class="form-group">
				        			<div class="col-sm-6">
				        				<select class="form-control select2 textbox input-lg" id="prov_1" name="prov_1" onchange="set_kota('prov_1','kota_1','Pilih Kab/Kota')" >
				                            <option value="">Pilih Provinsi</option>
		                                      @foreach ($list_provinsi as $key => $value)
		                                        @if(old('prov_1') != "" && old('prov_1') == $value)
		                                          <option value="{{ $value }}" selected>{{ $key }}</option>
		                                        @else
		                                          <option value="{{ $value }}">{{ $key }}</option>
		                                        @endif
		                                      @endforeach
				                          </select>
				        				 <span class="text-danger" ></span>
				        			</div>
				        			<div class="col-sm-6">
				        				 <select class="form-control select2 textbox input-lg" id="kota_1" name="kota_1" onchange="set_kecamatan('kota_1','kec_1','Pilih Kecamatan')" >
				                            <option value="">Pilih Kab/Kota</option>
		                                      @if(old('kota_1') != "")
		                                        @php
		                                          $kota = app()->make('App\Http\Controllers\Ajax')->set_list_kota(old('prov_1'));
		                                          foreach ($kota as $key) {
		                                              if(old('kota_1') == $key['id']){
		                                                  echo '<option value="'.$key['id'].'" selected>'.$key['nama'].'</option>';
		                                              }else{
		                                                  echo '<option value="'.$key['id'].'">'.$key['nama'].'</option>';
		                                              }
		                                          }
		                                        @endphp
		                                      @endif
				                          </select>
				        				  <span class="text-danger" ></span>
				        			</div>
				        		</div>
				        		<div class="form-group">
				        			<div class="col-sm-6">
				        				<select class="form-control select2 textbox input-lg" id="kec_1" name="kec_1" onchange="set_kelurahan('kec_1','kel_1','Pilih Kelurahan')" >
				                            <option value="">Pilih Kecamatan</option>
		                                      @if(old('kec_1') != "")
		                                        @php
		                                          $kecamatan = app()->make('App\Http\Controllers\Ajax')->set_list_kecamatan(old('kota_1'));
		                                          foreach ($kecamatan as $key) {
		                                              if(old('kec_1') == $key['id']){
		                                                  echo '<option value="'.$key['id'].'" selected>'.$key['nama'].'</option>';
		                                              }else{
		                                                  echo '<option value="'.$key['id'].'">'.$key['nama'].'</option>';
		                                              }
		                                          }
		                                        @endphp
		                                      @endif
				                          </select>
				        				 <span class="text-danger" ></span>
				        			</div>
				        			<div class="col-sm-6">
				        				<select class="form-control select2 textbox input-lg" id="kel_1" name="kel_1">
						                    <option value="">Pilih Kelurahan</option>
				                              @if(old('kel_1') != "")
				                                @php
				                                  $kecamatan = app()->make('App\Http\Controllers\Ajax')->set_list_kelurahan(old('kec_1'));
				                                  foreach ($kecamatan as $key) {
				                                      if(old('kel_1') == $key['id']){
				                                          echo '<option value="'.$key['id'].'" selected>'.$key['nama'].'</option>';
				                                      }else{
				                                          echo '<option value="'.$key['id'].'">'.$key['nama'].'</option>';
				                                      }
				                                  }
				                                @endphp
				                              @endif
						                  </select>
				        				  <span class="text-danger" ></span>
				        			</div>
				        		</div>
				        		<div class="form-group">
				        			<div class="col-sm-6">
				        				<input type="text" name="kode_pos" maxlength="5" class="form-control textbox input-lg" value="{{ old('kode_pos') }}" placeholder="Kode Pos">
				        				<span class="text-danger" ></span>
				        			</div>
				        		</div>
		                        <div class="form-group">
		                        	<label class="col-sm-12">ID KTP</label>
		                        	<div class="col-sm-12">
		                        		<input type="text" name="ktp" value="{{ old('ktp') }}" id="ktp" class="form-control textbox input-lg">
		                        		<span class="text-danger" ></span>
		                        	</div>
		                        </div>
		                        <div class="form-group">
		                        	<label class="col-sm-12">ID NPWP</label>
		                        	<div class="col-sm-12">
		                        		<input type="text" name="npwp" value="{{ old('npwp') }}" id="npwp" class="form-control textbox input-lg">
		                        		<span class="text-danger" ></span>
		                        	</div>
		                        </div>
				        		<br>
	        					<center>
		        			        <div class="form-group">
		                            	<button type="button" class="btn green btn-circle next btn-lg" id="submit"><i id="icon" class="icon-next"></i> Selanjutnya</button>
		                            </div>
	        					</center>
	        				</div>
	        			</div>
	        		</div>
	        		<div class="col-md-12 center-content" id="step2" style="display: none;">
          				<div class="portlet light portlet-fit portlet-custom">
	        				<div class="portlet-title">
	        					<strong>Informasi Bank</strong>
	        				</div>
	        				<div class="portlet-body">
	        					<div class="form-group">
	        						<label class="col-sm-12">Bank</label>
	        						<div class="col-sm-12">
				        				<select class="form-control select2 textbox input-lg" name="bank" style="width: 100%;">
					                        <option value="">Pilih Bank</option>
			                                  @foreach ($list_bank as $key => $value)
			                                      @if(old('bank') != "" && old('bank') == $value)
			                                        <option value="{{ $value }}" selected>{{ $key }}</option>
			                                      @else
			                                        <option value="{{ $value }}">{{ $key }}</option>
			                                      @endif
			                                  @endforeach
					                      </select>
				        				  <span class="text-danger" ></span>
				        			</div>
	        					</div>
	        					<div class="form-group">
	        						<label class="col-sm-12">Nama Akun Bank</label>
	        						<div class="col-sm-12">
	        							<input type="text" name="nama_akun" value="{{ old('nama_akun') }}" class="form-control textbox input-lg">
	        							<span class="text-danger" ></span>
	        						</div>
	        					</div>
	        					<div class="form-group">
	        						<label class="col-sm-12">Nomor Akun Bank</label>
	        						<div class="col-sm-12">
	        							<input type="text" name="no_rekening" value="{{ old('no_bank') }}" class="form-control textbox textbox input-lg">
	        							 <span class="text-danger" ></span>
	        						</div>
	        					</div>
	        					<center>
		        			        <div class="form-group">
		        			        	<button type="button" class="btn default btn-circle prev btn-lg" id="submit"><i id="icon" class="icon-prev"></i> Sebelumnya</button> &nbsp;
		                            	<button type="button" class="btn green btn-circle next btn-lg" id="submit"><i id="icon" class="icon-next"></i> Selanjutnya</button>
		                            </div>
	        					</center>
	        				</div>
	        			</div>
	        		</div>
	        		<div class="col-md-12 center-content" id="step3" style="display: none;">
          				<div class="portlet light portlet-fit portlet-custom">
	        				<div class="portlet-title"><strong>Akun</strong></div>
								<div class="portlet-body">
									<div class="form-group">
					        			<label class="col-sm-12">Email</label>
					        			<div class="col-sm-12">
					        				 <input type="email" name="email" id="email" value="{{ old('email') }}" class="form-control textbox input-lg">
					        				<span class="text-danger" ></span>
					        			</div>
					        		</div>
									<div class="form-group">
										<label for="" class="col-sm-12">
											Password
										</label>
										<div class="col-sm-12">
											<input type="password" name="password" class="form-control textbox textbox textbox input-lg" id="new"  placeholder="Password">
											<span class="text-danger" ></span>
										</div>
									</div>
									<div class="form-group">
											<label for="" class="col-sm-12">
												Ulangi Password
											</label>
										<div class="col-sm-12">
											<input type="password" name="repeat" class="form-control textbox input-lg" id="repeat" placeholder="Ulangi Password">
											<span class="text-danger" ></span>
										</div>
									</div>
		        					<br>
		        					<center>
			        					<div class="form-group">
			        					   <div class="g-recaptcha" data-sitekey="{{ env('CAPTCHA_SITEKEY') }}" data-callback="success" id="captcha"></div>
			        					   <span class="text-danger" id="alert"></span>
			                            </div>
			                            <br>
			                            <div class="form-group">
			                            	<label class="mt-checkbox">
						                        <input type="checkbox" id="setuju" value="setuju" name="setuju"> Saya telah membaca dan menyetujui ketentuan yang berlaku.
						                        <span></span>
						                    </label>
						                    <br>
						                    <span class="text-danger" id="alert_s"></span>
			                            </div>
			                            <div class="form-group">
			                            	<button type="button" class="btn default btn-circle prev btn-lg" id="submit"><i id="icon" class="icon-prev"></i> Sebelumnya</button> &nbsp;
			                            	<button type="submit" class="btn green btn-outline btn-circle btn-lg" id="submit"><i id="icon" class="icon-users"></i> Daftar</button>
			                            </div>
		        					</center>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@else
<script type="text/javascript">
	window.location = "{{ url('/404') }}";
</script>
@endif
@endsection

@push('page-plugin-scripts')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
@endpush

@push('page-scripts')
<script src="{{ asset('/assets/pages/scripts/table-datatables-managed.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('/js/pemodal/register.js') }}"></script>
<script type="text/javascript">
	var step = 1;
	function showStep(step) {
		$('#step1').fadeOut('slow');
		$('#step2').fadeOut('slow');
		$('#step3').fadeOut('slow');

		setTimeout(function(){
			document.body.scrollTop = 0; //Safari
			document.documentElement.scrollTop = 0;
		    $('#step' + step).fadeIn('slow');
		  }, 1000);
	}

	$(document).ready(function() {
		$('body').on('click', '.next', function() {
			$('#step' + step).find('input, select').blur();

			if($('input[name=jenis]')[0].checked==false && $('input[name=jenis]')[1].checked==false) {
				$('#alert_j').show().text('Pilih salah satu.');
				$('#alert_j').addClass('no-valid');
				return;
			}else {
		    	$('#alert_j').hide();
		    	$('#alert_j').removeClass('no-valid');
			}

		    if($('input[name=jenis_kelamin]')[0].checked==false && $('input[name=jenis_kelamin]')[1].checked==false) {
				$('#alert_k').show().text('Pilih salah satu.');
				$('#alert_k').addClass('no-valid');
				return;
			} else {
		    	$('#alert_k').hide();
		    	$('#alert_k').removeClass('no-valid');
			}

			var valid = true;
			$('#step' + step).find('input[type=text], select').each(function() {
				if($(this).val() === "" && !$(this).hasClass('exlude-validation')) {
					valid = false;
					return;
				}
			});

			if(valid) {
				step++;
				$('body').find('.current-step p').html(step);
				showStep(step);
			}
		});

		$('body').on('click', '.prev', function() {
			step--;
			$('body').find('.current-step p').html(step);
			showStep(step);
		});
	});
</script>
@endpush
