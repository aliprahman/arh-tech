@extends('layouts.app')

@push('page-plugin-styles')
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
  .table thead tr th{
    white-space: pre-wrap;
    vertical-align: middle;
    text-align: center;
    padding-top: 20px;
    padding-bottom: 20px;
  }
  .table tbody tr td{
    vertical-align: middle;
  }

  #data th, #data td {
    font-size: 13px;
  }
</style>
@endpush

@section('title')
Daftar Pendanaan
@endsection

@section('crumbs')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/dashboard') }}">Pemodal</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Daftar Pendanaan</span>
        </li>
    </ul>
@endsection

@section('content')
@php
function rupiah($angka)
{
 $rupiah = number_format($angka,2,',','.');
 return $rupiah;
}
@endphp
<div class="row">
  <div class="col-md-12">
    <div class="portlet light ">
      <div class="portlet-title">
          <div class="caption">
              <form method="POST" id="filter-list" class="form-inline" role="form">
                  <div class="form-group">
                      <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                          <input type="text" name="start" id="start" class="form-control col-sm-4" readonly="" value="{{ "01-".date('m-Y') }}" required>
                          <span class="input-group-btn">
                      <button class="btn default" type="button">
                           <i class="fa fa-calendar"></i>
                      </button>
                  </span>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                          <input type="text" name="end" id="end" class="form-control col-sm-4" readonly="" value="{{ date('t-m-Y') }}" required>
                          <span class="input-group-btn">
                      <button class="btn default" type="button">
                           <i class="fa fa-calendar"></i>
                      </button>
                  </span>
                      </div>
                  </div>
                  <button type="submit" class="btn btn-primary">
                      <i class="icon-magnifier"></i>
                      Search
                  </button>
              </form>
          </div>
          <div class="actions">
              <a class="btn btn-circle btn-default" href="javascript:;" id="print_pendanaan_pdf">
                  <i class="fa fa-print"></i> Print to PDF
              </a>
          </div>
      </div>
        <div class="portlet-body">
            <center><h1>Daftar Pendanaan</h1></center>
            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="data">
                <thead>
                    <tr>
                        <th> NO </th>
                        <th> Waktu Transakasi</th>
                        <th> Kode Pinjam</th>
                        <th> Nilai Pinjaman </th>
                        <th> Nilai Pendanaan </th>
                        <th> Waktu</th>
                        <th> Status </th>
                        <th>  </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div class="clearfix margin-bottom-20"> </div>
        </div>
      </div>
    </div>
   </div>
</div>

<div class="modal fade" id="printModal" tabindex="-1" role="dialog" aria-labelledby="printModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="contactModalLabel">Print Preview</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button id="printModal-print-btn" type="button" class="btn btn-primary"><i class="fa fa-print"></i> Print</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection
@push('page-plugin-scripts')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
@endpush

@push('page-scripts')
<script src="{{ asset('assets/pages/scripts/table-datatables-managed.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('.date-picker').datepicker({
      orientation: "left",
      autoclose: true
  });
    var oTable = $('#data').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: '{{url("/pemodal/daftar/pendanaan") }}',
          method: 'POST',
          data: function(d){
              d.startDate = $('#start').val();
              d.endDate =  $('#end').val();
          }
        },
        columns: [
            { data: "DT_Row_Index", orderable: false, searchable: false},
            { data: 'tanggal', name: 'tanggal' },
            { data: 'kode', name: 'kode' },
            { data: 'jumlah_pinjaman', name: 'jumlah_pinjaman' },
            { data: 'jumlah_pendanaan', name: 'jumlah_pendanaan' },
            { data: 'waktu_pinjaman', name: 'waktu_pinjaman' },
            { data: 'status', name: 'status'},
            { data: "action", orderable: false, searchable: false}
        ],
        rowCallback: function (row,data,index) {
          // nomer
          $('td:eq(0)', row).html( '<center>'+data.DT_Row_Index+'</center>' );
          // tanggal pendanaan
          $('td:eq(1)',row).html(moment(data.tanggal).format("DD-MM-YYYY"));
          // kode
          $('td:eq(2)',row).html('<a href="{{ url('pemodal/portofolio') }}/'+data.pinjaman_id+'" target="_blank">'+data.kode+'</a>');
          // jumlah pinjaman
          $('td:eq(3)',row).html("<b>Rp. "+ numeral(data.jumlah_pinjaman).format('0,0')+"</b>");
          // jumlah pendanaan
          $('td:eq(4)',row).html("<b>Rp. "+ numeral(data.jumlah_pendanaan).format('0,0')+"</b>");
          // waktu pinjaman
          $('td:eq(5)',row).html('<center>'+data.waktu_pinjaman+' Bulan</center>');
          // action
          $('td:eq(7)',row).html('<center><a href="{{ url('/pemodal/cicilan') }}/'+data.pinjaman_id+'" class="btn btn-circle btn-xs green btn-outline"><i class="icon-eye"></i> Lihat Cicilan</a></center>')
        }
      });
    $('#filter-list').on('submit', function(e) {
        oTable.ajax.reload();
        e.preventDefault();
    });
    $('#print_pendanaan_pdf').click(function () {
        var iframeHeight = $(window).height() - 220;
        var startDate = moment($('#start').val()).format("YYYY-MM-DD");
        var endDate = moment($('#end').val()).format("YYYY-MM-DD");
        var url = '{{ url("pemodal/daftar/pendanaan/pdf") }}?startDate='+startDate+'&endDate='+endDate;

        $('#printModal .modal-body').html('<iframe id="printModal-iframe" src="'+url+'" style="width: 100%; height: ' + iframeHeight + 'px; border: 1px solid #e5e5e5;background-image: url({{asset('assets/global/img/input-spinner.gif')}}); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>');
        $('#printModal-print-btn').attr('disabled', 'disabled');
        $('#printModal-iframe').on('load', function () {
            $('#printModal-print-btn').removeAttr('disabled');
        });
        $('#printModal').modal('show');
    });
    $('#printModal-print-btn').on('click', function () {
        $('#printModal-iframe').get(0).contentWindow.print();
    });
} );
</script>
@endpush
