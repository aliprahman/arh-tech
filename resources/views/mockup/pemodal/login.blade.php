<!DOCTYPE html> 
<!--  
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7 
Version: 4.7.5 
Author: KeenThemes 
Website: http://www.keenthemes.com/ 
Contact: support@keenthemes.com 
Follow: www.twitter.com/keenthemes 
Dribbble: www.dribbble.com/keenthemes 
Like: www.facebook.com/keenthemes 
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes 
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes 
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project. 
--> 
<!--[if IE 8]> <html lang="{{ app()->getLocale() }}" class="ie8 no-js"> <![endif]--> 
<!--[if IE 9]> <html lang="{{ app()->getLocale() }}" class="ie9 no-js"> <![endif]--> 
<!--[if !IE]><!--> 
<html lang="{{ app()->getLocale() }}"> 
    <!--<![endif]--> 
    <!-- BEGIN HEAD --> 
 
    <head> 
        <meta charset="utf-8" /> 
        <title>{{ config('app.name', 'Laravel') }}</title> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
        <meta content="width=device-width, initial-scale=1" name="viewport" /> 
        <meta content="Preview page of Metronic Admin Theme #5 for " name="description" /> 
        <meta content="" name="author" /> 
        <!-- BEGIN GLOBAL MANDATORY STYLES --> 
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> 
        <link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" /> 
        <link href="{{ asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" /> 
        <link href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> 
        <link href="{{ asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" /> 
        <!-- END GLOBAL MANDATORY STYLES --> 
        <!-- BEGIN PAGE LEVEL PLUGINS --> 
        <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" /> 
        <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> 
        <link href="{{ asset('/assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css"> 
        <!-- END PAGE LEVEL PLUGINS --> 
        <!-- BEGIN THEME GLOBAL STYLES --> 
        <link href="{{ asset('assets/global/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css" /> 
        <link href="{{ asset('assets/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" /> 
        <link href="{{ asset('assets/layouts/layout3/css/themes/blue-steel.css') }}" rel="stylesheet" type="text/css" /> 
        <link href="{{ asset('assets/layouts/layout3/css/custom.css') }}" rel="stylesheet" type="text/css" /> 
        <!-- END THEME GLOBAL STYLES --> 
        <!-- BEGIN PAGE LEVEL STYLES --> 
        <link href="{{ asset('assets/pages/css/login-3.min.css') }}" rel="stylesheet" type="text/css" /> 
        <!-- END PAGE LEVEL STYLES --> 
        <!-- BEGIN THEME LAYOUT STYLES --> 
        <!-- END THEME LAYOUT STYLES --> 
        <link rel="shortcut icon" href="favicon.ico" /> </head> 
    <!-- END HEAD --> 
 
    <body class=" login"> 
        <!-- BEGIN LOGO --> 
        <div class="logo"> 
            <a class="page-logo" href="index.html"> 
                <h1><i class="fa fa-group"></i> Finnesia</h1> 
            </a> 
        </div> 
        <!-- END LOGO --> 
        <!-- BEGIN LOGIN --> 
        <div class="content"> 
            <!-- BEGIN LOGIN FORM --> 
            <form class="login-form" action="{{ url('/pemodal/login') }}" method="post"> 
                @if(session()->has('notif')) 
                <div class=""> 
                    <div class="alert alert-{{ session()->get("notif.level") }}"> 
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> 
                        {!! session()->get('notif.message') !!} 
                    </div> 
                </div> 
                @endif 
                <h3 class="form-title">Masukan Akun Anda</h3> 
                {{ csrf_field() }} 
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}"> 
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that--> 
                    <label class="control-label visible-ie8 visible-ie9">Alamat E-mail</label> 
                    <div class="input-icon"> 
                        <i class="fa fa-user"></i> 
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="E-Mail" name="email" value="{{ old('email') }}" /> 
 
                        @if ($errors->has('email')) 
                            <span class="help-block"> 
                                <strong>{{ $errors->first('email') }}</strong> 
                            </span> 
                        @endif 
                    </div> 
                </div> 
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}"> 
                    <label class="control-label visible-ie8 visible-ie9">Password</label> 
                    <div class="input-icon"> 
                        <i class="fa fa-lock"></i> 
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> 
 
                        @if ($errors->has('password')) 
                            <span class="help-block"> 
                                <strong>{{ $errors->first('password') }}</strong> 
                            </span> 
                        @endif 
                    </div> 
                </div> 
                <div class="form-actions"> 
                    <label class="rememberme mt-checkbox mt-checkbox-outline"> 
                        <input type="checkbox" name="remember" value="1" /> Ingat Saya 
                        <span></span> 
                    </label> 
                    <button type="submit" class="btn green pull-right"> Masuk </button> 
                </div> 
                <div class="login-options"> 
                    <h4>Atau Masuk Dengan</h4> 
                    <ul class="social-icons"> 
                        <li> 
                            <a class="facebook" data-original-title="facebook" href="javascript:;"> </a> 
                        </li> 
                        <li> 
                            <a class="twitter" data-original-title="Twitter" href="javascript:;"> </a> 
                        </li> 
                        <li> 
                            <a class="googleplus" data-original-title="Goole Plus" href="javascript:;"> </a> 
                        </li> 
                        <li> 
                            <a class="linkedin" data-original-title="Linkedin" href="javascript:;"> </a> 
                        </li> 
                    </ul> 
                </div> 
                <div class="forget-password"> 
                    <h4>Lupa Password ?</h4> 
                    <p> Jangan takut, tekan 
                        <a href="javascript:;" id="forget-password"> ini </a> untuk mengatur ulang password. </p> 
                </div> 
                <div class="create-account"> 
                    <p> Anda tidak mempunyai akun ?&nbsp; 
                        <a href="{{ url('/register/pemodal') }}"> Buat Akun </a> 
                    </p> 
                </div> 
            </form> 
            <!-- END LOGIN FORM --> 
            <!-- BEGIN FORGOT PASSWORD FORM --> 
            <form class="forget-form" action="{{ url('/password/email') }}" method="post"> 
                <h3>Lupa Password ?</h3> 
                @if (session('status')) 
                    <div class="alert alert-success"> 
                        {{ session('status') }} 
                    </div> 
                @endif 
                <p> Masukan Alamat e-mail anda untuk mengatur ulang password.</p> 
                {{ csrf_field() }} 
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}"> 
                    <div class="input-icon"> 
                        <i class="fa fa-envelope"></i> 
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" value="{{ old('email') }}" /> 
 
                        @if ($errors->has('email')) 
                            <span class="help-block"> 
                                <strong>{{ $errors->first('email') }}</strong> 
                            </span> 
                        @endif 
                    </div> 
                </div> 
                <div class="form-actions"> 
                    <button type="button" id="back-btn" class="btn red btn-outline">Kembali </button> 
                    <button type="submit" class="btn green pull-right"> Kirim </button> 
                </div> 
            </form> 
            <!-- END FORGOT PASSWORD FORM --> 
            <!-- BEGIN REGISTRATION FORM --> 
            <form class="register-form" action="{{ url('/register') }}" method="post"> 
                <h3>Sign Up</h3> 
                <p> Enter your personal details below: </p> 
                {{ csrf_field() }} 
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}"> 
                    <label class="control-label visible-ie8 visible-ie9">Full Name</label> 
                    <div class="input-icon"> 
                        <i class="fa fa-font"></i> 
                        <input class="form-control placeholder-no-fix" type="text" placeholder="Full Name" name="name" value="{{ old('name') }}" /> 
 
                        @if ($errors->has('name')) 
                            <span class="help-block"> 
                                <strong>{{ $errors->first('name') }}</strong> 
                            </span> 
                        @endif 
                    </div> 
                </div> 
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}"> 
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that--> 
                    <label class="control-label visible-ie8 visible-ie9">Email</label> 
                    <div class="input-icon"> 
                        <i class="fa fa-envelope"></i> 
                        <input class="form-control placeholder-no-fix" type="text" placeholder="Email" name="email" value="{{ old('email') }}" /> 
 
                        @if ($errors->has('email')) 
                            <span class="help-block"> 
                                <strong>{{ $errors->first('email') }}</strong> 
                            </span> 
                        @endif 
                    </div> 
                </div> 
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}"> 
                    <label class="control-label visible-ie8 visible-ie9">Password</label> 
                    <div class="input-icon"> 
                        <i class="fa fa-lock"></i> 
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Password" name="password" /> 
 
                        @if ($errors->has('password')) 
                            <span class="help-block"> 
                                <strong>{{ $errors->first('password') }}</strong> 
                            </span> 
                        @endif 
                    </div> 
                </div> 
                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}"> 
                    <label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label> 
                    <div class="controls"> 
                        <div class="input-icon"> 
                            <i class="fa fa-check"></i> 
                            <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Re-type Your Password" name="password_confirmation" /> 
 
                            @if ($errors->has('password_confirmation')) 
                                <span class="help-block"> 
                                    <strong>{{ $errors->first('password_confirmation') }}</strong> 
                                </span> 
                            @endif 
                        </div> 
                    </div> 
                </div> 
                <div class="form-group"> 
                    <label class="mt-checkbox mt-checkbox-outline"> 
                        <input type="checkbox" name="tnc" /> I agree to the 
                        <a href="javascript:;">Terms of Service </a> & 
                        <a href="javascript:;">Privacy Policy </a> 
                        <span></span> 
                    </label> 
                    <div id="register_tnc_error"> </div> 
                </div> 
                <div class="form-actions"> 
                    <button id="register-back-btn" type="button" class="btn red btn-outline"> Back </button> 
                    <button type="submit" id="register-submit-btn" class="btn green pull-right"> Sign Up </button> 
                </div> 
            </form> 
            <!-- END REGISTRATION FORM --> 
        </div> 
        <!-- END LOGIN --> 
        <!-- BEGIN COPYRIGHT --> 
        <div class="copyright"> 2017 &copy; PT. Finnesia. </div> 
        <!-- END COPYRIGHT --> 
        <!--[if lt IE 9]> 
<script src="{{ asset('assets/global/plugins/respond.min.js') }}"></script> 
<script src="{{ asset('assets/global/plugins/excanvas.min.js') }}"></script>  
<script src="{{ asset('assets/global/plugins/ie8.fix.min.js') }}"></script>  
<![endif]--> 
        <!-- BEGIN CORE PLUGINS --> 
        <script src="{{ asset('assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script> 
        <script src="{{ asset('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script> 
        <script src="{{ asset('assets/global/plugins/js.cookie.min.js') }}" type="text/javascript"></script> 
        <script src="{{ asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script> 
        <script src="{{ asset('assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script> 
        <script src="{{ asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script> 
        <!-- END CORE PLUGINS --> 
        <!-- BEGIN PAGE LEVEL PLUGINS --> 
        <script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script> 
        <script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script> 
        <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script> 
        <script src="{{ asset('assets/global/plugins/backstretch/jquery.backstretch.min.js') }}" type="text/javascript"></script> 
        <script src="{{ asset('/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> 
        <!-- END PAGE LEVEL PLUGINS --> 
        <!-- BEGIN THEME GLOBAL SCRIPTS --> 
        <script src="{{ asset('assets/global/scripts/app.min.js') }}" type="text/javascript"></script> 
        <!-- END THEME GLOBAL SCRIPTS --> 
        <!-- BEGIN PAGE LEVEL SCRIPTS --> 
        <script src="{{ asset('assets/pages/scripts/login.min.js') }}" type="text/javascript"></script> 
        <script type="text/javascript"> </script> 
        <!-- END PAGE LEVEL SCRIPTS --> 
        <!-- BEGIN THEME LAYOUT SCRIPTS --> 
        <!-- END THEME LAYOUT SCRIPTS --> 
        <script> 
            $(document).ready(function() 
            { 
                $('#clickmewow').click(function() 
                { 
                    $('#radio1003').attr('checked', 'checked'); 
                }); 
            }) 
        </script> 
         
    </body> 
 
</html> 