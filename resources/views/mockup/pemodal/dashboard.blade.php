@extends('layouts.default')

@push('page-plugin-style')

@endpush

@section('nav')
<ul class="nav navbar-nav">
    <li class="menu active">
        <a href="{{ url('/pemodal/dashboard') }}"><i class="icon-bar-chart"></i> Dashboard </a>
    </li>
    <li class="menu">
        <a href="{{ url('/pemodal/cari') }}"><i class="icon-magnifier"></i> Cari Pinjaman </a>
    </li>
    <li class="menu">
        <a href="{{ url('/pemodal/daftar') }}"><i class="icon-diamond"></i> Daftar Pendanaan</a>
    </li>
    <li class="menu">
        <a href="{{ url('/pemodal/deposit') }}"><i class="fa fa-upload"></i> Deposit</a>
    </li>
    <li class="menu">
        <a href="{{ url('/pemodal/penarikan') }}"><i class="fa fa-download"></i> Penarikan</a>
    </li>
    <li class="menu">
        <a href="{{ url('/pemodal/history') }}"><i class="fa fa-history"></i> History Transaksi</a>
    </li>
</ul>                                   
@endsection

@section('title')
Dashboard
@endsection

@section('crumbs')
	<ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/pemodal/dashboard') }}">Pemodal</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Dashboard</span>
        </li>
    </ul>
@endsection

@section('content')
@php
function rupiah($angka)
{
 $rupiah = number_format($angka,2,',','.');
 return $rupiah;
}
@endphp
{{-- thumb --}}
<div class="row">
<div class="col-md-3">
    <!-- BEGIN WIDGET THUMB -->
    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
        <h4 class="widget-thumb-heading">Total Nilai Akun</h4>
        <div class="widget-thumb-wrap">
            <i class="widget-thumb-icon bg-green icon-user"></i>
            <div class="widget-thumb-body">
                <span class="widget-thumb-subtitle">-</span>
                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="10,225,000" style="font-size: 20px;"></span>
            </div>
        </div>
    </div>
    <!-- END WIDGET THUMB -->
</div>
<div class="col-md-3">
    <!-- BEGIN WIDGET THUMB -->
    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
        <h4 class="widget-thumb-heading">Dana Tersedia</h4>
        <div class="widget-thumb-wrap">
            <i class="widget-thumb-icon bg-red icon-bulb"></i>
            <div class="widget-thumb-body">
                <span class="widget-thumb-subtitle">Rp.</span>
                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="7,000,000" style="font-size: 20px;">0</span>
            </div>
        </div>
    </div>
    <!-- END WIDGET THUMB -->
</div>
<div class="col-md-3">
    <!-- BEGIN WIDGET THUMB -->
    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
        <h4 class="widget-thumb-heading">Dana Dialokasikan</h4>
        <div class="widget-thumb-wrap">
            <i class="widget-thumb-icon bg-purple icon-paper-plane"></i>
            <div class="widget-thumb-body">
                <span class="widget-thumb-subtitle">Rp.</span>
                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="3,000,000" style="font-size: 20px;">0</span>
            </div>
        </div>
    </div>
    <!-- END WIDGET THUMB -->
</div>
<div class="col-md-3">
    <!-- BEGIN WIDGET THUMB -->
    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
        <h4 class="widget-thumb-heading">Bunga Diterima</h4>
        <div class="widget-thumb-wrap">
            <i class="widget-thumb-icon bg-blue icon-bar-chart"></i>
            <div class="widget-thumb-body">
                <span class="widget-thumb-subtitle">Rp.</span>
                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="225.000" style="font-size: 20px;">0</span>
            </div>
        </div>
    </div>
     <!-- END WIDGET THUMB -->
 </div>
</div>
{{-- end thumb --}}
<div class="row">
<div class="col-md-12">
    <div class="portlet light ">
        <div class="portlet-title">
            <div class="caption ">
                <span class="caption-subject font-dark bold uppercase">Grafik Pendapatan 1 Tahun Terakhir</span>
                <span class="caption-helper">.</span>
            </div>
            <div class="actions">
                
            </div>
        </div>
        <div class="portlet-body">
            <div id="test" class="CSSAnimationChart"></div>
        </div>
    </div>
</div>
</div>

<div class="row">
 <div class="col-md-6">
	<div class="portlet light portlet-fit ">
	    <div class="portlet-title">
	        <div class="caption">
	            <i class="icon-microphone font-dark hide"></i>
	            <span class="caption-subject bold font-dark uppercase">Resume History Transaksi</span>
	            <span class="caption-helper"></span>
	        </div>
	    </div>

	    <div class="portlet-body">
	       <table class="table">
	       	<tr>
	       		<td>Total Pendapatan</td>
	       		<td>:</td>
	       		<td>Rp.<font class="pull-right">{{ rupiah(000000000) }}</font></td>
	       		<td></td>
	       		<td><a href="#" class="btn green btn-outline btn-circle btn-sm"> <i class="fa fa-search"></i> Detail</a></td>
	       	</tr>
	       	<tr>
	       		<td>Total Pendanaan</td>
	       		<td>:</td>
	       		<td>Rp.<font class="pull-right">{{ rupiah(000000000) }}</font></td>
	       		<td></td>
	       		<td><a href="#" class="btn green btn-outline btn-circle btn-sm"> <i class="fa fa-search"></i> Detail</a></td>
	       	</tr>
	       	<tr>
	       		<td>Total Penarikan</td>
	       		<td>:</td>
	       		<td>Rp.<font class="pull-right">{{ rupiah(000000000) }}</font></td>
	       		<td></td>
	       		<td><a href="#" class="btn green btn-outline btn-circle btn-sm"> <i class="fa fa-search"></i> Detail</a></td>
	       	</tr>
	       </table>
	    </div>
	</div>
  </div>
  <div class="col-md-6">
	<div class="portlet light portlet-fit ">
	    <div class="portlet-title">
	        <div class="caption">
	            <i class="icon-microphone font-dark hide"></i>
	            <span class="caption-subject bold font-dark uppercase">Transaksi Terakhir</span>
	            <span class="caption-helper"></span>
	        </div>
	    </div>

	    <div class="portlet-body">
	       <table class="table">
	       	<tr>
	       		<td>Deposit</td>
	       		<td>:</td>
	       		<td>Rp.<font class="pull-right">{{ rupiah(000000000) }}</font></td>
	       		<td></td>
	       		<td><a href="#" class="btn green btn-outline btn-circle btn-sm"> <i class="fa fa-search"></i> Detail</a></td>
	       	</tr>
	       	<tr>
	       		<td>Pendanaan</td>
	       		<td>:</td>
	       		<td>Rp.<font class="pull-right">{{ rupiah(000000000) }}</font></td>
	       		<td></td>
	       		<td><a href="#" class="btn green btn-outline btn-circle btn-sm"> <i class="fa fa-search"></i> Detail</a></td>
	       	</tr>
	       	<tr>
	       		<td>Penarikan</td>
	       		<td>:</td>
	       		<td>Rp.<font class="pull-right">{{ rupiah(000000000) }}</font></td>
	       		<td></td>
	       		<td><a href="#" class="btn green btn-outline btn-circle btn-sm"> <i class="fa fa-search"></i> Detail</a></td>
	       	</tr>
	       </table>
	    </div>
	</div>
  </div>
</div>
                                                     
@endsection
@push('page-plugin-scripts')
<script src="{{ asset('/assets/global/plugins/amcharts/amcharts/amcharts.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/amcharts/amcharts/serial.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/amcharts/amcharts/pie.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/amcharts/amcharts/radar.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/amcharts/amcharts/themes/light.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/amcharts/amcharts/themes/patterns.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/amcharts/amcharts/themes/chalk.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/amcharts/amstockcharts/amstock.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/flot/jquery.flot.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/flot/jquery.flot.categories.min.js"') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/morris/raphael-min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
@endpush

@push('page-scripts')
<script type="text/javascript">
    var chart = AmCharts.makeChart("test", {
                "type": "serial",
                "addClassNames": true,
                "theme": "light",
                "path": "../assets/global/plugins/amcharts/ammap/images/",
                "autoMargins": false,
                "marginLeft": 30,
                "marginRight": 8,
                "marginTop": 10,
                "marginBottom": 26,
                "balloon": {
                    "adjustBorderColor": false,
                    "horizontalPadding": 10,
                    "verticalPadding": 8,
                    "color": "#ffffff"
                },

                "dataProvider": [{
                    "mounth": "Januari",
                    "income": 0,
                    "expenses": 25
                }, {
                    "mounth": "Februari",
                    "income": 0,
                    "expenses": 25
                }, {
                    "mounth": "Maret",
                    "income": 0,
                    "expenses": 25
                }, {
                    "mounth": "April",
                    "income": 0,
                    "expenses": 25
                }, {
                    "mounth": "Mei",
                    "income": 0,
                    "expenses": 50,
                },{
                    "mounth": "Juni",
                    "income": 0,
                    "expenses": 75,
                },{
                    "mounth": "Juli",
                    "income": 0,
                    "expenses": 75,
                },{
                    "mounth": "Agustus",
                    "income": 0,
                    "expenses": 75,
                },{
                    "mounth": "September",
                    "income": 0,
                    "expenses": 75,
                },{
                    "mounth": "Oktober",
                    "income": 0,
                    "expenses": 75,
                },{
                    "mounth": "November",
                    "income": 0,
                    "expenses": 70,
                }, {
                    "mounth": "Desember",
                    "income": 0,
                    "expenses": 65,
                    "dashLengthColumn": 5,
                    "alpha": 0.2,
                    "additional": "(projection)"
                }],
                "valueAxes": [{
                    "axisAlpha": 0,
                    "position": "left"
                }],
                "startDuration": 1,
                "graphs": [{
                    
                }, {
                    "id": "graph2",
                    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                    "bullet": "round",
                    "lineThickness": 3,
                    "bulletSize": 7,
                    "bulletBorderAlpha": 1,
                    "bulletColor": "#FFFFFF",
                    "useLineColorForBulletBorder": true,
                    "bulletBorderThickness": 3,
                    "fillAlphas": 0,
                    "lineAlpha": 1,
                    "title": "Expenses",
                    "valueField": "expenses"
                }],
                "categoryField": "mounth",
                "categoryAxis": {
                    "gridPosition": "start",
                    "axisAlpha": 0,
                    "tickLength": 0
                },
                "export": {
                    "enabled": true
                }
            });
</script>
@endpush