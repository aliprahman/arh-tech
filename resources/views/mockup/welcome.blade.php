@extends('layouts.default')

@push('page-plugin-styles')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('title')
WELCOME
@endsection

@section('content')
@if(auth::guest())
<div class="row">
    <div class="col-md-12">
        <center>
            <a href="{{ url('/register/peminjam') }}" class="btn green btn-outline"> DAFTAR SEBAGAI PEMINJAM</a>
            <a href="{{ url('/register/pemodal') }}"  class="btn green btn-outline"> DAFTAR SEBAGAI PEMODAL</a>
        </center>
    </div>
</div>            
@else

@endif                
@endsection
@push('page-plugin-scripts')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
@endpush

@push('page-scripts')
<script src="{{ asset('assets/pages/scripts/table-datatables-managed.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#data').DataTable();
} ); 
</script>
@endpush
