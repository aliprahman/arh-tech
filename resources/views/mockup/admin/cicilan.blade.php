@extends('layouts.app')

@push('page-plugin-styles')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
@endpush

@section('title')
Daftar Cicilan
@endsection

@section('crumbs')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('dashboard') }}">Admin</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Daftar Cicilan</span>
        </li>
    </ul>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="portlet light ">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-diamond"></i>
                <span class="caption-subject bold uppercase">Daftar Cicilan</span>
            </div>
            <div class="actions">
                <a class="btn btn-circle btn-default" href="javascript:;">
                    <i class="fa fa-print"></i> Print to PDF
                </a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="col-md-12">
                <center><h1>Daftar Cicilan</h1></center>
            </div>
            <div class="col-md-12 margin-bottom-20">
                <form method="POST" id="filter-list" class="form-inline" role="form">
                    <div class="form-group">
                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                            <input type="text" name="start" id="start" class="form-control col-sm-4" readonly="" value="{{ date('Y-m')."-01" }}" required>
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                     <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                            <input type="text" name="end" id="end" class="form-control col-sm-4" readonly="" value="{{ date('Y-m-t') }}" required>
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                     <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="status" id="status">
                          <option value="">Semua</option>
                          <option value="unverified">Unverified</option>
                          <option value="pending">pending</option>
                          <option value="verified">Verified</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">
                        <i class="icon-magnifier"></i>
                        Search
                    </button>
                </form>
            </div>
            <div class="col-md-12">
                <table class="table table-striped table-bordered" id="cicilan-table">
                    <thead>
                        <tr>
                            <th> No</th>
                            <th> Nama</th>
                            <th> Tanggal <br> Jatuh Tempo</th>
                            <th> Jumlah</th>
                            <th> Transfer Ke </th>
                            <th> Tanggal <br> Transfer </th>
                            <th> Status </th>
                            <th> Aksi </th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="clearfix margin-bottom-20"></div>
         </div>
      </div>
    </div>
 </div>
 <div id="basic" class="modal fade" role="dialog" aria-hidden="true">
     <div class="modal-dialog modal-lg">
         <div class="modal-content">
             <form id="form_konfirmasi" method="post">
                 {{ csrf_field() }}
                 {{ method_field('PUT') }}
                 <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                     <h4 class="modal-title">Detail Transaksi</h4>
                 </div>
                 <div class="modal-body">
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="ref" class="control-label"><font class="pull-left">No Transaksi</font></label>
                          <input type="text" name="ref" id="ref" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                          <label for="jumlah" class="control-label"><font class="pull-left">Jumlah Cicilan</font></label>
                          <div class="input-group">
                              <span class="input-group-addon">Rp.</span>
                              <input type="text" name="jumlah" id="jumlah" class="form-control" readonly>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="bank_tujuan" class="control-label"><font class="pull-left">Bank Tujuan</font></label>
                          <input type="text" name="bank_tujuan" id="bank_tujuan" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                          <label for="tgl_transfer" class="control-label"><font class="pull-left">Tanggal Transfer</font></label>
                          <input type="text" name="tgl_transfer" id="tgl_transfer" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                          <label for="via" class="control-label"><font class="pull-left">Transfer Via</font></label>
                          <input type="text" name="via" id="via" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                          <label for="nama_pengirim" class="control-label"><font class="pull-left">Nama Pemilik Akun</font></label>
                          <input type="text" name="nama_pengirim" id="nama_pengirim" class="form-control" readonly>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="norek" class="control-label"><font class="pull-left">No Rekening</font></label>
                          <input type="text" name="norek" id="norek" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                          <label for="catatan" class="control-label"><font class="pull-left">Catatan</font></label>
                          <textarea name="catatan" id="catatan" class="form-control" rows="2" readonly></textarea>
                        </div>
                        <div class="form-group">
                          <label for="bukti" class="control-label"><font class="pull-left">Bukti Transfer</font></label><br>
                          <img id="bukti" style="max-height: 160px; max-width: 250px" alt="Bukti Transfer">
                        </div>
                        <div class="form-group">
                          <div class="g-recaptcha" id="captch" data-sitekey="{{ env('CAPTCHA_SITEKEY') }}"></div>
                        </div>
                      </div>
                    </div>
                 </div>
                 <div class="modal-footer">
                     <button type="button" style="float: left" class="btn dark btn-outline" data-dismiss="modal" aria-hidden="true">Close</button>
                     <button type="submit" style="float: right" id="btn_konfirm" class="btn green">Konfirmasi Cicilan</button>
                 </div>
             </form>
         </div>
     </div>
 </div>
@endsection
@push('page-plugin-scripts')
<!-- DataTables -->
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<!-- Select2 -->
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
@endpush

@push('page-scripts')
<script type="text/javascript">
$(function() {
    $('.date-picker').datepicker({
        orientation: "left",
        autoclose: true
    });
    $('.select2').select2();
    var oTable = $('#cicilan-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{url("/admin/daftar/cicilan") }}',
            method: 'POST',
            data: function(d){
                d.startDate = moment($('#start').val()).format('YYYY-MM-DD');
                d.endDate =  moment($('#end').val()).format('YYYY-MM-DD');
                d.status = $('#status').val();
            }
        },
        columns: [
            {data: "DT_Row_Index", orderable: false, searchable: false},
            { data: 'peminjam', name: 'peminjam' },
            { data: 'jatuh_tempo', name: 'jatuh_tempo' },
            { data: 'jumlah', name: 'jumlah' },
            { data: 'nama_bank', name: 'nama_bank' },
            { data: 'tgl_transfer', name: 'tgl_transfer' },
            { data: 'status', name: 'status' },
            { data: 'action', orderable: false, searchable: false }
        ],
        rowCallback: function(row,data,index){
            // nomer
            $('td:eq(0)', row).html( '<center>'+data.DT_Row_Index+'</center>' );
            // peminjam
            $('td:eq(1)',row).html('<a target="_blank" href="{{ url('pinjaman/analisa') }}/'+data.pinjaman_id+'">'+data.peminjam+'</a>')
            // tanggal jatuh tempo cicilan
            $('td:eq(2)',row).html(moment(data.jatuh_tempo).format("DD-MM-YYYY"));
            // jumlah_pinjaman
            $('td:eq(3)',row).html('<span class="pull-left">Rp.</span><span class="pull-right">'+numeral(data.jumlah).format('0,0')+'</span>');
            // tanggal transafer
            if(data.tgl_transfer != null){
              $('td:eq(5)',row).html(moment(data.tgl_transfer).format("DD-MM-YYYY"));
            }
            // status
            $('td:eq(6)',row).html('<center><font color="red"><b>'+data.status+'</b></font></center>');
            // action
            if(data.status == 'pending'){
              var label = 'Konfirmasi Cicilan';
            }else if(data.status == 'verified'){
              var label = 'Detail'
            }

            if(data.status != 'unverified'){
              $('td:eq(7)',row).html('<a href="#" class="btn btn-circle btn-sm green btn-outline" onclick="modal_cicilan('+data.id+')">'+label+'</a>');
            }
        }
    });
    $('#filter-list').on('submit', function(e) {
        oTable.ajax.reload();
        e.preventDefault();
    });
});

function modal_cicilan(id) {
  $.ajax({
    type: 'GET',
    url: '{{ url('admin/cicilan') }}'+'/'+id,
    success: function(result){
      $('#form_konfirmasi').attr('action','{{ url('admin/cicilan') }}'+'/'+id);
      $('#ref').val(result.ref);
      $('#jumlah').val(numeral(result.jumlah).format('0,0'));
      $('#bank_tujuan').val(result.nama_bank_tujuan_transfer);
      $('#tgl_transfer').val(result.tanggal);
      $('#via').val(result.akun_bank_nama_bank);
      $('#nama_pengirim').val(result.akun_bank_nama_akun);
      $('#norek').val(result.akun_bank_no_akun);
      $('#catatan').val(result.deskripsi);
      if(result.file_bukti == null){
        $('#bukti').css('display','none');
      }else{
        $('#bukti').attr('src','{{ asset('storage/bukti_transfer_cicilan') }}'+'/'+result.file_bukti);
      }
      if(result.status == 'verified'){
        $('#captch').css('display','none');
        $('#btn_konfirm').css('display','none');
      }else{
        $('#captch').css('display','block');
        $('#btn_konfirm').css('display','block');
      }
    },
    complete: function(){
      $('#basic').modal('show');
    }
  });
}
</script>
@if (session()->has('success'))
  <script type="text/javascript">
    $(function () {
      swal({
          allowOutsideClick: false,
          allowEscapeKey: false,
          text: "Transfer Cicilan Terverifikasi",
          title: "Berhasil",
          type: "success",
          confirmButtonClass: "btn green btn-circle btn-outline"
      });
    });
  </script>
@endif
@if ($errors->any())
  <script type="text/javascript">
    $(function () {
      swal({
          allowOutsideClick: false,
          allowEscapeKey: false,
          text: "Please ensure that you are a human.!",
          title: "Gagal",
          type: "error",
          confirmButtonClass: "btn green btn-circle btn-outline"
      });
    });
  </script>
@endif
@endpush
