@extends('layouts.app')

@push('page-plugin-styles')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endpush
@section('nav')
    <ul class="nav navbar-nav">
        <li class="menu">
            <a href="{{ url('/admin/dashboard') }}"><i class="icon-bar-chart"></i> Dashboard </a>
        </li>
        <li class="menu">
            <a href="{{ url('/admin/pinjaman') }}"><i class="icon-magnifier"></i> Daftar Pinjaman </a>
        </li>
        <li class="menu">
            <a href="{{ url('/admin/pendanaan') }}"><i class="icon-magnifier"></i> Daftar Pendanaan </a>
        </li>
        <li class="menu">
            <a href="{{ url('/admin/akun') }}"><i class="icon-magnifier"></i> Daftar Akun </a>
        </li>
    </ul>
@endsection

@section('title')
Daftar Pinjaman
@endsection

@section('crumbs')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/admin/dashboard') }}">Admin</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Daftar Pinjaman</span>
        </li>
    </ul>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="portlet light ">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-diamond"></i>
                <span class="caption-subject bold uppercase">Daftar Pinjaman</span>
            </div>
            <div class="actions">
                <a class="btn btn-circle btn-default" href="javascript:;">
                    <i class="fa fa-print"></i> Print to PDF
                </a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="col-md-12">
                <center><h1>Daftar Pinjaman</h1></center>
            </div>
            <div class="col-md-12 margin-bottom-20">
                <form method="POST" id="filter-list" class="form-inline" role="form">
                    <div class="form-group">
                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                            <input type="text" name="start" id="start" class="form-control col-sm-4" readonly="" value="{{ date('Y-m-d') }}" required>
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                     <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                            <input type="text" name="end" id="end" class="form-control col-sm-4" readonly="" value="{{ date('Y-m-d') }}" required>
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                     <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">
                        <i class="icon-magnifier"></i>
                        Search
                    </button>
                </form>
            </div>
            <div class="col-md-12">
                <table class="table table-striped table-bordered" id="pinjaman-table">
                    <thead>
                        <tr>
                            <th> No</th>
                            <th> Nama</th>
                            <th> Tanggal Pengajuan</th>
                            <th> Jumlah Pinjaman</th>
                            <th> Lama Pinjaman</th>
                            <th> Status </th>
                            <th> Aksi </th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="clearfix margin-bottom-20"></div>
         </div>
      </div>
    </div>
 </div>

@endsection
@push('page-plugin-scripts')
<!-- DataTables -->
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<!-- Select2 -->
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
@endpush

@push('page-scripts')
<script type="text/javascript">
$(function() {
    $('.date-picker').datepicker({
        orientation: "left",
        autoclose: true
    });
    $('.select2').select2();
    var oTable = $('#pinjaman-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{url("/admin/daftar/pinjaman") }}',
            method: 'POST',
            data: {
                startDate: moment($('#start').val()).format('YYYY-MM-DD HH:mm:ss'),
                endDate: moment($('#end').val()).add({
                    hours: 23,
                    minutes: 59,
                    seconds: 59
                }).format('YYYY-MM-DD HH:mm:ss')
            }
        },
        columns: [
            {data: "DT_Row_Index", orderable: false, searchable: false},
            { data: 'nama', name: 'nama' },
            { data: 'tanggal', name: 'tanggal' },
            { data: 'jumlah', name: 'jumlah' },
            { data: 'waktu', name: 'waktu' },
            { data: 'status', name: 'status' },
            { data: 'action', orderable: false, searchable: false }
        ],
        rowCallback: function(row,data,index){
            // nomer
            $('td:eq(0)', row).html( '<center>'+data.DT_Row_Index+'</center>' );
            // tanggal_pinjaman
            $('td:eq(2)',row).html(moment(data.tanggal).format("DD-MM-YYYY"));
            // jumlah_pinjaman
            $('td:eq(3)',row).html(numeral(data.jumlah).format('0,0'));
            // action
            var action_link = '<a href="'+$('meta[name="base_url"]').attr('content')+'/admin/pinjaman/details/'+data.id+'" class="btn btn-circle btn-sm red btn-outline"><i class="fa fa-search"></i> Analisis</a>'
            $('td:eq(6)', row).html( action_link );

        }
    });
    $('#filter-list').on('submit', function(e) {
        oTable.ajax.reload();
        e.preventDefault();
    });
});
</script>
@endpush
