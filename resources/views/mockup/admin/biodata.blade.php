@extends('layouts.app')

@push('page-plugin-styles')
<link href="{{ asset('/assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css">
<style type="text/css">
  .select2 {
   width:100%!important;
   }
</style>
@endpush

@section('title')
Biodata
@endsection
@section('crumbs')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/peminjam/dashboard') }}">Peminjam</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Biodata Peminjaman</span>
        </li>
    </ul>
@endsection
@section('content')
<div class="portlet light portlet-fit">
  <div class="portlet-body">
    <form class="form-horizontal" method="post" action="{{ url('admin/biodata') }}" enctype="multipart/form-data">
      {{ csrf_field() }}
      <div class="row">
          <div class="col-md-12">
              @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              @if(session('success') != "")
                  <div class="alert alert-success" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                      {{ session('success') }}
                  </div>
              @endif
              <div class="profile-userpic" style="margin: 0 auto;">
                  @if($user->file_uploads != "" && $user->file_uploads->name != "")
                    <img src="{{ asset('storage/profile_photo/'.$user->file_uploads->name) }}" class="preview img-responsive" id="profile_photo" style="width: 100px; height: 100px;" />
                  @else
                    <img src="{{ asset('/img/dummy-profile.jpg') }}" class="preview img-responsive" id="profile_photo" style="width: 100px; height: 100px;" />
                  @endif
              </div>
              <div class="profile-userbuttons" style="margin-top: 15px; margin-bottom: 10px;">
                  <span class="btn btn-circle btn-default btn-file">
                  <span class="fileinput-new"><i class="fa fa-cloud-upload"></i>  Change Profile</span>
                  <span class="fileinput-exists"> </span>
                  <input type="hidden"><input type="file" name="profile_photo" onchange="preview_image(event,'profile_photo')">
                  </span>
              </div>
          </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
              <h5><label class="control-label col-sm-4">Nama</label></h5>
              <div class="col-sm-8">
                  <input class="form-control" name="nama" type="text" value="{{ $user->name }}" required>
              </div>
          </div>
          <div class="form-group">
              <h5><label class="control-label col-sm-4">Email</label></h5>
              <div class="col-sm-8">
                  <input class="form-control" name="email" type="email" value="{{ $user->email }}" required>
              </div>
          </div>
          <div class="form-group">
              <h5><label class="control-label col-sm-4">Tempat Lahir</label></h5>
              <div class="col-sm-8">
                  <input class="form-control" name="tempat_lahir" type="text" value="{{ $biodata->tempat_lahir }}">
              </div>
          </div>
          <div class="form-group">
              <h5><label class="control-label col-sm-4">Tanggal Lahir</label></h5>
              <div class="col-sm-8">
                  <input class="form-control form-control date-picker" name="tanggal_lahir" size="16" type="text" value="{{ $biodata->tanggal_lahir }}">
              </div>
          </div>
          <div class="form-group" style="margin-bottom: 0">
              <h5><label class="control-label col-sm-4">Jenis Kelamin</label></h5>
              <div class="col-sm-8">
                   <div class="mt-radio-inline">
                      <label class="mt-radio">
                          <input type="radio" name="jenis_kelamin" value="laki-Laki" @if($biodata->jenis_kelamin == 'laki-laki') checked @endif> Laki-Laki
                          <span></span>
                      </label>
                      <label class="mt-radio">
                          <input type="radio" name="jenis_kelamin" value="perempuan" @if($biodata->jenis_kelamin == 'perempuan') checked @endif> Perempuan
                          <span></span>
                      </label>
                  </div>
              </div>
          </div>
          <div class="form-group">
              <h5><label class="control-label col-sm-4">KTP</label></h5>
              <div class="col-sm-8">
                  <input class="form-control" name="no_ktp" maxlength="16" type="text" value="{{ $biodata->no_ktp }}">
              </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
              <h5><label class="control-label col-sm-4">Telepon</label></h5>
              <div class="col-sm-8">
                  <div class="input-group">
                      <input type="text" class="form-control" name="no_hp" value="{{ $biodata->no_hp }}">
                      <span class="input-group-addon">
                          <i class="fa fa-mobile-phone"></i>
                      </span>
                  </div>
              </div>
              <div class="col-md-12">&nbsp;</div>
              <h5><label class="control-label col-sm-4">&nbsp;</label></h5>
              <div class="col-sm-8">
                  <div class="input-group">
                      <input type="text" class="form-control" name="no_telepon" value="{{ $biodata->no_telepon }}">
                      <span class="input-group-addon">
                          <i class="fa fa-phone"></i>
                      </span>
                  </div>
              </div>
          </div>
          <div class="form-group">
              <h5><label class="control-label col-sm-4">NPWP</label></h5>
              <div class="col-sm-8">
                  <input class="form-control" maxlength="20" name="no_npwp" type="text" value="{{ $biodata->no_npwp }}">
              </div>
          </div>
          <div class="form-group">
              <h5><label class="control-label col-sm-4">New Password</label></h5>
              <div class="col-sm-8">
                  <input class="form-control" name="new_password" type="password">
              </div>
          </div>
          <div class="form-group">
              <h5><label class="control-label col-sm-4">Confirm New Password</label></h5>
              <div class="col-sm-8">
                  <input class="form-control" name="new_password_confirmation" type="password">
              </div>
          </div>
          <div class="form-group">
              <div class="col-sm-8 col-md-offset-4">
                <button type="submit" class="btn btn-success">Simpan</button>
              </div>
          </div>
        </div>
      </div>
    </from>
  </div>
</div>
@endsection

@push('page-plugin-scripts')
<script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/common.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.number.min.js') }}" type="text/javascript"></script>
@endpush

@push('page-scripts')
<script src="{{ asset('/assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2({
            theme: "bootstrap"
        });
        $('.rupiah').number(true,2,',','.');
    });
</script>
@endpush
