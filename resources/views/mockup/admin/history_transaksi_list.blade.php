@extends('layouts.app')

@push('page-plugin-styles')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .table thead tr th{
            white-space: pre-wrap;
            vertical-align: middle;
            text-align: center;
            padding-top: 20px;
            padding-bottom: 20px;
        }
        .table tbody tr td{
            vertical-align: middle;
        }
        #data th, #data td {
            font-size: 13px;
        }
    </style>
@endpush
@section('nav')
    <ul class="nav navbar-nav">
        <li class="menu">
            <a href="{{ url('/admin/dashboard') }}"><i class="icon-bar-chart"></i> Dashboard </a>
        </li>
        <li class="menu">
            <a href="{{ url('/admin/pinjaman') }}"><i class="icon-magnifier"></i> Daftar Pinjaman </a>
        </li>
        <li class="menu">
            <a href="{{ url('/admin/pendanaan') }}"><i class="icon-magnifier"></i> Daftar Pendanaan </a>
        </li>
        <li class="menu">
            <a href="{{ url('/admin/akun') }}"><i class="icon-magnifier"></i> Daftar Akun </a>
        </li>
    </ul>
@endsection

@section('title')
History Transaksi
@endsection

@section('crumbs')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/admin/dashboard') }}">Admin</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>History Transaksi</span>
        </li>
    </ul>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="portlet light ">
        <div class="portlet-title">
            <div class="caption">                
                <span class="caption-subject bold uppercase">
                    History
                </span>
            </div>
            <div class="actions">
                <a class="btn btn-circle btn-default" href="javascript:;" id="print_history_pdf">
                    <i class="fa fa-print"></i> Print to PDF
                </a>
            </div>
        </div>
        <div class="portlet-body">       
            <div class="col-md-4 margin-bottom-40">{{ Form::selectMonthCustom() }}</div>
            <div class="col-md-4 margin-bottom-40">{{ Form::selectYearCustom() }}</div>
            <div class="col-md-2 margin-bottom-40">
                <button class="btn btn-block btn-primary" type="button" id="button-filter">
                    <span class="fa fa-filter"></span> Filter
                </button>
            </div>     
            <div class="col-md-12">       
            <table class="table table-striped table-bordered" id="history-table">
                    <thead>
                        <tr>
                            <th>No</th>  
                            <th>Tanggal</th>                                                                                 
                            <th>Kode</th>                            
                            <th>Debit</th>
                            <th>Kredit</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>                        
            </div>    
            <div class="clearfix margin-bottom-20"></div>
         </div>
      </div>
    </div>
 </div>

 <div class="modal fade" id="printModal" tabindex="-1" role="dialog" aria-labelledby="printModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="contactModalLabel">Print Preview</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button id="printModal-print-btn" type="button" class="btn btn-primary"><i class="fa fa-print"></i> Print</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('page-plugin-scripts')
<!-- DataTables -->
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<!-- Select2 -->
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
@endpush

@push('page-scripts')
<script type="text/javascript">
$(function() {   
    $('#print_history_pdf').on('click', function () {
        var iframeHeight = $(window).height() - 220;

        $('#printModal .modal-body').html('<iframe id="printModal-iframe" src="{{url("/admin/print/history_transaksi/") }}/'+$('#month').val()+'/'+$('#year').val()   +'" style="width: 100%; height: ' + iframeHeight + 'px; border: 1px solid #e5e5e5;background-image: url({{asset('assets/global/img/input-spinner.gif')}}); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>');
        $('#printModal-print-btn').attr('disabled', 'disabled');
        $('#printModal-iframe').on('load', function () {
            $('#printModal-print-btn').removeAttr('disabled');
        });
        $('#printModal').modal('show');
    });
    $('#printModal-print-btn').on('click', function () {
        $('#printModal-iframe').get(0).contentWindow.print();
    });

    var oTable = $('#history-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{url("/admin/daftar/history_transaksi") }}',
            method: 'POST',
            data: function ( d ) {
                d.month = $('#month').val();
                d.year = $('#year').val()                
            }
        },
        columns: [
            {data: "DT_Row_Index", orderable: false, searchable: false},            
            { data: 'tanggal', name: 'tanggal' },
            { data: 'kode', render: render_kode },            
            { data: 'debit', name: 'debit' },
            { data: 'credit', name: 'credit' }
        ],
        rowCallback: function(row,data,index){
            // nomer
            $('td:eq(0)', row).html( '<center>'+data.DT_Row_Index+'</center>' );
            // tanggal_pinjaman
            $('td:eq(1)',row).html(moment(data.tanggal).format("DD-MM-YYYY"));
            // jumlah_pinjaman
            $('td:eq(3)',row).html('<div class="pull-right">' + numeral(data.debit).format('0,0') + '</div>');
            $('td:eq(4)',row).html('<div class="pull-right deposit">' + numeral(data.credit).format('0,0') + '</div>');

        }
    });
    $('#button-filter').on('click', function(e) {                
        oTable.ajax.reload();
        e.preventDefault();
    });
});

function render_kode(data, type, row, meta) {
    var text;
    if(data == '01'){
        text = "Pendanaan";
    }else if(data == '02'){
        text = "Deposit";
    }else{
        text = "Penarikan";
    }
    return data+"."+moment(row.tanggal).format("YYMM") + " - " + text;
}
</script>
@endpush
