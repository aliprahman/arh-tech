@extends('layouts.app')

@push('page-plugin-styles')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/assets/global/plugins/bootstrap-toastr/toastr.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" /><link href="{{ asset('/assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
<style type="text/css">
  .table thead tr th{
    white-space: pre-wrap;
    vertical-align: middle;
    text-align: center;
    padding-top: 20px;
    padding-bottom: 20px;
  }
  .table tbody tr td{
    vertical-align: middle;
    text-align: center;
  }

  #data th, #data td {
    font-size: 13px;
  }
</style>
@endpush

@section('title')
Penarikan
@endsection

@section('crumbs')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/dashboard') }}">Admin</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Penarikan</span>
        </li>
    </ul>
@endsection

@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="portlet light ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-file-text-o"></i>
                <span class="caption-subject bold uppercase">Daftar Penarikan</span>
            </div>
        </div>
        <div class="portlet-body">
          <form method="POST" id="filter-list" class="form-inline" role="form">
              <div class="form-group">
                  <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                      <input type="text" name="start" id="start" class="form-control col-sm-4" readonly="" value="{{ date('Y-m-d') }}" required>
                      <span class="input-group-btn">
                          <button class="btn default" type="button">
                               <i class="fa fa-calendar"></i>
                          </button>
                      </span>
                  </div>
              </div>
              <div class="form-group">
                  <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                      <input type="text" name="end" id="end" class="form-control col-sm-4" readonly="" value="{{ date('Y-m-d') }}" required>
                      <span class="input-group-btn">
                          <button class="btn default" type="button">
                               <i class="fa fa-calendar"></i>
                          </button>
                      </span>
                  </div>
              </div>
              <div class="form-group">
                  <select class="form-control" name="status" id="status">
                    <option value="">Semua</option>
                    <option value="unverified">Unverified</option>
                    <option value="verified">Verified</option>
                  </select>
              </div>
              <button type="submit" class="btn btn-primary">
                  <i class="icon-magnifier"></i>
                  Search
              </button>
          </form>
          <br>
            <table class="table table-striped table-bordered table-hover" id="data">
                <thead>
                    <tr>
                        <th> No </th>
                        <th> Pemodal</th>
                        <th> Tanggal Request</th>
                        <th> Jumlah </th>
                        <th> Status </th>
                        <th> Action</th>
                    </tr>
                </thead>
                <tbody>
                   {{-- --}}
                </tbody>
            </table>
                <br>

            <div class="clearfix margin-bottom-20"> </div>
        </div>
      </div>
    </div>
 </div>
</div>
<div id="basic" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="form_konfirmasi" method="post">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Detail Transaksi</h4>
                </div>
                <div class="modal-body">
                   <div class="row">
                     <div class="col-lg-6">

                       <div class="form-group">
                         <label for="jumlah" class="control-label"><font class="pull-left">Nominal Penarikan</font></label>
                         <div class="input-group">
                             <span class="input-group-addon">Rp.</span>
                             <input type="text" id="jumlah" class="form-control" readonly>
                         </div>
                       </div>
                       <div class="form-group">
                         <label for="bank_tujuan" class="control-label"><font class="pull-left">Bank Tujuan</font></label>
                         <input type="text" id="bank_tujuan" class="form-control" readonly>
                       </div>
                       <div class="form-group">
                         <label for="tgl_transfer" class="control-label"><font class="pull-left">No Rekening Tujuan</font></label>
                         <input type="text" id="no_rekening_tujuan" class="form-control" readonly>
                       </div>
                       <div class="form-group">
                         <label for="via" class="control-label"><font class="pull-left">Nama Pemilik Akun Tujuan</font></label>
                         <input type="text" id="nama_akun_tujuan" class="form-control" readonly>
                       </div>
                     </div>
                     <div class="col-lg-6">
                       <div class="form-group">
                         <label for="ref" class="control-label"><font class="pull-left">No Transaksi</font></label>
                         <input type="text" name="ref" id="ref" class="form-control">
                       </div>
                       <div class="form-group">
                         <label for="tgl_transfer" class="control-label"><font class="pull-left">Tanggal Transfer</font></label>
                         <input type="text" name="tgl_transfer" id="tgl_transfer" class="form-control date-picker" required>
                       </div>
                       <div class="form-group">
                         <label for="via" class="control-label"><font class="pull-left">Transfer Via</font></label>
                         <input type="text" name="via" id="via" class="form-control" required>
                       </div>
                       <div class="form-group">
                         <label for="norek" class="control-label"><font class="pull-left">No Rekening</font></label>
                         <input type="text" name="norek" id="norek" class="form-control" required>
                       </div>
                       <div class="form-group">
                         <label for="norek" class="control-label"><font class="pull-left">Nama Pemilik Rekening</font></label>
                         <input type="text" name="nama_pemilik" id="nama_pemilik" class="form-control" required>
                       </div>
                       <div class="form-group">
                         <div class="g-recaptcha" id="captch" data-sitekey="{{ env('CAPTCHA_SITEKEY') }}"></div>
                       </div>
                     </div>
                   </div>
                </div>
                <div class="modal-footer">
                    <button type="button" style="float: left" class="btn dark btn-outline" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button type="submit" style="float: right" id="btn_konfirm" class="btn green">Konfirmasi WithDraw</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@push('page-plugin-scripts')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/ui-toastr.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-toastr/toastr.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>

@endpush

@push('page-scripts')
<script src="{{ asset('assets/pages/scripts/table-datatables-managed.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.number.min.js') }}" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script type="text/javascript">

    $(document).ready(function(){
      $('.date-picker').datepicker({
          orientation: "left",
          autoclose: true
      });
      $('.select2').select2();
      var table = $('#data').DataTable({
                      processing: true,
                      serverSide: true,
                      ajax: {
                          url: '{{ url('admin/penarikan/data') }}',
                          type: 'post',
                          data: function(d){
                              d.startDate = moment($('#start').val()).format('YYYY-MM-DD');
                              d.endDate =  moment($('#end').val()).format('YYYY-MM-DD');
                              d.status = $('#status').val();
                          }
                      },
                      columns: [
                          {data: "DT_Row_Index", orderable: false, searchable: false},
                          {data: "pemodal", name: 'pemodal'},
                          {data: "tanggal", name: 'tanggal'},
                          {data: "jumlah", name: 'jumlah'},
                          {data: "status", name: 'status'},
                          {data: "action", name: 'action'}
                      ],
                      rowCallback: function(row,data,index){
                          // nomer
                          $('td:eq(0)', row).html( '<center>'+data.DT_Row_Index+'</center>' );
                          if(data.status == 'unverified'){
                            var label = 'Konfirmasi WithDraw';
                          }else if(data.status == 'verified'){
                            var label = 'Detail'
                          }
                          $('td:eq(5)',row).html('<a href="#" class="btn btn-circle btn-sm green btn-outline" onclick="modal_penarikan('+data.id+')">'+label+'</a>');
                      }
                  });
                  $('#filter-list').on('submit', function(e) {
                    e.preventDefault();
                    table.ajax.reload();
                  });
    });

    function modal_penarikan(id) {
      $.ajax({
        type: 'GET',
        url: '{{ url('admin/penarikan') }}'+'/'+id,
        dataType: 'json',
        success: function(result){
          $('#form_konfirmasi').attr('action','{{ url('admin/penarikan') }}'+'/'+id);
          $('#ref').val(result.ref);
          $('#jumlah').val(numeral(result.jumlah).format('0,0'));
          $('#bank_tujuan').val(result.bank_tujuan);
          $('#nama_akun_tujuan').val(result.nama_akun_tujuan);
          $('#no_rekening_tujuan').val(result.no_akun_tujuan);
          $('#tgl_transfer').val(result.tanggal);
          $('#via').val(result.akun_bank_nama_bank);
          $('#nama_pengirim').val(result.akun_bank_nama_akun);
          $('#norek').val(result.akun_bank_no_akun);
          $('#nama_pemilik').val(result.akun_bank_nama_akun);
          $('#catatan').val(result.deskripsi);
          if(result.file_bukti == null){
            $('#bukti').css('display','none');
          }else{
            $('#bukti').attr('src','{{ asset('storage/bukti_transfer_deposit') }}'+'/'+result.file_bukti);
          }
          if(result.status == 'verified'){
            $('#captch').css('display','none');
            $('#btn_konfirm').css('display','none');
            $('#nama_pemilik, #norek, #tgl_transfer, #via, #ref').attr('readonly',true);
          }else{
            $('#captch').css('display','block');
            $('#btn_konfirm').css('display','block');
            $('#nama_pemilik, #norek, #tgl_transfer, #via, #ref').removeAttr('readonly');
          }
        },
        complete: function(){
          $('#basic').modal('show');
        }
      });
    }
</script>
@if (session()->has('success'))
  <script type="text/javascript">
    $(function () {
      swal({
          allowOutsideClick: false,
          allowEscapeKey: false,
          text: "Transfer WithDraw Terverifikasi",
          title: "Berhasil",
          type: "success",
          confirmButtonClass: "btn green btn-circle btn-outline"
      });
    });
  </script>
@endif
@endpush
