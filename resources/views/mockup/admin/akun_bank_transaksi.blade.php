@extends('layouts.app')

@push('page-plugin-styles')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
@endpush

@section('title')
Daftar Akun Bank Transaksi
@endsection

@section('crumbs')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/dashboard') }}">Admin</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Daftar Akun Bank</span>
        </li>
    </ul>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="portlet light ">
        <div class="portlet-title">
            <div class="caption">
                <span class="caption-subject bold uppercase">Daftar Akun Bank Transaksi</span>
            </div>
            <div class="actions">
                <a class="btn btn-circle btn-default" href="javascript:;" onclick="show()">
                    <i class="fa fa-plus"></i> Tambah Baru
                </a>
        </div>
        <div class="portlet-body">
            <div class="col-md-12">
                <table class="table table-striped table-bordered" id="akun-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Bank</th>
                            <th>Nama</th>
                            <th>No Rekening</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="clearfix margin-bottom-20"></div>
         </div>
      </div>
    </div>
 </div>
</div>
<div id="basic" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="form_akun_bank" method="post">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Akun Bank Baru</h4>
                </div>
                <div class="modal-body">
                   <div class="row">
                     <div class="col-lg-12">
                       <div class="form-group">
                         <label for="bank_id" class="control-label"><font class="pull-left">Bank</font></label>
                         <select class="form-control" name="bank_id" id="bank_id" required>
                           <option value="">-- Pilih --</option>
                           @foreach ($bank_list as $key)
                             <option value="{{ $key->id }}">{{ $key->nama }}</option>
                           @endforeach
                         </select>
                       </div>
                       <div class="form-group">
                         <label for="nama_akun_bank" class="control-label"><font class="pull-left">Nama Pemilik</font></label>
                         <input type="text" name="nama_akun_bank" id="nama_akun_bank" class="form-control" required>
                       </div>
                       <div class="form-group">
                         <label for="no_akun_bank" class="control-label"><font class="pull-left">No Rekening</font></label>
                         <input type="text" name="no_akun_bank" id="no_akun_bank" class="form-control" required>
                       </div>
                     </div>
                   </div>
                </div>
                <div class="modal-footer">
                    <button type="button" style="float: left" class="btn dark btn-outline" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button type="submit" style="float: right" id="btn_konfirm" class="btn green">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@push('page-plugin-scripts')
    <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <!-- Select2 -->
    <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
    <script src="{{ asset('/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
@endpush

@push('page-scripts')
<script type="text/javascript">
$(function() {
    $('.select2').select2();
    var oTable = $('#akun-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{url("admin/daftar/akun_bank") }}',
            method: 'POST',
        },
        columns: [
            { data: "DT_Row_Index", orderable: false, searchable: false},
            { data: 'nama_bank', name: 'nama_bank' },
            { data: 'nama_akun_bank', name: 'nama_akun_bank' },
            { data: 'no_akun_bank', name: 'no_akun_bank' },
            { data: "action", orderable: false, searchable: false}
        ],
        rowCallback: function(row,data,index){
            $('td:eq(4)',row).html('<a onclick="show('+data.id+')">Update</a>');
        }
    });
    $('#filter-list').on('submit', function(e) {
        oTable.draw();
        e.preventDefault();
    });
});

    function show(id = 0) {
      if(id == 0){
        $('#form_akun_bank').attr('action','{{ url('admin/akun_bank') }}');
        if($("input[value='_method']").length){
          $("input[value='_method']").remove();
        }
      }else{
        $('#form_akun_bank').attr('action','{{ url('admin/akun_bank') }}'+'/'+id);
        if(!$("input[value='_method']").length){
          $('#form_akun_bank').append('<input type="hidden" name="_method" value="PUT">');
        }
        $.ajax({
          type: 'GET',
          url: '{{ url('admin/akun_bank') }}'+'/'+id,
          success: function(result){
            $('#bank_id').val(result.bank_id);
            $('#nama_akun_bank').val(result.nama_akun_bank);
            $('#no_akun_bank').val(result.no_akun_bank);
          }
        })
      }
      $('#basic').modal('show');
    }
</script>
@if (session()->has('success'))
  <script type="text/javascript">
    $(function () {
      swal({
          allowOutsideClick: false,
          allowEscapeKey: false,
          text: "{{ session('success') }}",
          title: "Berhasil",
          type: "success",
          confirmButtonClass: "btn green btn-circle btn-outline"
      });
    });
  </script>
@endif
@endpush
